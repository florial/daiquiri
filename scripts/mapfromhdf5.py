#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse
import logging
from datetime import datetime

import h5py

from flask import Flask
from flask_apispec import FlaskApiSpec
from flask_socketio import SocketIO

from daiquiri.core.metadata import MetaData
from daiquiri.resources import utils

app = Flask(__name__)
docs = FlaskApiSpec(app)
socketio = SocketIO(app)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class MapFromFile:
    def __init__(self, metadata, session):
        self._metadata = metadata
        self._session = metadata.get_sessions(session=session, no_context=True)

    def create_xrf_map(
        self,
        subsampleid=None,
        datacollectionid=None,
        steps_x=0,
        steps_y=0,
        file=None,
        **kwargs,
    ):
        if not subsampleid and not datacollectionid:
            raise Exception("One of subsampleid or datacollectionid must be specified")

        points = steps_x * steps_y

        if not kwargs.get("scalar") and not kwargs.get("rois"):
            self.list_scalars(file)
            exit()

        if kwargs.get("scalar"):
            data = self.read_scalar_from_file(file, kwargs.get("scalar"))
            self.pad_data(data, points)

            datacollection = self.ensure_datacollection(
                subsampleid, datacollectionid, file, steps_x, steps_y
            )
            roi = self._metadata.add_xrf_map_roi_scalar(scalar=kwargs["scalar"])
            xmap = self._metadata.add_xrf_map(
                maproiid=roi["maproiid"],
                datacollectionid=datacollection["datacollectionid"],
                data=data,
                no_context=True,
            )

            logger.info(
                f"Created xrf map {xmap['mapid']} from scalar {kwargs.get('scalar')}"
            )

        elif kwargs.get("rois"):
            pass
            # TODO: Generate map from daiquiri rois
            # rois = self._metadata.get_xrf_map_rois(
            #     sampleid=subsample["sampleid"], no_context=True,
            # )

        else:
            logger.warning("No scalar selected and rois not enabled")
            exit()

        if datacollectionid is None:
            self._metadata.update_datacollection(
                datacollectionid=datacollection["datacollectionid"],
                endtime=datetime.now(),
                runstatus="Successful",
                comments="Import from mapfromhdf5",
                no_context=True,
            )

    def ensure_datacollection(
        self,
        subsampleid=None,
        datacollectionid=None,
        file=None,
        steps_x=None,
        steps_y=None,
    ):
        if subsampleid:
            subsample = self._metadata.get_subsamples(
                subsampleid=subsampleid, no_context=True
            )

            if subsample["type"] != "roi":
                logger.error(
                    f"Can only import from rois, subsample {subsample['subsampleid']} is {subsample['type']}"
                )
                exit()

            datacollection = self._metadata.add_datacollection(
                imagedirectory=os.path.dirname(file),
                filetemplate=os.path.basename(file),
                sessionid=self._session["sessionid"],
                sampleid=subsample["sampleid"],
                subsampleid=subsample["subsampleid"],
                starttime=datetime.now(),
                endtime=datetime.now(),
                experimenttype="XRF map",
                steps_x=steps_x,
                steps_y=steps_y,
                numberofimages=steps_x * steps_y,
                dx_mm=(subsample["x2"] - subsample["x"]) / steps_x * 1e-6,
                dy_mm=(subsample["y2"] - subsample["y"]) / steps_y * 1e-6,
                no_context=True,
            )

            logger.info(f"Created datacollection {datacollection['datacollectionid']}")

        else:
            datacollection = self._metadata.get_datacollections(
                datacollectionid=datacollectionid, no_context=True
            )

        return datacollection

    def pad_data(self, data, points):
        if len(data) < points:
            logger.info(f"Padding data from {len(data)} to {points}")
            missing = points - len(data)
            data.extend([-1 for x in range(missing)])

    def root_path(self):
        scan_nb = 1
        subscan_id = 1

        return f"/{scan_nb}.{subscan_id}/measurement"

    def read_scalar_from_file(self, file, scalar):
        path = f"{self.root_path()}/{scalar}"
        scalar_data = []
        with h5py.File(file, mode="r") as h5:
            try:
                measurement = h5[path]
                if measurement.ndim != 1:
                    logger.error(
                        f"Measurement '{scalar}' is not a scalar, dimensions are {measurement.ndim}, shape {measurement.shape}"
                    )
                    exit()

                data = measurement[()]

            except Exception:
                logger.warning("Data from scalar %s is not reachable", scalar)
                logger.warning("Path %s", path)
                exit()
            else:
                scalar_data = data

        return scalar_data.tolist()

    def list_scalars(self, file):
        scalars = []
        spectra = []
        with h5py.File(file, mode="r") as h5:
            try:
                measurements = h5[self.root_path()]
                for k in measurements.keys():
                    scalar = h5[self.root_path()][k]
                    if scalar.ndim == 1:
                        scalars.append([k, scalar.shape])
                    else:
                        spectra.append([k, scalar.shape])

            except Exception:
                logger.exception("Couldnt read measurements")

        print("Found the following scalars:")
        for scalar in scalars:
            print(f"  {scalar[0]}\t{scalar[1]}")

        print("Found the following spectra:")
        for spectrum in spectra:
            print(f"  {spectrum[0]}\t{spectrum[1]}")

        exit()


def main():
    args = parse_args()
    utils.add_resource_root(args.resource_folder)

    config = utils.load_config("config.yml")
    metadata = MetaData(
        config, app=app, schema=None, docs=docs, socketio=socketio
    ).init()

    mff = MapFromFile(metadata, args.session)
    mff.create_xrf_map(
        args.subsampleid,
        args.datacollectionid,
        file=args.file,
        steps_x=args.steps_x,
        steps_y=args.steps_y,
        scalar=args.scalar,
        rois=args.rois,
    )


def parse_args():
    parser = argparse.ArgumentParser(description="Import XRF Maps from bliss hdf5 file")
    parser.add_argument(
        "--resource-folder",
        dest="resource_folder",
        help="Server resource directories",
        required=True,
    )
    parser.add_argument(
        "--implementors",
        default=None,
        dest="implementors",
        help="Actor implementors module",
    )

    parser.add_argument(
        "--session", required=True, help="The session to import the map in to",
    )

    parser.add_argument(
        "--steps-x",
        type=int,
        dest="steps_x",
        required=True,
        help="Number of steps in x",
    )

    parser.add_argument(
        "--steps-y",
        type=int,
        dest="steps_y",
        required=True,
        help="Number of steps in y",
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--subsampleid", type=int, help="Subsampleid to import map to",
    )
    group.add_argument(
        "--datacollectionid", type=int, help="Datacollectionid to import map to",
    )

    parser.add_argument(
        "--scalar", help="scalar to generate map from",
    )

    parser.add_argument(
        "--rois",
        dest="rois",
        help="Generate the map from the subsamples rois",
        action="store_true",
        default=False,
    )

    parser.add_argument("file", help="The hdf5 file")

    return parser.parse_args()


if __name__ == "__main__":
    main()
