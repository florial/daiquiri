#!/bin/bash
DIR=$(dirname "$0")

DROP=0
HOST="localhost"
PORT=3306

function usage {
    echo "$(basename "$0") [-h] [-D] -u <username> -p <password> -d <database> [-H <host>] [-P <port>] 

Create the daiquiri database populated with example data
    -h  show this help text
    -u  mysql username
    -p  mysql password
    -d  mysql database
    -H  mysql host (default localhost)
    -P  mysql port (default 3306)
    -D  drop database and recreate
"
    exit
} 

#https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -u|--username)
    USERNAME="$2"
    shift
    shift
    ;;
    -p|--password)
    PASSWORD="$2"
    shift
    shift
    ;;
    -d|--database)
    DATABASE="$2"
    shift
    shift
    ;;
    -H|--host)
    HOST="$2"
    shift
    shift
    ;;
    -P|--port)
    PORT="$2"
    shift
    shift
    ;;
    -D|--drop)
    DROP=1
    shift
    ;;
    -h|--help)
    usage
    ;;
esac
done

for required in USERNAME PASSWORD DATABASE
do
    if [ -z "${!required}" ]
    then
          echo "No ${required} specified"
          usage
    fi
done

echo "USERNAME = ${USERNAME}"
echo "DATABASE = ${DATABASE}"
echo "HOST = ${HOST}"
echo "PORT = ${PORT}"

if [ $DROP -eq 1 ]
then
    echo "Dropping database before creation"
    mysql -u $USERNAME -p$PASSWORD -P $PORT -h $HOST $DATABASE << EOF
DROP DATABASE ${DATABASE};
CREATE DATABASE ${DATABASE};
exit
EOF
fi

FILES="tests/metadata/db/tables.sql
tests/metadata/db/lookups.sql
daiquiri/core/metadata/ispyalchemy/updates.sql
tests/metadata/db/data.sql"
for f in $FILES
do
    mysql -u $USERNAME -p$PASSWORD -P $PORT -h $HOST $DATABASE < "${DIR}/../${f}"
    if [ $? -gt 0 ]
    then
        echo "Could not create database, processing ${f}"
        exit
    fi
done

echo "Database created"
