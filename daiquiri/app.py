#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent.monkey

gevent.monkey.patch_all(thread=False)

import os
import json
import argparse
from flask import Flask, jsonify
from flask_socketio import SocketIO
from flask_apispec import FlaskApiSpec
from flask_restful import abort
from webargs.flaskparser import parser
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin

from daiquiri.core.authenticator import Authenticator
from daiquiri.core.session import Session
from daiquiri.core.queue import Queue
from daiquiri.core.metadata import MetaData
from daiquiri.core.components import Components
from daiquiri.core.hardware import Hardware
from daiquiri.core.schema import Schema
from daiquiri.core.layout import Layout
from daiquiri.core.saving import Saving
from daiquiri.resources import utils
from daiquiri.core.logging import log
from daiquiri.core.responses import nocache

import logging

logger = logging.getLogger(__name__)


def init_server(
    resource_folders=tuple(),
    static_folder="static.default",
    hardware_folder=None,
    testing=False,
    save_spec=False,
    implementors=None,
    **kwargs,
):
    """Instantiate a Flask application

    :param list(str) resource_folders: REST server resource root directories
    :param str static_folder: server side resources for the client
    :param str hardware_folder: ???
    :param bool testing: Attaches core components to the `app` so that can be retrieved in tests
    :param kwargs: arguments for Flask instantiation
    :returns Flask, SocketIO:
    """
    for resource_folder in reversed(resource_folders):
        if not resource_folder:
            continue
        if not os.path.isdir(resource_folder):
            raise ValueError(f"Resource folder '{resource_folder}' does not exist")
        utils.add_resource_root(resource_folder)
    if static_folder.startswith("static."):
        try:
            static_folder = utils.get_resource(static_folder, "")
        except RuntimeError:
            raise ValueError(
                f"Static resource '{static_folder}' does not exist"
            ) from None
    if not os.path.isdir(static_folder):
        raise ValueError(f"Static folder '{static_folder}' does not exist")
    if hardware_folder:
        if not os.path.isdir(hardware_folder):
            raise ValueError(f"Hardware folder '{hardware_folder}' does not exist")
        os.environ["HWR_ROOT"] = hardware_folder

    config = utils.load_config("config.yml")  # TODO: call it app.yml

    # Allow CLI to override implementors
    if implementors:
        config["implementors"] = implementors

    log.start(config=config)

    app = Flask(__name__, static_folder=static_folder, **kwargs)

    app.config["APISPEC_FORMAT_RESPONSE"] = None
    app.config["APISPEC_TITLE"] = "daiquiri"

    security_definitions = {
        "bearer": {"type": "apiKey", "in": "header", "name": "Authorization"}
    }

    app.config["APISPEC_SPEC"] = APISpec(
        title="daiquiri",
        version="v1",
        openapi_version="2.0",
        plugins=[MarshmallowPlugin()],
        securityDefinitions=security_definitions,
    )

    docs = FlaskApiSpec(app)

    sio_kws = {}
    if config["cors"] == True:
        from flask_cors import CORS

        sio_kws["cors_allowed_origins"] = "*"
        CORS(app)

    app.config["SECRET_KEY"] = config["iosecret"]
    socketio = SocketIO(app, **sio_kws)

    log.init_sio(socketio)

    @app.errorhandler(404)
    def page_not_found(e):
        return jsonify(error=str(e)), 404

    @app.errorhandler(405)
    def method_not_allowed(e):
        return jsonify(message="The method is not allowed for the requested URL."), 405

    # @app.errorhandler(422)
    # def unprocessable(e):
    #     return jsonify(error=str(e)), 422

    @parser.error_handler
    def handle_request_parsing_error(
        err, req, schema, error_status_code, error_headers
    ):
        abort(422, description=err.messages)

    if not app.debug or os.environ.get("WERKZEUG_RUN_MAIN") == "true":

        schema = Schema(app=app, docs=docs, socketio=socketio)
        ses = Session(
            config=config, app=app, docs=docs, socketio=socketio, schema=schema
        )
        schema.set_session(ses)

        Authenticator(config=config, app=app, session=ses, docs=docs, schema=schema)

        # TODO
        # This is BLISS specific, move to where bliss is handled ?
        if config.get("controls_session_type", None) == "bliss":
            # This is not very elegant but a solution until this section
            # have been moved to an appropriate place
            from daiquiri.core.hardware.bliss.session import BlissSession

            BlissSession(config["controls_session_name"])

        hardware = Hardware(app=app, socketio=socketio, docs=docs, schema=schema)

        queue = Queue(
            config=config,
            app=app,
            session=ses,
            docs=docs,
            socketio=socketio,
            schema=schema,
        )

        # TODO
        # Why is the queue launched here ?
        queue.launch()

        Layout(
            config, app=app, session=ses, docs=docs, socketio=socketio, schema=schema
        )

        metadata = MetaData(
            config, app=app, session=ses, docs=docs, socketio=socketio, schema=schema
        ).init()
        ses.set_metadata(metadata)

        saving = Saving(
            config,
            app=app,
            session=ses,
            docs=docs,
            socketio=socketio,
            schema=schema,
            metadata=metadata,
        ).init()

        components = Components(
            base_config=config,
            app=app,
            socketio=socketio,
            docs=docs,
            schema=schema,
            hardware=hardware,
            session=ses,
            metadata=metadata,
            queue=queue,
            saving=saving,
        )

    if save_spec:
        logger.info("Writing API spec and exiting")
        spec_dir = f"{os.path.dirname(os.path.realpath(__file__))}/../doc/api"
        if not os.path.exists(spec_dir):
            os.mkdir(spec_dir)
        with open(f"{spec_dir}/spec.json", "w") as spec:
            json.dump(docs.spec.to_dict(), spec)

        exit()

    if config["debug"] == True:
        app.debug = True

    if testing:
        app.hardware = hardware
        app.queue = queue
        app.metadata = metadata
        app.components = components

    @app.route("/manifest.json")
    def manifest():
        return app.send_static_file("manifest.json")

    @app.route("/meta.json")
    @nocache
    def meta():
        return app.send_static_file("meta.json")

    @app.route("/favicon.ico")
    def favicon():
        return app.send_static_file("favicon.ico")

    @app.route("/", defaults={"path": ""})
    @app.route("/<string:path>")
    @app.route("/<path:path>")
    @nocache
    def index(path):
        return app.send_static_file("index.html")

    return app, socketio


def get_ssl_context():
    """Build an ssl context for serving over HTTPS
    """
    config = utils.load_config("config.yml")

    if not config.get("ssl", False):
        return None

    import ssl

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)

    crt = utils.get_resource("certs", config["ssl_cert"])
    key = utils.get_resource("certs", config["ssl_key"])

    try:
        context.load_cert_chain(crt, key)
    except Exception as e:
        logger.exception(f"Could not load certificate chain {e}")
        raise

    return context


def run_server(port=8080, **kwargs):
    """Runs REST server

    :param int port:
    :param kwargs: see `init_server`
    """
    app, socketio = init_server(**kwargs)

    server_args = {}
    context = get_ssl_context()
    if context:
        server_args["ssl_context"] = context

    socketio.run(app, host="0.0.0.0", port=port, **server_args)


def parse_args():
    """Parse command line args"""
    parser = argparse.ArgumentParser(description="REST server for a beamline GUI")
    parser.add_argument(
        "--resource-folders",
        default="",
        dest="resource_folders",
        help="Server resources directories (first has priority)",
    )
    parser.add_argument(
        "--implementors",
        default=None,
        dest="implementors",
        help="Actor implementors module",
    )
    parser.add_argument(
        "--static-folder",
        dest="static_folder",
        default="static.default",
        help="Web server static folder (static.* refers to server resource)",
    )
    parser.add_argument(
        "--hardware-folder", dest="hardware_folder", default="", help="Hardware folder"
    )
    parser.add_argument(
        "-p", "--port", dest="port", default=8080, help="Web server port", type=int
    )

    parser.add_argument(
        "-s",
        "--save-spec",
        dest="save_spec",
        help="Save the current API spec",
        action="store_true",
        default=False,
    )
    return parser.parse_args()


def main():
    """Runs REST server with CLI configuration"""
    args = parse_args()
    resource_folders = args.resource_folders.split(",")
    resource_folders = [s.strip() for s in resource_folders]
    run_server(
        resource_folders=resource_folders,
        static_folder=args.static_folder,
        hardware_folder=args.hardware_folder,
        port=args.port,
        save_spec=args.save_spec,
        implementors=args.implementors,
        static_url_path="/",
    )


if __name__ == "__main__":
    main()
