# -*- coding: utf-8 -*-

# Allows running the REST server with "python -m daiquiri"
from daiquiri.app import main

main()
