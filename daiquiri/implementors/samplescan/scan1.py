#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
from marshmallow import fields, validate
import mmh3

from bliss.config.static import get_config
from bliss.scanning.scan import ScanState

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)
from daiquiri.core.hardware.bliss.session import *


class Scan1Schema(ComponentActorSchema):
    sampleid = fields.Int(required=True)
    motor_start = fields.Float(required=True, title="Start Position")
    motor_end = fields.Float(required=True, title="End Position")
    npoints = fields.Int(required=True, title="No. Points")
    time = fields.Float(
        validate=validate.Range(min=0.1, max=5),
        required=True,
        title="Time(s) per Point",
    )
    detectors = fields.List(
        OneOf(["diode", "simu1", "lima_simulator"]),
        uniqueItems=True,
        minItems=1,
        required=True,
        title="Detectors",
    )
    enqueue = fields.Bool(title="Queue Scan", default=True)

    class Meta:
        uischema = {"sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"}}


class Scan1Actor(ComponentActor):
    schema = Scan1Schema
    name = "scan1"
    saving_args = {"data_filename": "{sampleid.name}_scan{datacollectionid}"}

    def method(self, **kwargs):
        cfg = get_config()

        kwargs["motor"] = cfg.get("omega")
        kwargs["detectors"] = [cfg.get(d) for d in kwargs["detectors"]]

        print("capture params")
        kwargs["before_scan_starts"](self)

        scan = simple_scan(run=False, **kwargs)

        greenlet = gevent.spawn(scan.run)

        scan.wait_state(ScanState.STARTING)
        kwargs["update_datacollection"](
            self,
            datacollectionnumber=mmh3.hash(scan.node.db_name) & 0xFFFFFFFF,
            imagecontainersubpath="1.1/measurement",
            numberofimages=kwargs["npoints"],
            exposuretime=kwargs["time"],
        )

        try:
            greenlet.join()
        except ComponentActorKilled as e:
            greenlet.kill()
            raise

        greenlet.get()
