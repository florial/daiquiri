#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields, validate
from bliss.config.static import get_config

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import ComponentActor
from daiquiri.core.hardware.bliss.session import *

cfg = get_config()


class SimpleScanSchema(Schema):
    motor = OneOf(["robz", "roby"], required=True, title="Motor")
    motor_start = fields.Float(required=True, title="Start Position")
    motor_end = fields.Float(required=True, title="End Position")
    npoints = fields.Int(required=True, title="No. Points")
    time = fields.Float(
        validate=validate.Range(min=0.1, max=5),
        required=True,
        title="Time per Point",
        unit="s",
    )
    detectors = fields.List(
        OneOf(["diode", "simu1", "lima_simulator"]),
        uniqueItems=True,
        minItems=1,
        required=True,
        title="Detectors",
    )


class ScanActor(ComponentActor):
    schema = SimpleScanSchema
    name = "simple-scan"
    desc = "Simple ascan"

    def method(self, **kwargs):
        print("AddscanActor", kwargs)
        kwargs["motor"] = cfg.get(kwargs["motor"])
        kwargs["detectors"] = [cfg.get(d) for d in kwargs["detectors"]]

        # this is the bliss function
        simple_scan(**kwargs)

        return "im a return value"
