import math

import numpy as np

from daiquiri.core.components import ComponentActor
from daiquiri.core.utils import worker


class CreatemapActor(ComponentActor):
    name = "createmap"

    def method(self, **kwargs):
        evperbin = kwargs["spectra"]["conversion"]["scale"]
        offset = kwargs["spectra"]["conversion"]["zero"]

        pts = kwargs["spectra"]["npoints"]
        avail = kwargs["spectra"]["npoints_avail"]

        spectra = kwargs["spectra"]["data"]

        def generate():
            maps = []
            for s in spectra:
                spec = []
                mca = kwargs["spectra"]["data"][s]["data"]

                for r in kwargs["rois"]:
                    roi = []
                    for p in range(pts):
                        if p < avail:
                            start = max(0, math.floor((r["start"] - offset) / evperbin))
                            end = min(
                                math.ceil((r["end"] - offset) / evperbin), len(mca[p])
                            )
                            roi.append(int(np.sum(mca[p][start:end])))

                        else:
                            roi.append(-1)

                    spec.append({"maproiid": r["maproiid"], "data": roi})

                maps.append({"det": s, "maps": spec})

            return maps

        return worker(generate)
