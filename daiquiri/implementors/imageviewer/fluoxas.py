#!/usr/bin/env python
# -*- coding: utf-8 -*-
from decimal import Decimal, getcontext
from marshmallow import fields, validate, validates_schema, ValidationError

from bliss.config.static import get_config
from bliss.common.scans import amesh
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel
from bliss.scanning.scan import ScanState

import numpy
import gevent
import mmh3

from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)
from daiquiri.core.schema.components import RegionSchema
from daiquiri.core.utils import to_wavelength
from daiquiri.core.hardware.bliss.session import *

from .createmap import CreatemapActor
from .beamlineparams import BeamlineParamsSchema


getcontext().prec = 8

cfg = get_config()


class FluoxasSchema(ComponentActorSchema):
    subsampleid = fields.Int(required=True)
    steps_x = fields.Str(title="Steps Horizontally", readOnly=True)
    step_size_x = fields.Float(
        required=True,
        title="Step Size Horizontally",
        unit="um",
        validate=validate.Range(min=0.1),
    )
    steps_y = fields.Str(title="Steps Vertically", readOnly=True)
    step_size_y = fields.Float(
        required=True,
        title="Step Size Vertically",
        unit="um",
        validate=validate.Range(min=0.1),
    )
    reference = fields.Float(
        title="Energy Reference",
        unit="keV",
        validate=validate.Range(min=0.1),
        required=True,
    )
    regions = fields.List(
        fields.Nested(RegionSchema), title="Energy Regions", minItems=1, required=True,
    )
    beamlineparams = fields.Nested(BeamlineParamsSchema, title="Beamline Parameters")
    enqueue = fields.Bool(title="Queue Scan", default=True)

    def _steps(self, start, end, step_size, base_unit=1e-9):
        return abs(
            (Decimal(end) - Decimal(start))
            * Decimal(base_unit)
            / Decimal(step_size)
            / Decimal(1e-6)
        )

    @validates_schema
    def schema_validate(self, data, **kwargs):
        intervals = [["step_size_x", "x", "x2"], ["step_size_y", "y", "y2"]]

        for keys in intervals:
            if data.get("objects"):
                for obj in data.get("objects"):
                    steps = self._steps(obj[keys[1]], obj[keys[2]], data[keys[0]])
                    if not steps == steps.to_integral_value():
                        raise ValidationError(
                            f"{keys[0]} must be an integer value: {steps}"
                        )

    def calculated(self, data, **kwargs):
        calculated = {}

        intervals = {
            "steps_x": ["step_size_x", "x", "x2"],
            "steps_y": ["step_size_y", "y", "y2"],
        }

        for iv, keys in intervals.items():
            if data.get(keys[0]):
                steps = []
                if data.get("objects"):
                    for obj in data["objects"]:
                        step = self._steps(obj[keys[1]], obj[keys[2]], data[keys[0]])
                        step = (
                            step.to_integral_value()
                            if step.to_integral_value() == step
                            else round(step, 2)
                        )

                        steps.append(step)

                calculated[iv] = ", ".join(map(str, steps))

        return calculated

    class Meta:
        uiorder = [
            "subsampleid",
            "step_size_x",
            "step_size_y",
            "steps_x",
            "steps_y",
            "reference",
            "regions",
            "beamlineparams",
            "enqueue",
        ]
        uischema = {
            "subsampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "interval_x": {"ui:readonly": True},
            "interval_y": {"ui:readonly": True},
            "regions": {"ui:field": "arrayTable"},
            "beamlineparams": {"ui:field": "optionalParams"},
        }


class FluoxasActor(ComponentActor):
    schema = FluoxasSchema
    metatype = "XRF map xas"
    name = "fluoxas"

    def method(self, **kwargs):
        print("moving to roi")
        kwargs["absol"]["move_to"](kwargs["absol"])

        print("moving to additional positions")
        kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])

        mca = cfg.get("simu1")

        axes = kwargs["absol"]["axes"]
        steps_x = int(
            round(
                self.schema()._steps(
                    axes["x"]["destination"][0],
                    axes["x"]["destination"][1],
                    kwargs["step_size_x"],
                    base_unit=axes["x"]["unit_exponent"],
                )
            )
        )
        steps_y = int(
            round(
                self.schema()._steps(
                    axes["y"]["destination"][0],
                    axes["y"]["destination"][1],
                    kwargs["step_size_y"],
                    base_unit=axes["y"]["unit_exponent"],
                )
            )
        )

        step_size_x = (
            axes["x"]["destination"][1] - axes["x"]["destination"][0]
        ) / steps_x
        step_size_y = (
            axes["y"]["destination"][1] - axes["y"]["destination"][0]
        ) / steps_y

        print("calculated steps to be", steps_x, steps_y)

        print("capture params and image")
        kwargs["before_scan_starts"](self)

        seq = Sequence()
        seq.add_custom_channel(AcquisitionChannel(f"sum_energy", numpy.float, ()))

        rois = kwargs["get_rois"]()["rois"]
        roi_by_id = {}
        for roi in rois:
            seq.add_custom_channel(
                AcquisitionChannel(f"sum_{roi['name']}", numpy.float, ())
            )
            roi_by_id[roi["maproiid"]] = roi["name"]

        first_dc = self["datacollectionid"]

        with seq.sequence_context() as scan_seq:
            point = 0

            for region in kwargs["regions"]:
                intervals = (
                    int(
                        round(
                            (region["end_e"] - region["start_e"])
                            / (region["step"] if region["step"] > 0 else 1)
                        )
                    )
                    # Want to go to end of energy range
                    + 1
                )

                for i in range(intervals):
                    energy = (
                        (kwargs["reference"] * 1000)
                        + region["start_e"]
                        + (i * region["step"])
                    )

                    mesh = amesh(
                        axes["x"]["motor"].object(),
                        axes["x"]["destination"][0] + step_size_x / 2,
                        axes["x"]["destination"][1] - step_size_x / 2,
                        steps_x - 1,
                        axes["y"]["motor"].object(),
                        axes["y"]["destination"][0] + step_size_y / 2,
                        axes["y"]["destination"][1] - step_size_y / 2,
                        steps_y - 1,
                        region["dwell"],
                        mca,
                        diode,
                        run=False,
                    )

                    scan_seq.add(mesh)

                    greenlet = gevent.spawn(mesh.run)
                    mesh.wait_state(ScanState.STARTING)

                    scan_number = mmh3.hash(mesh.node.db_name) & 0xFFFFFFFF
                    kwargs["update_datacollection"](
                        self,
                        datacollectionnumber=scan_number,
                        imagecontainersubpath=f"{i+2}.1/measurement",
                        dx_mm=kwargs["step_size_x"] * 1e-3,
                        dy_mm=kwargs["step_size_y"] * 1e-3,
                        numberofimages=steps_x * steps_y,
                        exposuretime=region["dwell"],
                        wavelength=to_wavelength(energy),
                        steps_x=steps_x,
                        steps_y=steps_y,
                        orientation="horizontal",
                    )

                    try:
                        greenlet.join()
                    except ComponentActorKilled:
                        greenlet.kill()
                        raise
                    else:
                        spectra = kwargs["scans"].get_scan_spectra(
                            scan_number, allpoints=True
                        )

                        mapsActor = CreatemapActor()
                        maps = mapsActor.method(spectra=spectra, rois=rois)

                        seq.custom_channels[f"sum_energy"].emit([energy / 1000])
                        for j, mroi in enumerate(maps[0]["maps"]):
                            xanes = sum(mroi["data"])
                            name = roi_by_id[mroi["maproiid"]]
                            seq.custom_channels[f"sum_{name}"].emit([xanes])

                            if j == 0:
                                kwargs["add_scanqualityindicators"](
                                    self,
                                    point=point + 1,
                                    datacollectionid=first_dc,
                                    total=xanes,
                                )

                                point += 1

                        kwargs["generate_maps"](
                            self["subsampleid"], self["datacollectionid"]
                        )

                        if not (region == kwargs["regions"][-1] and i == intervals - 1):
                            kwargs["next_datacollection"](self, data=True, grid=True)
