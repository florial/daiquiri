#!/usr/bin/env python
# -*- coding: utf-8 -*-
from decimal import Decimal, getcontext
from marshmallow import fields, validate, validates_schema, ValidationError

from bliss.config.static import get_config
from bliss.common.scans import amesh
from bliss.scanning.scan import ScanState
import gevent
import mmh3

from daiquiri.core.components import (
    ComponentActor,
    ComponentActorKilled,
    ComponentActorSchema,
)
from daiquiri.core.utils import to_wavelength
from daiquiri.core.hardware.bliss.session import *

from .beamlineparams import BeamlineParamsSchema

# TODO: what are the implications of doing this?
getcontext().prec = 8

cfg = get_config()


class RoiscanSchema(ComponentActorSchema):
    subsampleid = fields.Int(required=True)
    steps_x = fields.Str(title="Steps in X", readOnly=True)
    step_size_x = fields.Float(
        required=True, title="Step Size X", unit="um", validate=validate.Range(min=0.1)
    )
    steps_y = fields.Str(title="Steps in Y", readOnly=True)
    step_size_y = fields.Float(
        required=True, title="Step Size Y", unit="um", validate=validate.Range(min=0.1)
    )
    dwell = fields.Float(
        required=True,
        title="Dwell time",
        unit="s",
        validate=validate.Range(min=0.001, max=60),
    )
    beamlineparams = fields.Nested(BeamlineParamsSchema, title="Beamline Parameters")
    enqueue = fields.Bool(title="Queue Scan", default=True)

    def _steps(self, start, end, step_size, base_unit=1e-9):
        return abs(
            (Decimal(end) - Decimal(start))
            * Decimal(base_unit)
            / Decimal(step_size)
            / Decimal(1e-6)
        )

    @validates_schema
    def schema_validate(self, data, **kwargs):
        intervals = [["step_size_x", "x", "x2"], ["step_size_y", "y", "y2"]]

        objs = data.get("objects")
        if objs:
            if len(objs) > 1 and data.get("enqueue") is False:
                raise ValidationError(
                    f"Can only queue scan when more than one object is selected. {len(objs)} objects selected"
                )

        for keys in intervals:
            if objs:
                for obj in data.get("objects"):
                    steps = self._steps(obj[keys[1]], obj[keys[2]], data[keys[0]])
                    if not steps == steps.to_integral_value():
                        raise ValidationError(
                            f"{keys[0]} must be an integer value: {steps}"
                        )

    def warnings(self, data, **kwargs):
        warnings = {}
        if data.get("objects"):
            for obj in data.get("objects"):
                size_x = (obj["x2"] - obj["x"]) * 1e-9 / 1e-6
                size_y = (obj["y2"] - obj["y"]) * 1e-9 / 1e-6

                if size_x > 100 or size_y > 100:
                    warnings[
                        obj["subsampleid"]
                    ] = f"Object {obj['subsampleid']} will use stepper rather than piezo as size is {size_x:.0f}x{size_y:.0f} um"

        return warnings

    def calculated(self, data, **kwargs):
        calculated = {}

        intervals = {
            "steps_x": ["step_size_x", "x", "x2"],
            "steps_y": ["step_size_y", "y", "y2"],
        }

        for iv, keys in intervals.items():
            if data.get(keys[0]):
                steps = []
                if data.get("objects"):
                    for obj in data["objects"]:
                        step = self._steps(obj[keys[1]], obj[keys[2]], data[keys[0]])
                        # print('calculated', key, step, step.to_integral_value() == step, obj[keys[2]] - obj[keys[1]])
                        step = (
                            step.to_integral_value()
                            if step.to_integral_value() == step
                            else round(step, 2)
                        )

                        steps.append(step)

                calculated[iv] = ", ".join(map(str, steps))

        return calculated

    def time_estimate(self, data):
        fudge = 1.5
        print("time_estimate", data)
        if data.get("step_size_x") and data.get("step_size_y"):
            if data.get("objects"):
                for obj in data["objects"]:
                    steps_x = (obj["x2"] - obj["x"]) * 1e-9 / data["step_size_x"] / 1e-6
                    steps_y = (obj["y2"] - obj["y"]) * 1e-9 / data["step_size_y"] / 1e-6
                    return data["dwell"] * steps_x * steps_y * fudge

    class Meta:
        uiorder = [
            "subsampleid",
            "dwell",
            "step_size_x",
            "step_size_y",
            "steps_x",
            "steps_y",
            "beamlineparams",
            "enqueue",
        ]
        uischema = {
            "subsampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "steps_x": {"ui:readonly": True},
            "steps_y": {"ui:readonly": True},
            "beamlineparams": {"ui:field": "optionalParams"},
        }


class RoiscanActor(ComponentActor):
    schema = RoiscanSchema
    name = "roiscan"
    metatype = "XRF map"

    def method(self, **kwargs):
        print("Add roiscan", kwargs)

        print("moving to roi")
        kwargs["absol"]["move_to"](kwargs["absol"])

        print("moving to additional positions")
        kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])

        mca = cfg.get("simu1")

        axes = kwargs["absol"]["axes"]
        steps_x = int(
            round(
                self.schema()._steps(
                    axes["x"]["destination"][0],
                    axes["x"]["destination"][1],
                    kwargs["step_size_x"],
                    base_unit=axes["x"]["unit_exponent"],
                )
            )
        )
        steps_y = int(
            round(
                self.schema()._steps(
                    axes["y"]["destination"][0],
                    axes["y"]["destination"][1],
                    kwargs["step_size_y"],
                    base_unit=axes["y"]["unit_exponent"],
                )
            )
        )

        step_size_x = (
            axes["x"]["destination"][1] - axes["x"]["destination"][0]
        ) / steps_x
        step_size_y = (
            axes["y"]["destination"][1] - axes["y"]["destination"][0]
        ) / steps_y

        print("calculated steps to be", steps_x, steps_y)

        print("capture params and image")
        kwargs["before_scan_starts"](self)

        mesh = amesh(
            axes["x"]["motor"].object(),
            axes["x"]["destination"][0] + step_size_x / 2,
            axes["x"]["destination"][1] - step_size_x / 2,
            steps_x - 1,
            axes["y"]["motor"].object(),
            axes["y"]["destination"][0] + step_size_y / 2,
            axes["y"]["destination"][1] - step_size_y / 2,
            steps_y - 1,
            kwargs["dwell"],
            mca,
            diode,
            run=False,
        )

        greenlet = gevent.spawn(mesh.run)
        mesh.wait_state(ScanState.STARTING)
        kwargs["update_datacollection"](
            self,
            datacollectionnumber=mmh3.hash(mesh.node.db_name) & 0xFFFFFFFF,
            imagecontainersubpath="1.1/measurement",
            dx_mm=kwargs["step_size_x"] * 1e-3,
            dy_mm=kwargs["step_size_y"] * 1e-3,
            numberofimages=steps_x * steps_y,
            exposuretime=kwargs["dwell"],
            wavelength=to_wavelength(10000),
            steps_x=steps_x,
            steps_y=steps_y,
            orientation="horizontal",
        )

        try:
            greenlet.join()
        except ComponentActorKilled:
            greenlet.kill()
            raise

        return greenlet.get()
