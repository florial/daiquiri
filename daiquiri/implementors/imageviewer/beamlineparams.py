from marshmallow import Schema, fields, validate

from daiquiri.core.components.params import ParamHandler, ParamsHandler, ParamSchema

from bliss.config.static import get_config

cfg = get_config()


class FastShutterParam(ParamHandler):
    def before(self, value):
        if value:
            shut = cfg.get("safshut")
            shut.open()

    def after(self, value):
        if value:
            shut = cfg.get("safshut")
            shut.close()


class DetectorDistanceParam(ParamHandler):
    def before(self, value):
        det = cfg.get("omega")
        det.move(value)


class BeamlineParamsHandler(ParamsHandler):
    fast_shutter = FastShutterParam()
    detector_distance = DetectorDistanceParam()


class DiodeSchema(Schema):
    name = fields.Str(enum=["d1", "d2", "d3"])
    value = fields.Int()


class ArbMotorSchema(Schema):
    name = fields.Str(enum=["x", "y", "z"])
    value = fields.Int()


# parallelise these moves
class BeamlineParamsSchema(ParamSchema):
    handler = BeamlineParamsHandler()

    detector_distance = fields.Float(
        title="Detector Distance",
        validate=validate.Range(min=20, max=400),
        unit="mm",
        description="Move the detector to specified distance",
    )

    fast_shutter = fields.Bool(
        title="Fast Shutter", description="Open fast shutter before and close after"
    )

    attenuators = fields.Str(
        title="Attenuators", enum=["0.1mm_al", "0.5mm_al", "0.7mm_al"]
    )

    pinhole = fields.Str(title="Pin Hole", enum=["10um", "5um", "1um"])

    light = fields.Bool(title="Turn off Light")

    diode_gain = fields.List(fields.Nested(DiodeSchema), title="Diode Gains")

    arb_motor = fields.List(fields.Nested(ArbMotorSchema), title="Motors")

    energy = fields.Float(title="Energy", unit="keV")

    class Meta:
        uischema = {
            "arb_motor": {"ui:field": "arrayTable"},
            "diode_gain": {"ui:field": "arrayTable"},
        }
