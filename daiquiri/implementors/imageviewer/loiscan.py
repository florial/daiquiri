#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import math
from marshmallow import fields, validate

from bliss.config.static import get_config

from daiquiri.core.components import ComponentActor, ComponentActorSchema
from daiquiri.core.schema.components import RegionSchema
from daiquiri.core.utils import format_eng
from daiquiri.core.hardware.bliss.session import *

from .beamlineparams import BeamlineParamsSchema

cfg = get_config()


class LoiscanSchema(ComponentActorSchema):
    subsampleid = fields.Int(required=True)
    steps = fields.Int(required=True, title="Steps", validate=validate.Range(min=1))
    step_size = fields.Str(title="Step Size", readOnly=True)
    regions = fields.List(
        fields.Nested(RegionSchema), title="Energy Regions", minItems=1, required=True
    )
    reference = fields.Float(
        title="Energy Reference",
        unit="keV",
        validate=validate.Range(min=0.1),
        required=True,
    )
    repeats = fields.Int(
        required=True, title="No. Repeats", validate=validate.Range(min=1), default=1
    )
    beamlineparams = fields.Nested(BeamlineParamsSchema, title="Beamline Parameters")

    def calculated(self, data):
        calculated = {}
        if data.get("objects") and data.get("steps"):
            ints = []
            for obj in data["objects"]:
                length = math.sqrt(
                    math.pow(obj["x2"] - obj["x"], 2)
                    + math.pow(obj["y2"] - obj["y"], 2)
                )
                eng = format_eng(length * 1e-9 / data["steps"])
                ints.append(f"{eng['scalar']:.2f} {eng['prefix']}m")

            calculated["step_size"] = ", ".join(map(str, ints))

        return calculated

    def time_estimate(self, data):
        fudge = 2
        return data["dwell"] * data["steps"] * fudge

    class Meta:
        uiorder = [
            "subsampleid",
            "reference",
            "regions",
            "steps",
            "step_size",
            "repeats",
        ]
        uischema = {
            "subsampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "step_size": {"ui:readonly": True},
            "regions": {"ui:field": "arrayTable"},
            "beamlineparams": {"ui:field": "optionalParams"},
        }


class LoiscanActor(ComponentActor):
    schema = LoiscanSchema
    name = "loiscan"

    def method(self, **kwargs):
        print("Add loiscan", kwargs)

        print("moving to loi")
        kwargs["absol"]["move_to"](kwargs["absol"])

        print("moving to addiional positions")
        kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])

        print("capture params and image")
        kwargs["before_scan_starts"](self)

        # kwargs["update_datacollection"](
        #     self,
        #     datacollectionnumber=mmh3.hash(scan.node.db_name) & 0xFFFFFFFF,
        # )

        time.sleep(3)
