import time

from daiquiri.core.components import ComponentActor
from marshmallow import Schema, fields, validate

from bliss.common.scans import ct
from bliss.config.static import get_config

cfg = get_config()


class MyscanSchema(Schema):
    subsampleid = fields.Int(required=True)

    class Meta:
        uischema = {"subsampleid": {"classNames": "hidden-row", "ui:widget": "hidden"}}


class MyscanActor(ComponentActor):
    schema = MyscanSchema
    name = "myscan today"

    def method(self, **kwargs):
        time.sleep(5)
        print("changed")

        diode = cfg.get("diode")

        ct(1, diode)
