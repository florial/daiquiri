#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from functools import wraps

from gevent import lock
from flask import send_file, g
from marshmallow import fields

from daiquiri.core import (
    CoreBase,
    CoreResource,
    marshal,
    no_blsession_required,
    require_control,
)
from daiquiri.core.utils import loader
from daiquiri.core.responses import image_response
from daiquiri.core.metadata.xrf import (
    generate_map_image,
    generate_composite_image,
    shape_map,
)

from daiquiri.core.schema import ErrorSchema, MessageSchema
from daiquiri.core.schema.metadata import (
    paginated,
    UserSchema,
    UserCacheSchema,
    ProposalSchema,
    SessionSchema,
    NewComponentSchema,
    ComponentSchema,
    NewSampleSchema,
    SampleSchema,
    NewSubSampleSchema,
    SubSampleSchema,
    NewSampleImageSchema,
    SampleImageSchema,
    DataCollectionSchema,
    DataCollectionAttachmentSchema,
    DataCollectionPlanSchema,
    ScanQualityIndicatorsSchema,
    XRFMapSchema,
    XRFMapHistogramSchema,
    XRFMapValueSchema,
    NewXRFCompositeMapSchema,
    XRFCompositeMapSchema,
    NewXRFMapROISchema,
    XRFMapROISchema,
    AutoProcProgramSchema,
    AutoProcProgramAttachmentSchema,
    AutoProcProgramMessageSchema,
)

import logging

logger = logging.getLogger(__name__)


class UserResource(CoreResource):
    @no_blsession_required
    @marshal(out=[[200, UserSchema(), "The current user"]])
    def get(self):
        """Get the current user"""
        return self._parent.get_user(), 200


class UserCacheResource(CoreResource):
    @no_blsession_required
    @marshal(out=[[200, UserCacheSchema(), "The cache for the current user"]])
    def get(self):
        """Get the current user cache"""
        return {"cache": self._parent.get_user_cache()}, 200

    @marshal(
        inp=UserCacheSchema,
        out=[
            [200, UserCacheSchema(), "The updated cache for the current user"],
            [400, ErrorSchema(), "Could not update the cache for the current user"],
        ],
    )
    def patch(self, **kwargs):
        """Update the user cache"""
        return {"cache": self._parent.update_user_cache(kwargs["cache"])}, 200


class ProposalsResource(CoreResource):
    @marshal(out=[[200, ProposalSchema(many=True), "List of proposals"]])
    def get(self):
        """Get a list of proposals"""
        return self._parent.get_proposals(), 200


class ProposalResource(CoreResource):
    @marshal(
        out=[
            [200, ProposalSchema(), "List of proposals"],
            [404, ErrorSchema(), "No such proposal"],
        ]
    )
    def get(self, proposal):
        """Get selected proposal"""
        proposal = self._parent.get_proposals(proposal)
        if proposal:
            return proposal, 200
        else:
            return {"error": "No such proposal"}, 404


class SessionsResource(CoreResource):
    @no_blsession_required
    @marshal(
        inp={"current": fields.Bool()},
        out=[[200, SessionSchema(many=True), "List of sessions"]],
    )
    def get(self, **kwargs):
        """Get a list of session"""
        return self._parent.get_sessions(**kwargs), 200


class SessionResource(CoreResource):
    @marshal(
        out=[
            [200, SessionSchema(), "Get selected session"],
            [404, ErrorSchema(), "No such session"],
        ]
    )
    def get(self, session):
        """Get selected session"""
        session = self._parent.get_sessions(session)
        if session:
            return session, 200
        else:
            return {"error": "No such session"}, 404


class SelectSessionResource(CoreResource):
    @no_blsession_required
    @marshal(
        inp={"session": fields.Str()},
        out=[
            [200, SessionSchema(), "The selected session"],
            [400, ErrorSchema(), "Could not select session"],
        ],
    )
    def post(self, **kwargs):
        """Set the session for the current user"""
        if self._parent.set_session(kwargs["session"]):
            return self._parent.get_sessions(kwargs["session"]), 200
        else:
            return {"error": "Could not select session"}, 400


class ComponentsResource(CoreResource):
    @marshal(out=[[200, ComponentSchema(many=True), "List of components"]])
    def get(self, **kwargs):
        """Get a list of components"""
        return self._parent.get_components(**kwargs), 200

    @marshal(
        inp=NewComponentSchema,
        out=[
            [200, SampleSchema(), "New Component"],
            [400, ErrorSchema(), "Could not create component"],
        ],
    )
    def post(self, **kwargs):
        """Create a component"""
        component = self._parent.add_component(**kwargs)
        if component:
            return component, 201
        else:
            return {"error": "Could not create component"}, 400


class ComponentResource(CoreResource):
    @marshal(
        out=[
            [200, ComponentSchema(), "Get selected component"],
            [404, ErrorSchema(), "No such component"],
        ]
    )
    def get(self, componentid):
        """Get selected component"""
        component = self._parent.get_components(componentid)
        if component:
            return component, 200
        else:
            return {"error": "No such component"}, 404

    @marshal(
        inp=ComponentSchema,
        out=[
            [200, SampleSchema(), "Updated component"],
            [404, ErrorSchema(), "No such component"],
        ],
    )
    def patch(self, componentid, **kwargs):
        """Update a component"""
        component = self._parent.update_component(componentid, **kwargs)
        if component:
            return component, 200
        else:
            return {"error": "No such component"}, 404


class SamplesResource(CoreResource):
    @marshal(out=[[200, SampleSchema(many=True), "List of samples"]])
    def get(self, **kwargs):
        """Get a list of samples"""
        return self._parent.get_samples(**kwargs), 200

    @marshal(
        inp=NewSampleSchema,
        out=[
            [200, SampleSchema(), "New Sample"],
            [400, ErrorSchema(), "Could not create sample"],
        ],
    )
    def post(self, **kwargs):
        """Create a sample"""
        sample = self._parent.add_sample(**kwargs)
        if sample:
            return sample, 201
        else:
            return {"error": "Could not create sample"}, 400


class SampleResource(CoreResource):
    @marshal(
        out=[
            [200, SampleSchema(), "Get selected samples"],
            [404, ErrorSchema(), "No such sample"],
        ]
    )
    def get(self, sampleid):
        """Get selected sample"""
        sample = self._parent.get_samples(sampleid)
        if sample:
            return sample, 200
        else:
            return {"error": "No such sample"}, 404

    @marshal(
        inp=SampleSchema,
        out=[
            [200, SampleSchema(), "Updated sample"],
            [404, ErrorSchema(), "No such sample"],
        ],
    )
    def patch(self, sampleid, **kwargs):
        """Update a sample"""
        sample = self._parent.update_sample(sampleid, **kwargs)
        if sample:
            return sample, 200
        else:
            return {"error": "No such sample"}, 404

    @marshal(
        out=[
            [200, MessageSchema(), "Sample deleted"],
            [404, ErrorSchema(), "No such sample"],
        ]
    )
    def delete(self, sampleid, **kwargs):
        """Delete a sample"""
        subsample = self._parent.remove_sample(sampleid, **kwargs)
        if subsample:
            return {"message": "Subsample deleted"}, 200
        else:
            return {"error": "No such subsample"}, 404


class SubSamplesResource(CoreResource):
    @marshal(
        inp={"sampleid": fields.Int()},
        out=[[200, SubSampleSchema(many=True), "List of sub samples"]],
    )
    def get(self, **kwargs):
        """Get a list of subsamples"""
        return self._parent.get_subsamples(**kwargs), 200

    @marshal(
        inp=NewSubSampleSchema,
        out=[
            [200, SubSampleSchema(), "New Subsample"],
            [400, ErrorSchema(), "Could not create subsample"],
        ],
    )
    def post(self, **kwargs):
        """Create a subsample"""
        subsample = self._parent.add_subsample(**kwargs)
        if subsample:
            return subsample, 201
        else:
            return {"error": "Could not create subsample"}, 400


class SubSampleResource(CoreResource):
    @marshal(
        out=[
            [200, SubSampleSchema(), "Get selected subsample"],
            [404, ErrorSchema(), "No such subsample"],
        ]
    )
    def get(self, subsampleid):
        """Get selected subsample"""
        subsample = self._parent.get_subsamples(subsampleid)
        if subsample:
            return subsample, 200
        else:
            return {"error": "No such subsample"}, 404

    @marshal(
        inp=SubSampleSchema,
        out=[
            [200, SubSampleSchema(), "Updated subsample"],
            [404, ErrorSchema(), "No such subsample"],
        ],
    )
    def patch(self, subsampleid, **kwargs):
        """Update a sub sample"""
        subsample = self._parent.update_subsample(subsampleid, **kwargs)
        if subsample:
            return subsample, 200
        else:
            return {"error": "No such subsample"}, 404

    @marshal(
        out=[
            [200, MessageSchema(), "Subsample deleted"],
            [404, ErrorSchema(), "No such subsample"],
        ]
    )
    def delete(self, subsampleid, **kwargs):
        """Delete a subsample"""
        subsample = self._parent.remove_subsample(subsampleid, **kwargs)
        if subsample:
            return {"message": "Subsample deleted"}, 200
        else:
            return {"error": "No such subsample"}, 404


class SampleImagesResource(CoreResource):
    @marshal(
        inp={"proposal": fields.Str(), "sampleid": fields.Int()},
        out=[[200, SampleImageSchema(many=True), "List of sample images"]],
    )
    def get(self, **kwargs):
        """Get a list of sample images"""
        return self._parent.get_sampleimages(**kwargs), 200

    @marshal(inp=NewSampleImageSchema, out=[[200, SampleImageSchema(), "Sample image"]])
    def post(self, **kwargs):
        """Create a sample image"""
        return self._parent.add_sampleimage(**kwargs), 201


class SampleImageResource(CoreResource):
    @marshal(inp={"no_cache": fields.Int(description="Send no cache header")})
    def get(self, sampleimageid, **kwargs):
        """Get a sample image"""
        path = self._parent.get_sampleimage(sampleimageid, **kwargs)
        if path:
            extra_args = {}
            if kwargs.get("no_cache"):
                extra_args["cache_timeout"] = -1

            if os.path.exists(path):
                return send_file(path, **extra_args)
            else:
                return {"error": "No such image"}, 404
        else:
            return {"error": "No such image"}, 404


class DataCollectionsResource(CoreResource):
    @marshal(
        inp={
            "session": fields.Str(),
            "datacollectiongroupid": fields.Int(),
            "subsampleid": fields.Int(),
            "sampleid": fields.Int(),
            "status": fields.Str(),
            "ungroup": fields.Int(),
        },
        out=[[200, paginated(DataCollectionSchema), "List of datacollections"]],
        paged=True,
        ordered=True,
    )
    def get(self, **kwargs):
        """Get a list of datacollections"""
        return self._parent.get_datacollections(**kwargs), 200


class DataCollectionAttachmentsResource(CoreResource):
    @marshal(
        inp={"datacollectionid": fields.Int(), "filetype": fields.Str()},
        out=[
            [
                200,
                DataCollectionAttachmentSchema(many=True),
                "List of datacollection file attachments",
            ]
        ],
    )
    def get(self, **kwargs):
        """Get a list of datacollection file attachments"""
        return self._parent.get_datacollection_attachments(**kwargs), 200


class DataCollectionAttachmentResource(CoreResource):
    def get(self, datacollectionattachmentid, **kwargs):
        """Get a datacollection attachment"""
        att = self._parent.get_datacollection_attachment(
            datacollectionattachmentid, **kwargs
        )
        if att:
            mime_args = {}
            # TODO: have to override flasks defaults here, why?
            mimes = {
                "json": "application/json",
                "txt": "text/plain",
                "log": "text/plain",
            }
            if att["extension"] in mimes:
                mime_args["mimetype"] = mimes[att["extension"]]

            if os.path.exists(att["filefullpath"]):
                return send_file(
                    att["filefullpath"],
                    cache_timeout=-1,
                    as_attachment=True,
                    **mime_args,
                )
            else:
                return {"error": "No such datacollection attachment"}, 404
        else:
            return {"error": "No such datacollection attachment"}, 404


class DataCollectionSnapshotResource(CoreResource):
    @marshal(
        inp={
            "thumb": fields.Int(description="Return thumbnail image"),
            "no_cache": fields.Int(description="Send no cache header"),
        }
    )
    def get(self, datacollectionid, snapshotid, **kwargs):
        """Get a datacollection snapshot"""
        path = self._parent.get_snapshot(datacollectionid, snapshotid, **kwargs)
        if path:
            extra_args = {}
            if kwargs.get("no_cache"):
                extra_args["cache_timeout"] = -1

            if kwargs.get("thumb"):
                ext = os.path.splitext(path)[1][1:].strip()
                path = path.replace(f".{ext}", f"t.{ext}")

            if os.path.exists(path):
                return send_file(path, **extra_args)
            else:
                return {"error": "No such snapshot"}, 404
        else:
            return {"error": "No such snapshot"}, 404


class DataCollectionPlansResource(CoreResource):
    @marshal(
        inp={
            "session": fields.Str(),
            "subsampleid": fields.Int(),
            "sampleid": fields.Int(),
        },
        out=[
            [200, DataCollectionPlanSchema(many=True), "List of datacollection plans"]
        ],
    )
    def get(self, **kwargs):
        """Get a list of datacollection plans"""
        return self._parent.get_datacollectionplans(**kwargs), 200

    @marshal(
        inp=DataCollectionSchema,
        out=[
            [200, DataCollectionPlanSchema(), "New Datacollection plan"],
            [400, ErrorSchema(), "Could not create datacollection plan"],
        ],
    )
    def post(self, **kwargs):
        """Create a datacollection plan"""
        dcplan = self._parent.add_datacollectionplan(**kwargs)
        if dcplan:
            return dcplan, 201
        else:
            return {"error": "Could not create datacollection plan"}, 400


class DataCollectionPlanResource(CoreResource):
    @marshal(
        out=[
            [200, DataCollectionPlanSchema(), "Get selected datacollection plan"],
            [404, ErrorSchema(), "No such datacollection plan"],
        ]
    )
    def get(self, datacollectionplanid):
        """Get selected datacollection plan"""
        dcplan = self._parent.get_datacollectionplans(datacollectionplanid)
        if dcplan:
            return dcplan, 200
        else:
            return {"error": "No such datacollection plan"}, 404

    @marshal(
        out=[
            [200, DataCollectionPlanSchema(), "Updated datacollection plan"],
            [404, ErrorSchema(), "No such datacollection plan"],
        ]
    )
    def patch(self, datacollectionplanid, **kwargs):
        """Update a datacollection plan"""
        dcplan = self._parent.update_datacollectionplan(datacollectionplanid, **kwargs)
        if dcplan:
            return dcplan, 200
        else:
            return {"error": "No such datacollection plan"}, 404

    @marshal(
        out=[
            [200, MessageSchema(), "Datacollection plan deleted"],
            [404, ErrorSchema(), "No such datacollection plan"],
        ]
    )
    def delete(self, datacollectionplanid, **kwargs):
        """Delete a datacollection plan"""
        subsample = self._parent.remove_datacollectionplan(
            datacollectionplanid, **kwargs
        )
        if subsample:
            return {"message": "Datacollection plan deleted"}, 200
        else:
            return {"error": "No such datacollection plan"}, 404


class ScanQualityIndicatorsResource(CoreResource):
    @marshal(out=[[200, ScanQualityIndicatorsSchema(), "Get scan quality indicators"]])
    def get(self, datacollectionid):
        """Get scan quality indicators"""
        sqis = self._parent.get_scanqualityindicators(datacollectionid=datacollectionid)
        return sqis, 200


class XRFMapsResource(CoreResource):
    @marshal(
        inp={
            "datacollectiongroupid": fields.Int(),
            "sampleid": fields.Int(description="The sampleid"),
            "subsampleid": fields.Int(description="The subsampleid"),
        },
        out=[[200, XRFMapSchema(many=True), "A list of XRF maps"]],
    )
    def get(self, **kwargs):
        """Get a list of XRF maps"""
        return self._parent.get_xrf_maps(**kwargs)


class XRFMapResource(CoreResource):
    @marshal(out=[[404, ErrorSchema(), "XRF map not found"]])
    def get(self, mapid, **kwargs):
        """Get an image of an XRF map"""
        img = self._parent.get_xrf_map_image(mapid=mapid, **kwargs)
        if img is not None:
            return image_response(img)

        else:
            return {"error": "No such map"}, 404

    @marshal(
        inp=XRFMapSchema,
        out=[
            [200, XRFMapSchema(), "The updated XRF map"],
            [400, ErrorSchema(), "Could not update XRF map"],
        ],
    )
    def patch(self, mapid, **kwargs):
        """Update an XRF map"""
        m = self._parent.update_xrf_map(mapid, **kwargs)
        if m:
            return m, 200
        else:
            return {"error": "Could not update XRF map"}, 400

    @marshal(
        out=[
            [200, MessageSchema(), "XRF map deleted"],
            [400, ErrorSchema(), "Could not delete XRF map"],
        ]
    )
    def delete(self, mapid, **kwargs):
        """Delete an XRF map"""
        _map = self._parent.remove_xrf_map(mapid, **kwargs)
        if _map:
            return {"message": "XRF Map deleted"}, 200
        else:
            return {"error": "No such XRF map"}, 404


class XRFMapHistogramResource(CoreResource):
    @marshal(
        out=[
            [200, XRFMapHistogramSchema(), "The XRF map histogram"],
            [400, ErrorSchema(), "XRF map not found"],
        ],
    )
    def get(self, mapid, **kwargs):
        """Get a histogram of values for an XRF map"""
        hist = self._parent.get_xrf_maps(mapid=mapid, histogram=True, **kwargs)
        if hist is not None:
            return hist
        else:
            return {"error": "No such map"}, 404


class XRFMapValueResource(CoreResource):
    @marshal(
        inp={"x": fields.Int(required=True), "y": fields.Int(required=True)},
        out=[
            [200, XRFMapValueSchema(), "XRF map value"],
            [400, ErrorSchema(), "Invalid point"],
            [404, ErrorSchema(), "XRF map not found"],
        ],
    )
    def get(self, mapid, **kwargs):
        """Get a value at an x,y point from an XRF map"""
        map_ = self._parent.get_xrf_maps(mapid=mapid, data=True, **kwargs)
        if map_ is not None:
            shaped = shape_map(map_)

            if kwargs["y"] < len(shaped):
                if kwargs["x"] < len(shaped[kwargs["y"]]):
                    return {
                        "mapid": mapid,
                        "x": kwargs["x"],
                        "y": kwargs["y"],
                        "value": shaped[kwargs["y"]][kwargs["x"]],
                    }

            return {"error": "Invalid point"}, 400

        else:
            return {"error": "No such map"}, 404


class XRFCompositesMapResource(CoreResource):
    @marshal(
        inp={"sampleid": fields.Int(description="The sampleid")},
        out=[[200, XRFCompositeMapSchema(many=True), "A list of XRF composite maps"]],
    )
    def get(self, **kwargs):
        """Get a list of XRF composite maps"""
        return self._parent.get_xrf_composites(**kwargs)

    @marshal(
        inp=NewXRFCompositeMapSchema,
        out=[
            [200, XRFCompositeMapSchema(), "New XRF composite map"],
            [400, ErrorSchema(), "Could not add XRF composite map"],
        ],
    )
    def post(self, **kwargs):
        """Add a new XRF composite map"""
        comp = self._parent.add_xrf_composite(**kwargs)
        if comp:
            return comp, 201
        else:
            return {"error": "Could not add XRF composite map"}, 400


class XRFCompositeMapResource(CoreResource):
    @marshal(out=[[404, ErrorSchema(), "XRF composite map not found"]])
    def get(self, compositeid, **kwargs):
        """Get an image of an XRF composite map"""
        img = self._parent.get_xrf_composite_image(compositeid=compositeid, **kwargs)
        if img is not None:
            return image_response(img)
        else:
            return {"error": "No such XRF composite map"}, 404

    @marshal(
        inp=XRFCompositeMapSchema,
        out=[
            [200, XRFCompositeMapSchema(), "The updated XRF composite map"],
            [400, ErrorSchema(), "Could not update XRF composite map"],
        ],
    )
    def patch(self, compositeid, **kwargs):
        """Update an XRF composite map"""
        c = self._parent.update_xrf_composite(compositeid, **kwargs)
        if c:
            return c, 200
        else:
            return {"error": "Could not update XRF composite map"}, 400

    @marshal(
        out=[
            [200, MessageSchema(), "XRF composite map deleted"],
            [404, ErrorSchema(), "No such XRF composite map"],
        ]
    )
    def delete(self, compositeid, **kwargs):
        """Delete an XRF composite map"""
        comp = self._parent.remove_xrf_composite(compositeid, **kwargs)
        if comp:
            return {"message": "XRF composite map deleted"}, 200
        else:
            return {"error": "No such XRF composite map"}, 404


class XRFMapROIsResource(CoreResource):
    @marshal(
        inp={"sampleid": fields.Int()},
        out=[[200, XRFMapROISchema(many=True), "A list of XRF map ROIs"]],
    )
    def get(self, **kwargs):
        """Get a list of XRF map ROIs"""
        return self._parent.get_xrf_map_rois(**kwargs)

    @require_control
    @marshal(
        inp=NewXRFMapROISchema,
        out=[
            [200, XRFMapROISchema(), "New XRF map ROI"],
            [400, ErrorSchema(), "Could not add XRF map ROI"],
        ],
    )
    def post(self, **kwargs):
        """Add a new XRF map ROI"""
        maproi = self._parent.add_xrf_map_roi(**kwargs)
        if maproi:
            return maproi, 201
        else:
            return {"error": "Could not add XRF map ROI"}, 400


class XRFMapROIResource(CoreResource):
    @require_control
    @marshal(
        inp=XRFMapROISchema,
        out=[
            [200, XRFMapROISchema(), "The updated XRF map ROI"],
            [400, ErrorSchema(), "Could not update XRF map ROI"],
        ],
    )
    def patch(self, maproiid, **kwargs):
        """Update an XRF map ROI"""
        m = self._parent.update_xrf_map_roi(maproiid, **kwargs)
        if m:
            return m, 200
        else:
            return {"error": "Could not update XRF map ROI"}, 400

    @require_control
    @marshal(
        out=[
            [200, MessageSchema(), "XRF map ROI deleted"],
            [404, ErrorSchema(), "No such XRF map ROI"],
        ]
    )
    def delete(self, maproiid, **kwargs):
        """Delete an XRF map ROI"""
        roi = self._parent.remove_xrf_map_roi(maproiid, **kwargs)
        if roi:
            return {"message": "XRF map roi deleted"}, 200
        else:
            return {"error": "No such XRF map roi"}, 404


class AutoProcProgramsResource(CoreResource):
    @marshal(
        inp={
            "sampleid": fields.Int(),
            "subsampleid": fields.Int(),
            "datacollectionid": fields.Int(),
        },
        out=[[200, paginated(AutoProcProgramSchema), "List of auto processings"]],
    )
    def get(self, **kwargs):
        """Get a list of auto processings"""
        return self._parent.get_autoprocprograms(**kwargs), 200


class AutoProcProgramAttachmentsResource(CoreResource):
    @marshal(
        inp={"autoprocprogramid": fields.Int()},
        out=[
            [
                200,
                paginated(AutoProcProgramAttachmentSchema),
                "List of auto processing attachments",
            ]
        ],
    )
    def get(self, **kwargs):
        """Get a list of auto processing attachments"""
        return (
            self._parent.get_autoprocprogram_attachments(**kwargs),
            200,
        )


class AutoProcProgramMessagesResource(CoreResource):
    @marshal(
        inp={"autoprocprogramid": fields.Int()},
        out=[
            [
                200,
                paginated(AutoProcProgramMessageSchema),
                "List of auto processing messages",
            ]
        ],
    )
    def get(self, **kwargs):
        """Get a list of auto processing messages"""
        return (
            self._parent.get_autoprocprogram_messages(**kwargs),
            200,
        )


class AutoProcProgramAttachmentResource(CoreResource):
    def get(self, autoprocprogramattachmentid, **kwargs):
        """Get an auto processing attachment"""
        attachment = self._parent.get_autoprocprogram_attachments(
            autoprocprogramattachmentid, **kwargs
        )
        path = attachment["filefullpath"]
        if path:
            if os.path.exists(path):
                return send_file(path, cache_timeout=-1, as_attachment=True)
            else:
                return {"error": "No such attachment"}, 404
        else:
            return {"error": "No such attachment"}, 404


class MetaData:
    def __init__(self, config, *args, **kwargs):
        kwargs["config"] = config

        self._meta = loader(
            "daiquiri.core.metadata",
            "MetaDataHandler",
            config["meta_type"],
            *args,
            **kwargs,
        )

    def init(self):
        return self._meta


class MetaDataHandler(CoreBase):
    _require_session = True
    _require_blsession = True
    _namespace = "metadata"
    _base_url = "metadata"

    _cache = {}

    def setup(self):
        self._cache_locks = {}

        self.register_route(UserResource, "/users/current")
        self.register_route(UserCacheResource, "/users/cache")

        self.register_route(ProposalsResource, "/proposals")
        self.register_route(ProposalResource, "/proposals/<string:proposal>")

        self.register_route(SessionsResource, "/sessions")
        self.register_route(SessionResource, "/sessions/<string:session>")
        self.register_route(SelectSessionResource, "/sessions/select")

        self.register_route(ComponentsResource, "/components")
        self.register_route(ComponentResource, "/components/<int:componentid>")

        self.register_route(SamplesResource, "/samples")
        self.register_route(SampleResource, "/samples/<int:sampleid>")
        self.register_route(SubSamplesResource, "/samples/sub")
        self.register_route(SubSampleResource, "/samples/sub/<int:subsampleid>")
        self.register_route(SampleImagesResource, "/samples/images")
        self.register_route(SampleImageResource, "/samples/images/<int:sampleimageid>")

        self.register_route(DataCollectionsResource, "/datacollections")
        self.register_route(
            DataCollectionAttachmentsResource, "/datacollections/attachments"
        )
        self.register_route(
            DataCollectionAttachmentResource,
            "/datacollections/attachments/<int:datacollectionattachmentid>",
        )
        self.register_route(
            DataCollectionSnapshotResource,
            "/datacollections/snapshots/<int:datacollectionid>/<int:snapshotid>",
        )
        self.register_route(DataCollectionPlansResource, "/datacollections/plans")
        self.register_route(
            DataCollectionPlanResource,
            "/datacollections/plans/<int:datacollectionplanid>",
        )
        self.register_route(
            ScanQualityIndicatorsResource,
            "/datacollections/sqis/<int:datacollectionid>",
        )

        self.register_route(XRFMapsResource, "/xrf/maps")
        self.register_route(XRFMapResource, "/xrf/maps/<int:mapid>")
        self.register_route(XRFMapValueResource, "/xrf/maps/value/<int:mapid>")
        self.register_route(XRFMapHistogramResource, "/xrf/maps/histogram/<int:mapid>")

        self.register_route(XRFCompositesMapResource, "/xrf/maps/composite")
        self.register_route(
            XRFCompositeMapResource, "/xrf/maps/composite/<int:compositeid>"
        )

        self.register_route(XRFMapROIsResource, "/xrf/maps/rois")
        self.register_route(XRFMapROIResource, "/xrf/maps/rois/<int:maproiid>")

        self.register_route(AutoProcProgramsResource, "/autoprocs")
        self.register_route(
            AutoProcProgramAttachmentsResource, "/autoprocs/attachments",
        )
        self.register_route(
            AutoProcProgramAttachmentResource,
            "/autoprocs/attachments/<int:autoprocprogramattachmentid>",
        )
        self.register_route(AutoProcProgramMessagesResource, "/autoprocs/messages")

        # Inject socketio notifier to subclassed methods
        for t in [
            "sample",
            "subsample",
            "datacollection",
            "sampleimage",
            "xrf_map",
            "xrf_composite",
        ]:
            for a in ["add", "update"]:
                fn = f"{a}_{t}"
                old = getattr(self, fn)
                if not hasattr(old, "_wrapped"):
                    new = wraps(fn)(self.notify(getattr(self, fn), type=t, action=a))
                    new._wrapped = True
                    setattr(self, fn, new)

        for t in ["subsample"]:
            for a in ["queue", "unqueue"]:
                fn = f"{a}_{t}"
                old = getattr(self, fn)
                if not hasattr(old, "_wrapped"):
                    new = wraps(fn)(self.notify(getattr(self, fn), type=t, action=a))
                    new._wrapped = True
                    setattr(self, fn, new)

    def notify(self, fn, **wkwargs):
        def wrapper(*args, **kwargs):
            ret = fn(*args, **kwargs)

            # "id": ret[f"{wkwargs['type']}id"]
            self.emit(
                "message", {"type": wkwargs["type"], "action": wkwargs["action"]},
            )

            return ret

        return wrapper

    def get_user(self):
        """Get the current user from the metadata handler
        The current login can be retrieved from flask.g.login

        The returned dict should provide schema consistent with
        daiquiri.core.schema.metadata.UserSchema

        Should return an instance of daiquiri.core.metadata.user.User
        Pass the user dict to initialize
            return User(**user)

        Returns:
            user (User(dict)): The user as a User object
        """
        raise NotImplementedError

    def verify_session(self, session):
        """ Verify the user has access to the passed session

        Args:
            session (str): The session of the form ab1234-1

        Returns:
            session (dict): The verified session
        """
        raise NotImplementedError

    def set_session(self, session):
        if self.verify_session(session):
            self._session.update({"blsession": session})
            return True

    def get_proposals(self, proposal=None, **kwargs):
        raise NotImplementedError

    def get_session(self, session=None, **kwargs):
        raise NotImplementedError

    def get_user_cache(self):
        """Get the cache for the current user"""
        return self._cache.get(g.login, {})

    def update_user_cache(self, cache):
        """Update the user cache for the current user"""
        if not g.login in self._cache:
            self._cache[g.login] = {}
            self._cache_locks[g.login] = lock.Semaphore()

        with self._cache_locks[g.login]:
            self._cache[g.login] = cache
            return self._cache[g.login]

    # Components
    def get_components(self, componentid=None, **kwargs):
        raise NotImplementedError

    def add_component(self, componentid, **kwargs):
        raise NotImplementedError

    def update_component(self, componentid, **kwargs):
        raise NotImplementedError

    # Samples
    def get_samples(self, sampleid=None, **kwargs):
        raise NotImplementedError

    def add_sample(self, **kwargs):
        raise NotImplementedError

    def update_sample(self, sampleid, **kwargs):
        raise NotImplementedError

    def remove_sample(self, sampleid, **kwargs):
        raise NotImplementedError

    def queue_sample(self, sampleid, **kwargs):
        raise NotImplementedError

    def unqueue_sample(self, sampleid, **kwargs):
        raise NotImplementedError

    # Subsamples
    def get_subsamples(self, subsampleid=None, **kwargs):
        raise NotImplementedError

    def add_subsample(self, **kwargs):
        raise NotImplementedError

    def update_subsample(self, subsampeleid, **kwargs):
        raise NotImplementedError

    def remove_subsample(self, subsampleid, **kwargs):
        raise NotImplementedError

    def queue_subsample(self, subsampleid, **kwargs):
        raise NotImplementedError

    def unqueue_subsample(self, subsampleid, **kwargs):
        raise NotImplementedError

    #  Sample Images
    def get_sampleimages(self, sampleimageid=None, **kwargs):
        raise NotImplementedError

    def add_sampleimage(self, **kwargs):
        raise NotImplementedError

    def update_sampleimage(self, **kwargs):
        raise NotImplementedError

    def get_sampleimage(self, sampleimageid, **kwargs):
        sampleimage = self.get_sampleimages(sampleimageid)
        if sampleimage:
            return sampleimage["file"]

    #  DC
    def get_datacollections(self, datacollectionid=None, **kwargs):
        raise NotImplementedError

    def _check_snapshots(self, dc):
        sns = {}
        for i, sn in enumerate(
            [
                "xtalsnapshotfullpath1",
                "xtalsnapshotfullpath2",
                "xtalsnapshotfullpath3",
                "xtalsnapshotfullpath4",
            ]
        ):
            if sn in dc:
                sns[i + 1] = os.path.exists(dc[sn]) if dc[sn] is not None else False
            else:
                sns[i + 1] = False

        dc["snapshots"] = sns
        return dc

    def add_datacollection(self, **kwargs):
        raise NotImplementedError

    def update_datacollection(self, datacollectionid, **kwargs):
        raise NotImplementedError

    def get_snapshot(self, datacollectionid, snapshotid=1, **kwargs):
        dc = self.get_datacollections(datacollectionid=datacollectionid)
        if dc:
            if snapshotid in dc["snapshots"]:
                if os.path.exists(dc[f"xtalsnapshotfullpath{snapshotid}"]):
                    return dc[f"xtalsnapshotfullpath{snapshotid}"]

    # DC Plan
    def get_datacollectionplans(self, datacollectionplanid=None, **kwargs):
        raise NotImplementedError

    def add_datacollectionplan(self, **kwargs):
        raise NotImplementedError

    def update_datacollectionplan(self, datacollectionplanid, **kwargs):
        raise NotImplementedError

    def remove_datacollectionplan(self, datacollectionplanid, **kwargs):
        raise NotImplementedError

    # DC Attachments
    def get_datacollection_attachments(self, datacollectionattachmentid=None, **kwargs):
        raise NotImplementedError

    def add_datacollection_attachment(self, **kwargs):
        raise NotImplementedError

    def get_datacollection_attachment(self, datacollectionattachmentid, **kwargs):
        att = self.get_datacollection_attachments(
            datacollectionattachmentid=datacollectionattachmentid, **kwargs
        )
        if att:
            if os.path.exists(att["filefullpath"]):
                return att

    # Scan Quality Indicators
    def get_scanqualityindicators(self, datacollectionid=None, **kwargs):
        raise NotImplementedError

    def add_scanqualityindicators(self, **kwargs):
        raise NotImplementedError

    # XRF Maps
    def get_xrf_maps(self, mapid=None, **kwargs):
        raise NotImplementedError

    def get_xrf_map_image(self, mapid, **kwargs):
        map_ = self.get_xrf_maps(mapid, data=True, **kwargs)

        if map_:
            return generate_map_image(map_)

    def add_xrf_map(self, **kwargs):
        raise NotImplementedError

    def update_xrf_map(self, mapid, **kwargs):
        raise NotImplementedError

    def remove_xrf_map(self, mapid):
        raise NotImplementedError

    # XRF Composites
    def get_xrf_composites(self, compositeid=None, **kwargs):
        raise NotImplementedError

    def get_xrf_composite_image(self, compositeid, **kwargs):
        comp = self.get_xrf_composites(compositeid=compositeid, **kwargs)

        if comp:
            maps = {
                col: self.get_xrf_maps(mapid=comp[col], data=True)
                for col in ["r", "g", "b"]
            }
            return generate_composite_image(comp, maps)

    def add_xrf_composite(self, **kwargs):
        raise NotImplementedError

    def update_xrf_composite(self, compositeid, **kwargs):
        raise NotImplementedError

    def remove_xrf_composite(self, compositeid):
        raise NotImplementedError

    #  XRF ROIs
    def get_xrf_map_rois(self, maproiid=None, **kwargs):
        raise NotImplementedError

    def add_xrf_map_roi(self, **kwargs):
        raise NotImplementedError

    def update_xrf_map_roi(self, maproiid, **kwargs):
        raise NotImplementedError

    def remove_xrf_map_roi(self, maproiid):
        raise NotImplementedError

    # Auto Processing
    def get_autoprocprograms(self, **kwargs):
        raise NotImplementedError

    def get_autoprocprogram_attachments(self, **kwargs):
        raise NotImplementedError

    def get_autoprocprogram_messages(self, **kwargs):
        raise NotImplementedError

    def get_autoprocprogram_attachment(self, autoprocprogramattachmentid, **kwargs):
        attachment = self.get_autoprocprogram_attachments(
            autoprocprogramattachmentid=autoprocprogramattachmentid
        )
        if attachment:
            if os.path.exists(attachment["filefullpath"]):
                return attachment["filefullpath"]
