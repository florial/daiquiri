# -*- coding: utf-8 -*-
import logging
import marshmallow

from daiquiri.core.metadata import MetaDataHandler


logger = logging.getLogger(__name__)


class RedisMetaDataHandler(MetaDataHandler):
    def setup(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        logger.debug("Loaded: {c}".format(c=self.__class__.__name__))

    def get_user(self, login):
        pass

    def get_visits(self, userid):
        pass

    def get_samples(self, **kwargs):
        pass

    def create_sample(self, *args, **kwargs):
        pass

    def update_sample(self, *args, **kwargs):
        pass

    def get_subsamples(self, **kwargs):
        pass

    def create_subsample(self, *args, **kwargs):
        pass

    def update_subsample(self, *args, **kwargs):
        pass

    def add_sample_image(self, sampleid, *args, **kwargs):
        pass

    def get_sample_images(self, sampleid):
        pass

    def get_datacollection_plans(self, *args, **kwargs):
        pass

    def create_datacollection_plan(self, sampleid, *args, **kwargs):
        pass
