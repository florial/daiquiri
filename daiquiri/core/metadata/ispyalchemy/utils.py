import logging
from daiquiri.core.utils import get_start_end

logger = logging.getLogger(__name__)


def order(query, sort_map, **kwargs):
    """Sort a result set by a field
    
    Args:
        query (sqlalchemy.query): The current query
        sort_map (dict): A mapping of field(str) -> sqlalchemy.column

    Kwargs:
        order_by (str): Field to sort by
        order (str): Asc or desc

    Returns
        query (sqlalchemy.query): The ordered query
    """
    sort_field = kwargs.get("order_by")
    sort_order = kwargs.get("order")

    if not (sort_field and sort_order):
        return query

    if sort_field not in sort_map:
        logger.warning(f"Unknown sort_field {sort_field}")
        return query

    return query.order_by(getattr(sort_map[sort_field], sort_order)())


def page(query, **kwargs):
    """Paginate a result set

    Kwargs:
        per_page (str): Number of rows per page
        page (str): Page number to display
        last (bool): Reverse order, last page first

    Returns
        query (sqlalchemy.query): The paginated query
    """
    lims = get_start_end(kwargs)
    return query.limit(lims["per_page"]).offset(lims["st"])
