# -*- coding: utf-8 -*-
import os

from flask import g

from sqlalchemy import func
from daiquiri.core.metadata.ispyalchemy.handler import IspyalchemyHandler
from daiquiri.core.metadata.ispyalchemy.utils import page


class AutoProcHandler(IspyalchemyHandler):
    exported = [
        "get_autoprocprograms",
        "get_autoprocprogram_attachments",
        "get_autoprocprogram_messages",
    ]

    def get_autoprocprograms(self, autoprocprogramid=None, **kwargs):
        with self.session_scope() as ses:
            autoprocprograms = (
                ses.query(
                    self.AutoProcProgram.autoprocprogramid,
                    self.ProcessingJob.datacollectionid,
                    self.AutoProcProgram.processingstatus.label("status"),
                    self.AutoProcProgram.processingmessage.label("message"),
                    self.AutoProcProgram.processingstarttime.label("starttime"),
                    self.AutoProcProgram.processingendtime.label("endtime"),
                    self.AutoProcProgram.processingprograms.label("programs"),
                    self.ProcessingJob.automatic,
                    func.sum(
                        func.IF(self.AutoProcProgramMessage.severity == "WARNING", 1, 0)
                    ).label("warnings"),
                    func.sum(
                        func.IF(self.AutoProcProgramMessage.severity == "ERROR", 1, 0)
                    ).label("errors"),
                    func.time_to_sec(
                        func.timediff(
                            self.AutoProcProgram.processingendtime,
                            self.AutoProcProgram.processingstarttime,
                        )
                    ).label("duration"),
                )
                .outerjoin(
                    self.AutoProcProgramMessage,
                    self.AutoProcProgramMessage.autoprocprogramid
                    == self.AutoProcProgram.autoprocprogramid,
                )
                .join(
                    self.ProcessingJob,
                    self.ProcessingJob.processingjobid
                    == self.AutoProcProgram.processingjobid,
                )
                .join(
                    self.DataCollection,
                    self.DataCollection.datacollectionid
                    == self.ProcessingJob.datacollectionid,
                )
                .join(
                    self.DataCollectionGroup,
                    self.DataCollectionGroup.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
                .group_by(self.AutoProcProgram.autoprocprogramid)
            )

            if not kwargs.get("no_context"):
                autoprocprograms = autoprocprograms.filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )

            if kwargs.get("datacollectionid"):
                autoprocprograms = autoprocprograms.filter(
                    self.ProcessingJob.datacollectionid == kwargs["datacollectionid"]
                )

            if kwargs.get("sampleid"):
                autoprocprograms = autoprocprograms.filter(
                    self.DataCollection.blsampleid == kwargs["sampleid"]
                )

            if kwargs.get("subsampleid"):
                autoprocprograms = autoprocprograms.filter(
                    self.DataCollection.blsubsampleid == kwargs["subsampleid"]
                )

            if autoprocprogramid:
                autoprocprograms = autoprocprograms.filter(
                    self.XRFFluorescenceMapping.xrffluorescencemappingid
                    == autoprocprogramid
                )
                autoprocprogram = autoprocprograms.first()
                if autoprocprogram:
                    return autoprocprogram._asdict()

            else:
                total = autoprocprograms.count()
                autoprocprograms = page(autoprocprograms, **kwargs)
                return {
                    "total": total,
                    "rows": [r._asdict() for r in autoprocprograms.all()],
                }

    def get_autoprocprogram_attachments(
        self, autoprocprogramattachmentid=None, **kwargs
    ):
        with self.session_scope() as ses:
            attachments = (
                ses.query(
                    self.AutoProcProgramAttachment.autoprocprogramattachmentid,
                    self.AutoProcProgram.autoprocprogramid,
                    self.AutoProcProgramAttachment.filename,
                    self.AutoProcProgramAttachment.filepath,
                    self.AutoProcProgramAttachment.filetype,
                    self.AutoProcProgramAttachment.importancerank.label("rank"),
                )
                .join(
                    self.AutoProcProgram,
                    self.AutoProcProgram.autoprocprogramid
                    == self.AutoProcProgramAttachment.autoprocprogramid,
                )
                .join(
                    self.ProcessingJob,
                    self.ProcessingJob.processingjobid
                    == self.AutoProcProgram.autoprocprogramid,
                )
                .join(
                    self.DataCollection,
                    self.DataCollection.datacollectionid
                    == self.ProcessingJob.datacollectionid,
                )
                .join(
                    self.DataCollectionGroup,
                    self.DataCollectionGroup.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
            )

            if not kwargs.get("no_context"):
                attachments = attachments.filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )

            if kwargs.get("autoprocprogramid"):
                attachments = attachments.filter(
                    self.AutoProcProgram.autoprocprogramid
                    == kwargs["autoprocprogramid"]
                )

            if kwargs.get("autoprocprogramattachmentid"):
                attachments = attachments.filter(
                    self.AutoProcProgramAttachment.autoprocprogramattachmentid
                    == kwargs["autoprocprogramattachmentid"]
                )

            if autoprocprogramattachmentid:
                attachments = attachments.filter(
                    self.AutoProcProgramAttachment.autoprocprogramattachmentid
                    == autoprocprogramattachmentid
                )
                attachment = attachments.first()
                if attachment:
                    attachment = attachment._asdict()
                    attachment["filefullpath"] = os.path.join(
                        attachment["filepath"], attachment["filename"]
                    )
                    return attachment

            else:
                total = attachments.count()
                attachments = page(attachments, **kwargs)
                return {
                    "total": total,
                    "rows": [r._asdict() for r in attachments.all()],
                }

    def get_autoprocprogram_messages(self, autoprocprogrammessageid=None, **kwargs):
        with self.session_scope() as ses:
            messages = (
                ses.query(
                    self.AutoProcProgramMessage.autoprocprogrammessageid,
                    self.AutoProcProgram.autoprocprogramid,
                    self.AutoProcProgramMessage.recordtimestamp.label("timestamp"),
                    self.AutoProcProgramMessage.severity,
                    self.AutoProcProgramMessage.message,
                    self.AutoProcProgramMessage.description,
                )
                .join(
                    self.AutoProcProgram,
                    self.AutoProcProgram.autoprocprogramid
                    == self.AutoProcProgramMessage.autoprocprogramid,
                )
                .join(
                    self.ProcessingJob,
                    self.ProcessingJob.processingjobid
                    == self.AutoProcProgram.autoprocprogramid,
                )
                .join(
                    self.DataCollection,
                    self.DataCollection.datacollectionid
                    == self.ProcessingJob.datacollectionid,
                )
                .join(
                    self.DataCollectionGroup,
                    self.DataCollectionGroup.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
            )

            if not kwargs.get("no_context"):
                messages = messages.filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )

            if kwargs.get("autoprocprogramid"):
                messages = messages.filter(
                    self.AutoProcProgram.autoprocprogramid
                    == kwargs["autoprocprogramid"]
                )

            if kwargs.get("autoprocprogrammessageid"):
                messages = messages.filter(
                    self.AutoProcProgramMessage.autoprocprogrammessageid
                    == kwargs["autoprocprogrammessageid"]
                )

            if autoprocprogrammessageid:
                messages = messages.filter(
                    self.AutoProcProgramMessage.autoprocprogrammessageid
                    == autoprocprogrammessageid
                )
                message = messages.first()
                if message:
                    return message._asdict()

            else:
                total = messages.count()
                messages = page(messages, **kwargs)
                return {"total": total, "rows": [r._asdict() for r in messages.all()]}
