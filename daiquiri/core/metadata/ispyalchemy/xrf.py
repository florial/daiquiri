# -*- coding: utf-8 -*-
from flask import g
from sqlalchemy import orm, func
from sqlalchemy.sql.expression import or_, distinct

from daiquiri.core.metadata.ispyalchemy.handler import IspyalchemyHandler
from daiquiri.core.metadata.xrf import generate_histogram, gunzip_json, gzip_json


class XRFHandler(IspyalchemyHandler):
    exported = [
        "get_xrf_maps",
        "add_xrf_map",
        "update_xrf_map",
        "remove_xrf_map",
        "get_xrf_composites",
        "add_xrf_composite",
        "update_xrf_composite",
        "remove_xrf_composite",
        "get_xrf_map_rois",
        "add_xrf_map_roi",
        "add_xrf_map_roi_scalar",
        "update_xrf_map_roi",
        "remove_xrf_map_roi",
    ]

    def get_xrf_maps(self, mapid=None, **kwargs):
        with self.session_scope() as ses:
            xrfmaps = (
                ses.query(
                    self.XRFFluorescenceMapping.xrffluorescencemappingid.label("mapid"),
                    self.XRFFluorescenceMapping.xrffluorescencemappingroiid.label(
                        "maproiid"
                    ),
                    self.XRFFluorescenceMapping.opacity,
                    self.XRFFluorescenceMapping.colourmap,
                    self.XRFFluorescenceMapping.points,
                    self.XRFFluorescenceMapping.min,
                    self.XRFFluorescenceMapping.max,
                    self.XRFFluorescenceMappingROI.element,
                    self.XRFFluorescenceMappingROI.edge,
                    self.XRFFluorescenceMappingROI.scalar,
                    self.GridInfo.steps_x.label("w"),
                    self.GridInfo.steps_y.label("h"),
                    self.GridInfo.snaked,
                    self.GridInfo.orientation,
                    func.count(
                        distinct(
                            self.XFEFluorescenceComposite.xfefluorescencecompositeid
                        )
                    ).label("composites"),
                    self.DataCollection.datacollectionid.label("datacollectionid"),
                    self.DataCollection.datacollectiongroupid.label(
                        "datacollectiongroupid"
                    ),
                    self.DataCollection.datacollectionnumber.label(
                        "datacollectionnumber"
                    ),
                    self.DataCollection.blsubsampleid.label("subsampleid"),
                    self.DataCollection.blsampleid.label("sampleid"),
                    func.concat(
                        f"/{self._base_url}/xrf/maps/",
                        self.XRFFluorescenceMapping.xrffluorescencemappingid,
                    ).label("url"),
                )
                .join(self.XRFFluorescenceMappingROI)
                .join(self.GridInfo)
                .join(
                    self.DataCollection,
                    self.GridInfo.datacollectionid
                    == self.DataCollection.datacollectionid,
                )
                .join(
                    self.DataCollectionGroup,
                    self.DataCollection.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
                .join(self.BLSession)
                .outerjoin(
                    self.XFEFluorescenceComposite,
                    or_(
                        self.XFEFluorescenceComposite.r
                        == self.XRFFluorescenceMapping.xrffluorescencemappingid,
                        or_(
                            self.XFEFluorescenceComposite.g
                            == self.XRFFluorescenceMapping.xrffluorescencemappingid,
                            self.XFEFluorescenceComposite.b
                            == self.XRFFluorescenceMapping.xrffluorescencemappingid,
                        ),
                    ),
                )
                .group_by(self.XRFFluorescenceMapping.xrffluorescencemappingid)
            )

            if kwargs.get("data") or kwargs.get("histogram"):
                xrfmaps = xrfmaps.add_columns(self.XRFFluorescenceMapping.data)

            if not kwargs.get("no_context"):
                xrfmaps = xrfmaps.filter(
                    self.BLSession.sessionid == g.blsession.get("sessionid")
                )

            if kwargs.get("datacollectiongroupid"):
                xrfmaps = xrfmaps.filter(
                    self.DataCollection.datacollectiongroupid
                    == kwargs.get("datacollectiongroupid")
                )

            if kwargs.get("sampleid"):
                xrfmaps = xrfmaps.filter(
                    self.DataCollection.blsampleid == kwargs.get("sampleid")
                )

            if kwargs.get("subsampleid"):
                xrfmaps = xrfmaps.filter(
                    self.DataCollection.blsubsampleid == kwargs.get("subsampleid")
                )

            if mapid:
                xrfmaps = xrfmaps.filter(
                    self.XRFFluorescenceMapping.xrffluorescencemappingid == mapid
                )
                xrfmap = xrfmaps.first()
                if xrfmap:
                    xmap = xrfmap._asdict()
                    xmap = self._additional(xmap, histogram=kwargs.get("histogram"))
                    return xmap

            else:
                maps = [r._asdict() for r in xrfmaps.all()]
                for m in maps:
                    m = self._additional(m, histogram=kwargs.get("histogram"))

                return maps

    def _additional(self, row, histogram=False):
        if row.get("data"):
            row["data"] = gunzip_json(row["data"])

            if histogram:
                row["histogram"] = generate_histogram(row["data"])

        return row

    def add_xrf_map(self, **kwargs):
        with self.session_scope() as ses:
            gridinfo = (
                ses.query(self.GridInfo.gridinfoid).filter(
                    self.GridInfo.datacollectionid == kwargs["datacollectionid"]
                )
            ).first()

            if not gridinfo:
                return

            xrfmap = self.XRFFluorescenceMapping(
                gridinfoid=gridinfo.gridinfoid,
                xrffluorescencemappingroiid=kwargs["maproiid"],
                data=gzip_json(kwargs["data"]),
                dataformat="json+gzip",
                points=kwargs.get("points"),
            )

            ses.add(xrfmap)
            ses.commit()

            return self.get_xrf_maps(
                mapid=xrfmap.xrffluorescencemappingid,
                no_context=kwargs.get("no_context"),
                data=kwargs.get("return_data"),
            )

    def update_xrf_map(self, mapid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_xrf_maps(mapid=mapid, no_context=kwargs.get("no_context"))
            if chk:
                xrfmap = (
                    ses.query(self.XRFFluorescenceMapping)
                    .filter(
                        self.XRFFluorescenceMapping.xrffluorescencemappingid == mapid
                    )
                    .first()
                )
                updatable = ["opacity", "min", "max", "data", "colourmap", "points"]
                for k, v in kwargs.items():
                    if k in updatable:
                        tv = v
                        if k == "data":
                            tv = gzip_json(v)

                        setattr(xrfmap, k, tv)

                ses.commit()

                return self.get_xrf_maps(
                    mapid=mapid, no_context=kwargs.get("no_context")
                )

    def remove_xrf_map(self, mapid):
        with self.session_scope() as ses:
            chk = self.get_xrf_maps(mapid=mapid)
            if chk:
                xrfmap = (
                    ses.query(self.XRFFluorescenceMapping)
                    .filter(
                        self.XRFFluorescenceMapping.xrffluorescencemappingid == mapid
                    )
                    .first()
                )

                roi = (
                    ses.query(self.XRFFluorescenceMappingROI)
                    .filter(
                        self.XRFFluorescenceMappingROI.xrffluorescencemappingroiid
                        == xrfmap.xrffluorescencemappingroiid
                    )
                    .first()
                )

                ses.delete(xrfmap)

                if roi.blsampleid is None:
                    ses.delete(roi)

                ses.commit()

                return True

    # # XRF Composites
    def get_xrf_composites(self, compositeid=None, **kwargs):
        with self.session_scope() as ses:
            xrfmap2 = orm.aliased(self.XRFFluorescenceMapping)
            xrfmap3 = orm.aliased(self.XRFFluorescenceMapping)

            xrfmaproi2 = orm.aliased(self.XRFFluorescenceMappingROI)
            xrfmaproi3 = orm.aliased(self.XRFFluorescenceMappingROI)

            composites = (
                ses.query(
                    self.XFEFluorescenceComposite.xfefluorescencecompositeid.label(
                        "compositeid"
                    ),
                    self.XFEFluorescenceComposite.r,
                    self.XFEFluorescenceComposite.g,
                    self.XFEFluorescenceComposite.b,
                    func.ifnull(
                        func.concat(
                            self.XRFFluorescenceMappingROI.element,
                            "-",
                            self.XRFFluorescenceMappingROI.edge,
                        ),
                        self.XRFFluorescenceMappingROI.scalar,
                    ).label("rroi"),
                    func.ifnull(
                        func.concat(xrfmaproi2.element, "-", xrfmaproi2.edge),
                        xrfmaproi2.scalar,
                    ).label("groi"),
                    func.ifnull(
                        func.concat(xrfmaproi3.element, "-", xrfmaproi3.edge),
                        xrfmaproi3.scalar,
                    ).label("broi"),
                    self.XFEFluorescenceComposite.ropacity,
                    self.XFEFluorescenceComposite.gopacity,
                    self.XFEFluorescenceComposite.bopacity,
                    self.XFEFluorescenceComposite.opacity,
                    func.max(self.DataCollection.blsampleid).label("sampleid"),
                    func.max(self.DataCollection.blsubsampleid).label("subsampleid"),
                    func.concat(
                        f"/{self._base_url}/xrf/maps/composite/",
                        self.XFEFluorescenceComposite.xfefluorescencecompositeid,
                    ).label("url"),
                )
                .join(
                    self.XRFFluorescenceMapping,
                    self.XRFFluorescenceMapping.xrffluorescencemappingid
                    == self.XFEFluorescenceComposite.r,
                )
                .join(
                    self.XRFFluorescenceMappingROI,
                    self.XRFFluorescenceMappingROI.xrffluorescencemappingroiid
                    == self.XRFFluorescenceMapping.xrffluorescencemappingroiid,
                )
                .join(
                    xrfmap2,
                    xrfmap2.xrffluorescencemappingid == self.XFEFluorescenceComposite.g,
                )
                .join(
                    xrfmaproi2,
                    xrfmaproi2.xrffluorescencemappingroiid
                    == xrfmap2.xrffluorescencemappingroiid,
                )
                .join(
                    xrfmap3,
                    xrfmap3.xrffluorescencemappingid == self.XFEFluorescenceComposite.b,
                )
                .join(
                    xrfmaproi3,
                    xrfmaproi3.xrffluorescencemappingroiid
                    == xrfmap3.xrffluorescencemappingroiid,
                )
                .join(
                    self.GridInfo,
                    self.GridInfo.gridinfoid == self.XRFFluorescenceMapping.gridinfoid
                    or self.GridInfo.gridinfoid == xrfmap2.gridinfoid
                    or self.GridInfo.gridinfoid == xrfmap3.gridinfoid,
                )
                .join(
                    self.DataCollection,
                    self.GridInfo.datacollectionid
                    == self.DataCollection.datacollectionid,
                )
                .join(
                    self.DataCollectionGroup,
                    self.DataCollection.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
                .filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )
                .group_by(self.XFEFluorescenceComposite.xfefluorescencecompositeid)
            )

            if kwargs.get("sampleid"):
                composites = composites.filter(
                    self.DataCollection.blsampleid == kwargs.get("sampleid")
                )

            if kwargs.get("subsampleid"):
                composites = composites.filter(
                    self.DataCollection.blsubsampleid == kwargs.get("subsampleid")
                )

            if compositeid:
                composites = composites.filter(
                    self.XFEFluorescenceComposite.xfefluorescencecompositeid
                    == compositeid
                )
                composite = composites.first()
                if composite:
                    return composite._asdict()

            else:
                return [r._asdict() for r in composites.all()]

    def add_xrf_composite(self, **kwargs):
        with self.session_scope() as ses:
            opacities = {}
            for mapid in ["r", "g", "b"]:
                opacities[f"{mapid}opacity"] = kwargs.get(f"{mapid}opacity")
                m = self.get_xrf_maps(mapid=kwargs[mapid])
                if m is None:
                    return

            xrf_composite = self.XFEFluorescenceComposite(
                r=kwargs["r"], g=kwargs["g"], b=kwargs["b"], **opacities
            )

            ses.add(xrf_composite)
            ses.commit()

            return self.get_xrf_composites(
                compositeid=xrf_composite.xfefluorescencecompositeid
            )

    def update_xrf_composite(self, compositeid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_xrf_composites(
                compositeid=compositeid, no_context=kwargs.get("no_context")
            )
            if chk:
                xrf_composite = (
                    ses.query(self.XFEFluorescenceComposite)
                    .filter(
                        self.XFEFluorescenceComposite.xfefluorescencecompositeid
                        == compositeid
                    )
                    .first()
                )

                updatable = ["opacity", "ropacity", "gopacity", "bopacity"]
                for k, v in kwargs.items():
                    if k in updatable:
                        setattr(xrf_composite, k, v)

                ses.commit()

                return self.get_xrf_composites(
                    compositeid=compositeid, no_context=kwargs.get("no_context")
                )

    def remove_xrf_composite(self, compositeid):
        with self.session_scope() as ses:
            chk = self.get_xrf_composites(compositeid=compositeid)
            if chk:
                xrf_composite = (
                    ses.query(self.XFEFluorescenceComposite)
                    .filter(
                        self.XFEFluorescenceComposite.xfefluorescencecompositeid
                        == compositeid
                    )
                    .first()
                )
                ses.delete(xrf_composite)
                ses.commit()

                return True

    # #  XRF ROIs
    def get_xrf_map_rois(self, maproiid=None, **kwargs):
        with self.session_scope() as ses:
            rois = (
                ses.query(
                    self.XRFFluorescenceMappingROI.xrffluorescencemappingroiid.label(
                        "maproiid"
                    ),
                    self.XRFFluorescenceMappingROI.startenergy.label("start"),
                    self.XRFFluorescenceMappingROI.endenergy.label("end"),
                    self.XRFFluorescenceMappingROI.blsampleid.label("sampleid"),
                    func.concat(
                        self.XRFFluorescenceMappingROI.element,
                        "-",
                        self.XRFFluorescenceMappingROI.edge,
                    ).label("name"),
                    self.XRFFluorescenceMappingROI.element,
                    self.XRFFluorescenceMappingROI.edge,
                    func.count(
                        self.XRFFluorescenceMapping.xrffluorescencemappingid
                    ).label("maps"),
                )
                .outerjoin(self.XRFFluorescenceMapping)
                .group_by(self.XRFFluorescenceMappingROI.xrffluorescencemappingroiid)
            )

            if kwargs.get("sampleid"):
                rois = rois.filter(
                    self.XRFFluorescenceMappingROI.blsampleid == kwargs["sampleid"]
                )

            if not kwargs.get("no_context"):
                rois = (
                    rois.join(self.BLSample)
                    .join(self.Crystal)
                    .join(self.Protein)
                    .filter(self.Protein.proposalid == g.blsession.get("proposalid"))
                )

            if maproiid:
                rois = rois.filter(
                    self.XRFFluorescenceMappingROI.xrffluorescencemappingroiid
                    == maproiid
                )
                roi = rois.first()
                if roi:
                    return roi._asdict()

            else:
                return [r._asdict() for r in rois.all()]

    def add_xrf_map_roi(self, **kwargs):
        with self.session_scope() as ses:
            sample = (
                ses.query(self.BLSample.blsampleid)
                .join(self.Crystal)
                .join(self.Protein)
                .filter(self.Protein.proposalid == g.blsession.get("proposalid"))
                .filter(self.BLSample.blsampleid == kwargs["sampleid"])
            ).first()

            if not sample:
                return

            maproi = self.XRFFluorescenceMappingROI(
                blsampleid=sample.blsampleid,
                startenergy=kwargs["start"],
                endenergy=kwargs["end"],
                element=kwargs["element"],
                edge=kwargs["edge"],
            )

            ses.add(maproi)
            ses.commit()

            return self.get_xrf_map_rois(maproiid=maproi.xrffluorescencemappingroiid)

    def add_xrf_map_roi_scalar(self, **kwargs):
        with self.session_scope() as ses:
            maproi = self.XRFFluorescenceMappingROI(
                scalar=kwargs["scalar"], startenergy=0, endenergy=0
            )

            ses.add(maproi)
            ses.commit()

            return self.get_xrf_map_rois(
                maproiid=maproi.xrffluorescencemappingroiid, no_context=True
            )

    def update_xrf_map_roi(self, maproiid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_xrf_map_rois(maproiid=maproiid)
            if chk:
                xrf_map_roi = (
                    ses.query(self.XRFFluorescenceMappingROI)
                    .filter(
                        self.XRFFluorescenceMappingROI.xrffluorescencemappingroiid
                        == maproiid
                    )
                    .first()
                )
                updatable = {
                    "start": "startenergy",
                    "end": "endenergy",
                    "element": "element",
                    "edge": "edge",
                }
                for k, v in kwargs.items():
                    if k in updatable:
                        setattr(xrf_map_roi, updatable[k], v)

                ses.commit()

                return self.get_xrf_map_rois(maproiid=maproiid)

    def remove_xrf_map_roi(self, maproiid):
        with self.session_scope() as ses:
            chk = self.get_xrf_map_rois(maproiid=maproiid)
            if chk:
                xrf_map_roi = (
                    ses.query(self.XRFFluorescenceMappingROI)
                    .filter(
                        self.XRFFluorescenceMappingROI.xrffluorescencemappingroiid
                        == maproiid
                    )
                    .first()
                )
                ses.delete(xrf_map_roi)
                ses.commit()

                return True
