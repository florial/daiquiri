-- DataCollection
-- * Add diffractionPlanId
ALTER TABLE `DataCollection`
  ADD `diffractionPlanId` INT UNSIGNED NULL DEFAULT NULL,
  ADD CONSTRAINT `DataCollection_dataCollectionPlanId`
    FOREIGN KEY (`diffractionPlanId`)
      REFERENCES `DiffractionPlan`(`diffractionPlanId`)
        ON DELETE NO ACTION ON UPDATE NO ACTION;

-- DataCollectionFileAttachment
ALTER TABLE `DataCollectionFileAttachment` CHANGE `fileType` `fileType` ENUM('snapshot','log','xy','recip','pia','warning','params') DEFAULT NULL;

-- DataCollectionGroup
ALTER TABLE `DataCollectionGroup` CHANGE `experimentType` `experimentType` enum('SAD','SAD - Inverse Beam','OSC','Collect - Multiwedge','MAD','Helical','Multi-positional','Mesh','Burn','MAD - Inverse Beam','Characterization','Dehydration','tomo','experiment','EM','PDF','PDF+Bragg','Bragg','single particle','Serial Fixed','Serial Jet','Standard','Time Resolved','Diamond Anvil High Pressure','Custom','XRF map','Energy scan','XRF spectrum','XRF map xas') DEFAULT NULL COMMENT 'Standard: Routine structure determination experiment. Time Resolved: Investigate the change of a system over time. Custom: Special or non-standard data collection.';

-- ContainerQueueSample
ALTER TABLE `ContainerQueueSample` 
  ADD `diffractionPlanId` INT UNSIGNED NULL DEFAULT NULL,
  ADD CONSTRAINT `ContainerQueueSample_dataCollectionPlanId`
    FOREIGN KEY (`diffractionPlanId`)
      REFERENCES `DiffractionPlan`(`diffractionPlanId`)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD `blSampleId` INT(10) UNSIGNED NULL DEFAULT NULL,
  ADD CONSTRAINT `ContainerQueueSample_blSampleId`
    FOREIGN KEY (`blSampleId`)
      REFERENCES `BLSample`(`blSampleId`)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD `status` VARCHAR(20) NULL DEFAULT NULL COMMENT 'The status of the queued item, i.e. skipped, reinspect. Completed / failed should be inferred from related DataCollection';

-- RobotAction
ALTER TABLE `RobotAction` CHANGE `actionType` `actionType` ENUM('LOAD','UNLOAD','DISPOSE','STORE','WASH','ANNEAL','MOSAIC') DEFAULT NULL;

-- BLSubSample
ALTER TABLE `BLSubSample` ADD `type` VARCHAR(20) DEFAULT NULL COMMENT 'The type of subsample, i.e. roi (region), poi (point), loi (line)';

-- BLSampleImage
-- * Add offsetx, offsety
ALTER TABLE `BLSampleImage`
  ADD `offsetx` INT NOT NULL DEFAULT '0' COMMENT 'The x offset of the image relative to the canvas',
  ADD `offsety` INT NOT NULL DEFAULT '0' COMMENT 'The y offset of the image relative to the canvas';

-- ImageQualityIndicators
ALTER TABLE `ImageQualityIndicators` CHANGE `autoProcProgramId` `autoProcProgramId` INT(10) UNSIGNED NULL COMMENT 'Foreign key to the AutoProcProgram table';

-- XRFFluorescenceMapping
DROP TABLE IF EXISTS `XRFFluorescenceMapping`;
CREATE TABLE `XRFFluorescenceMapping` (
  `xrfFluorescenceMappingId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `xrfFluorescenceMappingROIId` int(11) unsigned NOT NULL,
  `gridInfoId` int(11) unsigned NOT NULL,
  `dataFormat` varchar(15) NOT NULL COMMENT 'Description of format and any compression, i.e. json+gzip for gzipped json',
  `data` LONGBLOB NOT NULL COMMENT 'The actual data',
  `points` int(11) UNSIGNED DEFAULT NULL COMMENT 'The number of points available, for realtime feedback',
  `opacity` FLOAT NOT NULL DEFAULT '1' COMMENT 'Display opacity',
  `colourMap` VARCHAR(20) DEFAULT NULL COMMENT 'Colour map for displaying the data',
  `min` INT(3) DEFAULT NULL COMMENT 'Min value in the data for histogramming',
  `max` INT(3) DEFAULT NULL COMMENT 'Max value in the data for histogramming',
  PRIMARY KEY (`xrfFluorescenceMappingId`),
  CONSTRAINT `XRFFluorescenceMapping_ibfk1`
      FOREIGN KEY (`xrfFluorescenceMappingROIId`)
          REFERENCES `XRFFluorescenceMappingROI` (`xrfFluorescenceMappingROIId`)
              ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `XRFFluorescenceMapping_ibfk2`
      FOREIGN KEY (`gridInfoId`)
          REFERENCES `GridInfo` (`gridInfoId`)
              ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- XRFFluorescenceMappingROI
ALTER TABLE `XRFFluorescenceMappingROI`
    ADD `blSampleId` INT UNSIGNED NULL COMMENT 'ROIs can be created within the context of a sample',
    ADD CONSTRAINT `XRFFluorescenceMappingROI_FKblSampleId`
      FOREIGN KEY (`blSampleId`)
          REFERENCES `BLSample`(`blSampleId`) 
              ON DELETE RESTRICT ON UPDATE RESTRICT,
    ADD `scalar` VARCHAR(50) NULL DEFAULT NULL COMMENT 'For ROIs that are not an element, i.e. could be a scan counter instead',
    CHANGE `edge` `edge` VARCHAR(15) NULL DEFAULT NULL COMMENT 'Edge type, i.e. Ka1, could also be a custom edge in case of overlap Ka1-noFe';

-- xfeFluorescenceComposite
CREATE TABLE `XFEFluorescenceComposite` ( 
    `xfeFluorescenceCompositeId` INT UNSIGNED NOT NULL AUTO_INCREMENT , 
    `r` INT UNSIGNED NOT NULL COMMENT 'Red layer', 
    `g` INT UNSIGNED NOT NULL COMMENT 'Green layer', 
    `b` INT UNSIGNED NOT NULL COMMENT 'Blue layer', 
    `rOpacity` FLOAT NOT NULL DEFAULT '1' COMMENT 'Red layer opacity',
    `bOpacity` FLOAT NOT NULL DEFAULT '1' COMMENT 'Red layer opacity',
    `gOpacity` FLOAT NOT NULL DEFAULT '1' COMMENT 'Red layer opacity',
    `opacity` FLOAT NOT NULL DEFAULT '1' COMMENT 'Total map opacity',
    PRIMARY KEY (`xfeFluorescenceCompositeId`),
    CONSTRAINT `XFEFluorescenceComposite_ibfk1`
        FOREIGN KEY (`r`)
            REFERENCES `XRFFluorescenceMapping`(`xrfFluorescenceMappingId`)
                ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `XFEFluorescenceComposite_ibfk2`
        FOREIGN KEY (`g`)
            REFERENCES `XRFFluorescenceMapping`(`xrfFluorescenceMappingId`)
                ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `XFEFluorescenceComposite_ibfk3`
        FOREIGN KEY (`b`)
            REFERENCES `XRFFluorescenceMapping`(`xrfFluorescenceMappingId`)
                ON DELETE RESTRICT ON UPDATE RESTRICT
    )
    COMMENT 'A composite XRF map composed of three XRFFluorescenceMapping entries creating r, g, b layers'
    ENGINE = InnoDB;

-- Single Motor Position
-- To be captured with a BLSample or BLSubSample
CREATE TABLE `Positioner` ( 
    `positionerId` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
    `positioner` VARCHAR(50) NOT NULL, 
    `value` FLOAT NOT NULL, -- debatable, varchar?
    PRIMARY KEY (`positionerId`)
    ) 
    COMMENT 'An arbitrary positioner and its value, could be e.g. a motor. Allows for instance to store some positions with a sample or subsample'
    ENGINE = InnoDB;

CREATE TABLE `BLSample_has_Positioner` (
    `blSampleHasPositioner` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
    `blSampleId` INT UNSIGNED NOT NULL, 
    `positionerId` INT UNSIGNED NOT NULL, 
    PRIMARY KEY (`blSampleHasPositioner`),
    CONSTRAINT `BLSampleHasPositioner_ibfk1`
        FOREIGN KEY (`blSampleId`)
            REFERENCES `BLSample`(`blSampleId`)
                ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `BLSampleHasPositioner_ibfk2`
        FOREIGN KEY (`positionerId`)
            REFERENCES `Positioner`(`positionerId`)
    ) ENGINE = InnoDB;

CREATE TABLE `BLSubSample_has_Positioner` (
    `blSubSampleHasPositioner` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
    `blSubSampleId` INT UNSIGNED NOT NULL, 
    `positionerId` INT UNSIGNED NOT NULL, 
    PRIMARY KEY (`blSubSampleHasPositioner`),
    CONSTRAINT `BLSubSampleHasPositioner_ibfk1`
        FOREIGN KEY (`blSubSampleId`)
            REFERENCES `BLSubSample`(`blSubSampleId`)
                ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `BLSubSampleHasPositioner_ibfk2`
        FOREIGN KEY (`positionerId`)
            REFERENCES `Positioner`(`positionerId`)
                ON DELETE RESTRICT ON UPDATE RESTRICT
    ) ENGINE = InnoDB;
