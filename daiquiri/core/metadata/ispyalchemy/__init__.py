# -*- coding: utf-8 -*-
from contextlib import contextmanager
from datetime import datetime, timedelta

from flask import g

import sqlalchemy

# from sqlalchemy import orm, event, func, distinct, and_
from sqlalchemy import orm, event, func, and_
from sqlalchemy.schema import Table, MetaData
from sqlalchemy.ext.declarative import declarative_base

from marshmallow import fields, Schema, EXCLUDE
from daiquiri.core.metadata import MetaDataHandler
from daiquiri.core.metadata.user import User
from daiquiri.core.metadata.ispyalchemy.sample import SampleHandler
from daiquiri.core.metadata.ispyalchemy.dc import DCHandler
from daiquiri.core.metadata.ispyalchemy.xrf import XRFHandler
from daiquiri.core.metadata.ispyalchemy.autoproc import AutoProcHandler


class ISPyBConfigSchema(Schema):
    meta_url = fields.Str()
    meta_user = fields.Str()
    meta_password = fields.Str()
    meta_beamline = fields.Str()
    meta_staff = fields.Str()
    meta_charset = fields.Str()


@event.listens_for(Table, "column_reflect")
def column_reflect(inspector, table, column_info):
    column_info["key"] = column_info["name"].lower()

    # TODO: Fix for position table with virtual column
    # from sqlalchemy.schema import FetchedValue
    #  reflect does know these should be default = FetchedValue()
    # if table.name == "Position":
    #     if column_info["name"] in ["X", "Y", "Z"]:
    #         column_info["default"] = FetchedValue()


class IspyalchemyMetaDataHandler(MetaDataHandler):
    def __init__(self, *args, **kwargs):
        self._config = ISPyBConfigSchema().load(
            kwargs.get("config", {}), unknown=EXCLUDE
        )
        super().__init__(*args, **kwargs)

    def setup(self):
        char = ""
        if self._config.get("meta_charset"):
            char = f"?charset={self._config['meta_charset']}"

        self._engine = sqlalchemy.create_engine(
            f"mysql+mysqlconnector://{self._config['meta_user']}:{self._config['meta_pass']}@{self._config['meta_url']}{char}",
            # Blobs get decoded as str without this resulting in TypeError: string argument without an encoding
            # https://stackoverflow.com/a/53468522
            connect_args={"use_pure": True},
            isolation_level="READ UNCOMMITTED",
            # https://docs.sqlalchemy.org/en/13/core/pooling.html#dealing-with-disconnects
            pool_pre_ping=True,
            pool_recycle=3600,
            # pooling
            # https://docs.sqlalchemy.org/en/13/errors.html#error-3o7r
            # maybe consider https://docs.sqlalchemy.org/en/13/core/pooling.html#sqlalchemy.pool.NullPool ?
            pool_size=self._config.get("meta_pool", 10),
            max_overflow=self._config.get("meta_overflow", 20),
        )

        self._connection = self._engine.connect()
        self._metadata = MetaData(bind=self._engine)

        self._session_maker = orm.sessionmaker()
        self._session_maker.configure(bind=self._engine)

        self._tables = {}

        for t in [
            "Person",
            "Permission",
            "UserGroup",
            "UserGroup_has_Person",
            "UserGroup_has_Permission",
            "Proposal",
            "BLSession",
            "Session_has_Person",
            "DataCollectionGroup",
            "DataCollection",
            "DataCollectionFileAttachment",
            "DataCollectionComment",
            "GridInfo",
            "Protein",
            "Crystal",
            "Shipping",
            "Dewar",
            "Container",
            "ContainerHistory",
            "ContainerQueue",
            "ContainerQueueSample",
            "BLSample",
            "BLSubSample",
            "Position",
            "Positioner",
            "BLSubSample_has_Positioner",
            "DiffractionPlan",
            "BLSampleImage",
            "ContainerInspection",
            "XRFFluorescenceMapping",
            "XRFFluorescenceMappingROI",
            "XFEFluorescenceComposite",
            "RobotAction",
            "ImageQualityIndicators",
            "AutoProcProgram",
            "AutoProcProgramAttachment",
            "AutoProcProgramMessage",
            "ProcessingJob",
            "ProcessingJobParameter",
        ]:
            table = type(
                t,
                (declarative_base(),),
                {"__table__": Table(t, self._metadata, autoload=True)},
            )

            self._tables[t] = table
            setattr(self, t, table)

        self._handlers = []
        for cls in [SampleHandler, DCHandler, XRFHandler, AutoProcHandler]:
            handler = cls(
                tables=self._tables,
                session_scope=self.session_scope,
                config=self._config,
            )
            self._handlers.append(handler)

            for m in handler.exported:
                setattr(self, m, getattr(handler, m))

        for m in SampleHandler.exported:
            setattr(DCHandler, m, getattr(SampleHandler, m))

        super().setup()

    @contextmanager
    def session_scope(self):
        session = self._session_maker()
        try:
            yield session
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

    def _row_to_dict(self, row):
        d = {}
        for column in row.__table__.columns:
            d[column.key] = getattr(row, column.key)

        return d

    def get_user(self, **kwargs):
        with self.session_scope() as ses:
            p = (
                ses.query(
                    self.Person.givenname,
                    self.Person.familyname,
                    self.Person.personid,
                    func.concat(
                        self.Person.givenname, " ", self.Person.familyname
                    ).label("fullname"),
                    func.group_concat(self.Permission.type).label("permissions"),
                )
                .outerjoin(
                    self.UserGroup_has_Person,
                    # self.UserGroup_has_Person.personid == self.Person.personid,
                )
                .outerjoin(
                    self.UserGroup_has_Permission,
                    self.UserGroup_has_Permission.usergroupid
                    == self.UserGroup_has_Person.usergroupid,
                )
                .outerjoin(
                    self.Permission,
                    # self.Permission.permissionid
                    # == self.UserGroup_has_Permission.permissionid,
                )
                .filter(self.Person.login == g.login)
                .group_by(self.Person.personid)
                .first()
            )

            if not p:
                return None

            dct = p._asdict()
            dct["permissions"] = (
                dct["permissions"].split(",") if dct["permissions"] else []
            )
            dct["is_staff"] = self._config["meta_staff"] in dct["permissions"]

            return User(**dct)

    def verify_session(self, session):
        if not session:
            return None

        return self.get_sessions(session=session)

    def get_sessions(self, session=None, **kwargs):
        grace = 1

        with self.session_scope() as ses:
            sessions = (
                ses.query(
                    func.concat(
                        self.Proposal.proposalcode,
                        self.Proposal.proposalnumber,
                        "-",
                        self.BLSession.visit_number,
                    ).label("session"),
                    func.concat(
                        self.Proposal.proposalcode, self.Proposal.proposalnumber
                    ).label("proposal"),
                    self.Proposal.proposalid,
                    self.BLSession.sessionid,
                    self.BLSession.startdate,
                    self.BLSession.enddate,
                    self.BLSession.beamlinename,
                )
                .join(
                    self.Proposal, self.Proposal.proposalid == self.BLSession.proposalid
                )
                .filter(self.BLSession.beamlinename == self._config["meta_beamline"])
                .filter(
                    and_(
                        self.BLSession.startdate
                        <= (datetime.now() + timedelta(hours=grace)),
                        self.BLSession.enddate
                        >= (datetime.now() - timedelta(hours=grace)),
                    )
                )
            )

            if not kwargs.get("no_context"):
                if not g.user.staff():
                    sessions = sessions.join(
                        self.Session_has_Person,
                        self.Session_has_Person.sessionid == self.BLSession.sessionid,
                    )
                    sessions = sessions.filter(
                        self.Session_has_Person.personid == g.user["personid"]
                    )

            sessions = sessions.group_by(self.BLSession.sessionid)

            if session or kwargs.get("sessionid"):
                if session:
                    sessions = sessions.filter(
                        func.concat(
                            self.Proposal.proposalcode,
                            self.Proposal.proposalnumber,
                            "-",
                            self.BLSession.visit_number,
                        )
                        == session
                    )
                else:
                    sessions = sessions.filter(
                        self.BLSession.sessionid == kwargs["sessionid"]
                    )

                session = sessions.first()
                if session:
                    return session._asdict()

            else:
                return [r._asdict() for r in sessions.all()]
