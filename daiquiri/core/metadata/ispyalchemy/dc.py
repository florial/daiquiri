# -*- coding: utf-8 -*-
import os

from flask import g

import sqlalchemy
from sqlalchemy import func
from sqlalchemy.sql.expression import cast

from daiquiri.core.metadata.ispyalchemy.handler import IspyalchemyHandler
from daiquiri.core.metadata.ispyalchemy.utils import page, order


class DCHandler(IspyalchemyHandler):
    exported = [
        "get_datacollections",
        "add_datacollection",
        "update_datacollection",
        "get_datacollection_attachments",
        "add_datacollection_attachment",
        "add_sampleaction",
        "update_sampleaction",
        "get_sampleactions",
        "get_scanqualityindicators",
        "add_scanqualityindicators",
    ]

    def get_datacollections(self, datacollectionid=None, **kwargs):
        with self.session_scope() as ses:
            duration = func.time_to_sec(
                func.timediff(
                    self.DataCollection.endtime, self.DataCollection.starttime,
                )
            )
            dcid = self.DataCollection.datacollectionid
            starttime = self.DataCollection.starttime
            endtime = self.DataCollection.endtime

            if not kwargs.get("datacollectiongroupid") and not kwargs.get("ungroup"):
                duration = func.sum(duration)
                dcid = func.min(self.DataCollection.datacollectionid).label(
                    "datacollectionid"
                )
                starttime = func.min(self.DataCollection.starttime).label("starttime")
                endtime = func.max(self.DataCollection.endtime).label("endtime")

            datacollections = (
                ses.query(
                    dcid,
                    self.DataCollectionGroup.datacollectiongroupid,
                    self.DataCollection.blsampleid.label("sampleid"),
                    self.DataCollection.blsubsampleid.label("subsampleid"),
                    self.DataCollection.datacollectionnumber,
                    starttime,
                    endtime,
                    self.DataCollection.xtalsnapshotfullpath1,
                    self.DataCollection.xtalsnapshotfullpath2,
                    self.DataCollection.xtalsnapshotfullpath3,
                    self.DataCollection.xtalsnapshotfullpath4,
                    self.DataCollection.filetemplate,
                    self.DataCollection.imagedirectory,
                    duration.label("duration"),
                    self.DataCollection.runstatus,
                    self.DataCollectionGroup.experimenttype,
                    self.DataCollection.exposuretime,
                    self.DataCollection.numberofimages,
                    self.DataCollection.numberofpasses,
                    self.DataCollection.wavelength,
                    self.DataCollection.xbeam,
                    self.DataCollection.ybeam,
                    self.DataCollection.beamsizeatsamplex,
                    self.DataCollection.beamsizeatsampley,
                    cast(self.GridInfo.steps_x, sqlalchemy.Integer).label("steps_x"),
                    cast(self.GridInfo.steps_y, sqlalchemy.Integer).label("steps_y"),
                    self.GridInfo.dx_mm.label("dx_mm"),
                    self.GridInfo.dy_mm.label("dy_mm"),
                )
                .join(
                    self.DataCollectionGroup,
                    self.DataCollectionGroup.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
                .join(self.BLSession)
                .outerjoin(
                    self.GridInfo,
                    self.GridInfo.datacollectionid
                    == self.DataCollection.datacollectionid,
                )
            )

            if kwargs.get("datacollectiongroupid"):
                datacollections = datacollections.filter(
                    self.DataCollection.datacollectiongroupid
                    == kwargs["datacollectiongroupid"]
                )
            elif kwargs.get("ungroup") is not None:
                pass
            else:
                datacollections = datacollections.add_columns(
                    func.count(
                        func.distinct(self.DataCollection.datacollectionid)
                    ).label("datacollections")
                )
                datacollections = datacollections.group_by(
                    self.DataCollection.datacollectiongroupid
                )

            if not kwargs.get("no_context"):
                datacollections = datacollections.filter(
                    self.BLSession.sessionid == g.blsession.get("sessionid")
                )

                if not g.user.staff():
                    datacollections = datacollections.join(
                        self.Session_has_Person
                    ).filter(self.Session_has_Person.personid == g.user.get("personid"))

            if kwargs.get("sampleid"):
                datacollections = datacollections.filter(
                    self.DataCollection.blsampleid == kwargs.get("sampleid")
                )

            if kwargs.get("subsampleid"):
                datacollections = datacollections.filter(
                    self.DataCollection.blsubsampleid == kwargs.get("subsampleid")
                )

            if kwargs.get("status"):
                datacollections = datacollections.filter(
                    self.DataCollection.runstatus == kwargs.get("status")
                )

            if datacollectionid:
                datacollections = datacollections.filter(
                    self.DataCollection.datacollectionid == datacollectionid
                )
                datacollection = datacollections.first()
                if datacollection:
                    datacollection = datacollection._asdict()
                    return self._check_snapshots(datacollection)

            else:
                total = datacollections.count()
                datacollections = order(
                    datacollections,
                    {
                        "starttime": self.DataCollection.starttime,
                        "runstatus": self.DataCollection.runstatus,
                        "experimenttype": self.DataCollection.experimenttype,
                        "duration": duration,
                    },
                    **kwargs,
                )
                datacollections = page(datacollections, **kwargs)

                dcs = [r._asdict() for r in datacollections.all()]
                for dc in dcs:
                    dc = self._check_snapshots(dc)

                return {"total": total, "rows": dcs}

    def add_datacollection(self, **kwargs):
        with self.session_scope() as ses:
            dcg_id = kwargs.get("datacollectiongroupid")
            if dcg_id is None:
                dcg = self.DataCollectionGroup(
                    sessionid=kwargs.get("sessionid"),
                    experimenttype=kwargs.get("experimenttype"),
                    scanparameters=kwargs.get("scanparameters"),
                )

                ses.add(dcg)
                ses.commit()

                dcg_id = dcg.datacollectiongroupid

            dc = self.DataCollection(
                datacollectiongroupid=dcg_id,
                filetemplate=kwargs.get("filetemplate"),
                imagedirectory=kwargs.get("imagedirectory"),
                starttime=kwargs.get("starttime"),
                # This is set in dcg, not dc
                # experimenttype=kwargs.get("experimenttype"),
                diffractionplanid=kwargs.get("diffractionplanid"),
                blsampleid=kwargs.get("sampleid"),
                blsubsampleid=kwargs.get("subsampleid"),
                datacollectionnumber=kwargs.get("datacollectionnumber", None),
                exposuretime=kwargs.get("exposuretime", None),
                numberofimages=kwargs.get("numberofimages", None),
                xtalsnapshotfullpath1=kwargs.get("xtalsnapshotfullpath1"),
            )

            ses.add(dc)
            ses.commit()

            if kwargs.get("steps_x") or kwargs.get("steps_y"):
                grid = self.GridInfo(
                    datacollectionid=dc.datacollectionid,
                    steps_x=kwargs.get("steps_x"),
                    steps_y=kwargs.get("steps_y"),
                    dx_mm=kwargs.get("dx_mm"),
                    dy_mm=kwargs.get("dy_mm"),
                    pixelspermicronx=kwargs.get("pixelspermicronx"),
                    pixelspermicrony=kwargs.get("pixelspermicrony"),
                    snapshot_offsetxpixel=kwargs.get("snapshot_offsetxpixel"),
                    snapshot_offsetypixel=kwargs.get("snapshot_offsetypixel"),
                )

                ses.add(grid)
                ses.commit()

            return self.get_datacollections(
                datacollectionid=dc.datacollectionid, no_context=True
            )

    def update_datacollection(self, datacollectionid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_datacollections(
                datacollectionid=datacollectionid, no_context=kwargs.get("no_context")
            )
            if chk:
                dc = (
                    ses.query(self.DataCollection)
                    .filter(self.DataCollection.datacollectionid == datacollectionid)
                    .first()
                )

                updatable = [
                    "runstatus",
                    "endtime",
                    "comments",
                    "datacollectionnumber",
                    "numberofimages",
                    "numberofpasses",
                    "xtalsnapshotfullpath1",
                    "xtalsnapshotfullpath2",
                    "xtalsnapshotfullpath3",
                    "xtalsnapshotfullpath4",
                    "wavelength",
                    "transmission",
                    "xbeam",
                    "ybeam",
                    "beamsizeatsamplex",
                    "beamsizeatsampley",
                    "imageprefix",
                    "imagedirectory",
                    "imagesuffix",
                    "filetemplate",
                    "imagecontainersubpath",
                    "detectordistance",
                    "detectorid",
                    "flux",
                    "exposuretime",
                ]
                for kw in kwargs:
                    if kw in updatable:
                        setattr(dc, kw, kwargs[kw])

                gridinfo = (
                    ses.query(self.GridInfo)
                    .filter(self.GridInfo.datacollectionid == dc.datacollectionid)
                    .first()
                )

                gr_updatable = [
                    "orientation",
                    "steps_x",
                    "steps_y",
                    "dx_mm",
                    "dy_mm",
                    "pixelspermicronx",
                    "pixelspermicrony",
                    "snapshot_offsetxpixel",
                    "snapshot_offsetypixel",
                ]
                update_grid = False
                for kw in kwargs:
                    if kw in gr_updatable:
                        update_grid = True

                if update_grid:
                    if not gridinfo:
                        gridinfo = self.GridInfo(datacollectionid=dc.datacollectionid)
                        ses.add(gridinfo)
                        ses.commit()

                    for kw in kwargs:
                        if kw in gr_updatable:
                            setattr(gridinfo, kw, kwargs[kw])

                ses.commit()

                return self.get_datacollections(
                    datacollectionid=datacollectionid,
                    no_context=kwargs.get("no_context"),
                )

    def get_datacollection_attachments(self, datacollectionattachmentid=None, **kwargs):
        with self.session_scope() as ses:
            attachments = (
                ses.query(
                    self.DataCollectionFileAttachment.datacollectionfileattachmentid,
                    self.DataCollectionFileAttachment.filefullpath,
                    self.DataCollectionFileAttachment.filetype,
                )
                .join(self.DataCollection)
                .join(self.DataCollectionGroup)
            )

            if not kwargs.get("no_context"):
                attachments = attachments.filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )

            if kwargs.get("datacollectionid"):
                attachments = attachments.filter(
                    self.DataCollection.datacollectionid == kwargs["datacollectionid"]
                )

            if kwargs.get("filetype"):
                attachments = attachments.filter(
                    self.DataCollectionFileAttachment.filetype == kwargs["filetype"]
                )

            if datacollectionattachmentid:
                attachments = attachments.filter(
                    self.DataCollectionFileAttachment.datacollectionfileattachmentid
                    == datacollectionattachmentid
                )
                attachment = attachments.first()
                if attachment:
                    return self._add_file_to_attachment(attachment._asdict())

            else:
                return [
                    self._add_file_to_attachment(r._asdict()) for r in attachments.all()
                ]

    def _add_file_to_attachment(self, attachment):
        attachment["filename"] = os.path.basename(attachment["filefullpath"])
        attachment["extension"] = (
            os.path.splitext(attachment["filename"])[1][1:].strip().lower()
        )
        return attachment

    def add_datacollection_attachment(self, **kwargs):
        dc = self.get_datacollections(
            datacollectionid=kwargs.get("datacollectionid"),
            no_context=kwargs.get("no_context"),
        )

        if dc:
            with self.session_scope() as ses:
                attachment = self.DataCollectionFileAttachment(
                    datacollectionid=kwargs.get("datacollectionid"),
                    filetype=kwargs.get("filetype"),
                    filefullpath=kwargs.get("filepath"),
                )

                ses.add(attachment)
                ses.commit()

                return self.get_datacollection_attachments(
                    attachment.datacollectionfileattachmentid,
                    no_context=kwargs.get("no_context"),
                )

    def get_sampleactions(self, sampleactionid=None, **kwargs):
        with self.session_scope() as ses:
            sampleactions = ses.query(
                self.RobotAction.robotactionid.label("sampleactionid"),
                self.RobotAction.blsessionid.label("sessionid"),
                self.RobotAction.blsampleid.label("sampleid"),
                self.RobotAction.actiontype,
                self.RobotAction.starttimestamp,
                self.RobotAction.endtimestamp,
                self.RobotAction.status,
                self.RobotAction.message,
                self.RobotAction.xtalsnapshotbefore,
                self.RobotAction.xtalsnapshotafter,
            )

            if not kwargs.get("no_context"):
                sampleactions = sampleactions.filter(
                    self.RobotAction.blsessionid == g.blsession.get("sessionid")
                )

            if sampleactionid:
                sampleactions = sampleactions.filter(
                    self.RobotAction.robotactionid == sampleactionid
                )
                sampleaction = sampleactions.first()
                if sampleaction:
                    return sampleaction._asdict()

            else:
                return [r._asdict() for r in sampleactions.all()]

    def add_sampleaction(self, **kwargs):
        with self.session_scope() as ses:
            sample = self.get_samples(
                sampleid=kwargs["sampleid"], no_context=kwargs.get("no_context")
            )

            if sample:
                sampleaction = self.RobotAction(
                    blsessionid=kwargs.get("sessionid"),
                    blsampleid=kwargs.get("sampleid"),
                    actiontype=kwargs.get("actiontype"),
                    starttimestamp=kwargs.get("starttime"),
                    status=kwargs.get("status"),
                    message=kwargs.get("message"),
                    xtalsnapshotbefore=kwargs.get("xtalsnapshotbefore"),
                    xtalsnapshotafter=kwargs.get("xtalsnapshotafter"),
                )

                ses.add(sampleaction)
                ses.commit()

                return self.get_sampleactions(
                    sampleactionid=sampleaction.robotactionid,
                    no_context=kwargs.get("no_context"),
                )

    def update_sampleaction(self, sampleactionid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_sampleactions(
                sampleactionid=sampleactionid, no_context=kwargs.get("no_context")
            )
            if chk:
                sampleaction = (
                    ses.query(self.RobotAction)
                    .filter(self.RobotAction.robotactionid == sampleactionid)
                    .first()
                )

                updatable = [
                    "starttimestamp",
                    "endtimestamp",
                    "status",
                    "message",
                    "xtalsnapshotbefore",
                    "xtalsnapshotafter",
                ]
                for kw in kwargs:
                    if kw in updatable:
                        val = kwargs[kw]

                        if kw == "message":
                            val = val[:255]

                        setattr(sampleaction, kw, val)

                ses.commit()

                return self.get_sampleactions(
                    sampleactionid=sampleactionid, no_context=kwargs.get("no_context")
                )

    def get_scanqualityindicators(self, **kwargs):
        with self.session_scope() as ses:
            sqis = (
                ses.query(
                    self.ImageQualityIndicators.imagenumber.label("point"),
                    self.ImageQualityIndicators.totalintegratedsignal.label("total"),
                    self.ImageQualityIndicators.spottotal.label("spots"),
                    self.ImageQualityIndicators.autoprocprogramid,
                )
                .join(
                    self.DataCollection,
                    self.DataCollection.datacollectionid
                    == self.ImageQualityIndicators.datacollectionid,
                )
                .join(self.DataCollectionGroup)
            )

            if not kwargs.get("no_context"):
                sqis = sqis.filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )

            sqis = sqis.filter(
                self.DataCollection.datacollectionid == kwargs.get("datacollectionid")
            )

            sqsi = sqis.order_by(self.ImageQualityIndicators.imagenumber)

            data = {}
            for sqi in sqis.all():
                row = sqi._asdict()
                for k in ["point", "total", "spots", "autoprocprogramid"]:
                    if not k in data:
                        data[k] = []

                    data[k].append(row[k])

            return data

    def add_scanqualityindicators(self, **kwargs):
        with self.session_scope() as ses:
            dc = self.get_datacollections(
                datacollectionid=kwargs["datacollectionid"],
                no_context=kwargs.get("no_context"),
            )

            if dc:
                args = {}
                for key in ["total", "spots"]:
                    args[key] = None
                    if kwargs.get(key) is not None:
                        args[key] = float(kwargs[key])

                sqi = self.ImageQualityIndicators(
                    datacollectionid=kwargs["datacollectionid"],
                    imagenumber=kwargs["point"],
                    totalintegratedsignal=args["total"],
                    spottotal=args["spots"],
                )

                ses.add(sqi)
                ses.commit()

                return f"{sqi.datacollectionid}-{sqi.imagenumber}"
