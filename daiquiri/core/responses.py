#!/usr/bin/env python
# -*- coding: utf-8 -*-
from io import BytesIO
from datetime import datetime
from functools import update_wrapper, wraps
from flask import Response, make_response
import gzip
import json


def image_response(img, img_format="PNG"):
    """Create a flask response with an image

    Creates the correct content length so that an XHR request
    can monitor progress correctly

    Args:
        img (Image): A PIL image to send as a flask response

    Kwargs:
        img_format (str): The image type, default png

    Returns
        resp (Response): The flask response
    """
    img_io = BytesIO()
    img.save(img_io, img_format, quality=100)
    img_io.seek(0)

    resp = Response(response=img_io, mimetype=f"image/{img_format.lower()}", status=200)
    resp.headers.add("Content-Length", img_io.getbuffer().nbytes)

    return resp


def nocache(view):
    """A response with no-cache

    Stolen from https://arusahni.net/blog/2014/03/flask-nocache.html
    """

    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers["Last-Modified"] = datetime.now()
        response.headers[
            "Cache-Control"
        ] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "-1"
        return response

    return update_wrapper(no_cache, view)


def gzipped(data, compress_level=6):
    """A gzipped response

    Args:
        data (dict): A list of data to compress

    Kwargs:
        compress_level (int): The compression level to apply

    Returns:
        resp (Response): The flask response
    """
    gzip_buffer = BytesIO()
    gzip_file = gzip.GzipFile(
        mode="wb", compresslevel=compress_level, fileobj=gzip_buffer
    )
    gzip_file.write(json.dumps(data).encode("utf-8"))
    gzip_file.close()

    resp = Response(
        response=gzip_buffer.getvalue(), mimetype=f"application/json", status=200
    )
    resp.headers["Content-Encoding"] = "gzip"
    resp.headers["Content-Length"] = len(resp.get_data())

    return resp
