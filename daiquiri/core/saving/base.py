#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
import string
from abc import ABC, abstractmethod, abstractproperty
from glob import glob
from flask import g
from marshmallow import fields

from daiquiri.core import CoreBase, CoreResource, marshal
from daiquiri.core.utils import loader
from daiquiri.core.schema import ErrorSchema, MessageSchema
from daiquiri.core.schema.saving import SavingSchema

logger = logging.getLogger(__name__)


class Saving:
    def __init__(self, config, *args, **kwargs):
        kwargs["config"] = config

        self._saving = loader(
            "daiquiri.core.saving",
            "SavingHandler",
            config["saving_type"],
            *args,
            **kwargs,
        )

    def init(self):
        return self._saving


class SavingResource(CoreResource):
    @marshal(
        inp={
            "sessionid": fields.Int(),
            "sampleid": fields.Int(),
            "subsampleid": fields.Int(),
        },
        out=[[200, SavingSchema(), "The current saving setup"]],
    )
    def get(self, **kwargs):
        """Get the current saving configuration"""
        kwargs["sessionid"] = g.blsession.get("sessionid")
        return {"arguments": self._parent.get_schema_fields(**kwargs)}, 200

    @marshal(
        inp=SavingSchema,
        out=[
            [200, MessageSchema(), "Updated saving arguments"],
            [400, ErrorSchema(), "Could not update saving arguments"],
        ],
    )
    def patch(self, **kwargs):
        """Update the saving arguments"""
        if False:
            return {"message": "saving arguments updated"}
        else:
            return {"error": "Could not update saving arguments"}, 400


class FormatterObject:
    """Used in interpolating Saving arguments
    """

    def __init__(self, resource, srepr, info):
        self._resource = resource
        self._srepr = srepr
        self._info = info

    def __str__(self):
        return self._srepr

    def __getattr__(self, attr):
        try:
            return self._info[attr]
        except KeyError:
            raise AttributeError(
                f"{self._resource}.{attr} (available: {list(self._info.keys())}"
            ) from None


class SavingFormatter(string.Formatter):
    """Used for interpolating Saving arguments
    """

    def __init__(self, handler, actor_args, partial_evaulation=False):
        """
        :param SavingHandler handler:
        """
        super().__init__()
        self._handler = handler
        self._actor_args = actor_args
        self._partial_evaluation = partial_evaulation

    def get_value(self, key, args, kwds):
        """Order of finding the key:

         - format named arguments
         - handler config
         - actor arguments
        """
        if isinstance(key, str):
            try:
                return kwds[key]
            except KeyError:
                pass
            try:
                return self._handler._config[key]
            except KeyError:
                pass
            try:
                return self._get_from_actor_args(key)
            except KeyError:
                pass

            #  TODO: Which edge cases does this not work for?
            if self._partial_evaluation:
                return f"{{{key}}}"

        return super().get_value(key, args, kwds)

    @property
    def _metadata(self):
        return self._handler._metadata

    def _get_from_actor_args(self, key):
        """Get key from actor arguments and allow special metadata keys
        to be compounded like `{sessionid.name}`

        :param str key:
        :returns FormatterObject:
        """
        value = self._actor_args[key]
        info = None
        if key == "sessionid":
            info = self._metadata.get_sessions(sessionid=value, no_context=True)
        elif key == "sampleid":
            info = self._metadata.get_samples(sampleid=value, no_context=True)
        elif key == "subsampleid":
            info = self._metadata.get_subsamples(subsampleid=value, no_context=True)
        elif key == "datacollectionid":
            info = self._metadata.get_datacollections(
                datacollectionid=value, no_context=True
            )
        if info is None:
            info = {}
        return FormatterObject(key, str(value), info)


class SavingHandler(CoreBase):
    _require_session = True
    _require_blsession = True
    _base_url = "saving"

    _callbacks = []
    _formatter = string.Formatter()

    def setup(self):
        self.saving_arguments = self._config.get("saving_arguments", {})
        self.register_route(SavingResource, "")

    def eval_saving_arguments(
        self,
        eval_args,
        extra_saving_args=None,
        raise_on_missing=True,
        partial_evaulation=False,
    ):
        logger.debug(f"saving arguments {eval_args}")
        formatter = SavingFormatter(self, eval_args, partial_evaulation)

        all_saving_args = {**self.saving_arguments}
        if extra_saving_args:
            all_saving_args.update(extra_saving_args)

        kwargs = {}
        for name, template in all_saving_args.items():
            try:
                kwargs[name] = formatter.format(template)
            except (KeyError, AttributeError):
                if raise_on_missing:
                    raise
                kwargs[name] = template
        return kwargs

    def get_schema_fields(self, **eval_args):
        """Return the saving arguments"""
        return self.eval_saving_arguments(
            eval_args, raise_on_missing=False, partial_evaulation=True
        )

    def set_filename(self, extra_saving_args=None, **eval_args):
        """Define the file name"""
        self._set_filename(
            **self.eval_saving_arguments(eval_args, extra_saving_args=extra_saving_args)
        )
        return self.filename

    @abstractmethod
    def _set_filename(self, **eval_args):
        """Child class implementation of applying the saving arguments"""
        pass

    @abstractproperty
    def filename(self):
        """Child class implementation of returning the current filename"""
        pass

    @abstractproperty
    def create_root_path(self):
        pass

    @abstractmethod
    def create_path(self, path):
        pass

    @property
    def dirname(self):
        return os.path.dirname(self.filename)

    @property
    def basename(self):
        return os.path.basename(self.filename)
