#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
from bliss.config.static import get_config
from daiquiri.core.saving.base import SavingHandler

logger = logging.getLogger(__name__)


class Bliss_BasicSavingHandler(SavingHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.session = get_config().get(self._config["saving_session"])

    @property
    def scan_saving(self):
        return self.session.scan_saving

    def _set_filename(self, base_path=None, template=None, data_filename=None):
        self.scan_saving.base_path = base_path
        self.scan_saving.template = os.path.join(template, data_filename)
        self.scan_saving.data_filename = data_filename

    @property
    def filename(self):
        return self.scan_saving.filename

    def create_root_path(self):
        self.scan_saving.create_root_path()

    def create_path(self, path):
        self.scan_saving.create_path(path)
