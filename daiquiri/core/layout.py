#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import logging
import yaml
from flask import g
from daiquiri.core import CoreBase, CoreResource
from daiquiri.resources.utils import get_resources


logger = logging.getLogger(__name__)


class LayoutsResource(CoreResource):
    # @marshal(out=[[200, SchemasSchema(many=True), "A list of layouts"]])
    def get(self):
        """List of layouts"""
        return self._parent.list(), 200


class Layout(CoreBase):
    """The layouts handler"""

    _require_session = True
    _schemas = {}

    def setup(self):
        self.register_route(LayoutsResource, "")

    def list(self):
        """Get a list of layouts"""
        layouts = []
        for l in sorted(get_resources("layout", "*.yml")):
            with open(l, "r") as stream:
                try:
                    layout = yaml.safe_load(stream)

                    # Make acronym url safe
                    layout["acronym"] = "".join(
                        [
                            c
                            for c in layout.get("acronym", layout["name"])
                            if re.match(r"\w", c)
                        ]
                    )

                    requires = layout.get("require_staff", None)
                    if requires:
                        if hasattr(g, "user"):
                            logger.info(f"User login {g.user} require_staff:{requires}")
                            if g.user.staff():
                                layouts.append(layout)
                    else:
                        layouts.append(layout)

                except yaml.YAMLError as ex:
                    layouts.append(
                        {
                            "acronym": os.path.basename(l).replace(".yml", ""),
                            "name": os.path.basename(l),
                            "error": "Invalid YAML Syntax\n" + str(ex),
                        }
                    )

        return layouts
