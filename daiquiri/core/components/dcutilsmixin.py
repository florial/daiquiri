#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pprint
import traceback
from datetime import datetime
from contextlib import contextmanager

import logging

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter()


class DCUtilsMixin:
    """DC Utils Mixin

    Mixin class to add reusable functionality for saving data collections
        Sets up file saving
        Saves data collection params
        Saves data collection exceptions
        Add scan quality indicators
    """

    def start_datacollection(self, actor):
        """Sets the data collection number and the filename
        """
        self.create_datacollection(actor)
        self.initialize_saving(actor)

    def initialize_saving(self, actor):
        """Set the data collection file name if not already done

        :param actor ComponentActor:
        """
        if actor.get("datacollectionid"):
            if actor.get("imagedirectory") is not None:
                return
        filename = self._saving.set_filename(
            extra_saving_args=actor.saving_args, **actor.all_data
        )
        self._saving.create_root_path()
        if actor.get("datacollectionid"):
            self.update_datacollection(
                actor,
                imagedirectory=os.path.dirname(filename),
                filetemplate=os.path.basename(filename),
            )
        logger.info(
            f"Scan saving initialized ({actor.name}, {actor.uid}, file={filename})"
        )

    def create_datacollection(self, actor):
        """Create data collection when it does not exist yet

        Args:
            actor (ComponentActor): The actor
        """
        if actor.get("datacollectionid") is not None:
            return

        dc = self.next_datacollection(actor)
        actor.update(datacollectiongroupid=dc["datacollectiongroupid"])
        logger.info(
            f"Create datacollection ({actor.name}, {actor.uid}, id={actor['datacollectionid']})"
        )

    def next_datacollection(self, actor, **opts):
        """Start a new datacollection in the actor

        This is useful for actors which want to create multiple
        datacollections within a datacollectiongroup

        Args:
            actor (ComponentActor): The actor

        Kwargs:
            grid (bool): Duplicate gridinfo parameters
            data (bool): Duplicate data location parameters

        Returns
            dc (dict): The new datacollection
        """
        self.update_datacollection(
            actor, endtime=datetime.now(), runstatus="Successful"
        )

        kwargs = {
            "sessionid": actor["sessionid"],
            "diffractionplanid": actor["diffractionplanid"],
            "sampleid": actor["sampleid"],
            "subsampleid": actor.get("subsampleid"),
            "starttime": datetime.now(),
            "experimenttype": actor.metatype if actor.metatype else actor.name,
            "datacollectiongroupid": actor.get("datacollectiongroupid"),
        }

        # Data
        if opts.get("data"):
            for k in ["imagedirectory", "filetemplate"]:
                kwargs[k] = actor.get(k)

        # GridInfo
        if opts.get("grid"):
            for k in [
                "steps_x",
                "steps_y",
                "dx_mm",
                "dy_mm",
                "xtalsnapshotfullpath1",
                "pixelspermicronx",
                "pixelspermicrony",
                "snapshot_offsetxpixel",
                "snapshot_offsetypixel",
            ]:
                kwargs[k] = actor.get(k)

        dc = self._metadata.add_datacollection(**kwargs)
        actor.update(
            datacollectionid=dc["datacollectionid"],
            datacollectionnumber=None,
            endtime=None,
            runstatus=None,
        )

        return dc

    def update_datacollection(self, actor, **params):
        """Update a datacollection"""
        actor.update(**params)
        datacollectionid = actor.get("datacollectionid")
        if datacollectionid is not None:
            return self._metadata.update_datacollection(
                datacollectionid=datacollectionid, no_context=True, **params
            )

    def add_scanqualityindicators(self, actor, point, datacollectionid=None, **columns):
        """Adds a scan quality indicator for a point

        Args:
            actor(obj): The actor
            point(int): The point to associate this indicator with

        Kwargs:
            datacollectionid(int): A specific datacollectionid if required
            total(int): Total integrated signal or similar
            spots(int): No. of spots
        """
        self._metadata.add_scanqualityindicators(
            no_context=True,
            datacollectionid=datacollectionid
            if datacollectionid
            else actor["datacollectionid"],
            point=point,
            **columns,
        )

    @contextmanager
    def _open_dc_attachment(self, actor, filetype, suffix="", ext="log"):
        dcid = actor["datacollectionid"]
        directory = actor["imagedirectory"]
        filename = os.extsep.join([f"{dcid}{suffix}", ext])
        filepath = os.path.join(directory, filename)
        with open(filepath, "w") as lf:
            try:
                yield lf
            finally:
                self._metadata.add_datacollection_attachment(
                    no_context=True,
                    datacollectionid=dcid,
                    filetype=filetype,
                    filepath=filepath,
                )

    def _save_dc_params(self, actor):
        """Save the initial parameters of an actor's datacollection"""
        with self._open_dc_attachment(
            actor, filetype="params", suffix="_args", ext="json"
        ) as lf:
            lf.write(actor.all_data_json_serialized)

    def _save_dc_exception(self, actor, exception):
        """Save a exception traceback of an actor's datacollection"""
        with self._open_dc_attachment(
            actor, filetype="log", suffix="_error", ext="log"
        ) as lf:
            s = pprint.pformat(actor.initkwargs)
            lf.write(f"Actor Arguments:\n{s}\n\n")
            s = pprint.pformat(actor.data)
            lf.write(f"Actor Data:\n{s}\n\n")
            lf.write(f"Exception:\n")
            traceback.print_tb(exception.__traceback__, None, lf)
            lf.write(f"\n{exception.__class__.__name__}: {str(exception)}\n")
