#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import gevent
from flask import g
from marshmallow import fields, ValidationError

from daiquiri.core import marshal, require_control
from daiquiri.core.logging import log
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.hardware import (
    HardwareObjectBaseSchema,
    HardwareGroupSchema,
    HardwareTypeSchema,
    SetObjectProperty,
    CallObjectFunction,
)

import logging

logger = logging.getLogger(__name__)


class HardwaresResource(ComponentResource):
    @marshal(
        inp={
            "type": fields.Str(description="Filter by a specific type"),
            "group": fields.Str(description="Filter by a specific group name"),
        },
        out=[[200, HardwareObjectBaseSchema(many=True), "List of object statuses"]],
    )
    def get(self, **kwargs):
        """Get a list of all hardware statuses"""
        return (
            self._parent.get_objects(
                group=kwargs.get("group", None), type=kwargs.get("type", None)
            ),
            200,
        )


class HardwareResource(ComponentResource):
    @marshal(
        out=[
            [200, HardwareObjectBaseSchema(), "A single object status"],
            [404, ErrorSchema(), "Object not found"],
        ]
    )
    def get(self, id, **kwargs):
        """Get the status of a particular hardware object"""
        obj = self._parent.get_object(id)
        if obj:
            return obj.state(), 200
        else:
            return {"error": "No such object"}, 404

    @require_control
    @marshal(
        inp=SetObjectProperty,
        out=[
            [200, SetObjectProperty(), "Object property updated"],
            [400, ErrorSchema(), "Could not set property"],
            [422, ErrorSchema(), "Invalid value"],
            [404, ErrorSchema(), "Object not found"],
        ],
    )
    def put(self, id, **kwargs):
        """Update a property on a hardware object"""
        obj = self._parent.get_object(id)
        if obj:
            try:
                obj.set(kwargs["property"], kwargs["value"])
                log.get("user").info(
                    f"Requested property {kwargs['property']} change to {kwargs['value']} for {obj.id()}",
                    type="hardware",
                )
                return {"property": kwargs["property"], "value": kwargs["value"]}, 200
            except ValidationError as e:
                return {"error": str(e)}, 422
            # To catch gevent.Timeout as well
            except BaseException as e:
                log.get("user").exception(
                    f"Could not change property {kwargs['property']}: {str(e)} for {obj.id()}",
                    type="hardware",
                )
                return {"error": str(e)}, 400
        else:
            return {"error": "No such object"}, 404

    @require_control
    @marshal(
        inp=CallObjectFunction,
        out=[
            [200, CallObjectFunction(), "Object function called"],
            [400, ErrorSchema(), "Could not call function"],
            [422, ErrorSchema(), "Invalid value"],
            [404, ErrorSchema(), "Object not found"],
        ],
    )
    def post(self, id, **kwargs):
        """Call a function on a hardware object"""
        obj = self._parent.get_object(id)
        if obj:
            try:
                resp = obj.call(kwargs["function"], kwargs.get("value", None))
                log.get("user").info(
                    f"Requested function {kwargs['function']} with value {kwargs.get('value', None)} for {obj.id()}",
                    type="hardware",
                )
                return {"function": kwargs["function"], "response": resp}, 200
            except ValidationError as e:
                return {"error": str(e)}, 422
            # To catch gevent.Timeout as well
            except BaseException as e:
                log.get("user").exception(
                    f"Could not call function {kwargs['function']}: {e} for {obj.id()}",
                    type="hardware",
                )
                return {"error": "Could not call function: {err}".format(err=e)}, 400
        else:
            return {"error": "No such object"}, 404


class HardwareGroupsResource(ComponentResource):
    @marshal(out=[[200, HardwareGroupSchema(many=True), "List of hardware groups"]])
    def get(self):
        """Get a list of the hardware object groups"""
        return self._parent.get_groups(), 200


class HardwareTypesResource(ComponentResource):
    @marshal(out=[[200, HardwareTypeSchema(many=True), "List of hardware types"]])
    def get(self):
        """Get a list of the different hardware types in use"""
        return self._parent.get_types(), 200


class Hardware(Component):
    """The Hardware Feauture

    This makes all loaded hardware available via flask resources, properties can be changed
    via put, and functions called via post requests.

    Hardware changes are notified via socketio events

    """

    _config_export = ["monitor"]
    _last_value = {}

    def subscribe(self):
        for o in self._hardware.get_objects():
            o.subscribe("all", self._obj_param_change)
            o.subscribe_online(self._obj_online_change)

    def setup(self):
        self._emit_timeout = {}
        self._last_emit_time = {}
        self._group_status_timeout = {}
        self._last_group_emit = {}
        self.subscribe()

        self.register_route(HardwaresResource, "")
        self.register_route(HardwareResource, "/<string:id>")
        self.register_route(HardwareGroupsResource, "/groups")
        self.register_route(HardwareTypesResource, "/types")

    def reload(self):
        self.subscribe()

    def _ensure_last_val(self, obj, prop):
        """Ensure a last value

        Args:
            obj (str): The object id
            prop (str): The property
        
        Ensure a last value for obj/prop
        """
        if obj not in self._last_value:
            self._last_value[obj] = {}
        if obj not in self._emit_timeout:
            self._emit_timeout[obj] = {}
        if obj not in self._last_emit_time:
            self._last_emit_time[obj] = {}

        if prop not in self._last_value[obj]:
            self._last_value[obj][prop] = None
        if prop not in self._emit_timeout[obj]:
            self._emit_timeout[obj][prop] = None
        if prop not in self._last_emit_time[obj]:
            self._last_emit_time[obj][prop] = 0

        if obj not in self._last_group_emit:
            self._last_group_emit[obj] = 0
        if obj not in self._group_status_timeout:
            self._group_status_timeout[obj] = None

    def _obj_online_change(self, obj, value):
        """Hardware online callback

        Emits socketio event on online change
        """
        self._ensure_last_val(obj.id(), "online")

        if self._last_value[obj.id()]["online"] != value:
            self.emit("online", {"id": obj.id(), "state": value})
            self._queue_update_group_status(obj.id())
        else:
            logger.debug(f"Duplicate update for {obj.id()}, online, {value}")

    def _obj_param_change(self, obj, prop, value):
        """Hardware parameter change callback

        Emits a socketio event on parameter change

        Args:
            obj (obj): The hardware object whos parameter changed
            prop (str): The property that changed
            value (mixed): The new value
        """
        self._ensure_last_val(obj.id(), prop)

        if self._last_value[obj.id()][prop] != value:
            self._queue_emit_value(obj, prop, value)
            self._last_value[obj.id()][prop] = value

        else:
            logger.debug(f"Duplicate update for {obj.id()}, {prop}, {value}")

    def _queue_emit_value(self, obj, prop, value):
        if self._emit_timeout[obj.id()][prop] is not None:
            self._emit_timeout[obj.id()][prop].kill()
            self._emit_timeout[obj.id()][prop] = None

        now = time.time()
        if now - self._last_emit_time[obj.id()][prop] > 0.2:
            self._emit_value(obj, prop, value)
        else:
            self._emit_timeout[obj.id()][prop] = gevent.spawn_later(
                0.2, self._emit_value, obj, prop, value
            )

    def _emit_value(self, obj, prop, value):
        data = {}
        data[prop] = value
        self.emit("change", {"id": obj.id(), "data": data})
        self._last_emit_time[obj.id()][prop] = time.time()

        if prop == "state":
            self._queue_update_group_status(obj.id())

    def _queue_update_group_status(self, objectid):
        if self._group_status_timeout[objectid] is not None:
            self._group_status_timeout[objectid].kill()
            self._group_status_timeout[objectid] = None

        now = time.time()
        if now - self._last_group_emit[objectid] > 2:
            self.update_group_status(objectid)
        else:
            self._group_status_timeout[objectid] = gevent.spawn_later(
                2, self.update_group_status, objectid
            )

    def update_group_status(self, objectid):
        """
        Emits a socketio event for the group(s) related to objectid
        """
        for group in self._hardware.get_groups(objectid=objectid):
            self._ensure_last_val("group", group["groupid"])

            if self._last_value["group"][group["groupid"]] != group["state"]:
                self.emit(
                    "group",
                    {"groupid": group["groupid"], "data": {"state": group["state"]}},
                )
                self._last_value["group"][group["groupid"]] = group["state"]
            else:
                logger.debug(
                    f"Duplicate update for group {group['groupid']}, {group['state']}"
                )

            self._last_group_emit[objectid] = time.time()

    def get_objects(self, *args, **kwargs):
        """Get a list of all hardware object states"""
        return [o.state() for o in self._hardware.get_objects(*args, **kwargs)]

    def get_object(self, objectid):
        """Get a specific object

        Args:
            objectid (str): The objects id

        Returns:
            object (obj): The hardware object
        """
        obj = self._hardware.get_object(objectid)
        if obj:
            if (obj.require_staff() and g.user.staff()) or not obj.require_staff():
                return obj

    def get_groups(self):
        """Get a list of the hardware groups"""
        return self._hardware.get_groups()

    def get_types(self):
        """Get a list of the hardware types and their schemas"""
        return [{"type": k, "schema": s} for k, s in self._hardware.get_types().items()]
