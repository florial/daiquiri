#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core import require_control
from daiquiri.core.components import Component, ComponentResource, actor

import logging

logger = logging.getLogger(__name__)


class ScanResource(ComponentResource):
    @require_control
    @actor("scan", enqueue=True)
    def post(self, *args, **kwargs):
        return kwargs


class Simplescan(Component):
    _actors = ["scan"]

    def setup(self):
        self.register_actor_route(ScanResource, "")
