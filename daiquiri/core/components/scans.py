#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from marshmallow import fields
from PIL import Image
import numpy

from daiquiri.core import marshal
from daiquiri.core.logging import log
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema.components.scan import (
    ScanSchema,
    ScanStatusSchema,
    ScanDataSchema,
    ScanSpectraSchema,
)
from daiquiri.core.responses import image_response, gzipped
from daiquiri.core.utils import make_json_safe, worker

import logging

logger = logging.getLogger(__name__)


class ScansResource(ComponentResource):
    @marshal(out=[[200, paginated(ScanSchema), "List of scans info"]], paged=True)
    def get(self, **kwargs):
        """Get a list of all scans and their info"""
        return self._parent.get_scans(**kwargs), 200


class ScanStatusResource(ComponentResource):
    @marshal(out=[[200, ScanStatusSchema(), "Scan status"]],)
    def get(self, **kwargs):
        """Get current scan status
        
        i.e. if there is a running scan
        """
        return self._parent.get_scan_status()


class ScanResource(ComponentResource):
    @marshal(
        out=[
            [200, ScanSchema(), "List of scans"],
            [404, ErrorSchema(), "Scan not found"],
        ]
    )
    def get(self, scanid, **kwargs):
        """Get info for a specific scan"""
        scan = self._parent.get_scans(scanid=scanid)
        if scan:
            return scan, 200
        else:
            return {"error": "No such scan"}, 404


class ScanDataResource(ComponentResource):
    @marshal(
        inp={"scalars": fields.List(fields.Str())},
        out=[
            [200, ScanDataSchema(), "Scan data"],
            [404, ErrorSchema(), "Scan not found"],
        ],
        paged=True,
    )
    def get(self, scanid, **kwargs):
        """Get the data for a specific scan"""
        scan = self._parent.get_scan_data(scanid=scanid, **kwargs)
        if scan:

            def gzip():
                return gzipped(scan)

            return worker(gzip)
        else:
            return {"error": "No such scan"}, 404


class ScanSpectraResource(ComponentResource):
    @marshal(
        inp={
            "point": fields.Int(
                required=True, description="The point to return a spectrum for"
            )
        },
        out=[
            [200, ScanSpectraSchema(), "Scan spectra"],
            [404, ErrorSchema(), "Scan not found"],
        ],
    )
    def get(self, scanid, **kwargs):
        """Get the spectra for a specific scan"""
        spectra = self._parent.get_scan_spectra(scanid=scanid, point=kwargs["point"])
        if spectra is not None:
            return spectra, 200
        else:
            return {"error": "No such scan"}, 404


class ScanImageResource(ComponentResource):
    @marshal(
        inp={
            "node_name": fields.Str(
                description="The scan node name to get images from"
            ),
            "image_no": fields.Int(description="The image number to load"),
            "raw": fields.Bool(
                description="Return the raw data rather than an image (gzipped)"
            ),
        },
        out=[
            # [200, ScanDataSchema(), 'Scan data'],
            [404, ErrorSchema(), "Scan not found"]
        ],
    )
    def get(self, scanid, **kwargs):
        """Get the image for a specific scan"""
        arr = self._parent.get_scan_image(
            scanid=scanid, node_name=kwargs["node_name"], image_no=kwargs["image_no"]
        )
        if arr is not None:
            if kwargs.get("raw"):

                def gzip():
                    flat = arr.flatten()
                    return gzipped(
                        make_json_safe(
                            {
                                "domain": [numpy.amin(flat), numpy.amax(flat)],
                                "shape": arr.shape,
                                "data": flat,
                            }
                        )
                    )

                return worker(gzip)

            else:

                def generate():
                    im = Image.fromarray(arr)
                    if im.mode != "RGB":
                        im = im.convert("RGB")

                    return image_response(im)

                return worker(generate)

        else:
            return {"error": "No such image"}, 404


class Scans(Component):
    """Scan Component
    
    The scan component loads a scan source as defined in scans.yml, and then provides
    access to this data via a flask resource.

    It also subscribes to the scan sources new scan, new data, and scan end watchers,
    and emits these changes via socketio
    """

    _scan_sources = []
    _scan_status = {"scanid": None, "progress": 0}
    _config_export = ["mca"]

    def setup(self, *args, **kwargs):
        self._last_new_data = 0

        for source in self._config["sources"]:
            if source["type"] == "bliss":
                from daiquiri.core.hardware.bliss.scans import BlissScans

                src = BlissScans(source["session"], self._config)
                src.watch_new_scan(self._new_scan)
                src.watch_end_scan(self._end_scan)
                src.watch_new_data(self._new_data)

                self._scan_sources.append(src)

                logger.debug("Registered Bliss scan source")

        self.register_route(ScansResource, "")
        self.register_route(ScanStatusResource, "/status")
        self.register_route(ScanResource, "/<int:scanid>")
        self.register_route(ScanDataResource, "/data/<int:scanid>")
        self.register_route(ScanSpectraResource, "/spectra/<int:scanid>")
        self.register_route(ScanImageResource, "/image/<int:scanid>")

    def get_scans(self, scanid=None, **kwargs):
        """Get scans from a scan source

        Args:
            scanid (int): A specific scanif to return

        Returns:
            scans (dict): A dict of total => number of scans, scans => paginated list of scan infos if scanid is None
            scan (dict): A scan dict if scanid is not None
        """
        scans = {}
        if len(self._scan_sources):
            scans = self._scan_sources[0].get_scans(scanid=scanid, **kwargs)

        return scans

    def get_scan_data(self, scanid, **kwargs):
        """Get the data for a scan

        Args:
            scanid (int): The scanid

        Returns:
            data (dict): Returns
        """
        scan = {}
        if len(self._scan_sources):
            scan = self._scan_sources[0].get_scan_data(scanid=scanid, **kwargs)

        return scan

    def get_scan_spectra(self, scanid, point=0, allpoints=False):
        """Get the data for a scan

        Args:
            scanid (int): The scanid
            point (int): The point to return
            allpoints (bool): Return all available points

        Returns:
            data (dict): Returns
        """
        scan = {}
        if len(self._scan_sources):
            scan = self._scan_sources[0].get_scan_spectra(
                scanid=scanid, point=point, allpoints=allpoints
            )

        return scan

    def get_scan_image(self, scanid, node_name, image_no):
        """Get the data for a scan

        Args:
            scanid (int): The scanid
            node_name (str): The node to return data for
            image_no (int): The image to return

        Returns:
            data (dict): Returns
        """
        image = None
        if len(self._scan_sources):
            image = self._scan_sources[0].get_scan_image(
                scanid=scanid, node_name=node_name, image_no=image_no
            )

        return image

    def get_scan_status(self):
        return self._scan_status

    def _new_scan(self, scanid, type, title):
        self._scan_status["scanid"] = scanid

        log.get("user").info(
            f"New scan started with id {scanid} and title {title}", type="scan"
        )
        self.emit(
            "new_scan", {"scanid": scanid, "type": type, "title": title},
        )

    def _end_scan(self, scanid):
        if self._scan_status["scanid"] == scanid:
            self._scan_status["scanid"] = None

        log.get("user").info(f"Scan {scanid} finished", type="scan")
        self.emit("end_scan", {"scanid": scanid})

    def _new_data(self, scanid, channel, progress):
        if self._scan_status["scanid"] == scanid:
            self._scan_status["progress"] = progress

        now = time.time()
        if (now - self._last_new_data) < 1:
            return

        self._last_new_data = now

        # logger.info(f"_new_data debounced {scanid} {channel}")
        self.emit(
            "new_data", {"scanid": scanid, "channel": channel, "progress": progress},
        )
