#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import os
import pprint
from datetime import datetime
import gevent
from marshmallow import fields
from flask import g
from PIL import Image

from daiquiri.core import marshal, require_control
from daiquiri.core.logging import log
from daiquiri.core.components import (
    Component,
    ComponentResource,
    actor,
    ComponentActorKilled,
)
from daiquiri.core.schema import ErrorSchema, MessageSchema
from daiquiri.core.schema.components.imageviewer import (
    ImageSource,
    MapAdditionalSchema,
)
from daiquiri.core.components.dcutilsmixin import DCUtilsMixin
from daiquiri.core.components.imageviewer.source import Source
from daiquiri.core.components.imageviewer.annotate import AnnotateImage

import logging

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter()


class SourcesResource(ComponentResource):
    @marshal(out=[[200, ImageSource(many=True), "A list of image/video sources"]])
    def get(self, **kwargs):
        """ Get a list of image sources defined in the current 2d viewer """
        return self._parent.get_sources()


class SourceImageResource(ComponentResource):
    @marshal(
        inp={
            "sampleid": fields.Int(description="The sampleid", required=True),
            "subsampleid": fields.Int(description="Optionally a subsampleid"),
        },
        out=[
            [200, MessageSchema(), "Source image created"],
            [400, ErrorSchema(), "Could not create source image"],
        ],
    )
    def post(self, **kwargs):
        """Capture an image from a source"""
        try:
            ret = self._parent.save_sample_image(**kwargs)
            if ret:
                return {"message": "Source image created"}, 200
            else:
                return {"error": "Could not create source image"}, 400

        except Exception as e:
            logger.exception("Could not save image")
            log.get("user").exception("Could not save source image", type="hardware")
            return {"error": f"Could not create source image: {str(e)}"}, 400


class GenerateMapsResource(ComponentResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Maps generated"],
            [400, ErrorSchema(), "Could not create maps"],
        ]
    )
    def post(self, subsampleid, **kwargs):
        """ Generate a new map for a subsampleid """
        maps = self._parent.generate_maps(
            subsampleid, datacollectionid=kwargs.get("datacollectionid", None)
        )
        if maps:
            return {"message": "Maps created"}, 200
        else:
            return {"error": "Could not create maps"}, 400


class CreateMapAdditionalResource(ComponentResource):
    @marshal(
        inp=MapAdditionalSchema,
        out=[
            [200, MessageSchema(), "Map generated"],
            [400, ErrorSchema(), "Could not create map"],
        ],
    )
    def post(self, **kwargs):
        """ Generate a new map from additional scalars """
        amap = self._parent.generate_additional_map(**kwargs)
        if amap:
            return {"message": "Map created"}, 200
        else:
            return {"error": "Could not create additional map"}, 400


class MoveResource(ComponentResource):
    @require_control
    @marshal(
        inp={
            "x": fields.Float(required=True, title="X Position"),
            "y": fields.Float(required=True, title="Y Position"),
        }
    )
    def post(self, **kwargs):
        """Move the cursor position to the current origin marking position"""
        self._parent.move(kwargs)
        return {"message": "ok"}, 200


class MoveToResource(ComponentResource):
    @require_control
    def post(self, subsampleid, **kwargs):
        """Move the specified subsample to the current origin marking position"""
        self._parent.move_to(subsampleid)
        return {"message": "ok"}, 200


class MosaicResource(ComponentResource):
    @require_control
    @actor("mosaic", enqueue=False, preprocess=True)
    def post(self, **kwargs):
        """Create a tiled mosaic actor"""
        kwargs["absol"] = self._parent.get_absolute_fp(
            {"x": kwargs["x1"], "y": kwargs["y1"]},
            {"x": kwargs["x2"], "y": kwargs["y2"]},
        )

        sample = self._metadata.get_samples(sampleid=kwargs["sampleid"])
        if not sample:
            raise AttributeError(f"No such sample {kwargs['sampleid']}")

        sessionid = g.blsession.get("sessionid")

        def save_image(x, y):
            return self._parent.save_image(
                sessionid=sessionid,
                sampleid=kwargs["sampleid"],
                file_prefix=f"mosaic_{x}_{y}_",
            )

        kwargs["sessionid"] = sessionid
        kwargs["save"] = save_image

        return kwargs


class UploadImage(ComponentResource):
    @marshal(
        inp={"image": fields.Str(requred=True, description="Base64 encoded image")},
        out=[
            [200, MessageSchema(), "Image uploaded"],
            [400, ErrorSchema(), "Could not upload image"],
        ],
    )
    def post(self, **kwargs):
        """Upload an image and send it to an actor"""
        success = self._parent.upload_image(**kwargs)
        if success:
            return {"message": "Image uploaded"}, 200
        else:
            return {"error": "Could not upload image"}, 400


class Imageviewer(Component, DCUtilsMixin):
    _actors = ["createmap", "mosaic", "move", "upload_canvas"]
    _config_export = ["options", "scantypes", "upload_canvas"]

    def setup(self):
        self._scan_actors = []
        self._map_actors = []
        self._in_generate = False

        self.register_route(SourcesResource, "/sources")
        self.register_route(SourceImageResource, "/sources/image")
        self.register_route(GenerateMapsResource, "/maps/generate/<int:subsampleid>")
        self.register_route(CreateMapAdditionalResource, "/maps/additional")
        self.register_route(MoveResource, "/move")
        self.register_route(MoveToResource, "/move/<int:subsampleid>")
        self.register_route(UploadImage, "/image/<int:sampleid>")
        self.register_actor_route(MosaicResource, "/mosaic")
        self._generate_scan_actors()
        self._sources = []
        for i, src in enumerate(self._config["sources"]):
            self._sources.append(
                Source(
                    src, i + 1, self._hardware, self.emit, config_file=self._config.file
                )
            )
        self._create_maps = self._config["createmaps"]
        self._check_running_actors = True
        self._check_actor_thread = gevent.spawn(self.check_running_actors)

    def reload(self):
        self._generate_scan_actors()
        for i, src in enumerate(self._config["sources"]):
            if i < len(self._sources):
                self._sources[i].update_config(src)
            else:
                self._sources.append(
                    Source(
                        src,
                        i + 1,
                        self._hardware,
                        self.emit,
                        config_file=self._config.file,
                    )
                )

    def _generate_scan_actors(self):
        """Dynamically generate scan actor resources"""

        def post(self, **kwargs):
            subsample = self._parent._metadata.get_subsamples(kwargs["subsampleid"])
            if not subsample:
                raise AttributeError(f"No such subsample {kwargs['subsampleid']}")
            kwargs["sampleid"] = subsample["sampleid"]
            kwargs["sessionid"] = g.blsession.get("sessionid")
            (
                kwargs["containerqueuesampleid"],
                kwargs["diffractionplanid"],
            ) = self._parent._metadata.queue_subsample(kwargs["subsampleid"])
            kwargs["absol"] = self._parent.get_absolute(kwargs["subsampleid"])
            kwargs["before_scan_starts"] = self._parent.before_scan_starts
            kwargs["update_datacollection"] = self._parent.update_datacollection
            kwargs["next_datacollection"] = self._parent.next_datacollection
            kwargs["generate_maps"] = self._parent.generate_maps
            kwargs["open_attachment"] = self._parent._open_dc_attachment
            kwargs["add_scanqualityindicators"] = self._parent.add_scanqualityindicators
            kwargs["scans"] = self._parent.get_component("scans")

            def get_rois():
                return {
                    "rois": self._metadata.get_xrf_map_rois(
                        sampleid=kwargs["sampleid"], no_context=True,
                    ),
                    "conversion": self._parent.get_component("scans")._config["mca"][
                        "conversion"
                    ],
                }

            kwargs["get_rois"] = get_rois

            kwargs["enqueue"] = kwargs.get("enqueue", True)
            return kwargs

        for key, scans in self._config["scantypes"].items():
            for scanname in scans:
                if scanname in self._actors:
                    continue

                self._actors.append(scanname)
                self._scan_actors.append(scanname)
                act_res = type(
                    scanname,
                    (ComponentResource,),
                    {"post": require_control(actor(scanname, preprocess=True)(post))},
                )
                self.register_actor_route(act_res, f"/scan/{scanname}")

    def before_scan_starts(self, actor):
        """Saving directory is created"""
        if actor.get("datacollectionid"):
            self._save_dc_params(actor)
            self._save_dc_image(actor)

    def _save_dc_image(self, actor):
        """Save an image for a datacollection

        Includes origin and scalebar, and subsample location.
        Should ideally be called once the subsample has been moved to the
        origin location
        """
        try:
            details = self.save_image(
                sessionid=actor["sessionid"],
                sampleid=actor["sampleid"],
                subsampleid=actor["subsampleid"],
                savemeta=False,
                annotate=True,
                file_prefix=f"snapshot1_",
            )

            extra = {}
            if details.get("subsample"):
                subsample = details["subsample"]
                extra["pixelspermicronx"] = details["scale"]["x"] * 1e-9 / 1e-6
                extra["pixelspermicrony"] = details["scale"]["y"] * 1e-9 / 1e-6
                extra["snapshot_offsetxpixel"] = subsample["x"]
                extra["snapshot_offsetypixel"] = subsample["y"]

            self.update_datacollection(
                actor, xtalsnapshotfullpath1=details["path"], **extra
            )
        except Exception as e:
            logger.exception("Could not save image")
            log.get("user").exception(
                f"Could not save data collection image", type="actor"
            )

    def save_sample_image(self, **kwargs):
        """Saves a sample image

        Sets up file saving, and saves an image from the source image

        Kwargs:
            sampleid (int): Sample id
        Returns:
            path (str): Path to the new image
        """
        self._saving.set_filename(
            extra_saving_args=self._config.get(
                "sample_image_saving", {"data_filename": "{sampleid.name}_image{time}"}
            ),
            **{
                "sampleid": kwargs["sampleid"],
                "sessionid": g.blsession.get("sessionid"),
                "time": int(time.time()),
            },
        )
        self._saving.create_root_path()
        return self.save_image(
            sampleid=kwargs.get("sampleid", None),
            subsampleid=kwargs.get("subsampleid", None),
            sessionid=g.blsession.get("sessionid"),
            file_prefix="sampleimage_",
        )

    def upload_image(self, **kwargs):
        """Send an image to an actor

        Kwargs:
            sampleid (int): The associated sampleid
            image (str): The base64 encoded image
        """
        sample = self._metadata.get_samples(sampleid=kwargs["sampleid"])
        self.actor(
            "upload_canvas",
            error=self._upload_failed,
            spawn=True,
            actargs={"image": kwargs["image"], "sample": sample},
        )

        return True

    def _upload_failed(self, actid, exception, actor):
        logger.error(
            f"Could not upload image for {actor['sampleid']} exception was {exception}"
        )
        log.get("user").exception(
            f"Could not upload image for {actor['sampleid']} exception was {exception}",
            type="queue",
        )

    def actor_started(self, actid, actor):
        """Callback when an actor starts
            
        For scan actors this will generate a datacollection
        """
        if actor.name in self._scan_actors:
            self.start_datacollection(actor)

        if actor.name in ["mosaic"]:
            args = {
                "sessionid": actor["sessionid"],
                "sampleid": actor["sampleid"],
                "starttime": datetime.now(),
                "actiontype": actor.name,
            }
            sampleaction = self._metadata.add_sampleaction(**args, no_context=True)
            actor.update(sampleactionid=sampleaction["sampleactionid"])

            filename = self._saving.set_filename(
                extra_saving_args=actor.saving_args, **actor.all_data
            )
            self._saving.create_root_path()

        logger.info(f"Actor '{actor.name}' with id '{actid}' started")

    def actor_success(self, actid, response, actor):
        """Callback when an actor finishes successfully
        
        For scan actors this update the datacollection with the endtime and 
        'success' status

        For ROI scans it will launch map generation
        """
        if actor.name in self._scan_actors:
            self.update_datacollection(
                actor, endtime=datetime.now(), runstatus="Successful"
            )

        if actor.name in self._create_maps:
            self.generate_maps(actor["subsampleid"], actor["datacollectionid"])

        if actor.name in ["mosaic"]:
            path = self.save_full_mosaic(actor["full"])

            self._metadata.update_sampleaction(
                sampleactionid=actor["sampleactionid"],
                no_context=True,
                endtimestamp=datetime.now(),
                status="success",
                xtalsnapshotafter=path,
            )

        logger.info(f"Actor '{actor.name}' with id '{actid}' finished")

    def save_full_mosaic(self, image):
        """Saves the full mosaic image
        
        Also creates a thumbnail

        Args:
            image (PIL.Image): The image to save

        Returns
            path (str): Path to the newly saved image
        """
        directory = self._saving.dirname
        if self._config.get("image_subdirectory"):
            directory = os.path.join(directory, self._config.get("image_subdirectory"))
            if not os.path.exists(directory):
                os.makedirs(directory)

        filename = os.extsep.join([f"mosaic_full_{time.time()}", "png"])
        path = os.path.join(directory, filename)
        image.save(path)
        self._generate_thumb(path)
        return path

    def actor_error(self, actid, exception, actor):
        """Callback when an actor fails

        For scan actors this will update the datacollection with the end time and
        'failed' status
        """
        status = "Aborted" if isinstance(exception, ComponentActorKilled) else "Failed"
        if actor.name in self._scan_actors:
            self.update_datacollection(actor, endtime=datetime.now(), runstatus=status)
            if status == "Failed":
                self._save_dc_exception(actor, exception)
        if actor.name in ["mosaic"]:
            self._metadata.update_sampleaction(
                sampleactionid=actor["sampleactionid"],
                no_context=True,
                endtimestamp=datetime.now(),
                status="error",
                message=str(exception),
            )
        if status == "Failed":
            logger.error(f"Actor '{actor.name}' with id '{actid}' failed")
        else:
            logger.info(f"Actor '{actor.name}' with id '{actid}' aborted")

    def actor_remove(self, actid, actor):
        """Callback when an actor is removed from the queue

        For scan actors this will remove the item from the database queue
        """
        if actor.name in self._scan_actors:
            self._metadata.unqueue_subsample(
                actor["subsampleid"],
                containerqueuesampleid=actor["containerqueuesampleid"],
                no_context=True,
            )

    def check_running_actors(self):
        """Periodicically check for any running actors

        This is used to trigger automated downstream procesing, i.e.
        map generation so that long scans can be followed
        """
        logger.debug("Starting periodic actor checker")
        while self._check_running_actors:
            running_copy = self._running_actors.copy()
            for actid, actall in running_copy.items():
                actor = actall[0]
                if actor.name in self._create_maps:
                    if (
                        actor.get("subsampleid")
                        and actor.get("datacollectionid")
                        and actor.get("datacollectionnumber")
                    ):
                        logger.debug(
                            f"Re/generating maps for {actor.name} dcid:{actor['datacollectionid']}"
                        )
                        self.generate_maps(
                            actor["subsampleid"], actor["datacollectionid"], auto=True
                        )
            try:
                time.sleep(self._config["regenerate_interval"])
            except KeyError:
                time.sleep(60)

    def generate_maps(self, subsampleid, datacollectionid=None, auto=False):
        """Launch a series of actors to generate maps for each of the MCA ROIs"""
        self._in_generate = True

        dcs = self._metadata.get_datacollections(
            datacollectionid, subsampleid=subsampleid, no_context=True, ungroup=True
        )
        scans = self.get_component("scans")

        if datacollectionid:
            dcs = [dcs]
        else:
            dcs = dcs["rows"]

        existing = self._metadata.get_xrf_maps(subsampleid=subsampleid, no_context=True)

        if not auto:
            self.emit(
                "message", {"type": "generate_maps", "status": "started"},
            )
            log.get("user").info(
                f"Starting map generation", type="actor",
            )

        count = 0
        for dc in dcs:
            running = False
            for actid in self._map_actors:
                actall = self._running_actors.get(actid)
                if not actall:
                    continue

                actor = actall[0]

                if (
                    actor["subsampleid"] == subsampleid
                    and actor["datacollectionid"] == dc["datacollectionid"]
                ):
                    running = True
                    break

            if running:
                logger.info(
                    f"Generate map actor already running for subsample {subsampleid} datacollection {dc['datacollectionid']}"
                )
                continue

            rois = self._metadata.get_xrf_map_rois(
                sampleid=dc["sampleid"], no_context=True
            )
            spectra = scans.get_scan_spectra(dc["datacollectionnumber"], allpoints=True)
            scalars = scans.get_scan_data(
                dc["datacollectionnumber"], per_page=1e10, all_scalars=True
            )
            if spectra:
                self._map_actors.append(
                    self.actor(
                        "createmap",
                        spawn=True,
                        success=self._append_map,
                        error=self._map_failed,
                        actargs={
                            "datacollectionid": dc["datacollectionid"],
                            "datacollectionnumber": dc["datacollectionnumber"],
                            "subsampleid": subsampleid,
                            "rois": rois,
                            "spectra": spectra,
                            "scalars": scalars,
                        },
                    )
                )
                count += 1
            else:
                logger.warning(
                    f"Generate Map: Cant get spectra for scan {dc['datacollectionnumber']} (datacollectionid: {dc['datacollectionid']})"
                )
                log.get("user").error(
                    f"Cant get spectra for scan {dc['datacollectionnumber']} (datacollectionid: {dc['datacollectionid']})",
                    type="queue",
                )

            # Update scalar maps
            for m in existing:
                if m["scalar"] and m["datacollectionid"] == dc["datacollectionid"]:
                    new_data = self._get_additional_map(m["scalar"], scalars=scalars)
                    if new_data:
                        points = len(new_data) - new_data.count(-1)
                        self._metadata.update_xrf_map(
                            mapid=m["mapid"],
                            data=new_data,
                            points=points,
                            no_context=True,
                        )

        self._in_generate = False

        if count == 0 and not auto:
            self.emit(
                "message",
                {
                    "type": "generate_maps",
                    "status": "warning",
                    "message": "No maps to generate, check the log",
                },
            )
            log.get("user").warning(
                f"No maps to generation", type="actor",
            )

        return True

    def _map_failed(self, actid, exception, actor):
        logger.error(
            f"Could not generate map for scan {actor['datacollectionnumber']} (datacollectionid: {actor['datacollectionid']}), exception was {exception}"
        )
        log.get("user").exception(
            f"Could not generate map for scan {actor['datacollectionnumber']} (datacollectionid: {actor['datacollectionid']})",
            type="queue",
        )
        self._update_map_actor_status(actid)

    def _append_map(self, actid, maps, actor):
        """Add new map to the maplist

        Will try to updating an existing map if it matched dcid and maproiid
        """
        dc = self._metadata.get_datacollections(
            actor["datacollectionid"], subsampleid=actor["subsampleid"], no_context=True
        )

        existing = self._metadata.get_xrf_maps(
            subsampleid=dc["subsampleid"], no_context=True
        )

        if not maps:
            logger.warning("No maps generated to append")
            return

        # Create / update ROI maps
        for m in maps[0]["maps"]:
            exists = False
            for ex in existing:
                if (
                    dc["datacollectionid"] == ex["datacollectionid"]
                    and m["maproiid"] == ex["maproiid"]
                ):
                    mapid = ex["mapid"]
                    points = len(m["data"]) - m["data"].count(-1)
                    self._metadata.update_xrf_map(
                        mapid=ex["mapid"],
                        data=m["data"],
                        points=points,
                        no_context=True,
                    )
                    exists = True
                    break

            if not exists:
                newmap = self._metadata.add_xrf_map(
                    maproiid=m["maproiid"],
                    datacollectionid=dc["datacollectionid"],
                    data=m["data"],
                    no_context=True,
                )
                if not newmap:
                    continue

                mapid = newmap["mapid"]

            self.emit(
                "message",
                {
                    "type": "map",
                    "mapid": mapid,
                    "sampleid": dc["sampleid"],
                    "subsampleid": dc["subsampleid"],
                },
            )

        self._update_map_actor_status(actid)

    def _update_map_actor_status(self, actid):
        self._map_actors.remove(actid)

        if self._in_generate:
            return

        if len(self._map_actors) == 0:
            self.emit(
                "message", {"type": "generate_maps", "status": "finished"},
            )

            log.get("user").info(
                f"Map generation complete", type="actor",
            )
        else:
            self.emit(
                "message",
                {
                    "type": "generate_maps",
                    "status": "progress",
                    "remaining": len(self._map_actors),
                },
            )

    def _get_additional_map(self, scalar, scalars):
        """Get data for an additional map
        
        Args:
            scalar (str): The key for scalar data to use
            scalars (dict): Dict of scan data scalars

        Returns:
            data (ndarray): The map data
        """
        if scalar in scalars["data"]:
            data = scalars["data"][scalar]["data"]

            if not data:
                logger.warning(f"Scalar {scalar} data length is zero")
                return

            if len(data) < scalars["npoints"]:
                missing = scalars["npoints"] - len(data)
                data.extend([-1 for x in range(missing)])

            return data

        else:
            logger.warning(f"Cannot find scalar {scalar} in scan data")

    def generate_additional_map(self, **kwargs):
        """Generate additional maps based on scan scalars"""
        dc = self._metadata.get_datacollections(
            datacollectionid=kwargs["datacollectionid"]
        )

        if not dc:
            return

        scans = self.get_component("scans")
        scalars = scans.get_scan_data(scanid=dc["datacollectionnumber"], per_page=1e10)

        if not scalars:
            logger.warning(f"Scan id {dc['datacollectionnumber']} is not available")
            return

        for scalar in kwargs["scalars"]:
            data = self._get_additional_map(scalar=scalar, scalars=scalars)
            if data:
                roi = self._metadata.add_xrf_map_roi_scalar(scalar=scalar)
                self._metadata.add_xrf_map(
                    maproiid=roi["maproiid"],
                    datacollectionid=dc["datacollectionid"],
                    data=data,
                    no_context=True,
                )

        return True

    def get_sources(self):
        """Return list of image sources"""
        return [src.info() for src in self._sources]

    def move(self, args):
        """Move the source image"""
        absol = self.get_absolute_fp(args)
        self.actor("move", spawn=True, actargs={"absol": absol})
        return True

    def save_image(
        self,
        sessionid=None,
        sampleid=None,
        subsampleid=None,
        savemeta=True,
        annotate=False,
        file_prefix="",
    ):
        """Save an image from the source device, like objects, images are marked
        relative to the origin marking position

        Args:
            sessionid (int): The session id
            sampleid (int): The sample id
            subsampleid (int): Optionally a subsample id
            savemeta (bool): Whether to save this as a sample image
            annotate (bool): Whether to annotate this image (origin, scalebar, etc)

        Returns:
            path (str): The path of the saved image
        """
        directory = self._saving.dirname
        for src in self._sources:
            if not src.origin:
                continue

            filename = os.extsep.join([f"{file_prefix}{time.time()}", "png"])

            if self._config.get("image_subdirectory"):
                directory = os.path.join(
                    directory, self._config.get("image_subdirectory")
                )
                if not os.path.exists(directory):
                    os.makedirs(directory)

            path = os.path.join(directory, filename)

            if not src.device.online():
                raise RuntimeError("Cannot save image, camera is offline")

            src.device.call("save", path)

            image_info = src.canvas.vlm_image_info
            beam = src.canvas.beam_info

            subsample = None
            if annotate:
                if subsampleid:
                    subsample = self._metadata.get_subsamples(
                        subsampleid=subsampleid, no_context=True
                    )
                ann = AnnotateImage(path)
                details = ann.annotate(
                    image_info["center"],
                    beam["position"],
                    image_info["pixelsize"],
                    src.unit,
                    subsample,
                )
                subsample = details["subsample"]

            if sampleid and savemeta:
                self._metadata.add_sampleimage(
                    no_context=True,
                    sampleid=sampleid,
                    offsetx=int(image_info["center"][0]),
                    offsety=int(image_info["center"][1]),
                    scalex=float(image_info["pixelsize"][0]),
                    scaley=float(image_info["pixelsize"][1]),
                    file=path,
                )

            self._generate_thumb(path)
            return {
                "path": path,
                "subsample": subsample,
                "scale": {
                    "x": float(image_info["pixelsize"][0]),
                    "y": float(image_info["pixelsize"][1]),
                },
            }

    def move_to(self, subsampleid):
        """Move to a specific subsample

        Args:
            subsampleid (int): The subsample id

        Returns:
            success (bool): Whether the move was successful
        """
        absol = self.get_absolute(subsampleid)
        self.actor("move", spawn=True, actargs={"absol": absol})
        return True

    @property
    def origin_defining_source(self):
        for src in self._sources:
            if src.origin == True:
                return src

    def get_absolute_fp(self, pos, pos2=None):
        """Return absolute motor positions to bring a position to the centre of view
        
        Args:
            pos (dict): Dictionary containing 'x' and 'y' positions

        Returns:
            absolute (dict): Absolute positions and their associated motors
        """
        src = self.origin_defining_source
        if src is None:
            return None

        if pos2:
            absol = src.canvas.canvas_to_motor(
                [[pos["x"], -pos["y"]], [pos2["x"], -pos2["y"]]]
            )
        else:
            absol = src.canvas.canvas_to_motor([pos["x"], -pos["y"]])

        absol["axes"] = self.find_axes_from_variable(absol["variable"])
        absol["move_to"] = self.move_to_absol

        # print("------ get_absolute_fp ------")
        # pp.pprint(pos)
        # pp.pprint(absol)

        return absol

    def get_absolute(self, subsampleid):
        """Return absolute motor positions to bring a subsample id to the origin marking

        Args:
            subsampleid (int): The subsample to get the position of

        Returns:
            absolute (dict): A dictionary of the absolute positions for the subsample
        """
        src = self.origin_defining_source
        if src is None:
            return None

        obj = self._metadata.get_subsamples(subsampleid)
        if obj is None:
            return

        if obj["type"] == "loi" or obj["type"] == "roi":
            pos = [[obj["x"], -obj["y"]], [obj["x2"], -obj["y2"]]]
        else:
            pos = [obj["x"], -obj["y"]]
        absol = src.canvas.canvas_to_motor(pos)
        # src.canvas.sampleposition=....

        # print("------ get_absolute ------")
        # pp.pprint(obj)
        # pp.pprint(absol)

        if "z" in absol["fixed"]:
            del absol["fixed"]["z"]

        absol["axes"] = self.find_axes_from_variable(absol["variable"])
        absol["move_to_additional"] = src.move_to_additional
        absol["positions"] = obj["positions"]
        absol["move_to"] = self.move_to_absol

        return absol

    def find_axes_from_variable(self, variable):
        axes = {}
        for key, obj in variable.items():
            for axis in ["x", "y", "z"]:
                if key.startswith(axis):
                    axes[axis] = obj

        return axes

    def move_to_absol(self, absol):
        all_objs = list(absol["fixed"].values()) + list(
            absol.get("variable", {}).values()
        )
        for obj in all_objs:
            if type(obj["destination"]) == list:
                obj["motor"].move(obj["destination"][0])
            else:
                obj["motor"].move(obj["destination"])

        for obj in all_objs:
            obj["motor"].wait()

    def _generate_thumb(self, path):
        size = (250, 250)
        thumb = Image.open(path)
        thumb.thumbnail(size, Image.ANTIALIAS)
        thumb.save(path.replace(".png", "t.png"))
