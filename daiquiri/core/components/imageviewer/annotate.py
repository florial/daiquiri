#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
import numpy as np
from PIL import Image, ImageDraw, ImageFont, ImageOps
from daiquiri.core.utils import format_eng
from daiquiri.resources.utils import get_resource


logger = logging.getLogger(__name__)


class AnnotateImage:
    font_size = 14

    def __init__(self, path):
        self.path = path
        self.font = ImageFont.truetype(
            get_resource("fonts", "Poppins-Regular.ttf"), self.font_size
        )

    def annotate(self, image_center, beam_center, pixelsize, unit, subsample):
        """Annotate the image
        
        Args:
            image_center (list): x, y positions for the image
            beam_center (list): x, y positions for the origin
            pizelsize (list): x, y pixel size in `unit`
            unit (int): the base unit for all calculations (1e-9 default)
            subsample (dict): (optionally) a subsample to annotate
        """
        self.image_center = image_center
        self.beam_center = beam_center
        self.pixelsize = pixelsize
        self.unit = unit
        self.subsample = subsample

        self.im = Image.open(self.path)
        self.im_w, self.im_h = self.im.size

        # For a data colleciton image the image should be flipped for normal
        # viewing
        if self.pixelsize[0] < 0:
            self.im = ImageOps.mirror(self.im)

        if self.pixelsize[1] > 0:
            self.im = ImageOps.flip(self.im)

        self.origin_marking()
        self.scalebar()

        subret = None
        if subsample:
            subret = self.draw_subsample()

        self.im.save(self.path)
        self.im.close()

        return {"subsample": subret}

    def to_image_coords(self, pos):
        """Convert from canvas units to image
    
        Coordinates from the transfoms module assume that an image is x,y,
        however in image space they are actually x, -y 

        If the image is flipped i.e. -pixelsize[0], or +pixelsize[1] this is accounted
        for by the flip/mirror above   
        """
        # TODO: Replace me with a src.transform
        return (
            np.array([self.im_w / 2, self.im_h / 2])
            - (
                (self.image_center - np.array(pos))
                / [abs(self.pixelsize[0]), -abs(self.pixelsize[1])]
            )
        ).tolist()

    def origin_marking(self):
        """Origin Marking
        
        Draw a cross for the origin marking
        """
        colors = {"normal": "yellow", "invert": "green"}

        beam = self.to_image_coords(self.beam_center)
        # print("origin_marking", self.image_center, self.beam_center, beam)

        color = colors["invert"] if self._invert(beam[0], beam[1]) else colors["normal"]

        mark_size = 20
        mark = ImageDraw.Draw(self.im)
        mark.line(
            (beam[0] - mark_size, beam[1], beam[0] + mark_size, beam[1]), fill=color
        )
        mark.line(
            (beam[0], beam[1] - mark_size, beam[0], beam[1] + mark_size), fill=color
        )

    def scalebar(self):
        """Draw Scalebar

        Draw a single line for the scalebar with rounded size
        """
        sb_w = int(0.1 * self.im_w)
        sb_h = int(0.1 * self.im_h)

        px = format_eng(abs(self.pixelsize[0]) * self.unit * sb_w)

        # round to 10
        rem = px["scalar"] % 10
        fact = (px["scalar"] - rem) / px["scalar"]

        sb = ImageDraw.Draw(self.im)
        sb.line(
            (
                sb_w / 2,
                self.im_h - sb_h / 2,
                (sb_w / 2) + (sb_w * fact),
                self.im_h - sb_h / 2,
            ),
            fill="grey",
            width=2,
        )

        sb.text(
            (sb_w / 2, self.im_h - sb_h / 2 - 20),
            f"{px['scalar']-rem:.0f} {px['prefix']}m",
            font=self.font,
            fill="grey",
        )

    def draw_subsample(self):
        """Draw Subsample

        For ROIs draw a rectangle,
            LOIs a line
            POIs a cross

        Subsamples are marked relative to the origin marking
        """
        colors = {"normal": "cyan", "invert": "blue"}

        tl = self.to_image_coords([self.subsample["x"], -self.subsample["y"]])
        # print(
        #     "draw_subsample",
        #     self.image_center,
        #     [self.subsample["x"], -self.subsample["y"]],
        #     tl,
        # )

        invert = self._invert(tl[0], tl[1])
        color = colors["invert"] if invert else colors["normal"]

        ss = ImageDraw.Draw(self.im)
        if self.subsample["type"] == "roi" or self.subsample["type"] == "loi":
            br = self.to_image_coords([self.subsample["x2"], -self.subsample["y2"]])

            if self.subsample["type"] == "roi":
                ss.rectangle([tl[0], tl[1], br[0], br[1]], outline=color)
            else:
                ss.line([tl[0], tl[1], br[0], br[1]], fill=color)
            ss.text(
                (br[0], br[1]),
                str(self.subsample["subsampleid"]),
                font=self.font,
                fill=color,
            )

            return {"x": tl[0], "y": tl[1]}

        else:
            ss_size = 15
            ss.line((tl[0] - ss_size, tl[1], tl[0] + ss_size, tl[1]), fill=color)
            ss.line((tl[0], tl[1] - ss_size, tl[0], tl[1] + ss_size), fill=color)
            ss.text(
                (tl[0], tl[1]),
                str(self.subsample["subsampleid"]),
                font=self.font,
                fill=color,
            )

    def _invert(self, x, y):
        """Determine if a pixel is very bright"""
        pix = self.im.getpixel((x, y))
        return (0.299 * pix[0] + 0.587 * pix[1] + 0.114 ** pix[2]) > 200
