#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import daiquiri
from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema.components import VersionSchema


logger = logging.getLogger(__name__)


class VersionResource(ComponentResource):
    @marshal(out=[[200, VersionSchema(), "Current version information"]])
    def get(self):
        """Returns the current daiquiri version"""
        return {"version": daiquiri.__version__}


class Version(Component):
    """Version Component

    Simply returns the current app version
    """

    def setup(self):
        self.register_route(VersionResource, "")
