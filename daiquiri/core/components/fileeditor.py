#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import importlib
from marshmallow import Schema, fields

from daiquiri.core import marshal, require_staff
from daiquiri.resources.utils import RESOURCE_ROOTS
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema

import logging

logger = logging.getLogger(__name__)


class FileSchema(Schema):
    path = fields.Str()
    contents = fields.Str(required=True)
    ext = fields.Str()


class DirectorySchema(Schema):
    """Directory Schema"""

    name = fields.Str()
    path = fields.Str()
    type = fields.Str()
    ext = fields.Str()
    children = fields.Nested(lambda: DirectorySchema(many=True))


class DirectoryResource(ComponentResource):
    @require_staff
    @marshal(
        out=[
            [200, DirectorySchema(many=True), "Directory listing for current resources"]
        ]
    )
    def get(self, **kwargs):
        """Get the directory listing"""
        return self._parent.get_directory(**kwargs)


class FileResource(ComponentResource):
    @require_staff
    @marshal(
        inp={"path": fields.Str()},
        out=[
            [200, FileSchema(), "Read file contents"],
            [400, ErrorSchema(), "Could not read file contents"],
        ],
    )
    def get(self, path, **kwargs):
        """Get a file"""
        file = self._parent.read_file(path, **kwargs)
        if file:
            return file
        else:
            return {"message": "Could not read file"}, 400

    @require_staff
    @marshal(
        inp=FileSchema,
        out=[
            [200, FileSchema(), "File successfully updated"],
            [400, ErrorSchema(), "Could not update file"],
        ],
    )
    def patch(self, path, **kwargs):
        """Update a file"""
        file = self._parent.update_file(path, **kwargs)
        if file:
            return file

        return {"error": "Could not update file"}, 400


def path_to_dict(path, root=None):
    name = os.path.basename(path)

    if name in ["__pycache__"]:
        return None

    d = {"name": os.path.basename(path), "path": path[1:]}
    if os.path.isdir(path):
        d["type"] = "directory"
        d["children"] = [
            path_to_dict(os.path.join(path, x), root)
            for x in os.listdir(path)
            if path_to_dict(os.path.join(path, x), root) is not None
        ]
    else:
        d["type"] = "file"
        d["ext"] = os.path.splitext(name)[1][1:]
    return d


class Fileeditor(Component):
    _base_url = "editor"

    def setup(self):
        self.register_route(DirectoryResource, "/directory")
        self.register_route(FileResource, "/file/<path:path>")

        implementors = importlib.import_module(
            self._base_config["implementors"].replace("/", ".")
        )
        self._directories = [
            os.path.abspath(RESOURCE_ROOTS[0]),
            os.path.dirname(implementors.__file__),
        ]

        if self._config.get("directories"):
            self._directories.extend(self._config["directories"])

    def get_directory(self, **kwargs):
        return [
            path_to_dict(directory, directory + os.path.sep)
            for directory in self._directories
        ]

    def read_file(self, filepath, **kwargs):
        contents = ""

        fullpath = None
        for d in self._directories:
            test = os.path.abspath(os.path.sep + filepath)
            if os.path.exists(test):
                if os.path.commonprefix([test, d]) == d:
                    fullpath = test
                    break
                else:
                    logger.warning(f"File {filepath} is not in allowed root {d}")

        if fullpath is None:
            return

        with open(fullpath) as file:
            contents = file.read()

        return {
            "path": fullpath[1:],
            "fullpath": fullpath,
            "contents": contents,
            "ext": os.path.splitext(filepath)[1][1:],
        }

    def update_file(self, filepath, **kwargs):
        file = self.read_file(filepath)
        if file:
            try:
                logger.info(f"tring to write {file['fullpath']}")
                with open(file["fullpath"], "w") as file:
                    file.write(kwargs["contents"])
                return {"contents": kwargs["contents"], "path": filepath}

            except Exception:
                logger.exception(f"Could not update file {filepath}")
