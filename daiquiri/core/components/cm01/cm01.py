# -*- coding: utf-8 -*-
from marshmallow import fields, ValidationError

from daiquiri.core import marshal
from daiquiri.core.logging import log
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.cm01 import ProjectDataSchema


class SampleDefinitionResource(ComponentResource):
    @marshal(inp=ProjectDataSchema, out=[[200, "", "Project data"]])
    def post(self):
        """Creates a project"""
        return "", 200


class Cm01(Component):
    """
    """

    def setup(self):
        self.register_route(SampleDefinitionResource, "/define-project")
