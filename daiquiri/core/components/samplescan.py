#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from flask import g

from daiquiri.core import require_control, require_staff, marshal
from daiquiri.core.logging import log
from daiquiri.core.components import (
    Component,
    ComponentResource,
    actor,
    ComponentActorKilled,
)
from daiquiri.core.components.dcutilsmixin import DCUtilsMixin
from daiquiri.core.schema.samplescan import ScanConfigSchema, AvailableScansSchema

import logging


logger = logging.getLogger(__name__)


class AvailableScansResource(ComponentResource):
    @marshal(out=[[200, AvailableScansSchema(many=True), "A list of available scans"]])
    def get(self, **kwargs):
        """Get a list of available scans"""
        return self._parent.get_available_scans()


class Samplescan(Component, DCUtilsMixin):
    _base_url = "samplescan"
    _config_schema = ScanConfigSchema()

    def setup(self):
        self._scan_actors = []
        self.register_route(AvailableScansResource, "")

        self._generate_scan_actors()
        self._generate_process_actors()

    def reload(self):
        self._generate_scan_actors()

    def _generate_scan_actors(self):
        """Dynamically generate scan actor resources"""

        def post(self, **kwargs):
            sample = self._parent._metadata.get_samples(kwargs["sampleid"])
            if not sample:
                raise AttributeError(f"No such sample {kwargs['sampleid']}")
            kwargs["sampleid"] = sample["sampleid"]
            kwargs["sessionid"] = g.blsession.get("sessionid")
            (
                kwargs["containerqueuesampleid"],
                kwargs["diffractionplanid"],
            ) = self._parent._metadata.queue_sample(kwargs["sampleid"])
            kwargs["before_scan_starts"] = self._parent.before_scan_starts
            kwargs["update_datacollection"] = self._parent.update_datacollection
            kwargs["open_attachment"] = self._parent._open_dc_attachment
            kwargs["add_scanqualityindicators"] = self._parent.add_scanqualityindicators
            kwargs["enqueue"] = kwargs.get("enqueue", True)
            return kwargs

        for scan in self._config["scans"]:
            if not self._config.get("actors"):
                self._config["actors"] = {}

            self._config["actors"][scan["actor"]] = scan["actor"]

            if scan["actor"] in self._actors:
                continue

            self._actors.append(scan["actor"])
            self._scan_actors.append(scan["actor"])
            fn = require_control(actor(scan["actor"], preprocess=True)(post))

            if scan.get("require_staff"):
                fn = require_staff(fn)

            print("generate scan actor", scan.get("require_staff"))
            act_res = type(scan["actor"], (ComponentResource,), {"post": fn})
            self.register_actor_route(act_res, f"/{scan['actor']}")

    def _generate_process_actors(self):
        for act, processes in self._config.get("processing", {}).items():
            for process in processes:
                self._actors.append(process["actor"])

    def get_available_scans(self):
        scans = []
        for scan in self._config["scans"]:
            print(scan["actor"], scan.get("require_staff"))
            if (not scan.get("require_staff")) or (
                scan.get("require_staff") and g.user.staff()
            ):
                s = {"actor": scan["actor"]}
                if scan["actor"] in self._config.get("processing", {}):
                    ps = []
                    for p in self._config["processing"][scan["actor"]]:
                        ps.append(p)
                    s["processing"] = ps

                scans.append(s)

        return scans

    def before_scan_starts(self, actor):
        """Saving directory is created"""
        if actor.get("datacollectionid"):
            self._save_dc_params(actor)

    def actor_started(self, actid, actor):
        """Callback when an actor starts

        For scan actors this will generate a datacollection
        """
        if actor.name in self._scan_actors:
            self.start_datacollection(actor)

        logger.info(f"Actor '{actor.name}' with id '{actid}' started")

    def actor_success(self, actid, response, actor):
        """Callback when an actor finishes successfully

        For scan actors this update the datacollection with the endtime and
        'success' status

        Possibility to launch post processing
        """
        if actor.name in self._scan_actors:
            self.update_datacollection(
                actor, endtime=datetime.now(), runstatus="Successful"
            )

        if actor.name in self._config.get("processing", {}):
            pass

        logger.info(f"Actor '{actor.name}' with id '{actid}' finished")

    def actor_error(self, actid, exception, actor):
        """Callback when an actor fails

        For scan actors this will update the datacollection with the end time and
        'failed' status
        """
        status = "Aborted" if isinstance(exception, ComponentActorKilled) else "Failed"
        if actor.name in self._scan_actors:
            self.update_datacollection(actor, endtime=datetime.now(), runstatus=status)
            if status == "Failed":
                self._save_dc_exception(actor, exception)
        if status == "Failed":
            logger.error(f"Actor '{actor.name}' with id '{actid}' failed")
        else:
            logger.info(f"Actor '{actor.name}' with id '{actid}' aborted")

    def actor_remove(self, actid, actor):
        """Callback when an actor is removed from the queue

        For scan actors this will remove the item from the database queue
        """
        if actor.name in self._scan_actors:
            self._metadata.unqueue_sample(
                actor["sampleid"],
                containerqueuesampleid=actor["containerqueuesampleid"],
                no_context=True,
            )
