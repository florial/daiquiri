#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import gevent
import uuid

from marshmallow import fields

from daiquiri.core.logging import log
from daiquiri.core import CoreBase, CoreResource, marshal, require_control
from daiquiri.core.components import ComponentActor, ComponentActorKilled
from daiquiri.core.schema import ErrorSchema, MessageSchema
from daiquiri.core.schema.queue import (
    QueueStatusSchema,
    ChangeQueueStatusSchema,
    ChangeQueueSettingSchema,
    MoveQueueItemSchema,
)

import logging

logger = logging.getLogger(__name__)


class PauseActor(ComponentActor):
    name = "pause"

    def method(*args, **kwargs):
        pass


class SleepActor(ComponentActor):
    name = "sleep"

    def method(*args, **kwargs):
        time.sleep(kwargs.get("time", 10))


class QueueResource(CoreResource):
    @marshal(
        inp={"status": fields.Str()},
        out=[[200, QueueStatusSchema(), "Current queue status"]],
    )
    def get(self, **kwargs):
        """Get current queue status"""
        return self._parent.status(**kwargs), 200

    @marshal(
        inp=ChangeQueueStatusSchema,
        out=[
            [200, ChangeQueueStatusSchema(), "New queue state"],
            [400, ErrorSchema(), "Could not change queue state"],
        ],
    )
    @require_control
    def put(self, *args, **kwargs):
        """Start or pause the queue"""
        state = kwargs.get("state", False)
        if state:
            self._parent.start()
        else:
            self._parent.stop()

        return {"state": kwargs["state"]}, 200

    @marshal(
        inp=ChangeQueueSettingSchema,
        out=[
            [200, MessageSchema(), "Setting changed successfully"],
            [400, ErrorSchema(), "Could not change queue setting"],
        ],
    )
    @require_control
    def patch(self, *args, **kwargs):
        """Update a queue setting"""
        success = self._parent.update_queue(**kwargs)
        if success:
            return {"message": "Queue setting updated"}, 200
        else:
            return {"error": "Could not update queue setting"}, 400

    @marshal(
        inp={
            "arr": fields.Str(
                description="The name of the array we would like to clear"
            ),
        },
        out=[[200, MessageSchema(), "Queue cleared"]]
        )
    @require_control
    def delete(self,  *args, **kwargs):
        """Clear the queue"""
        log.get("user").info(f"Queue {kwargs.get('arr')} was cleared", type="queue")
        self._parent.clear(kwargs.get('arr'))
        return {"message": "Queue cleared"}, 200


class QueueItemResource(CoreResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Queue item removed"],
            [400, ErrorSchema(), "Could not remove queue item"],
        ]
    )
    @require_control
    def delete(self, uuid):
        """Remove an item from the queue"""
        success = self._parent.remove(uuid)
        if success:
            log.get("user").info(f"Queue item {uuid} was removed", type="queue")
            return {"message": "Queue item removed"}, 200
        else:
            return {"error": "Could not remove queue item"}, 400


class QueueKillResource(CoreResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Queue item killed"],
            [400, ErrorSchema(), "Could not kill queue item"],
        ]
    )
    @require_control
    def post(self):
        """Kill the currently running queue item"""
        uuid = self._parent.kill()
        self._parent.stop()
        if uuid:
            log.get("user").info(f"Queue item {uuid} was killed", type="queue")
            return {"message": "Queue item killed"}, 200
        else:
            return {"error": "Could not kill queue item"}, 400


class MoveQueueItemResource(CoreResource):
    @marshal(
        inp=MoveQueueItemSchema,
        out=[
            [200, MessageSchema(), "Queue item moved"],
            [400, ErrorSchema(), "Could not move queue item"],
        ],
    )
    @require_control
    def post(self, **kwargs):
        """Move a queue item to a different position"""
        moved = self._parent.move_item(kwargs["uid"], kwargs["position"])

        if moved:
            log.get("user").info(f"Queue item {kwargs['uid']} was moved", type="queue")
            return {"message": "Queue item moved"}, 200
        else:
            return {"error": "Could not move queue item"}, 400


class PauseItemResource(CoreResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Pause item added"],
            [400, ErrorSchema(), "Could not add pause item"],
        ]
    )
    @require_control
    def post(self):
        """Add a pause item to the queue (pauses the queue when reached)"""
        uid = self._parent.add_pause()
        if uid:
            log.get("user").info(f"Pause item {uid} was added", type="queue")
            return {"message": "Pause item added"}, 200
        else:
            return {"error": "Could not add pause item"}, 400


class SleepItemResource(CoreResource):
    @marshal(
        inp={"time": fields.Int(required=True)},
        out=[
            [200, MessageSchema(), "Sleep item added"],
            [400, ErrorSchema(), "Could not add sleep item"],
        ],
    )
    @require_control
    def post(self, **kwargs):
        """Add a sleep item to the queue (sleeps for n seconds when reached)"""
        uid = self._parent.add_sleep(kwargs["time"])
        if uid:
            log.get("user").info(
                f"Sleep item {uid} for {kwargs['time']}s was added", type="queue"
            )
            return {"message": "Sleep item added"}, 200
        else:
            return {"error": "Could not add sleep item"}, 400


class Queue(CoreBase):
    """Core Queue functionality

    The queue consists of a stack of `Actor` instances. The queue is started at initialise
    time and then by default paused. The queue can be started and stopped via flask resources

    """

    _namespace = "queue"
    _require_session = True
    _require_blsession = True
    _stack = []
    _finished = []
    _running_actor = None
    _greenlet = None

    _running = False

    def setup(self):
        self.register_route(QueueResource, "")
        self.register_route(QueueKillResource, "/kill")
        self.register_route(MoveQueueItemResource, "/move")
        self.register_route(PauseItemResource, "/pause")
        self.register_route(SleepItemResource, "/sleep")
        self.register_route(QueueItemResource, "/<uuid>")

        self._pause_on_fail = self._config.get("pause_queue_on_fail", True)

    def push(self, actor):
        """Push an Actor onto the stack

        Pushes an actor onto the stack and emits a message

        Args:
            actor (obj): An instance of an `Actor`
        """
        assert isinstance(actor, ComponentActor)
        log.get("user").info(
            f"Actor added to the queue ({actor.name}, {actor.uid})", type="queue"
        )
        self.emit(
            "message", {"type": "add", "message": "Adding Actor", "uid": actor.uid},
        )
        self._stack.append(actor)

    def run_now(self, actor, pause=True):
        """Push an Actor onto the stack and run it now

        Args:
            actor (obj): An instance of an `Actor`
            pause (bool): Whether the queue should pause after this actor
        """
        if pause and len(self._stack) > 0:
            pause = self._create_pause()
            self._stack.insert(0, pause)

        self._stack.insert(0, actor)
        self.start()

    def start(self):
        """Start the queue running

        Emits a message that the queue has started
        """
        self._running = True
        log.get("user").info(f"Queue was started", type="queue")
        self.emit("message", {"type": "status", "message": True})

    def stop(self):
        """Stop the queue

        Emits a message that the queue has stopped
        """
        self._running = False
        log.get("user").info(f"Queue was paused", type="queue")
        self.emit("message", {"type": "status", "message": False})

    def clear(self, arr):
        """Clear the queue"""
        if arr == "stack":
            for i in self._stack:
                i.remove()

            self._stack = []

        elif arr == "finished":
            for i in self._finished:
                i.remove()

            self._finished = []

        self.emit("message", {"type": "clear", "message": "Queue Cleared"})

    def remove(self, uuid):
        """Remove an item from the queue

        Args:
            uuid (uuid): The queue item uuid to remove
        """
        item = None
        for i in self._stack:
            if i.uid == uuid:
                item = i
                break

        if item:
            item.remove()
            self._stack.remove(item)

            self.emit(
                "message", {"type": "remove", "message": "Actor removed", "uid": uuid},
            )

            return True

    def move_item(self, uid, position):
        """Move a queue item to a different position
    
        Args:
            uid (uuid): The queue item uuid to move
            position (int): The new queue item position
        """
        item = None
        for i in self._stack:
            if i.uid == uid:
                item = i

        if item:
            self._stack.remove(item)
            self._stack.insert(position, item)

            self.emit(
                "message", {"type": "move", "message": "Actor moved", "uid": uid},
            )

            return True

    def _create_pause(self):
        """Create a pause Actor
        Return:
            actor (obj): An instance of an `Actor`
        """

        def cb(*args, **kwargs):
            pass

        pause = PauseActor(
            uid=str(uuid.uuid4()), name="pause", started=cb, error=cb, finished=cb
        )
        pause.prepare()
        return pause

    def add_pause(self):
        """Add a pause item to the queue"""
        pause = self._create_pause()

        self.push(pause)
        return pause.uid

    def add_sleep(self, sleep_time):
        """Add a sleep item to the queue"""

        def cb(*args, **kwargs):
            pass

        sleep = SleepActor(
            uid=str(uuid.uuid4()), name="sleep", started=cb, error=cb, finished=cb
        )
        sleep.prepare(time=sleep_time)

        self.push(sleep)
        return sleep.uid

    def update_queue(self, **kwargs):
        """Update a queue setting"""
        if kwargs["setting"] == "pause_on_fail":
            try:
                val = bool(kwargs["value"])
            except ValueError:
                logger.error(
                    f"Value {kwargs['pause_on_fail']} is not a valid value for pause_on_fail"
                )
            else:
                self._pause_on_fail = val
                self.emit(
                    "message", {"type": "update", "message": "Setting updated"},
                )

                return True

    def status(self, **kwargs):
        """Return the queue status

        Returns the queue status and a list of the queue stack

        Returns:
            running (bool): Whether the queue is running
            stack (list(dict)): The list of queued actors
            current (dict): The currently running actor
            all (list(dict)): The list of finished, running, and queued actors

        """
        stack = []
        for s in self._stack:
            stack.append({**s.info(), "status": "queued"})

        finished = []
        for f in self._finished:
            info = f.info()
            finished.append(
                {**info, "status": "failed" if info["failed"] else "finished"}
            )

        cur = None
        if self._running_actor:
            cur = {**self._running_actor.info(), "status": "running"}

        allq = ([cur] if cur else []) + stack

        #if kwargs.get("status") == "Finished":
        allq = finished + allq

        return {
            "running": self._running,
            "stack": stack,
            "current": cur,
            "all": allq,
            "pause_on_fail": self._pause_on_fail,
        }

    def kill(self):
        """Kill the currently running actor"""
        if self._greenlet:
            act = self._running_actor
            self._greenlet.kill(exception=ComponentActorKilled)
            return act.uid

    def launch(self):
        """Launch the queue background task"""
        logger.debug("Starting Queue background task")
        self._task = gevent.spawn(self.process)

    def process(self):
        """Queue Task

        Iterate through the stack, pausing as needed
        The queue is automatically paused when the stack is empty

        """
        while True:
            if self._running_actor is None and self._running:
                if len(self._stack) > 0:
                    self._running_actor = self._stack.pop(0)

                    # Pause the queue on a stop action
                    if isinstance(self._running_actor, PauseActor):
                        self.stop()

                    log.get("user").info(
                        f"Queue processing new actor {self._running_actor.name} {self._running_actor.uid}",
                        type="queue",
                    )
                    self.emit(
                        "message",
                        {
                            "type": "start",
                            "message": "Starting Actor",
                            "uid": self._running_actor.uid,
                        },
                    )
                    logger.debug(
                        "Executing queued actor {name} {uid}".format(
                            name=self._running_actor.name, uid=self._running_actor.uid
                        )
                    )

                    try:
                        self._greenlet = gevent.spawn(self._running_actor.execute)
                        self._greenlet.join()

                        error = self._greenlet.value
                        if error:
                            raise error

                        log.get("user").info(
                            f"Actor finished {self._running_actor.name} {self._running_actor.uid}",
                            type="queue",
                        )
                        self.emit(
                            "message",
                            {
                                "type": "end",
                                "message": "Actor Finished",
                                "uid": self._running_actor.uid,
                            },
                        )

                    except Exception as e:
                        log.get("user").exception(
                            f"Error running actor: {self._running_actor.name} {self._running_actor.uid}, {e}",
                            type="queue",
                        )
                        self.emit(
                            "message",
                            {
                                "type": "error",
                                "message": str(e),
                                "uid": self._running_actor.uid,
                            },
                        )

                        if self._pause_on_fail:
                            logger.warning("Queued actor failed, pausing queue")
                            self._running = False
                        else:
                            if len(self._stack) > 0:
                                logger.warning(
                                    "Queued actor failed, waiting for 30s then continuing"
                                )
                                sleep = self.add_sleep(30)
                                self.move_item(sleep, 0)

                    if not isinstance(
                        self._running_actor, PauseActor
                    ) and not isinstance(self._running_actor, SleepActor):
                        self._finished.append(self._running_actor)
                    self._running_actor = None
                    self._greenlet = None

                # stop queue when empty
                else:
                    self._running = False
                    log.get("user").info(f"Queue completed, pausing", type="queue")
                    self.emit(
                        "message",
                        {"type": "finished", "message": "Queue Finished, Pausing"},
                    )
            time.sleep(1)
