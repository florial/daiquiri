#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import ValidationError, INCLUDE

from daiquiri.core import CoreBase
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.utils import loader
from daiquiri.resources.utils import load_config
from daiquiri.core.exceptions import InvalidYAML

import logging

logger = logging.getLogger(__name__)


class Hardware(CoreBase):
    """Core Hardware Control 

    This initialises all hardware defined in hardware.yml, first loading the protocol
    handlers, then initialising each object with the relevant handler. A list of groups
    is built from the yaml, as well as a list of object types along with their schema

    """

    _blueprint = False
    _handlers = {}
    _objects = []
    _types = {}
    _groups = []

    def setup(self):
        yml = "hardware.yml"

        try:
            self._config = load_config(yml)
        except IOError:
            logger.warning(
                "Configuration file missing, hardware component not configured"
            )
        else:
            for h in self._config["protocols"]:
                logger.debug("Loading handler {hand}".format(hand=h))
                self._handlers[h["id"]] = loader(
                    "daiquiri.core.hardware", "Handler", h["type"], **h
                )

            hoschema = HOConfigSchema()
            ids = []
            for oid, obj in enumerate(self._config["objects"]):
                try:
                    hoschema.load(obj, unknown=INCLUDE)
                except ValidationError as err:
                    raise InvalidYAML(
                        {
                            "message": "Hardware object definition is invalid",
                            "file": yml,
                            "obj": obj,
                            "errors": err.messages,
                        }
                    )

                if obj["protocol"] in self._handlers:
                    if obj["id"] in ids:
                        raise InvalidYAML(
                            {
                                "message": f'Hardware object id: {obj["id"]} is not unique',
                                "file": yml,
                                "obj": obj,
                            }
                        )
                    ids.append(obj["id"])

                    if self.get_object(obj["id"]):
                        continue

                    o = self._handlers[obj["protocol"]].get(**obj)
                    self._objects.append(o)

                    if not (o.type() in self._types):
                        self._types[o.type()] = o.schema().__class__.__name__
                    self._schema.register(o.schema())

                else:
                    raise Exception(
                        f'No protcol handler {obj["protocol"]} for {obj["name"]}'
                    )

            self._groups = self._config["groups"].copy()
            for gid, g in enumerate(self._groups):
                for oid in g["objects"]:
                    obj = self.get_object(oid)
                    if obj is None:
                        raise InvalidYAML(
                            {
                                "message": f'Could not find object: {oid} for group: {g["name"]}',
                                "file": yml,
                                "obj": g,
                            }
                        )

    def reload(self):
        self.setup()
        ids = [o["id"] for o in self._config["objects"]]
        removed = [o for o in self.get_objects() if o.id() not in ids]
        for obj in removed:
            self._objects.remove(obj)
            logger.info(f"Removing {obj.id()}")

    def get_objects(self, *args, **kwargs):
        """Get a list of hardware objects

        Kwargs:
            type (str): Filter the list of objects by a type (i.e. motor)
            group (int): Filter the objects by a group id

        Returns:
            A list of objects
        """
        objs = self._objects

        ty = kwargs.get("type", None)
        if ty:
            objs = filter(lambda obj: obj.type() == ty, objs)

        group = kwargs.get("group", None)
        if group:
            group = self.get_group(group)
            objs = filter(lambda obj: obj.id() in group["objects"], objs)

        return objs

    def get_object(self, objectid):
        """Get a specific object

        Args:
            objectid (str): The object id

        Returns
            The object
        """
        for o in self._objects:
            if o.id() == objectid:
                return o

    def get_types(self):
        """Return the dict of object types loaded and their schema"""
        return self._types

    def get_groups(self, groupid=None, objectid=None):
        """Get the list of groups loaded"""
        groups = self._groups
        ret = []
        for g in groups:
            g["state"] = True
            for o in g["objects"]:
                obj = self.get_object(o)
                g["state"] = g["state"] and obj.online() and obj.state_ok()

            #  TODO: optimise me
            if groupid:
                if groupid == g["groupid"]:
                    return g

            if objectid:
                if objectid in g["objects"]:
                    ret.append(g)
            else:
                ret.append(g)

        return ret

    def get_group(self, groupid):
        """Get a specific group

        Args:
            groupid (int): The groupid to return details for

        Returns:
            The group for the groupid
        """
        for g in self._groups:
            if g["groupid"] == groupid:
                return g
