#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bliss.config.static import get_config
from bliss.common import event

from daiquiri.core.hardware.abstract import MappedHardwareObject, HardwareObject
from daiquiri.core.utils import get_nested_attr

import logging

logger = logging.getLogger(__name__)


class BlissObject(MappedHardwareObject):
    _protocol = "bliss"
    _online = True

    def __init__(self, obj=None, **kwargs):
        super().__init__(**kwargs)

        if obj is None:
            c = get_config()
            self._object = c.get(kwargs["address"])
        else:
            self._object = obj

        for p in self.property_map.values():
            # TODO: workaround for repr of tango_shutter being broke, remove me!
            try:
                logger.debug("connecting to {p} {obj}".format(p=p, obj=self._object))
            except:
                pass
            event.connect(self._object, p, self._event)

    def _event(self, value, *args, **kwargs):
        # logger.debug('BlissMotor._event {v} {kw}'.format(v=value, kw=kwargs))
        # Not sure why this happens with s1ho?
        if not value:
            return

        for p, v in self.property_map.items():
            if kwargs["signal"] == v:
                self._update(p, value)

    def _do_set(self, prop, value):
        return setattr(self._object, self.property_map[prop], value)

    def _do_get(self, prop):
        return get_nested_attr(self._object, self.property_map[prop])

    def _do_call(self, function, value, **kwargs):
        fn = getattr(self._object, self.callable_map[function])
        if value is None:
            return fn(**kwargs)
        else:
            return fn(value, **kwargs)


class BlissDummyObject(HardwareObject):
    """Dummy Bliss Object

    Used when an object cannot be retrieved from Beacon
    """

    _type = "unknown"
    _protocol = "bliss"
    _online = False

    def _call(self, *args, **kwargs):
        pass

    def _get(self, *args, **kwargs):
        pass

    def _set(self, *args, **kwargs):
        pass
