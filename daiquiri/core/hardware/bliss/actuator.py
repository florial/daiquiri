#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math

from daiquiri.core.hardware.abstract.actuator import Actuator as AbstractActuator
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Actuator(BlissObject, AbstractActuator):
    property_map = {"state": "state"}

    callable_map = {"move_in": "open", "move_out": "close", "toggle": "toggle"}
