#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.volpi import Volpi as AbstractVolpi, VolpiStates
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Volpi(BlissObject, AbstractVolpi):
    property_map = {
        "intensity": "intensity",
    }

    def _get_state(self):
        return VolpiStates[0]
