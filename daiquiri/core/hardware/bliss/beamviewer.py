#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.beamviewer import (
    Beamviewer as AbstractBeamviewer,
    BeamviewerStates,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class BeamviewerTranslator(HardwareTranslator):
    def from_state(self, value):
        return value


class Beamviewer(BlissObject, AbstractBeamviewer):
    translator = BeamviewerTranslator

    property_map = {
        "led": "led_status",
        "foil": "foil_status",
        "screen": "screen_status",
        "diode_range": "diode_range",
        "diode_ranges": "diode_range_available",
    }

    def _get_state(self):
        return self._object._bpm._cam_proxy.state()

    def _call_led(self, value, **kwargs):
        if value:
            self._object.led_on()
        else:
            self._object.led_off()

    def _call_foil(self, value, **kwargs):
        if value:
            self._object.foil_in()
        else:
            self._object.foil_out()

    def _call_screen(self, value, **kwargs):
        if value:
            self._object.screen_in()
        else:
            self._object.screen_out()

    def _call_current(self, value, **kwargs):
        return self._object.current
