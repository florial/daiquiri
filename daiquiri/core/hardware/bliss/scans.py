#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
import numpy as np
import math
import mmh3

from bliss.config.settings import scan as rdsscan
from bliss.data.node import _get_or_create_node, get_nodes
from bliss.data.scan import watch_session_scans
from bliss.data.nodes.scan_group import GroupScanNode

from daiquiri.core.hardware.bliss.helpers import get_data_from_file

from daiquiri.core.hardware.abstract.scansource import ScanSource, ScanStates
from daiquiri.core.utils import get_start_end, make_json_safe

import logging

logger = logging.getLogger(__name__)


class BlissScans(ScanSource):
    def __init__(self, session, config):
        self._config = config
        self._session_name = session
        logger.info("Initialising Bliss Scan Watcher")

        self._new_scan_watchers = []
        self._new_data_watchers = []
        self._end_scan_watchers = []

        self._session = _get_or_create_node(self._session_name, node_type="session")
        self._task = gevent.spawn(
            watch_session_scans,
            self._session_name,
            self._scan_start,
            self._scan_child,
            self._scan_data,
            self._scan_end,
        )

    def _scanid(self, node):
        return mmh3.hash(node) & 0xFFFFFFFF

    def _scan_start(self, info, *args, **kwargs):
        self._npoints = {}
        for cb in self._new_scan_watchers:
            cb(self._scanid(info["node_name"]), info["type"], info["title"])

    def _scan_end(self, info, *args, **kwargs):
        for cb in self._end_scan_watchers:
            cb(self._scanid(info["node_name"]))

    def _scan_child(self, *args, **kwargs):
        pass

    def _scan_data(self, dims, channel, details):
        if not ("data" in details):
            return

        if dims == "0d":
            # Data is accumulated
            self._npoints["0d"] = min(len(arr) for arr in details["data"].values())
        else:
            # Only new data is provided
            key = details["channel_data_node"].db_name
            self._npoints.setdefault(key, 0)
            self._npoints[key] += len(details["data"])

        min_points = min(n for n in self._npoints.values())

        try:
            progress = min_points / details["scan_info"]["npoints"] * 100
        except (ZeroDivisionError, KeyError):
            progress = 0

        for cb in self._new_data_watchers:
            cb(self._scanid(details["scan_info"]["node_name"]), channel, progress)

    def _get_scan_nodes(self):
        db_names = rdsscan(
            f"{self._session.name}:*_children_list",
            count=1000000,
            connection=self._session.db_connection,
        )
        return (
            node
            for node in get_nodes(
                *(db_name.replace("_children_list", "") for db_name in db_names)
            )
            if node is not None and node.type == "scan"
        )

    def _get_state(self, state):
        """Convert bliss scans states to abstract scan state"""
        state_map = {0: 0, 1: 1, 2: 2, 3: 2, 4: 3, 5: 4, 6: 5}
        try:
            return ScanStates[state_map[state]]
        except KeyError:
            logger.error(f"No state mapping for scan state {state}")
            return None

    def _get_shape(self, info):
        """Return scan shape
    
        This is very bliss specific, maybe generalise in the future
        """
        shape = {}
        for k in ["npoints1", "npoints2", "dim", "requests"]:
            shape[k] = info.get(k)
        return shape

    def get_scans(self, scanid=None, **kwargs):
        scans = []

        if self._session is None:
            return scans

        nodes = list(self._get_scan_nodes())
        nodes = sorted(nodes, key=lambda k: k.info.get("start_timestamp", 0))
        nodes.reverse()
        paging = get_start_end(kwargs, points=len(nodes))

        if scanid:
            filtered = nodes
        else:
            filtered = nodes[paging["st"] : paging["en"]]

        for scan in filtered:
            if scanid:
                if self._scanid(scan.db_name) != scanid:
                    continue

            info = scan.info.get_all()
            try:
                info["node_name"]
            except KeyError:
                logger.exception(f"No node_name for scan {scan.db_name}")
                continue

            if scanid:
                if self._scanid(info["node_name"]) != scanid:
                    continue

            sobj = {"scanid": self._scanid(info["node_name"])}
            for k in [
                "count_time",
                "node_name",
                "npoints",
                "filename",
                "end_timestamp",
                "start_timestamp",
                "title",
                "type",
            ]:
                sobj[k] = info.get(k)

            sobj["status"] = self._get_state(info["state"])
            sobj["shape"] = self._get_shape(info)

            if isinstance(scan, GroupScanNode):
                sobj["group"] = True

                children = []
                for child in scan.iterator.walk(filter="node_ref_channel", wait=False):
                    child_scans = child.get(0, -1)

                    for child_scan in child_scans:
                        child_scan_info = child_scan.info.get_all()
                        children.append(
                            {
                                "scanid": self._scanid(child_scan_info["node_name"]),
                                "type": child_scan_info["type"],
                            }
                        )

                sobj["children"] = children

            else:
                try:
                    sobj["estimated_time"] = info["estimation"]["total_time"]
                except KeyError:
                    sobj["estimated_time"] = 0

                xs = []
                ys = {"images": [], "scalars": [], "spectra": []}
                for k, el in info["acquisition_chain"].items():
                    mast = el["master"]
                    for t in ["images", "scalars", "spectra"]:
                        xs.extend(mast[t])

                    for t in ["images", "scalars", "spectra"]:
                        ys[t].extend(el[t])

                sobj["axes"] = {"xs": xs, "ys": ys}

            scans.append(make_json_safe(sobj))

        if scanid:
            if scans:
                return scans[0]
        else:
            return {"total": len(nodes), "rows": scans}

    def get_scan_data(self, scanid, **kwargs):
        if self._session is None:
            return {}

        for scan in self._get_scan_nodes():
            if self._scanid(scan.db_name) != scanid:
                continue

            sobj = {"data": {}, "info": {}}
            info = scan.info.get_all()

            try:
                info["node_name"]
            except KeyError:
                logger.exception(f"No node_name for scan {scan.db_name}")
                continue

            min_points = self._get_available_points(scan)
            sobj["npoints_avail"] = min_points

            paging = get_start_end(kwargs, points=min_points, last=True)
            for k in ["page", "pages", "per_page"]:
                sobj[k] = paging[k]

            sobj["scanid"] = self._scanid(scan.db_name)
            sobj["npoints"] = info.get("npoints")
            sobj["shape"] = self._get_shape(info)

            xs = []
            ys = {"images": [], "scalars": [], "spectra": []}
            for k, el in info["acquisition_chain"].items():
                mast = el["master"]
                for t in ["images", "scalars", "spectra"]:
                    xs.extend(mast[t])

                for t in ["images", "scalars", "spectra"]:
                    ys[t].extend(el[t])

            sobj["axes"] = {"xs": xs, "ys": ys}

            scalarid = 0
            scalars = kwargs.get("scalars", [])
            hdf5_data = None
            for node in scan.iterator.walk(filter="channel", wait=False):
                sobj["data"][node.name] = {
                    "name": node.name,
                    "shape": node.info["shape"],
                }

                data = np.array([])
                if len(node.info["shape"]) == 0 and paging["en"] > paging["st"]:
                    if (
                        node.name not in scalars
                        and node.name not in xs
                        and not kwargs.get("all_scalars")
                        and (
                            (
                                len(scalars) == 0
                                and (node.name.startswith("timer") or scalarid >= 2)
                            )
                            or len(scalars) > 0
                        )
                    ):
                        continue

                    try:
                        if hdf5_data is not None:
                            raise RuntimeError("Getting all data from hdf5")

                        data = node.get_as_array(paging["st"], paging["en"])
                    except (RuntimeError, IndexError, TypeError):
                        if hdf5_data is None:
                            try:
                                hdf5_data, points = get_data_from_file(scan.name, info)
                                logger.info(f"Retrieving data from hdf5")
                                logger.debug(f"Available keys: {hdf5_data.keys()}")
                                sobj["npoints_avail"] = points
                                paging = get_start_end(kwargs, points=points, last=True)
                            except OSError:
                                hdf5_data = {}
                                logger.exception("Could not read hdf5 file")
                        if node.name in hdf5_data:
                            data = hdf5_data[node.name][paging["st"] : paging["en"]]
                        else:
                            logger.exception(
                                f"Couldnt get paged scan data for {node.db_name}. Requested {paging['st']} to {paging['en']}, node length {len(node)}"
                            )

                # TODO: convert nan -> None
                # TODO: Make sure the browser doesnt interpret at infinity (1e308)
                data = np.nan_to_num(data, posinf=1e200, neginf=-1e200)
                sobj["data"][node.name]["data"] = data

                if not node.name.startswith("timer") and node.name not in xs:
                    scalarid += 1

            sobj = make_json_safe(sobj)
            return sobj

    def _get_available_points(self, scan):
        """TODO: This is problematic because len(node) actually has to
            retrieve the data, for a scan with ~2000 ish points it takes
            of the order of 500ms
        """
        import time

        start = time.time()
        shortest = None
        min_points = math.inf
        for node in scan.iterator.walk(filter="channel", wait=False):
            if len(node) < min_points:
                shortest = node.name
                min_points = len(node)

        if min_points == math.inf:
            min_points = 0

        took = time.time() - start
        logger.debug(f"_get_available_points {shortest} {min_points} took: {took} s")

        return min_points

    def get_scan_spectra(self, scanid, point=0, allpoints=False):
        for scan in self._get_scan_nodes():
            if self._scanid(scan.db_name) != scanid:
                continue

            info = scan.info.get_all()
            try:
                info["node_name"]
            except KeyError:
                logger.exception(f"No node_name for scan {scan.db_name}")
                continue

            min_points = self._get_available_points(scan)

            hdf5_data = None
            spectra = {}
            for node in scan.iterator.walk(filter="channel", wait=False):
                if len(node.info["shape"]) > 0:
                    data = np.array([])
                    try:
                        if hdf5_data is not None:
                            raise RuntimeError("Getting all spectra from hdf5")

                        data = (
                            node.get_as_array(0, to_index=int(info.get("npoints", 0)))
                            if allpoints
                            else np.array([node.get_as_array(point)])
                        )
                    except (RuntimeError, IndexError, TypeError):
                        if hdf5_data is None:
                            try:
                                hdf5_data, points = get_data_from_file(
                                    scan.name, info, type="spectrum"
                                )
                                logger.info(
                                    f"Retrieving data from hdf5: {hdf5_data.keys()}"
                                )
                            except OSError:
                                hdf5_data = {}
                                logger.exception("Could not read hdf5 file")

                        if node.name in hdf5_data:
                            if allpoints:
                                data = hdf5_data[node.name]
                            else:
                                if point < len(hdf5_data[node.name]):
                                    data = np.array([hdf5_data[node.name][point]])
                                else:
                                    logger.warning(
                                        f"Couldnt get scan spectra for {node.db_name}. Requested point {point} outside range {len(hdf5_data[node.name])}"
                                    )
                                    return None
                        else:
                            logger.exception(
                                f"Couldnt get scan spectra for {node.db_name}. Requested 0 to {info.get('npoints')}, node length {len(node)}"
                            )
                            return None

                    spectra[node.name] = {
                        "data": data,
                        "name": node.name,
                    }

            return make_json_safe(
                {
                    "scanid": scanid,
                    "data": spectra,
                    "npoints": info.get("npoints"),
                    "npoints_avail": min_points,
                    "conversion": {
                        "zero": self._config["mca"]["conversion"]["zero"],
                        "scale": self._config["mca"]["conversion"]["scale"],
                    },
                }
            )

    def get_scan_image(self, scanid, node_name, image_no):
        for scan in self._get_scan_nodes():
            if self._scanid(scan.db_name) != scanid:
                continue

            info = scan.info.get_all()
            try:
                info["node_name"]
            except KeyError:
                logger.exception(f"No node_name for scan {scan.db_name}")
                continue

            for node in scan.iterator.walk(filter="lima", wait=False):
                if node_name != node.name:
                    continue

                view = node.get(image_no, image_no)

                try:
                    return view.get_image(image_no)
                except RuntimeError:
                    logger.exception("Could not load image")

    def watch_new_scan(self, fn):
        assert callable(fn)
        if not (fn in self._new_scan_watchers):
            self._new_scan_watchers.append(fn)
        else:
            logger.warning(
                "Function {f} is already subscribed to new scan".format(f=fn)
            )

    def watch_new_data(self, fn):
        assert callable(fn)
        if not (fn in self._new_data_watchers):
            self._new_data_watchers.append(fn)
        else:
            logger.warning(
                "Function {f} is already subscribed to new data".format(f=fn)
            )

    def watch_end_scan(self, fn):
        assert callable(fn)
        if not (fn in self._end_scan_watchers):
            self._end_scan_watchers.append(fn)
        else:
            logger.warning(
                "Function {f} is already subscribed to end scan".format(f=fn)
            )
