#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import ValidationError, fields
from bliss.config.static import get_config

from daiquiri.core.hardware.abstract import ProtocolHandler
from daiquiri.core.hardware.bliss.object import BlissDummyObject
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.utils import loader

import logging

logger = logging.getLogger(__name__)

bl = logging.getLogger("bliss")
bl.disabled = True

bl = logging.getLogger("bliss.common.mapping")
bl.disabled = True


class BlissHOConfigSchema(HOConfigSchema):
    """The Bliss Hardware Object Config Schema"""

    address = fields.Str(required=True, description="Beacon object id")


class BlissHandler(ProtocolHandler):
    """The bliss protocol handler

    Returns an instance of an abstracted bliss object

    The bliss protocol handler first checks the kwargs conform to the BlissHOConfigSchema
    defined above. This address is used to retrieve the  bliss object. Its class is then mapped 
    to an abstract class and a bliss specific instance is created (see hardware/bliss/motor.py)
    """

    _class_map = {
        "Axis": "motor",
        "ModuloAxis": "motor",
        "MockupAxis": "motor",
        "NoSettingsAxis": "motor",
        "Actuator": "actuator",
        "MultiplePositions": "multiposition",
        "TangoShutter": "shutter",
        "Shutter": "shutter",
        "EBV": "beamviewer",
        "ChannelFromConfig": "channelfromconfig",
        "volpi": "volpi",
    }

    def get(self, **kwargs):
        try:
            BlissHOConfigSchema().load(kwargs)
        except ValidationError as err:
            raise InvalidYAML(
                {
                    "message": "Bliss hardware object definition is invalid",
                    "file": "hardware.yml",
                    "obj": kwargs,
                    "errors": err.messages,
                }
            ) from err

        config = get_config()
        try:
            kwargs["obj"] = config.get(kwargs.get("address"))
        except Exception:
            logger.error(f"Couldnt get bliss object {kwargs.get('address')}")
            return BlissDummyObject(**kwargs)

        else:
            cls = kwargs["obj"].__class__.__name__
            if cls in self._class_map:
                return loader(
                    "daiquiri.core.hardware.bliss", "", self._class_map[cls], **kwargs
                )
            else:
                raise Exception("No class found for {cls}".format(cls=cls))
