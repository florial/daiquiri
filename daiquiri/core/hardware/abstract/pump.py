#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

PumpStates = ["ON", "OFF", "UNKNOWN", "ERROR"]


class PumpPropertiesSchema(HardwareSchema):
    state = OneOf(PumpStates, readOnly=True)
    status = fields.Str(readOnly=True)
    pressure = fields.Float(readOnly=True)
    voltage = fields.Float(readOnly=True)
    current = fields.Float(readOnly=True)


class PumpCallablesSchema(HardwareSchema):
    on = RequireEmpty()
    off = RequireEmpty()


class Pump(HardwareObject):
    _type = "pump"
    _state_ok = [PumpStates[0], PumpStates[1]]

    _properties = PumpPropertiesSchema()
    _callables = PumpCallablesSchema()
