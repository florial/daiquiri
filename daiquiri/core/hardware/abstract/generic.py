#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

GenericStates = ["ON", "OFF", "UNKNOWN", "ERROR"]


class GenericPropertiesSchema(HardwareSchema):
    state = OneOf(GenericStates, readOnly=True)


class GenericCallablesSchema(HardwareSchema):
    pass


class Generic(HardwareObject):
    _type = "generic"
    _state_ok = [GenericStates[0]]

    _properties = GenericPropertiesSchema()
    _callables = GenericCallablesSchema()
