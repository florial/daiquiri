#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty

import logging

logger = logging.getLogger(__name__)

MultipositionStates = ["MOVING", "READY", "UNKNOWN", "ERROR"]


class MultipositionAxis(Schema):
    axis = fields.Str()
    destination = fields.Float()
    tolerance = fields.Float()


class MultipositionPosition(Schema):
    position = fields.Str()
    description = fields.Str()
    target = fields.Nested(MultipositionAxis, many=True)


class MultipositionPropertiesSchema(HardwareSchema):
    position = fields.Str(readOnly=True)
    positions = fields.Nested(MultipositionPosition, many=True, readOnly=True)
    state = fields.Str(enum=MultipositionStates, readOnly=True)


class MultipositionCallablesSchema(HardwareSchema):
    move = fields.Str()
    stop = RequireEmpty()


class Multiposition(HardwareObject):
    _type = "multiposition"
    _state_ok = [MultipositionStates[0], MultipositionStates[1]]

    _properties = MultipositionPropertiesSchema()
    _callables = MultipositionCallablesSchema()

    def move(self, value):
        self.call("move", value)

    def stop(self):
        self.call("stop", None)
