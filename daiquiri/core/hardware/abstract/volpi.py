#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields, validate

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

VolpiStates = ["READY", "FAULT"]


class VolpiPropertiesSchema(HardwareSchema):
    intensity = fields.Int(validate=validate.Range(min=0, max=100))
    state = fields.Str(enum=VolpiStates, readOnly=True)


class Volpi(HardwareObject):
    _type = "volpi"
    _state_ok = []

    _properties = VolpiPropertiesSchema()
