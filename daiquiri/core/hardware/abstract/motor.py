#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty

import logging

logger = logging.getLogger(__name__)

MotorStates = ["READY", "MOVING", "FAULT", "UNKNOWN", "LOWLIMIT", "HIGHLIMIT"]


class MotorPropertiesSchema(HardwareSchema):
    position = fields.Float()
    tolerance = fields.Float()
    acceleration = fields.Float()
    velocity = fields.Float()
    limits = fields.List(fields.Float(), length=2)
    state = fields.List(fields.Str(enum=MotorStates), readOnly=True)
    unit = fields.Str(readOnly=True)


class MotorCallablesSchema(HardwareSchema):
    move = fields.Float()
    rmove = fields.Float()
    stop = RequireEmpty()
    wait = RequireEmpty()


class Motor(HardwareObject):
    _type = "motor"
    _state_ok = [MotorStates[0], MotorStates[1]]

    _properties = MotorPropertiesSchema()
    _callables = MotorCallablesSchema()

    def rmove(self, value):
        self.call("rmove", value)

    def move(self, value):
        self.call("move", value)

    def stop(self):
        self.call("stop", None)

    def wait(self):
        self.call("wait", None)
