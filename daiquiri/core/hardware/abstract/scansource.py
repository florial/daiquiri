#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

import logging

logger = logging.getLogger(__name__)

ScanStates = ["CREATED", "PREPARING", "RUNNING", "FINISHED", "ABORTED", "FAILED"]


class ScanSource(ABC):
    """The scan source interface"""

    @abstractmethod
    def get_scans(self, scanid=None):
        """Get a list of scans

        Returns a list of scan dicts, can be filtered to only scanid to return
        details of a specific scan

        Args:
            scanid (int): Scan id to return

        Returns:
            scans (list): A list of valid `ScanSchema` dicts
        """
        pass

    # @marshal_with(ScanDataSchema())
    @abstractmethod
    def get_scan_data(self, scanid, per_page=25, page=1):
        """Return scan data for the specific scan
        
        Args:
            scanid (int): The scan id
            per_page (int): Number of items to return per page
            page (int): Page of data to return

        Returns:
            data (dict): A valid `ScanDataSchema` dict

        """
        pass

    @abstractmethod
    def get_scan_spectra(self, scanid, point=0, allpoints=False):
        """Return a scan specific for the specific scan
        
        Args:
            scanid (int): The scan id
            point (int): The image number to return

        Returns:
            data (dict): A valid `ScanSpectraSchema` dict

        """
        pass

    @abstractmethod
    def get_scan_image(self, scanid, node_name, image_no):
        """Return a scan image for the specific scan
        
        Args:
            scanid (int): The scan id
            node_name (str): The node for the requested image
            image_no (int): The image number to return

        Returns:
            data (dict): A valid `ScanDataSchema` dict

        """
        pass

    @abstractmethod
    def watch_new_scan(self, fn):
        """Method to call when a new scan is started

        Args:
            fn (callable): Callback function with signature cb(scanid, type, title)
        """
        pass

    @abstractmethod
    def watch_new_data(self, fn):
        """Method to call when a new scan is started

        Args:
            fn (callable): Callback function with signature cb(scanid)
        """
        pass

    @abstractmethod
    def watch_end_scan(self, fn):
        """Method to call when a new scan finishes

        Args:
            fn (callable): Callback function with signature cb(scanid)
        """
        pass
