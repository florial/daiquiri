#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields
import numpy as np

from PIL import Image

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)

CameraStatuses = ["READY", "ACQUIRING", "UNKNOWN", "ERROR"]


class CameraPropertiesSchema(HardwareSchema):
    state = fields.Str()
    exposure = fields.Float()
    gain = fields.Float()
    mode = fields.Str(readOnly=True)
    live = fields.Bool()
    width = fields.Int(readOnly=True)
    height = fields.Int(readOnly=True)


class CameraCallablesSchema(HardwareSchema):
    save = fields.Str()


class Camera(HardwareObject):
    _type = "camera"
    _state_ok = [CameraStatuses[0], CameraStatuses[1]]

    _properties = CameraPropertiesSchema()
    _callables = CameraCallablesSchema()

    def frame(self):
        """Return a frame from the camera"""
        return np.array([])

    def _call_save(self, path, type="PNG"):
        """Return a frame from the camera to disk"""
        frame = self.frame()
        if frame is None:
            raise RuntimeError("Could not get frame from camera")

        im = Image.fromarray(self.frame())
        if im.mode != "RGB":
            im = im.convert("RGB")

        im.save(path, type, quality=100)
