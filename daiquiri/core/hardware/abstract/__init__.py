#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
from marshmallow import Schema, fields

from daiquiri.core.schema.hardware import HardwareSchema, HardwareObjectBaseSchema


import logging

logger = logging.getLogger(__name__)


class ProtocolHandler:
    """A protocol handler instantiates a hardware object from a specific protocol

    i.e. initialise a motor object from bliss, or a shutter from tango 
    """

    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def get(self, *args, **kwargs):
        """Get the specific object from the protocol handler.

        This function checks that kwargs conforms to Schema defined for the
        protocol handler (see core/hardware/bliss/__init__.py for a concrete example)

        Returns:
            The hardware object instance for the specific protocol

        """
        pass


class HardwareTranslator:
    """Translate between the controls system specific nomenclature and the abstract one

    The hardware abstraction layer provides a consistent set of property values, use
    a translator to convert between the controls system specific nomenclature and that
    used in the abstraction layer

    """

    def __init__(self, *args, **kwargs):
        self.__dict__.update(("_" + k, v) for k, v in kwargs.items())

    def translate_from(self, prop, value):
        """Translate the value from the controls layer to the abstraction layer
        i.e for getters

        Args:
            prop (str): The property key.
            value: The property value to translate.

        Returns:
            The translated value
        """
        return self._translate("from", prop, value)

    def translate_to(self, prop, value):
        """Translate the value to the controls layer from the abstraction layer
        i.e for setters

        Args:
            prop (str): The property key.
            value: The property value to translate.

        Returns:
            The translated value
        """
        return self._translate("to", prop, value)

    def _translate(self, direction, prop, value):
        fn = "{dir}_{prop}".format(dir=direction, prop=prop)
        if hasattr(self, fn):
            return getattr(self, fn)(value)
        else:
            return value


class HardwareObject(ABC):
    """Base HardwareObject from which all inherit

    The base hardware object defines the objects procotol, type, its properties 
    and callables schema, and mechanisms to subscribe to property changes

    Attributes:
        _object (obj): The instance of the control system object.
        _protocol (str): The protocol for this object, i.e. bliss
        _type (str): The object type, i.e. motor, shutter

    """

    _object = None
    _online = False
    _state_ok = []

    _protocol = None
    _type = None

    _properties = HardwareSchema()
    _callables = HardwareSchema()

    translator = None
    _translator_instance = None

    def schema_name(self):
        ty = self._type
        return ty[0].upper() + ty[1:]

    def __init__(self, *args, **kwargs):
        self._callbacks = {}
        self._online_callbacks = []

        self._id = kwargs.get("id", None)
        self._name = kwargs.get("name", "")
        self._require_staff = kwargs.get("require_staff", False)

        if self.translator:
            self._translator_instance = self.translator(parent=self)

    def get(self, prop):
        """Get a property from a hardware object
            
        First checks the property is defined in the objects
        property schema, then delegates to the local getter 
        implementation _get

        Args:
            prop (str): The property to retreive.

        Returns:
            The property value if the property exists, otherwise
            rasises an exception

        """
        if not self._online:
            return

        if prop in self._properties:
            value = self._get(prop)
            value = self.translate_from(prop, value)

            return value
        else:
            raise KeyError("Unknown property {p}".format(p=prop))

    def set(self, prop, value):
        """Set a property on a hardware object

        First checks the property is defined in the objects
        property schema, then delegates to the local setter 
        implementation _set

        Args:
            prop (str): The property to set.
            value: The property to set.

        Returns:
            The the result from the object setter if the property exists
            otherwise rasises an exception

        """
        if not self._online:
            return

        if prop in self._properties:
            if self._properties.read_only(prop):
                raise AttributeError("Property {p} is read only".format(p=prop))

            value = self._properties.validate(prop, value)
            value = self.translate_to(prop, value)

            return self._set(prop, value)
        else:
            raise KeyError("Unknown property {p}".format(p=prop))

    def call(self, function, value, **kwargs):
        """Calls a function on a hardware object

        First checks the function is defined in the objects
        callables schema, then delegates to the local call 
        implementation _call

        Args:
            function (str): The function to call.
            value: The value to call the function with.

        Returns:
            The the result from the object function if the function exists
            otherwise rasises an exception

        """
        if not self._online:
            return

        if function in self._callables:
            value = self._callables.validate(function, value)
            # try:
            ret = self._call(function, value, **kwargs)
            if ret:
                return ret
            else:
                return True
            # except Exception as e:
            #     return e
        else:
            raise KeyError("Unknown function {fn}".format(fn=function))

    @abstractmethod
    def _get(self, prop):
        """Local implementation of getter
        """
        pass

    @abstractmethod
    def _set(self, prop, value):
        """Local implementation of setter
        """
        pass

    @abstractmethod
    def _call(self, function, value):
        """Local implementation of call
        """
        pass

    def state(self, **kwargs):
        """Gets the current state of a hardware object
        
        Builds a dictionary of the basic info of the object, plus its properties, and
        callables.

        Returns:
            A dict of the hardware object status

        """
        info = {
            k: getattr(self, "_" + k)
            for k in ["name", "type", "id", "protocol", "online"]
        }
        info["callables"] = [k for k in self._callables]
        info["properties"] = {}
        for p in self._properties:
            info["properties"][p] = self.get(p)

        info["properties"] = self._properties.dump(info["properties"])
        info["require_staff"] = self.require_staff()

        return info

    def schema(self):
        """Returns the schema for the current hardware object

        The hardware schema is built from the `HardwareObjectBaseSchema`
        and the object specific _property and _callable schema 
        (both instances of `HardwareSchema`)

        Returns:
            An instance of a schema

        """
        schema = type(
            f"HW{self.schema_name()}Schema",
            (HardwareObjectBaseSchema,),
            {
                "properties": fields.Nested(self._properties.__class__),
                "callables": fields.Nested(self._callables.__class__),
            },
        )
        return schema()

    def set_online(self, state):
        """Set the online state of the device

        Sets the state and execute any registered callbacks

        Args:
            state (boolean): Set the online state
        """
        self._online = state

        for cb in self._online_callbacks:
            cb(self, self._online)

    def subscribe_online(self, fn):
        """Subscribe to the online state of the hardware object
    
        Add a function to a list of callbacks for when the online state of the object change

        Args:
            fn: (:callable) The function to call when this property changes.

        """
        assert callable(fn)
        if not (fn in self._online_callbacks):
            self._online_callbacks.append(fn)

    def id(self):
        return self._id

    def name(self):
        return self._name

    def type(self):
        return self._type

    def object(self):
        return self._object

    def online(self):
        return self._online

    def require_staff(self):
        return self._require_staff

    def state_ok(self):
        """Returns if the current object state is `ok`"""
        state = self.get("state")
        if type(state) == type([]):
            st = False
            for ok in self._state_ok:
                if ok in state:
                    st = True
            return st

        else:
            return state in self._state_ok

    def subscribe(self, prop, fn):
        """Subscribe to property changes on the hardware object
    
        Add a function to a list of callbacks for when properties on the object change

        Args:
            prop (str): The property to subscribe to. Can pass 'all' to subscribe to all changes
            fn: (:callable) The function to call when this property changes.

        """
        assert callable(fn)
        if prop in self._properties or prop == "all":
            if not (prop in self._callbacks):
                self._callbacks[prop] = []

            if not (fn in self._callbacks[prop]):
                self._callbacks[prop].append(fn)

        else:
            raise KeyError("No such property: {p}".format(p=prop))

    def _update(self, prop, value):
        """Internal function to call when a property has changed

        This delegates to all subscribes that a property has changed

        Args:
            prop (str): The property that has changed.
            value: The new value.

        """
        # logger.debug('{c}._update {n} - {p}: {v}'.format(c=self.__class__.__name__, n=self._address, p=prop, v=value))
        value = self.translate_from(prop, value)
        if prop in self._properties:
            if prop in self._callbacks:
                for cb in self._callbacks[prop]:
                    cb(self, prop, value)

            if "all" in self._callbacks:
                for cb in self._callbacks["all"]:
                    cb(self, prop, value)

    def translate_from(self, prop, value):
        if self._translator_instance:
            value = self._translator_instance.translate_from(prop, value)

        return value

    def translate_to(self, prop, value):
        if self._translator_instance:
            value = self._translator_instance.translate_to(prop, value)

        return value


class MappedHardwareObject(HardwareObject):
    """Hardware object that maps properties via a simple dict
    
    HardwareObject that has a simple map between abstract properties and their
    actual properties on the object with fallback to a function on the parent
    """

    property_map = {}
    callable_map = {}

    def _set(self, prop, value):
        """Set a property on the child object
            
        First try from the simple property map which maps properties to attributes
        on the child object. Delegates to _do_set which locally implements the setter

        Second, if not in the map, try calling the function _get_<prop> on the parent

        Args:
            prop (str): The property to set.
            value: Its value.
        """
        if prop in self.property_map:
            self._do_set(prop, value)

        elif hasattr(self, "_set_{prop}".format(prop=prop)):
            getattr(self, "_set_{prop}".format(prop=prop))(value)

        else:
            raise KeyError("Couldnt find a setter for property {p}".format(p=prop))

    @abstractmethod
    def _do_set(self, prop, value):
        """Local implementation of how to set the property on the object
        """
        pass

    def _get(self, prop):
        """Get a property from the child object
            
        First try from the simple property map which maps properties to attributes
        on the child object. Delegates to _do_get which locally implements the getter

        Second, if not in the map, try calling the function _get_<prop> on the parent

        Args:
            prop (str): The property to set.

        Returns:
            The property value
        """
        if prop in self.property_map:
            try:
                return self._do_get(prop)
            except NotImplementedError:
                logger.warning(
                    "Object {id} does not implement {prop}".format(
                        id=self._id, prop=prop
                    )
                )

        elif hasattr(self, "_get_{prop}".format(prop=prop)):
            return getattr(self, "_get_{prop}".format(prop=prop))()

        else:
            raise KeyError("Couldnt find a getter for property {p}".format(p=prop))

    @abstractmethod
    def _do_get(self, prop):
        """Local implementation of how to get the property from the object
        """
        pass

    def _call(self, function, value, **kwargs):
        """Call a function on the child object
            
        First try from the simple function map which maps to function names
        on the child object. Delegates to _do_call which locally implements the getter

        Second, if not in the map, try calling the function _call_<fn> on the parent

        Args:
            function (str): The function to call.
            value: The value to call the function with

        Returns:
            True if function successfully called
        """
        if function in self.callable_map:
            ret = self._do_call(function, value, **kwargs)
        elif hasattr(self, "_call_{fn}".format(fn=function)):
            ret = getattr(self, "_call_{fn}".format(fn=function))(value, **kwargs)

        else:
            raise KeyError("Couldnt find a handler for function {f}".format(f=function))

        if ret is None:
            ret = True
        return True

    @abstractmethod
    def _do_call(self, function, value, **kwargs):
        """
            Local implementation of how to get the function from the object
        """
        pass


class GetterSetterHardwareObject(HardwareObject):
    """HardwareObject that has one map to get properties and another map
    to set them
    """

    property_setter_map = {}
    property_getter_map = {}
    callable_map = {}

    def _set(self, prop, value):
        """Set a property on the child object using the setter map

        Args:
            prop (str): The property to set.
            value: Its value.
        """
        if prop in self.property_setter_map:
            return setattr(self._object, self.property_setter_map[prop])(value)
        else:
            raise KeyError("Couldnt find a setter for property {p}".format(p=prop))

    def _get(self, prop):
        """Get a property on the child object using the getter map

        Args:
            prop (str): The property to set.

        Returns:
            The property value
        """
        if prop in self.property_getter_map:
            return getattr(self._object, self.property_getter_map[prop])()
        else:
            raise KeyError("Couldnt find a getter for property {p}".format(p=prop))

    def _call(self, function, value, **kwargs):
        """Call a function on the child object using the callable map

        Args:
            function (str): The function to call.
            value: The value to call the function with

        Returns:
            True if function successfully called
        """
        if function in self.callable_map:
            ret = getattr(self, self.callable_map[function])(value, **kwargs)
            if ret is None:
                ret = True
            return ret
        else:
            raise KeyError("Couldnt find a handler for function {f}".format(f=function))
