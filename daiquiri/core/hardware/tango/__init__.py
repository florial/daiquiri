#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import ValidationError, fields, Schema

from daiquiri.core.hardware.abstract import ProtocolHandler
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.utils import loader

import logging

logger = logging.getLogger(__name__)


class TangoHOConfig(HOConfigSchema):
    """The Tango Hardware Object Config Schema"""

    type = fields.Str(required=True, description="The object type")
    tango_url = fields.Str(required=True, description="URL to tango device/attribute")


class TangoHandler(ProtocolHandler):
    def get(self, **kwargs):
        try:
            valid = TangoHOConfig().load(kwargs)
        except ValidationError as err:
            raise InvalidYAML(
                {
                    "message": "Tango hardware object definition is invalid",
                    "file": "hardware.yml",
                    "obj": kwargs,
                    "errors": err.messages,
                }
            ) from err

        try:
            return loader(
                "daiquiri.core.hardware.tango", "", kwargs.get("type"), **kwargs
            )
        except ModuleNotFoundError:
            logger.error(
                "Could not find class for tango object {type}".format(
                    type=kwargs.get("type")
                )
            )
            raise
