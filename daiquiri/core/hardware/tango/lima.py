#!/usr/bin/env python
# -*- coding: utf-8 -*-
import struct
import numpy

from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.camera import Camera as AbstractCamera
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


try:
    import cv2
except ImportError:
    logger.warning("opencv not available, will not be able to bayer decode")
    cv2 = None


class LimaTranslator(HardwareTranslator):
    def from_state(self, value):
        vals = {"Ready": "READY", "Running": "ACQUIRING"}

        if value in vals:
            return vals[value]

        return "UNKNOWN"


class Lima(TangoObject, AbstractCamera):
    translator = LimaTranslator

    property_map = {
        "state": "acq_status",
        "exposure": "video_exposure",
        "gain": "video_gain",
        "mode": "video_mode",
        "live": "video_live",
        "height": "image_height",
        "width": "image_width",
    }

    def frame(self):
        self._brightness = 1
        VIDEO_HEADER_FORMAT = "!IHHqiiHHHH"
        HEADER_SIZE = struct.calcsize(VIDEO_HEADER_FORMAT)

        _, raw_data = self._object.video_last_image
        if len(raw_data) > HEADER_SIZE:
            (
                magic,
                header_version,
                image_mode,
                image_frameNumber,
                image_width,
                image_height,
                endian,
                header_size,
                pad0,
                pad1,
            ) = struct.unpack(VIDEO_HEADER_FORMAT, raw_data[:HEADER_SIZE])

            if magic != 0x5644454F or header_version != 1:
                raise IndexError("Bad image header.")
            if image_frameNumber < 0:
                raise IndexError("Image (from Lima live interface) not available yet.")

            video_modes = (numpy.uint8, numpy.uint16, numpy.int32, numpy.int64)
            try:
                mode = video_modes[image_mode]

            except IndexError:
                mode = numpy.uint16

            data = numpy.frombuffer(raw_data[HEADER_SIZE:], dtype=mode)
            data.shape = image_height, image_width

            if cv2:
                if image_mode >= 11 or image_mode <= 14:
                    data = cv2.cvtColor(data, cv2.COLOR_BAYER_RG2RGB)

                data = cv2.convertScaleAbs(data, alpha=self._brightness)
                data = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)

            return data
