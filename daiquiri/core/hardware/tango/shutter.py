#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
from PyTango import DevState

from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.shutter import (
    Shutter as AbstractShutter,
    ShutterStates,
)
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class ShutterTranslator(HardwareTranslator):
    def from_state(self, value):
        val_map = {
            DevState.CLOSE: "CLOSED",
            DevState.OPEN: "OPEN",
            DevState.FAULT: "FAULT",
            DevState.UNKNOWN: "UNKNOWN",
        }
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Shutter(TangoObject, AbstractShutter):
    translator = ShutterTranslator

    property_map = {"state": "state", "status": "status"}

    callable_map = {"open": "Open", "close": "Close", "reset": "Reset"}

    def _get_open_text(self):
        return ""

    def _get_closed_text(self):
        return ""

    def _get_valid(self):
        return True
