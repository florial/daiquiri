#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.valve import Valve as AbstractValve
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class ValveTranslator(HardwareTranslator):
    def from_state(self, value):
        val_map = {
            DevState.OPEN: "OPEN",
            DevState.CLOSE: "CLOSED",
            DevState.UNKNOWN: "UNKNOWN",
            DevState.FAULT: "FAULT",
        }
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Valve(TangoObject, AbstractValve):
    translator = ValveTranslator

    property_map = {"state": "state", "status": "status"}

    callable_map = {"open": "open", "close": "close", "reset": "reset"}
