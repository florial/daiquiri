#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields, validate

from daiquiri.core.hardware.abstract.generic import (
    GenericPropertiesSchema,
    GenericCallablesSchema,
)


class RTSchemaMixin:
    """RunTime Schema Mixin

    Allows the validation schema for the object to be generated at
    run time from the hardware.yml config
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        attrs, ui = self._map_to_schema(kwargs.get("attributes", {}))

        class Meta:
            uischema = ui

        attrs["Meta"] = Meta

        properties_schema = type(
            self.id() + "PropertiesSchema", (GenericPropertiesSchema,), attrs
        )
        self._properties = properties_schema()

        callables_schema = type(
            self.id() + "CallablesSchema", (GenericCallablesSchema,), attrs
        )
        self._callables = callables_schema()

    def schema_name(self):
        return self.id()

    def _map_to_schema(self, attributes):
        attr_map = {
            "int": fields.Int,
            "float": fields.Float,
            "bool": fields.Bool,
            "str": fields.Str,
        }

        attrs = {}
        ui = {}
        for a in attributes:
            if a.get("type") in attr_map:
                field = attr_map[a["type"]]
                val = None
                if a.get("min") is not None and a.get("max") is not None:
                    val = (validate.Range(min=a["min"], max=a["max"]),)

                extra_kws = {}
                if a.get("step"):
                    extra_kws["multipleOf"] = a["step"]

                attrs[a["id"]] = field(
                    validate=val, title=a.get("name", None), **extra_kws
                )

                if a.get("ui_schema"):
                    ui[a.get("id")] = a.get("ui_schema")

                self.property_map[a["id"]] = a["id"]

        return attrs, ui
