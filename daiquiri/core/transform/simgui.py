"""Simulate a GUI based in a Canvas instance
"""

# TODO: this module should go somewhere else

import numpy
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
from matplotlib.backend_bases import MouseButton
from matplotlib.widgets import RadioButtons


class SimGUI:
    """Matplotlib based GUI simulator
    """

    def __init__(self, canvas):
        self.canvas = canvas

    def start(self):
        fig, ax = plt.subplots()
        self._draw_info = {
            "fig": fig,
            "ax": ax,
            "limits": "canvas",
            "fixsample": True,
            "move": True,
            "zoom": 1.2,
            "skipevent": False,
        }
        cid1 = fig.canvas.mpl_connect("button_press_event", self._button_press_event)
        cid2 = fig.canvas.mpl_connect(
            "button_release_event", self._button_release_event
        )
        cid3 = fig.canvas.mpl_connect("scroll_event", self._scroll_event)

        axcolor = "lightgoldenrodyellow"
        rax = plt.axes([0, 0.7, 0.10, 0.15], facecolor=axcolor)
        labels = self.canvas.vlm_to_vi.zoomlevels
        radio1 = RadioButtons(rax, labels, active=labels.index(self.canvas.zoomlevel))
        radio1.on_clicked(self._zoom_event)

        rax = plt.axes([0, 0.5, 0.10, 0.15], facecolor=axcolor)
        labels = ["image", "canvas"]
        radio2 = RadioButtons(
            rax, labels, active=labels.index(self._draw_info["limits"])
        )
        radio2.on_clicked(self._limits_event)

        rax = plt.axes([0, 0.3, 0.10, 0.15], facecolor=axcolor)
        labels = ["vlm", "sample"]
        radio3 = RadioButtons(rax, labels, active=int(self._draw_info["fixsample"]))
        radio3.on_clicked(self._fix_event)

        rax = plt.axes([0, 0.1, 0.10, 0.15], facecolor=axcolor)
        labels = ["scan", "move"]
        radio4 = RadioButtons(rax, labels, active=int(self._draw_info["move"]))
        radio4.on_clicked(self._action_event)

        self._draw()
        plt.show()

    def _button_press_event(self, event):
        if event.button == MouseButton.LEFT:
            self._draw_info["start"] = [event.xdata, event.ydata]

    def _button_release_event(self, event):
        if self._draw_info["skipevent"]:
            self._draw_info["skipevent"] = False
            return
        end = [event.xdata, event.ydata]
        if event.button == MouseButton.LEFT:
            if self._draw_info["move"]:
                self.canvas.move(end)
            else:
                pts = [self._draw_info["start"], end]
                print(f"Scan coordinates: {self.canvas.canvas_to_motor(pts)}")
        elif event.button == MouseButton.RIGHT:
            self.canvas.beamposition = end
        else:
            return
        self._draw()

    def _scroll_event(self, event):
        if event.button == "up":
            inc = 0.1
        else:
            inc = -0.1
        self._draw_info["zoom"] = max(1.0, self._draw_info["zoom"] + inc)
        self._draw()

    def _zoom_event(self, label):
        self.canvas.zoomlevel = label
        self._draw()
        self._draw_info["skipevent"] = True

    def _limits_event(self, label):
        self._draw_info["limits"] = label
        self._draw()
        self._draw_info["skipevent"] = True

    def _fix_event(self, label):
        self._draw_info["fixsample"] = label == "sample"
        self._draw_info.pop("fixed", None)
        self._draw()
        self._draw_info["skipevent"] = True

    def _action_event(self, label):
        self._draw_info["move"] = label == "move"
        self._draw_info["skipevent"] = True

    def _draw(self):
        # Borders
        reach_polyline = self.canvas.reach_polyline
        fine_polyline = self.canvas.fine_polyline
        coarse_polyline = self.canvas.coarse_polyline
        vi_polyline = self.canvas.vi_polyline

        # Markers
        beamposition = self.canvas.beamposition
        sampleposition = self.canvas.sampleposition
        focalpoint = self.canvas.focalpoint
        vi_center = self.canvas.vi_center

        # Part of the sample that the VLM is seeing
        testdata = self._draw_info.get("testdata")
        if testdata is None:
            xmin, ymin = reach_polyline.min(axis=0)
            xmax, ymax = reach_polyline.max(axis=0)
            testdata = self._testimagedata(xmin, xmax, ymin, ymax, 200)
            self._draw_info["testdata"] = testdata
        (left, bottom), (right, top) = self.canvas.vi_corners
        extent = (left, right, bottom, top)
        imshape = tuple(self.canvas.vlm_to_vi.imageshape.tolist())
        x = numpy.linspace(left, right, imshape[0])
        y = numpy.linspace(bottom, top, imshape[1])
        vlmimage = self._testimage(x, y, testdata)

        # Draw objects
        fig = self._draw_info["fig"]
        ax = self._draw_info["ax"]
        ax.clear()

        ax.imshow(vlmimage, extent=extent, interpolation="nearest", origin="lower")
        self._draw_polyline(ax, vi_polyline, edgecolor="black", fill=False, label="VLM")
        ax.scatter(*sampleposition, s=100, marker="x", color="black", label="Sample")
        ax.scatter(*focalpoint, s=100, marker="o", color="red", label="VLM foc")
        ax.scatter(*vi_center, s=100, marker="^", color="blue", label="VLM cen")
        self._draw_polyline(
            ax, reach_polyline, edgecolor="magenta", fill=False, label="Reach"
        )
        self._draw_polyline(
            ax, coarse_polyline, edgecolor="red", fill=False, label="Coarse"
        )
        self._draw_polyline(
            ax, fine_polyline, edgecolor="orange", fill=False, label="Fine"
        )
        ax.scatter(
            *beamposition, s=100, marker="+", linewidths=20, color="black", label="Beam"
        )

        # Axes limits
        limits = self._draw_info["limits"]
        if limits == "image":
            fixed = vi_polyline
        else:
            fixed = reach_polyline
        if self._draw_info["fixsample"]:
            fixed = self._draw_info.setdefault("fixed", fixed)
        xmin, ymin = fixed.min(axis=0)
        xmax, ymax = fixed.max(axis=0)
        dx = (xmax - xmin) * self._draw_info["zoom"]
        dy = (ymax - ymin) * self._draw_info["zoom"]
        xcen = (xmin + xmax) / 2.0
        ycen = (ymin + ymax) / 2.0
        ax.set_xlim(xcen - dx / 2, xcen + dx / 2)
        ax.set_ylim(ycen - dy / 2, ycen + dy / 2)

        # Axes labels, legend and title
        ax.set_xlabel(f"Y({self.canvas.motors_to_world.units})")
        ax.set_ylabel(f"Z({self.canvas.motors_to_world.units})")
        ax.set_aspect("equal", adjustable="box")
        info = self.canvas.current_motor_positions_dict
        title = ", ".join(f"{k}={v}" for k, v in info.items())
        title += "\n"
        align_info = self.canvas.align_info
        title += ", ".join(f"{k}: {v}" for k, v in align_info.items())
        ax.set_title(title)
        ax.legend(loc="upper left", bbox_to_anchor=(1, 1))
        fig.canvas.draw()

    @staticmethod
    def _draw_polyline(ax, verts, **kwargs):
        codes = [Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]
        path = Path(verts, codes)
        patch = patches.PathPatch(path, **kwargs)
        ax.add_patch(patch)

    def _testimage(self, x, y, data):
        x, y = numpy.meshgrid(x, y)
        ret = numpy.zeros(x.shape, dtype=float)
        for x0, y0, sx, sy, angle, A in data:
            particle = self._testimage_particle(x, y, x0, y0, sx, sy, angle, A)
            ret = numpy.maximum(particle, ret)
        return ret

    @staticmethod
    def _testimage_particle(x, y, x0, y0, sx, sy, angle, A):
        angle = numpy.radians(angle)
        cosa = numpy.cos(angle)
        sina = numpy.sin(angle)
        sin2a = numpy.sin(2 * angle)
        varx = sx ** 2
        vary = sy ** 2
        a = cosa ** 2 / (2 * varx) + sina ** 2 / (2 * vary)
        b = -sin2a / (4 * varx) + sin2a / (4 * vary)
        c = sina ** 2 / (2 * varx) + cosa ** 2 / (2 * vary)
        num = a * (x - x0) ** 2 - 2 * b * (x - x0) * (y - y0) + c * (y - y0) ** 2
        return A * numpy.exp(-num)

    def _testimagedata(self, xmin, xmax, ymin, ymax, npeaks):
        # numpy.random.seed(1)
        area = (xmax - xmin) * (ymax - ymin)
        particlearea = area / (npeaks * 10.0)
        sxy = numpy.sqrt(particlearea)
        x0, y0, npeaks = self._testimage_positions(xmin, xmax, ymin, ymax, npeaks)
        sx = numpy.random.uniform(sxy * 0.8, sxy * 1.2, npeaks)
        sy = numpy.random.uniform(sxy * 0.8, sxy * 1.2, npeaks)
        rho = numpy.random.uniform(0, 0.2, npeaks)
        A = numpy.random.uniform(1, 5, npeaks)
        return list(zip(x0, y0, sx, sy, rho, A))

    @staticmethod
    def _testimage_positions(a, b, c, d, npeaks):
        x = numpy.random.uniform(a, b, npeaks)
        y = numpy.random.uniform(c, d, npeaks)
        dr = max(b - a, d - c) * 0.1
        xm = (a + b) / 2.0
        ym = (c + d) / 2.0
        r = ((x - xm) ** 2 + (y - ym) ** 2) ** 0.5
        ind = r > dr
        x = x[ind]
        y = y[ind]
        npeaks = sum(ind)
        return x, y, npeaks
