"""These are the coordinate transformations to relate motors,
VLM image and display canvas for the ImageViewer component source.

All coordinate transformations are done in homogeneous coordinates
"""

# TODO: this module should go somewhere else

import logging
import itertools
import numpy
from daiquiri.core.transform import hw_sets
from daiquiri.core.transform import hw_maps
from daiquiri.core.transform import maps
from daiquiri.core.transform import sets
from daiquiri.core.transform.units import get_exponent

from daiquiri.core.utils import timed

logger = logging.getLogger(__name__)


def tohomogeneous(coord):
    """
    :param array coord: npoints x ndim
    :returns array: npoints x ndim+1
    """
    coord = numpy.atleast_2d(coord)
    ones = numpy.ones((coord.shape[0], 1))
    return numpy.hstack([coord, ones])


def fromhomogeneous(coord):
    """
    :param array coord: npoints x ndim+1
    :returns array: npoints x ndim
    """
    coord = numpy.atleast_2d(coord)
    coord = coord[:, :-1] / coord[:, -1, numpy.newaxis]
    return coord


def polyline(coord):
    """Corners for drawing a boundary based on low and high point

    :param array coord: shape is (2, ndim)
    :param array: (ndim*2^(ndim-1)+(ndim>1), ndim)
    """
    coord = numpy.atleast_2d(coord)
    npoints, ndim = coord.shape
    if npoints != 2:
        raise ValueError("Provide low and high point")
    if ndim == 1:
        return coord
    elif ndim == 2:
        x = coord[[0, 1, 1, 0, 0], 0]
        y = coord[[0, 0, 1, 1, 0], 1]
        return numpy.vstack([x, y]).T
    else:
        raise NotImplementedError


class MotorsToWorld(maps.Map):
    """Maps motors (R^3/R^6) to World (R^4) frame coordinates:

        xworld = x + x_fine
        yworld = y + y_fine
        zworld = z + zfine

    Each motor has its own units and the World frame has its own units as well.
    """

    def __init__(
        self,
        x=None,
        y=None,
        z=None,
        x_fine=None,
        y_fine=None,
        zfine=None,
        units="nm",
        unitdict=None,
    ):
        """
        :param Motor x:
        :param Motor y:
        :param Motor z:
        :param Motor x_fine:
        :param Motor y_fine:
        :param Motor zfine:
        :param str units: the World frame units (nm by default)
        :param dict unitdict: motor units (tries to get them from the hardware when missing)
        """
        motor_labels = []
        self.trnx, labels = self._init_transform(
            x, "x", units, unitdict, motfine=x_fine
        )
        motor_labels += labels
        self.trny, labels = self._init_transform(
            y, "y", units, unitdict, motfine=y_fine
        )
        motor_labels += labels
        self.trnz, labels = self._init_transform(z, "z", units, unitdict, motfine=zfine)
        motor_labels += labels
        self.motor_labels = motor_labels
        self.unitdict = unitdict
        self.units = units
        domain = hw_sets.CompositeMotorDomain(
            [self.trnx.domain, self.trny.domain, self.trnz.domain]
        )
        super().__init__(domain=domain, codomain=None)

    def _init_transform(self, mot, label, units, unitdict, motfine=None):
        """Instantiate a transformation object for coarse+fine motor translation

        :param Motor mot:
        :param str label:
        :param str units: the World frame units
        :param Motor motfine:
        :returns MotorLinearCombination, list(str):
        """
        if mot is None:
            raise ValueError(f"Motor '{label}' is missing")
        if motfine is None:
            labels = [label]
            x = [mot]
            closed = [[True, True]]
        else:
            labels = [label, label + "_fine"]
            x = [mot, motfine]
            closed = [[True, True], [True, True]]
        trn = hw_maps.MotorLinearCombination(
            x, closed=closed, units=units, unitdict=unitdict
        )
        return trn, labels

    @property
    def codomain(self):
        fillvalue = [numpy.nan, numpy.nan, numpy.nan, 1]
        return sets.RealIntervalComposite(
            [
                self.trnx.codomain,
                self.trny.codomain,
                self.trnz.codomain,
                sets.RealVectorSpace(ndim=1, fillvalue=1),
            ],
            fillvalue=fillvalue,
        )

    @property
    def image(self):
        return sets.RealIntervalComposite(
            [
                self.trnx.image,
                self.trny.image,
                self.trnz.image,
                sets.RealVectorSpace(ndim=1, fillvalue=1),
            ]
        )

    @property
    def units(self):
        return self._units

    @units.setter
    def units(self, value):
        self._units = value
        self.trnx.units = value
        self.trny.units = value
        self.trnz.units = value

    def _forward(self, coord):
        """Motor positions to World frame coordinates
    
        :param array coord: shape is (npoints, ndomain)
        :returns array: shape is (npoints, ncodomain)
        """
        n1 = self.trnx.ndomain
        n2 = n1 + self.trny.ndomain
        icoordx = self.trnx.forward(coord[:, :n1])
        icoordy = self.trny(coord[:, n1:n2])
        icoordz = self.trnz.forward(coord[:, n2:])
        ones = numpy.ones((icoordx.shape[0], 1))
        return numpy.hstack([icoordx, icoordy, icoordz, ones])

    def _inverse(self, icoord):
        """World frame coordinates to motor position

        :param num or array-like icoord: shape is (npoints, ncodomain)
        :returns array-like: shape is (npoints, ndomain)
        """
        icoord = fromhomogeneous(icoord)
        coordx = self.trnx.inverse(icoord[:, 0, numpy.newaxis])
        coordy = self.trny.inverse(icoord[:, 1, numpy.newaxis])
        coordz = self.trnz.inverse(icoord[:, 2, numpy.newaxis])
        return numpy.hstack([coordx, coordy, coordz])

    def motor_world_limits(self, motname):
        """Motor limits in the World frame (low, high)

        :param str motname:
        :returns array: shape is (2, ncodomain-1)
        """
        axis = self.domain.motor_index(motname)
        n1 = self.trnx.ndomain
        n2 = n1 + self.trny.ndomain
        if axis < n1:
            trn = self.trnx
        elif axis >= n1 and axis < n2:
            trn = self.trny
        else:
            trn = self.trnz
        return trn.codomain_limits(motname)

    def motor_world_index(self, motname):
        """Motor index in the World frame

        :param str motname:
        :returns int:
        """
        axis = self.domain.motor_index(motname)
        n1 = self.trnx.ndomain
        n2 = n1 + self.trny.ndomain
        if axis < n1:
            return 0
        elif axis >= n1 and axis < n2:
            return 1
        else:
            return 2

    @property
    def world_limits(self):
        """World limits in the World frame

        :returns array: shape is (2, ncodomain-1)
        """
        limits = self.image.limits
        limits = [limits.low.tolist()[:-1], limits.high.tolist()[:-1]]
        return numpy.array(limits)

    @property
    # @timed
    def current_world_position(self):
        """
        :returns array: shape is (ncodomain,)
        """
        pos = self.forward(self.domain.current_position)
        return fromhomogeneous(pos)[0]

    @property
    def centered_position(self):
        """Current motor position with the fine axes centered
        """
        x = self._center_fine(self.trnx)
        y = self._center_fine(self.trny)
        z = self._center_fine(self.trnz)
        return numpy.array(x + y + z)

    @staticmethod
    def _center_fine(trn):
        pmotors = trn.current_domain_position
        if trn.ndomain == 1:
            return pmotors.tolist()
        else:
            pworld = trn.forward(pmotors)[0, 0]
            pcen = trn.domain.center
            pcoeff = trn.coefficients.tolist()
            return [(pworld - pcoeff[1] * pcen[1]) / pcoeff[0], pcen[1]]

    @property
    def motor_names(self):
        return [mot.id() for mot in self.domain.motors]

    def motor_name(self, label):
        idx = self.motor_labels.index(label)
        return self.motor_names[idx]

    def motor_label(self, name):
        idx = self.motor_names.index(name)
        return self.motor_labels[idx]

    @property
    def current_motor_positions_dict(self):
        ret = self.domain.current_position_dict
        return {
            label: ret[name] for name, label in zip(self.motor_names, self.motor_labels)
        }


class WorldToSample(maps.Isomorphism):
    """Maps World (R^4) to Sample (R^4) frame coordinates
    """

    def __init__(self, sampleoffset=None, motormap=None):
        """When sample shift is zero, the sample position is the current motor position

        :param 3-array sampleoffset: sample shift in the World frame
        :param MotorsToWorld motormap:
        """
        self.sampleoffset = sampleoffset
        self.motormap = motormap
        fillvalue = [numpy.nan, numpy.nan, numpy.nan, 1]
        domain = codomain = sets.RealVectorSpace(ndim=4, fillvalue=fillvalue)
        super().__init__(domain=domain, codomain=codomain, matrix=self._forward_matrix)

    @property
    def sampleoffset(self):
        return self._sampleoffset

    @sampleoffset.setter
    def sampleoffset(self, values):
        if values is None:
            values = [0, 0, 0]
        values = numpy.atleast_1d(values)
        if values.shape != (3,):
            raise ValueError("Needs to be an array of 3 values")
        self._sampleoffset = values

    @property
    def matrix(self):
        """
        :returns array: shape is (ncodomain, ndomain)
        """
        self._matrix = self._parse_matrix(self._forward_matrix)
        return self._matrix

    @property
    def _forward_matrix(self):
        position = self.motormap.current_world_position
        offset = self.sampleoffset - position

        return [
            [1, 0, 0, offset[0]],
            [0, 1, 0, offset[1]],
            [0, 0, 1, offset[2]],
            [0, 0, 0, 1],
        ]


class VLMToWorld(maps.Isomorphism):
    """Maps VLM (R^4) to World (R^4) frame coordinates
    """

    def __init__(self, focaloffset=None):
        """
        :param 3-array focaloffset: VLM focal point in the World frame
        """
        self.focaloffset = focaloffset
        fillvalue = [numpy.nan, numpy.nan, numpy.nan, 1]
        domain = codomain = sets.RealVectorSpace(ndim=4, fillvalue=fillvalue)
        super().__init__(domain=domain, codomain=codomain, matrix=self._forward_matrix)

    @property
    def focaloffset(self):
        return self._focaloffset

    @focaloffset.setter
    def focaloffset(self, values):
        if values is None:
            values = [0, 0, 0]
        values = numpy.atleast_1d(values)
        if values.shape != (3,):
            raise ValueError("Needs to be an array of 3 values")
        self._focaloffset = values

    @property
    def matrix(self):
        """
        :returns array: shape is (ncodomain, ndomain)
        """
        self._matrix = self._parse_matrix(self._forward_matrix)
        return self._matrix

    @property
    def _forward_matrix(self):
        offset = self.focaloffset
        return [
            [1, 0, 0, offset[0]],
            [0, 1, 0, offset[1]],
            [0, 0, 1, offset[2]],
            [0, 0, 0, 1],
        ]


class VLMToVI(maps.LinearMap, maps.Surjection):
    """Maps R^4 VLM (video light microscope) to R^3 VI frame (video image) coordinates
    """

    def __init__(self, imageshape=None, zoomlevel=None, zoominfo=None):
        """
        :param 2-array imageshape: in pixels
        :param str zoomlevel: key in `zoominfo`
        :param dict(dict) zoominfo: zoom level -> {"pixelsize":[., .], "focalpoint":[., .]}
                                    pixelsize: VLM units per VI unit
                                    focalpoint: focal point in VI units relative to the image center
        """
        self.imageshape = imageshape
        self.zoominfo = zoominfo
        self.zoomlevel = zoomlevel
        fillvalue = [numpy.nan, numpy.nan, numpy.nan, 1]
        domain = sets.RealVectorSpace(ndim=4, fillvalue=fillvalue)
        fillvalue = [numpy.nan, numpy.nan, 1]
        codomain = sets.RealVectorSpace(ndim=3, fillvalue=fillvalue)
        super().__init__(domain=domain, codomain=codomain, matrix=self._forward_matrix)

    @property
    def imageshape(self):
        return self._imageshape

    @imageshape.setter
    def imageshape(self, values):
        values = numpy.atleast_1d(values)
        if values.shape != (2,):
            raise ValueError("Needs to be an array of 2 values")
        self._imageshape = values

    @property
    def focalpoint(self):
        """Focal point in the VI frame in VI units (pixels)
        """
        try:
            return self.zoominfo[self.zoomlevel]["focalpoint"] + self.vi_center
        except ValueError:
            return self.vi_center

    @property
    def pixelsize(self):
        """1 VI unit in VLM units
        """
        try:
            pixelsize = self.zoominfo[self.zoomlevel]["pixelsize"]
        except ValueError:
            pixelsize = [1, 1]
        pixelsize = numpy.atleast_1d(pixelsize)
        if pixelsize.shape != (2,):
            raise ValueError("Needs to be an array of 2 values")
        return pixelsize

    @property
    def zoomlevel(self):
        level = self._zoomlevel
        if level not in self.zoominfo:
            msg = f"'{level}' not a known zoom level ([{list(self.zoominfo.keys())}])'"
            logger.error(msg)
            raise ValueError(msg)
        return level

    @property
    def zoomlevels(self):
        arr = []
        names = []
        for name, info in self.zoominfo.items():
            arr.append(info["pixelsize"][0])
            names.append(name)
        return [names[i] for i in numpy.argsort(arr)]

    def zoomin(self):
        levels = self.zoomlevels
        i = levels.index(self.zoomlevel)
        try:
            self.zoomlevel = levels[i + 1]
        except IndexError:
            return

    def zoomout(self):
        levels = self.zoomlevels
        i = max(levels.index(self.zoomlevel), 1)
        try:
            self.zoomlevel = levels[i - 1]
        except IndexError:
            return

    @zoomlevel.setter
    def zoomlevel(self, value):
        self._zoomlevel = value

    @property
    def matrix(self):
        """
        :returns array: shape is (ncodomain, ndomain)
        """
        self._matrix = self._parse_matrix(self._forward_matrix)
        return self._matrix

    @property
    def _forward_matrix(self):
        focalpoint = self.focalpoint
        ipixelsize = 1.0 / self.pixelsize
        return [
            [ipixelsize[0], 0, 0, focalpoint[0]],
            [0, ipixelsize[1], 0, focalpoint[1]],
            [0, 0, 0, 1],
        ]

    @property
    def _inverse_matrix(self):
        # Only XY-plane
        focalpoint = self.focalpoint
        pixelsize = self.pixelsize
        shift = -focalpoint * pixelsize
        return numpy.array(
            [[pixelsize[0], 0, shift[0]], [0, pixelsize[1], shift[1]], [0, 0, 1]]
        )

    def _right_inverse(self, icoord):
        """
        :param array icoord: shape is (npoints, ncodomain)
        :returns array-like: shape is (npoints, ndomain)
        """
        coord = self._inverse_matrix.dot(icoord.T).T
        result = numpy.zeros_like(coord, shape=(coord.shape[0], coord.shape[1] + 1))
        result[:, :-2] = coord[:, :-1]
        result[:, -1] = coord[:, -1]
        return result

    @property
    def vi_corners(self):
        """Bounds in the VI frame which the image needs to be displayed.

        :returns array: (left, bottom), (right, top)
        """
        return numpy.array([[0, 0], self.imageshape.tolist()])

    @property
    def vi_polyline(self):
        """
        :returns array: (npoints, ndomain)
        """
        return polyline(self.vi_corners)

    @property
    def vi_center(self):
        """
        :returns array: (ndomain,)
        """
        return self.imageshape / 2.0

    @property
    def vi_origin(self):
        """
        :returns array: (ndomain,)
        """
        return numpy.array([0, 0])


class SampleToCanvas(maps.LinearMap, maps.Surjection):
    """Maps Sample (R^4) to Canvas (R^3) frame coordinates
    """

    def __init__(self, downstream=True):
        """
        :param bool downstream: look at the sample in the Z direction (~ beam direction)
        """
        self.downstream = downstream
        fillvalue = [numpy.nan, numpy.nan, numpy.nan, 1]
        domain = sets.RealVectorSpace(ndim=4, fillvalue=fillvalue)
        fillvalue = [numpy.nan, numpy.nan, 1]
        codomain = sets.RealVectorSpace(ndim=3, fillvalue=fillvalue)
        super().__init__(domain=domain, codomain=codomain, matrix=self._forward_matrix)

    @property
    def matrix(self):
        """
        :returns array: shape is (ncodomain, ndomain)
        """
        self._matrix = self._parse_matrix(self._forward_matrix)
        return self._matrix

    @property
    def _sx(self):
        if self.downstream:
            return -1
        else:
            return 1

    @property
    def _forward_matrix(self):
        return [[self._sx, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1]]

    @property
    def _inverse_matrix(self):
        return numpy.asarray([[self._sx, 0, 0], [0, 1, 0], [0, 0, 1]])

    def _right_inverse(self, icoord):
        """
        :param array icoord: shape is (npoints, ncodomain)
        :returns array-like: shape is (npoints, ndomain)
        """
        coord = self._inverse_matrix.dot(icoord.T).T
        result = numpy.zeros_like(coord, shape=(coord.shape[0], coord.shape[1] + 1))
        result[:, :-2] = coord[:, :-1]
        result[:, -1] = coord[:, -1]
        return result


class BeamToWorld(maps.Isomorphism):
    """Maps Beam (R^4) to World (R^4) frame coordinates
    """

    def __init__(self, beamangle=0, beamoffset=None, beamsize=None):
        """
        :param num beamangle: rotation around the Y-axis (degrees)
        :param 3-array beamoffset: beam position in the world frame
        :param 3-array beamsize: beam size in the world frame
        """
        self.beamangle = beamangle
        self.beamoffset = beamoffset
        self.beamsize = beamsize
        fillvalue = [numpy.nan, numpy.nan, numpy.nan, 1]
        domain = codomain = sets.RealVectorSpace(ndim=4, fillvalue=fillvalue)
        super().__init__(domain=domain, codomain=codomain, matrix=self._forward_matrix)

    @property
    def beamoffset(self):
        return self._beamoffset

    @beamoffset.setter
    def beamoffset(self, values):
        if values is None:
            values = [0, 0, 0]
        values = numpy.atleast_1d(values)
        if values.shape != (3,):
            raise ValueError("Needs to be an array of 3 values")
        self._beamoffset = values

    @property
    def beamsize(self):
        return self._beamsize

    @beamsize.setter
    def beamsize(self, values):
        if values is None:
            values = [0, 0, 0]
        values = numpy.atleast_1d(values)
        if values.shape != (3,):
            raise ValueError("Needs to be an array of 3 values")
        self._beamsize = values

    @property
    def beam_corners(self):
        """Corners of the beam ellipsoid
        """
        bs = self.beamsize
        low = -bs / 2
        low[-1] = 0
        high = bs / 2
        return numpy.asarray(list(itertools.product(*zip(low, high))))

    @property
    def matrix(self):
        """
        :returns array: shape is (ncodomain, ndomain)
        """
        self._matrix = self._parse_matrix(self._forward_matrix)
        return self._matrix

    @property
    def _forward_matrix(self):
        cosa = numpy.cos(numpy.radians(self.beamangle))
        sina = numpy.sin(numpy.radians(self.beamangle))
        beamoffset = self.beamoffset
        return [
            [cosa, 0, sina, beamoffset[0]],
            [0, 1, 0, beamoffset[1]],
            [-sina, 0, cosa, beamoffset[2]],
            [0, 0, 0, 1],
        ]


class ImageViewerCanvas:
    """This class groups all ImageViewer coordinate transformations
    and provides an API for dislay, motion and scanning.

    Coordinates in Canvas frame
        - markers (beam, VLM center, VLM focus)
        - borders (VLM, fine, coarse, reach)

    Actions
        - move the sample (point beam on in Canvas frame)
        - move the motors
        - define the beam
        - center fine

    Conversions
        - convert a group of points to motor positions
          (e.g. corners of a map)
    """

    def __init__(
        self,
        motors,
        units="nm",
        unitdict=None,
        sampleoffset=None,
        beamoffset=None,
        beamangle=0,
        beamsize=None,
        focaloffset=None,
        vlmimageshape=None,
        zoomlevel=None,
        zoominfo=None,
        downstream=True,
    ):
        """
        :param dict(Motor) motors:
        :param str units: the World frame units (nm by default)
        :param dict unitdict: motor units (tries to get them from the hardware when missing)
        :param 3-array sampleoffset: sample shift in the World frame
        :param 3-array beamoffset: beam position in the world frame
        :param 3-array beamsize: beam size in the world frame
        :param num beamangle: angle (degrees) between world and beam frame
        :param 3-array focaloffset: VLM focalpoint in the world frame
        :param 2-array vlmimageshape: in pixels
        :param str zoomlevel: key in `zoominfo`
        :param dict(dict) zoominfo: zoom level -> {"pixelsize":[., .], "focalpoint":[., .]}
                                    pixelsize: 1 VI unit in VLM units
                                    focalpoint: focal point in VI units relative to the image center
        :param bool downstream: look at the sample downstream
        """
        self.motors_to_world = MotorsToWorld(**motors, units=units, unitdict=unitdict)
        self.world_to_sample = WorldToSample(
            sampleoffset=sampleoffset, motormap=self.motors_to_world
        )
        self.beam_to_world = BeamToWorld(
            beamangle=beamangle, beamoffset=beamoffset, beamsize=beamsize
        )
        self.vlm_to_vi = VLMToVI(
            imageshape=vlmimageshape, zoomlevel=zoomlevel, zoominfo=zoominfo
        )
        self.vlm_to_world = VLMToWorld(focaloffset=focaloffset)
        self.sample_to_canvas = SampleToCanvas(downstream=downstream)

    def to_dict(self):
        return {
            "units": self.units,
            "unitdict": self.unitdict,
            "sampleoffset": self.world_to_sample.sampleoffset,
            "beamoffset": self.beam_to_world.beamoffset,
            "beamsize": self.beam_to_world.beamsize,
            "beamangle": self.beam_to_world.beamangle,
            "focaloffset": self.vlm_to_world.focaloffset,
            "vlmimageshape": self.vlm_to_vi.imageshape,
            "zoomlevel": self.zoomlevel,
            "zoominfo": self.zoominfo,
            "downstream": self.downstream,
        }

    def from_dict(self, dic):
        self.units = dic["units"]
        self.unitdict = dic["unitdict"]
        self.world_to_sample.sampleoffset = dic["sampleoffset"]
        self.beam_to_world.beamoffset = dic["beamoffset"]
        self.beam_to_world.beamsize = dic["beamsize"]
        self.beam_to_world.beamangle = dic["beamangle"]
        self.vlm_to_world.focaloffset = dic["focaloffset"]
        self.vlm_to_vi.imageshape = dic["vlmimageshape"]
        self.zoomlevel = dic["zoomlevel"]
        self.zoominfo = dic["zoominfo"]
        self.downstream = dic["downstream"]

    @property
    def align_info(self):
        return {
            "sampleoffset": self.world_to_sample.sampleoffset,
            "beamoffset": self.beam_to_world.beamoffset,
            "beamsize": self.beam_to_world.beamsize,
            "focaloffset": self.vlm_to_world.focaloffset,
        }

    @property
    def units(self):
        return self.motors_to_world.units

    @units.setter
    def units(self, value):
        self.motors_to_world.units = value

    @property
    def unitdict(self):
        return self.motors_to_world.unitdict

    @unitdict.setter
    def unitdict(self, value):
        self.motors_to_world.unitdict = value

    @property
    def downstream(self):
        return self.sample_to_canvas.downstream

    @downstream.setter
    def downstream(self, value):
        self.sample_to_canvas.downstream = value

    @property
    def zoomlevel(self):
        return self.vlm_to_vi.zoomlevel

    @zoomlevel.setter
    def zoomlevel(self, value):
        self.vlm_to_vi.zoomlevel = value

    @property
    def zoominfo(self):
        return self.vlm_to_vi.zoominfo

    @zoominfo.setter
    def zoominfo(self, dic):
        self.vlm_to_vi.zoominfo = dic

    @property
    def focalpoint(self):
        """In the Canvas frame
        """
        fp = self.vlm_to_vi.focalpoint
        return self._transform_vi_to_canvas(fp)[0]

    @property
    def vi_center(self):
        """In the Canvas frame
        """
        corners = self.vlm_to_vi.vi_center
        return self._transform_vi_to_canvas(corners)[0]

    @property
    def vi_origin(self):
        """In the Canvas frame
        """
        corners = self.vlm_to_vi.vi_origin
        return self._transform_vi_to_canvas(corners)[0]

    @property
    def vi_corners(self):
        """In the Canvas frame

        :returns array: (left, bottom), (right, top)
        """
        corners = self.vlm_to_vi.vi_corners
        return self._transform_vi_to_canvas(corners)

    @property
    def vi_polyline(self):
        """In the Canvas frame
        """
        corners = self.vlm_to_vi.vi_polyline
        return self._transform_vi_to_canvas(corners)

    @property
    def beamposition(self):
        """In the Canvas frame
        """
        bp = self.beam_to_world.beamoffset
        return self._transform_world_to_canvas(bp)[0]

    @beamposition.setter
    def beamposition(self, bp):
        """In the Canvas frame
        """
        bp = self.sample_to_canvas.inverse(bp)
        bp = self.world_to_sample.inverse(bp)
        bp = fromhomogeneous(bp)[0]
        self.beam_to_world.beamoffset = bp

    @property
    def sampleposition(self):
        """In the Canvas frame
        """
        coord = [0, 0, 0]
        coord = self.sample_to_canvas.forward(coord)
        return fromhomogeneous(coord)[0]

    @property
    def vlm_image_info(self):
        # In VI frame
        corners1 = self.vlm_to_vi.vi_corners
        width1 = corners1[1] - corners1[0]
        # In Canvas frame
        corners2 = self.vi_corners
        width2 = corners2[1] - corners2[0]
        # REMARK: assume image is still a rectangle
        center = numpy.mean(corners2, axis=0)
        pixelsize = width2 / width1

        return {"center": center, "pixelsize": pixelsize}

    @property
    def beam_info(self):
        bp = self.beamposition
        # TODO: this is not correct
        # (need intersaction between lines and sample surface)
        corners = self.beam_to_world.beam_corners
        corners = self._transform_beam_to_canvas(corners)
        mi = corners.min(axis=0)
        ma = corners.max(axis=0)
        center = corners.mean(axis=0)
        return {"position": bp, "center": center, "size": ma - mi}

    @property
    def current_motor_positions_dict(self):
        return self.motors_to_world.current_motor_positions_dict

    def move_motors(self, values, wait=True, timeout=None):
        """Move the motors directly

        :param array or dict values:
        :param bool wait: wait until motion has finished
        :param num or None timeout: `None` waits indefinitely
        """
        self.motors_to_world.domain.move(values, wait=wait, timeout=timeout)

    def wait_motion_done(self, timeout=None):
        """Wait until the motors do not move anymore
        """
        self.motors_to_world.domain.wait_motion_done(timeout)

    def move(self, coord, wait=True, timeout=None):
        """Move the sample so the beam hits the new
        position `coord` in the Canvas frame

        :param array coord: in Canvas frame
        :param bool wait: wait until motion has finished
        :param num or None timeout: `None` waits indefinitely
        """
        coord = self._transform_motion(coord)
        self.motors_to_world.domain.move(coord[0], wait=wait, timeout=timeout)

    def center_fine(self, wait=True, timeout=None):
        """Stay on the same position but center the fine axes

        :param bool wait: wait until motion has finished
        :param num or None timeout: `None` waits indefinitely
        """
        motpos = self.motors_to_world.centered_position
        self.motors_to_world.domain.move(motpos, wait=wait, timeout=timeout)

    def canvas_to_motor(self, coord):
        """Get motor positions for a group of points in the Canvas

        :param array coord: (npoints, 2) in Canvas frame
        :returns dict: fixed and variable motor positions
        """
        coord = self._transform_motion(coord)
        fixed = {}
        variable = {}
        labels = self.motors_to_world.motor_labels
        unitdict = self.motors_to_world.unitdict
        for label, mot, motpos in zip(
            labels, self.motors_to_world.domain.motors, coord.T
        ):
            if all(motpos == motpos[0]):
                destination = motpos[0]
                dest = fixed
            else:
                destination = motpos.tolist()
                dest = variable
            dest[label] = {
                "motor": mot,
                "destination": destination,
                "unit": unitdict[mot.id()],
                "unit_exponent": get_exponent(unitdict[mot.id()]),
            }
        return {"fixed": fixed, "variable": variable}

    @property
    def reach_polyline(self):
        """Corners of the reachable Canvas area (see `move`)
        """
        return self._get_polyline()

    @property
    def fine_polyline(self):
        """Corners of the reachable fine motor area (see `move`)
        """
        return self._get_polyline(["x_fine", "y_fine", "z_fine"])

    @property
    def coarse_polyline(self):
        """Corners of the reachable coarse motor area (see `move`)
        """
        return self._get_polyline(["x", "y", "z"])

    def _get_polyline(self, motors=None):
        """Corners of reachable area around the beam position

        :param 3-array motors:
        :returns array: (npoints, 3)
        """
        if motors:
            limits = self.motors_to_world.current_world_position
            limits = numpy.tile(limits, (2, 1))
            for label in motors:
                try:
                    motname = self.motors_to_world.motor_name(label)
                except ValueError:
                    continue
                i = self.motors_to_world.motor_world_index(motname)
                limits[:, i] = self.motors_to_world.motor_world_limits(motname)
        else:
            limits = self.motors_to_world.world_limits
        limits = polyline(self._transform_world_to_canvas(limits))
        return self._relative_to_beam(limits)

    def _transform_vi_to_canvas(self, coord):
        coord = self.vlm_to_vi.inverse(coord)
        coord = self.vlm_to_world.forward(coord)
        return self._transform_world_to_canvas(coord)

    def _transform_world_to_canvas(self, coord):
        coord = self.world_to_sample.forward(coord)
        coord = self.sample_to_canvas.forward(coord)
        return fromhomogeneous(coord)

    def _transform_beam_to_canvas(self, coord):
        coord = self.beam_to_world.forward(coord)
        coord = self.world_to_sample.forward(coord)
        coord = self.sample_to_canvas.forward(coord)
        return fromhomogeneous(coord)

    def _transform_motion(self, coord):
        """
        :param array coord: Canvas coordinates
        :returns array: motor coordinates to have the beam on `coord`
        """
        coord = self._relative_to_beam(coord)
        coord = self.sample_to_canvas.inverse(coord)
        coord = self.world_to_sample.inverse(coord)
        return self.motors_to_world.inverse(coord)

    def _relative_to_beam(self, coord):
        """
        :param array coord: Canvas coordinates
        :returns array: relative motion to put beam on coord
        """
        return numpy.atleast_2d(self.beamposition) - numpy.atleast_2d(coord)
