"""
Sets in the mathematical sense

    Set: any set of elements
    RealSubset: set of real numbers
    RealInterval: R^n with limits [a, b] (open, closed or half-open)
                  on each dimension (you can use inf)
    RealVectorSpace: R^n
"""
import sys
import itertools
from abc import ABC, abstractmethod, abstractproperty
import numpy
from daiquiri.core.transform import utils

from daiquiri.core.utils import timed


class DomainError(ValueError):
    pass


class NotInSetError(DomainError):
    pass


class DimensionError(DomainError):
    pass


class Set(ABC):
    """A set in the mathematical sense
    """

    def __contains__(self, other):
        """
        :param num or array-like or Set other: element(s) or set
        :returns bool:
        """
        if isinstance(other, Set):
            return self.isSubset(other)
        else:
            return self.isElement(other)

    def __eq__(self, other):
        return other in self and self in other

    def __ne__(self, other):
        return not self.__eq__(other)

    def __le__(self, other):
        return self in other

    def __gt__(self, other):
        return self not in other

    def __ge__(self, other):
        return other in self

    def __lt__(self, other):
        return other not in self

    @abstractmethod
    def isSubset(self, other):
        """Is other a subset of self (not strict)

        :param Set other:
        :returns bool:
        """
        pass

    def isElement(self, elements):
        """
        :param num or sequence elements:
        :returns bool:
        """
        return all(self.isElementAsSequence(elements))

    @abstractmethod
    def isElementAsSequence(self, elements):
        """
        :param num or sequence elements:
        :returns sequence(bool):
        """
        pass

    @abstractproperty
    def isFinite(self):
        """
        :returns bool:
        """
        pass

    @property
    def isInfinite(self):
        """
        :returns bool:
        """
        return not self.isFinite

    def raiseIfNotIn(self, other):
        """
        :param num or array-like or Set other: element(s) or set
        :returns bool:
        """
        if isinstance(other, Set):
            if other not in self:
                raise ValueError("Is not a subset")
        else:
            self.raiseIfNotIsElement(other)

    # @timed
    def raiseIfNotIsElement(self, elements):
        """
        :param num or sequence elements:
        :returns bool:
        """
        # TODO: This takes of the order of 10-50ms per call (!)
        #   due to `isElementAsSequence`, disabled for now
        if not hasattr(sys, "_running_pytest"):
            return

        elements = self.as_sequence(elements)
        inside = self.isElementAsSequence(elements)
        if not inside.all():
            outside = elements[~inside]
            raise NotInSetError(
                "These elements are not part of the set {}:\n {}".format(
                    repr(self), outside
                )
            )

    def as_sequence(self, elements):
        """
        :param num or sequence elements:
        :returns array:
        """
        return numpy.asarray(elements)


class RealSubset(Set):
    """Subset of real vector space R^n
    """

    def __init__(self, ndim=None, fillvalue=None):
        if ndim is None or ndim < 1:
            raise ValueError("Specify a dimension >= 1")
        self.__ndim = ndim
        if fillvalue is None:
            fillvalue = numpy.nan
        fillvalue = numpy.atleast_1d(fillvalue)
        if fillvalue.shape == (1,):
            fillvalue = numpy.full((ndim,), fillvalue)
        elif fillvalue.shape != (ndim,):
            raise ValueError(f"Expected one or {ndim} fill values")
        self.__fillvalue = fillvalue

    @property
    def ndim(self):
        """Dimension of R^n
        """
        return self.__ndim

    def as_sequence(self, points):
        """
        :param num or sequence points: shape is (), (ndim,) or (npoints, ndim)
        :returns array: shape is (npoints, ndim)
        """
        points = numpy.atleast_2d(points)
        ndimextra = self.ndim - points.shape[1]
        if points.ndim != 2 or ndimextra < 0:
            raise DimensionError(
                "Coordinate shape must be (npoints, {})".format(self.ndim)
            )
        if ndimextra:
            addnan = self.__fillvalue[numpy.newaxis, -ndimextra:]
            addnan = numpy.tile(addnan, (points.shape[0], 1))
            points = numpy.hstack([points, addnan])
        return points

    def isSubset(self, other):
        """Is other a subset of self (not strict)

        :param RealVectorSpace other:
        :returns bool:
        """
        if not isinstance(other, RealInterval):
            raise ValueError("Must be an instance of RealInterval")
        if self.isVectorSpace:
            return other.ndim <= self.ndim
        else:
            return self._isSubset(other)

    @abstractmethod
    def _isSubset(self, other):
        """Is other a subset of self (not strict)

        :param RealVectorSpace other:
        :returns bool:
        """
        pass

    def isElementAsSequence(self, points):
        """
        :param num or sequence points:
        :returns array(bool): shape is (npoints,)
        """
        return self.isElementAsArray(points).all(axis=1)

    def isElementAsArray(self, points):
        """
        :param num or sequence points:
        :returns array(bool): shape is (npoints, ndim)
        """
        try:
            points = self.as_sequence(points)
        except DimensionError:
            points = numpy.atleast_2d(points)
            return numpy.full(points.shape, False)
        return self._isElementAsArray(points)

    @abstractmethod
    def _isElementAsArray(self, points):
        """
        :param array points: shape is (npoints, ndim)
        :returns array(bool): shape is (npoints, ndim)
        """
        pass

    @property
    def isVectorSpace(self):
        """
        :returns bool: ndim
        """
        return False


class RealInterval(RealSubset):
    """Subset of real vector space R^n defined by limits (open, closed, half-open) in each dimensions
    """

    def __init__(self, ndim=None, limits=None, **kwargs):
        """
        :param num ndim: dimension of real vector space
        :param num or array-like limits: limits with shape (ndim, 4) or (ndim, 2)
                                         For example [-1, 1, True, False]
                                         refers to the interval [-1, 1).
        """
        if ndim is None and limits is not None:
            limits = numpy.atleast_2d(limits)
            ndim = len(limits)
        super().__init__(ndim=ndim, **kwargs)
        self._limits = self._parse_limits(limits)

    @property
    def limits(self):
        """
        :returns recarray: shape is (ndim, 4)
        """
        return self._limits

    def __repr__(self):
        if self.isVectorSpace:
            return "R^{}".format(self.ndim)
        else:
            return "R^{}(interval={})".format(self.ndim, self.limits)

    def _parse_limits(self, values):
        """Closed by default

        :param num or array-like values: limits with shape (ndim, 4) or (ndim, 2)
        :returns recarray: shape is (ndim, 4)
        """
        if values is None:
            values = [[-numpy.inf, numpy.inf]] * self.ndim
        values = numpy.atleast_2d(values)
        shape = (self.ndim, 2)
        if values.shape == shape:
            cvalues = numpy.full(shape, True)
            values = numpy.concatenate([values, cvalues], axis=1)
        shape = (self.ndim, 4)
        if values.shape != shape:
            raise DimensionError("Required shape is {}".format(shape))
        idx = utils.argsort(values[:, :2], axis=1)
        values[:, :2] = values[:, :2][idx]
        values[:, 2:] = values[:, 2:][idx]
        values = numpy.array([tuple(v) for v in values], dtype=self._limit_dtype)
        return values.view(numpy.recarray)

    @property
    def _limit_dtype(self):
        return [("low", "<f8"), ("high", "<f8"), ("clow", "?"), ("chigh", "?")]

    def _isSubset(self, other):
        """Is other a subset of self (not strict)

        :param RealVectorSpace other:
        :returns bool:
        """
        if not isinstance(other, RealInterval):
            raise ValueError("Must be an instance of RealInterval")
        if self.isVectorSpace:
            return other.ndim <= self.ndim
        else:
            return other.border_inlim in self

    @timed
    def _isElementAsArray(self, points):
        """
        :param array points: shape is (npoints, ndim)
        :returns array(bool): shape is (npoints, ndim)
        """
        #  TODO: This takes a long time to execute, why?
        limits = self.limits
        b = numpy.full(points.shape, True)
        if self.isVectorSpace:
            return b
        # Lower limit (open or closed)
        clow = limits.clow
        olow = ~clow
        with numpy.errstate(invalid="ignore"):
            # Ignore nan's
            b[:, clow] &= points[:, clow] >= limits.low[numpy.newaxis, clow]
            b[:, olow] &= points[:, olow] > limits.low[numpy.newaxis, olow]
        # Upper limit (open or closed)
        chigh = limits.chigh
        ohigh = ~chigh
        with numpy.errstate(invalid="ignore"):
            # Ignore nan's
            b[:, chigh] &= points[:, chigh] <= limits.high[numpy.newaxis, chigh]
            b[:, ohigh] &= points[:, ohigh] < limits.high[numpy.newaxis, ohigh]
        # Nan's: coordinate in subspace
        b[numpy.isnan(points)] = True
        return b

    @property
    def border(self):
        """Could be infinite or open

        :returns array: shape is (npoints, ndim)
        """
        return numpy.asarray(
            list(itertools.product(*zip(self.limits.low, self.limits.high)))
        )

    def border_finite(self, maxnum=None):
        """Could be open limit

        :returns array: shape is (npoints, ndim)
        """
        return numpy.asarray(
            list(
                itertools.product(
                    *zip(
                        self._low_finite(maxnum=maxnum),
                        self._high_finite(maxnum=maxnum),
                    )
                )
            )
        )

    @property
    def border_inlim(self):
        """Could be infinite

        :returns array: shape is (npoints, ndim)
        """
        return numpy.asarray(
            list(itertools.product(*zip(self._low_inlim, self._high_inlim)))
        )

    @property
    def center(self):
        """Center of the domain

        :returns array: ndim
        """
        cen = (self._high_finite() + self._low_finite()) / 2.0
        cen[self.fullDimension] = 0
        return cen

    @property
    def length(self):
        """Length of the interval

        :returns array: ndim
        """
        return self.limits.high - self.limits.low

    @property
    def length_inlim(self):
        """Length of the interval

        :returns array: ndim
        """
        return self._high_inlim - self._low_inlim

    def random_uniform(self, n, maxnum=None):
        """
        :param num n: number of points
        :param num maxnum:
        :returns array: shape (n, ndim)
        """
        # numpy.random.uniform: [low, high)

        # Open lower limit when needed
        low = self._low_finite(maxnum=maxnum)
        lowopen = ~self.limits.clow
        low = self._nextafter(low, lowopen, self._high_inlim)

        # Close higher limit when needed
        high = self._high_finite(maxnum=maxnum)
        highclosed = self.limits.chigh & (low != high)
        high = self._nextafter(high, highclosed, numpy.inf)
        high = utils.makeFinite(high, maxnum=maxnum)

        return numpy.random.uniform(low, high, (n, self.ndim))

    @property
    def isFinite(self):
        """
        :returns bool:
        """
        return self.finite_dimensions.all()

    @property
    def finite_dimensions(self):
        """
        :returns array(bool): ndim
        """
        return numpy.isfinite(self.length)

    @property
    def fullDimension(self):
        """
        :returns array(bool): ndim
        """
        return (self.limits.low == -numpy.inf) & (self.limits.high == numpy.inf)

    @property
    def isVectorSpace(self):
        """
        :returns bool: ndim
        """
        return self.fullDimension.all()

    def _low_finite(self, maxnum=None):
        """Finite but outside the limits when open

        :returns array: ndim
        """
        return utils.makeFinite(self.limits.low.copy(), maxnum=maxnum)

    def _high_finite(self, maxnum=None):
        """Finite but outside the limits when open

        :returns array: ndim
        """
        return utils.makeFinite(self.limits.high.copy(), maxnum=maxnum)

    def _low_finite_inlim(self, maxnum=None):
        return utils.makeFinite(self._low_inlim, maxnum=maxnum)

    def _high_finite_inlim(self, maxnum=None):
        return utils.makeFinite(self._high_inlim, maxnum=maxnum)

    @property
    def _low_inlim(self):
        """Inside the limits (respect open limit)

        :returns array: ndim
        """
        arr = self.limits.low
        isopen = ~self.limits.clow
        towards = self.limits.high
        return self._nextafter(arr, isopen, towards)

    @property
    def _high_inlim(self):
        """Inside the limits (respect open limit)

        :returns array: ndim
        """
        arr = self.limits.high
        isopen = ~self.limits.chigh
        towards = self.limits.low
        return self._nextafter(arr, isopen, towards)

    @staticmethod
    def _nextafter(arr, select, direction):
        """
        :param array arr:
        :param array(bool) select: mask to find next
        :param num or array direction: direction to find the next dtype value
        """
        arr = numpy.tile(arr, (2, 1))
        try:
            direction = direction[select]
        except TypeError:
            pass
        arr[1][select] = direction
        with numpy.errstate(under="ignore", over="ignore"):
            return numpy.nextafter(*arr)


class RealIntervalComposite(RealInterval):
    """Combine several Real intervals
    """

    def __init__(self, intervals, **kwargs):
        """
        :param list(RealInterval) intervals:
        """
        self._intervals = intervals
        ndim = sum(d.ndim for d in intervals)
        super().__init__(ndim=ndim, **kwargs)

    @property
    def limits(self):
        limits = []
        for interval in self._intervals:
            limits.extend(interval.limits.tolist())
        return self._parse_limits(limits)


class RealVectorSpace(RealInterval):
    """R^n
    """

    def __init__(self, ndim=None, **kwargs):
        """
        :param num ndim: dimension of real vector space
        """
        super().__init__(ndim=ndim, **kwargs)
