"""Sets based in daiquiri hardware objects
"""

import numpy
from daiquiri.core.transform import maps
from daiquiri.core.transform import hw_sets
from daiquiri.core.transform import units


class MotorLinearCombination(maps.AxisLinearCombination):
    """For example:
        y(nm) = samy(mm) + sampy(um) + offset(nm)
    """

    def __init__(self, motors, offset=0, units=None, unitdict=None, closed=None):
        """
        :param list(Motor) motors:
        :param num or Quantity offset: codomain values are in these units
        :param str or Unit units: codomain units
        :param dict unitdict: motor units (override the unit attribute of the motor object)
        :param list(2-tuple(bool)) closed: min/max included or not for each motor
        """
        self._coeff_cache = {}
        self._offset_cache = None

        if units is None:
            if units.isquantity(offset):
                self.units = offset.unit
            else:
                raise ValueError("Missing transformation destination units")
        else:
            self.units = units
        if unitdict is None:
            unitdict = {}
        self.unitdict = unitdict
        domain = hw_sets.MotorDomain(motors, closed=closed)
        super(maps.AxisLinearCombination, self).__init__(
            domain=domain, coefficients=motors, offset=offset
        )

    @property
    def offset(self):
        if self._offset_cache is None:
            self._offset_cache = self._offset.to(self.units).magnitude

        return self._offset_cache

    @offset.setter
    def offset(self, value):
        self._offset = units.asquantity(value, self.units)
        self._offset_cache = self._offset.to(self.units).magnitude

    def _parse_matrix(self, motors):
        """
        :param list motors:
        """
        return None

    @property
    def matrix(self):
        """
        :returns array: shape (1, domain.ndim)
        """
        u = self.units
        matrix = [self._calc_coefficient(mot) for mot in self.domain.motors]
        return numpy.atleast_2d(matrix)

    def _calc_coefficient(self, mot):
        """Calculate the motor coefficient from the motor units and destination units

        :param Motor mot:
        :returns num:
        """
        if mot.id() in self._coeff_cache:
            return self._coeff_cache[mot.id()]

        u = self.unitdict.get(mot.id())
        if u is None:
            u = mot.get("unit")
        if u is None:
            raise AttributeError(f"Motor {mot.name()} does not have a unit")
        coeff = units.asquantity(1, u).to(self.units).magnitude

        self._coeff_cache[mot.id()] = coeff
        return coeff

    @property
    def default_reference(self):
        return self.domain.current_position

    def codomain_limits(self, motname, reference=None):
        """Limits projected on the codmain

        :param str motname:
        :param array reference: shape is (1, ndomain)
        :returns array-like: shape is (2,)
        """
        axis = self.domain.motor_index(motname)
        return super().codomain_limits(axis, reference=reference)

    @property
    def current_domain_position(self):
        return self.domain.current_position

    @property
    def current_codomain_position(self):
        return self.forward(self.current_domain_position)
