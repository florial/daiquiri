# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

import logging

logger = logging.getLogger(__name__)


class ProjectDataSchema(Schema):
    project_name = fields.Str()
    sample_name = fields.Str()
    protein_name = fields.Str()
