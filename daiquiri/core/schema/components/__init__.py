#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields, validate

import logging

logger = logging.getLogger(__name__)


class ComponentInfoSchema(Schema):
    name = fields.Str()
    baseurl = fields.Str()
    config = fields.Dict()


class VersionSchema(Schema):
    version = fields.Str()


class RegionSchema(Schema):
    start_e = fields.Float(required=True, title="Start Energy", unit="eV")
    end_e = fields.Float(required=True, title="End Energy", unit="eV")
    step = fields.Float(
        required=True, title="Step Size", unit="eV", validate=validate.Range(min=0.01)
    )
    dwell = fields.Float(
        required=True, title="Dwell time", unit="s", validate=validate.Range(min=0.01)
    )

    class Meta:
        uiorder = ["start_e", "end_e", "step", "dwell"]

        presets = {
            "Five Region": [
                {"start_e": -200, "end_e": -100, "step": 5},
                {"start_e": -100, "end_e": -20, "step": 0.5},
                {"start_e": -20, "end_e": 100, "step": 0.1},
                {"start_e": 100, "end_e": 250, "step": 1},
                {"start_e": 250, "end_e": 500, "step": 5},
            ],
            "Three Region": [
                {"start_e": -200, "end_e": -100, "step": 5},
                {"start_e": -100, "end_e": 250, "step": 1},
                {"start_e": 250, "end_e": 500, "step": 5},
            ],
        }
