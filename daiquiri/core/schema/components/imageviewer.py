#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from marshmallow import Schema, fields

logger = logging.getLogger(__name__)


class SourceMarking(Schema):
    markingid = fields.Int()
    sourceid = fields.Int()
    scale = fields.Dict()
    name = fields.Str()
    sizex = fields.Float()
    sizey = fields.Float()
    origin = fields.Bool()


class UpdateSourceMarking(Schema):
    scalelabel = fields.Str()
    x = fields.Int()
    y = fields.Int()


class ImageSource(Schema):
    sourceid = fields.Int()

    url = fields.String(required=True)
    name = fields.String(required=True)
    type = fields.String(required=True)
    origin = fields.Bool()

    additional = fields.Dict(keys=fields.Str(), values=fields.Float())

    center = fields.List(fields.Float(), length=2)
    markings = fields.Dict()
    pixelsize = fields.List(fields.Float(), length=2)
    polylines = fields.Dict(
        keys=fields.Str(), values=fields.List(fields.List(fields.Float(), length=2))
    )


class MoveToCoords(Schema):
    x = fields.Int()
    y = fields.Int()


class MapAdditionalSchema(Schema):
    datacollectionid = fields.Int(required=True)
    scalars = fields.List(fields.Str(), required=True, minItems=1)
