from marshmallow import Schema, fields, validate

from daiquiri.core.schema.validators import ValidatedRegexp, OneOf

# Helper to paginate a schema
def paginated(schema):
    class PaginatedSchema(Schema):
        total = fields.Int()
        rows = fields.Nested(schema, many=True)

    cls_name = f"Paginated<{schema.__name__}>"
    PaginatedSchema.__name__ = cls_name
    PaginatedSchema.__qualname__ = cls_name

    return PaginatedSchema


# Users
class UserSchema(Schema):
    user = fields.Str()
    personid = fields.Int()
    login = fields.Str()
    permissions = fields.List(fields.Str())
    groups = fields.List(fields.Str())
    fullname = fields.Str()
    givenname = fields.Str()
    familyname = fields.Str()
    is_staff = fields.Bool()


class UserCacheSchema(Schema):
    cache = fields.Dict()


# Proposals
class ProposalSchema(Schema):
    proposalid = fields.Int()
    proposal = fields.Str()
    start = fields.DateTime()
    title = fields.Str()
    visits = fields.Int()


# Visits
class SessionSchema(Schema):
    blsessionid = fields.Int()
    proposalid = fields.Int()
    session = fields.Str()
    startdate = fields.DateTime()
    enddate = fields.DateTime()
    proposal = fields.Str()
    beamlinename = fields.Str()

    class Meta:
        datetimeformat = "%d-%m-%Y %H:%M"


# Components
class NewComponentSchema(Schema):
    name = ValidatedRegexp("word-dash-space", title="Name", required=True)
    acronym = ValidatedRegexp("word-dash", title="Acronym", required=True)
    molecularmass = fields.Float(title="Mass", unit="Da")
    density = fields.Float(title="Density", unit="kg/m3")
    sequence = ValidatedRegexp("smiles", title="SMILES")

    class Meta:
        uiorder = ["name", "acronym", "molecularmass", "density", "sequence"]


class ComponentSchema(NewComponentSchema):
    componentid = fields.Int()
    name = ValidatedRegexp("word-dash-space", title="Name")
    acronym = ValidatedRegexp(
        "word-dash", title="Acronym", description="A short name for the component"
    )
    samples = fields.Int()
    datacollections = fields.Int()


# Samples
class NewSampleSchema(Schema):
    name = ValidatedRegexp("word-dash", title="Name", required=True)
    offsetx = fields.Int(title="X Offset", default=0)
    offsety = fields.Int(title="Y Offset", default=0)
    positions = fields.Dict(keys=fields.Str(), values=fields.Float())
    componentid = fields.Int(title="Component")
    comments = ValidatedRegexp("word-special", title="Comments", allow_none=True)

    class Meta:
        uiorder = ["componentid", "name", "offsetx", "offsety", "comments", "positions"]
        uischema = {
            "positions": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "offsetx": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "offsety": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "componentid": {
                "ui:widget": "connectedSelect",
                "ui:options": {
                    "selector": "components",
                    "key": "acronym",
                    "value": "componentid",
                },
            },
        }


class SampleSchema(NewSampleSchema):
    name = ValidatedRegexp("word-dash", title="Name")
    offsetx = fields.Int()
    offsety = fields.Int()
    sampleid = fields.Int()
    component = fields.Str()
    subsamples = fields.Int()
    datacollections = fields.Int()
    queued = fields.Bool()


class NewSubSampleSchema(Schema):
    sampleid = fields.Int(required=True)
    name = ValidatedRegexp("word-dash-space", title="Name")
    type = OneOf(["roi", "poi", "loi"], title="Type")
    comments = ValidatedRegexp("word-special", title="Comments", allow_none=True)
    x = fields.Int()
    y = fields.Int()
    x2 = fields.Int()
    y2 = fields.Int()
    positions = fields.Dict(keys=fields.Str(), values=fields.Float())


class SubSampleSchema(NewSubSampleSchema):
    sampleid = fields.Int()
    subsampleid = fields.Int()
    datacollections = fields.Int()
    queued = fields.Bool()


class NewSampleImageSchema(Schema):
    sampleid = fields.Int(required=True)
    offsetx = fields.Int()
    offsety = fields.Int()
    scalex = fields.Float()
    scaley = fields.Float()
    rotation = fields.Float()


class SampleImageSchema(NewSampleImageSchema):
    sampleimageid = fields.Int()
    sampleid = fields.Int()
    url = fields.Str()


# Datacollections
class DataCollectionSchema(Schema):
    sessionid = fields.Int(required=True)
    datacollections = fields.Int()
    datacollectionid = fields.Int()
    datacollectiongroupid = fields.Int()
    sampleid = fields.Int()
    subsampleid = fields.Int()
    starttime = fields.DateTime()
    endtime = fields.DateTime()
    duration = fields.Int()
    experimenttype = fields.Str()
    datacollectionnumber = fields.Int()
    runstatus = fields.Str()

    # Paths
    filetemplate = fields.Str()
    imagedirectory = fields.Str()

    # DC Params
    exposuretime = fields.Float()
    wavelength = fields.Float()
    transmission = fields.Float()
    numberofimages = fields.Float()
    numberofpasses = fields.Int()

    # Beam position and size
    xbeam = fields.Float()
    ybeam = fields.Float()
    beamsizeatsamplex = fields.Float()
    beamsizeatsampley = fields.Float()

    # Gridinfo columns
    steps_x = fields.Int()
    steps_y = fields.Int()
    dx_mm = fields.Float()
    dy_mm = fields.Float()

    # Snapshots
    snapshots = fields.Dict(keys=fields.Str(), values=fields.Bool())

    class Meta:
        datetimeformat = "%d-%m-%Y %H:%M:%S"


class DataCollectionAttachmentSchema(Schema):
    datacollectionfileattachmentid = fields.Int()
    dataCollectionid = fields.Int()
    filename = fields.Str()
    filetype = fields.Str()


class DataCollectionPlanSchema(Schema):
    datacollectionplanid = fields.Int()
    sampleid = fields.Int()
    subsampleid = fields.Int()
    experimentkind = fields.Str()
    boxsizex = fields.Float()
    boxsizey = fields.Float()
    energy = fields.Float()


class ScanQualityIndicatorsSchema(Schema):
    point = fields.List(fields.Int(), description="Scan point")
    total = fields.List(fields.Int(), description="Total signal")
    spots = fields.List(fields.Int(), description="Number of spots")
    autoprocprogramid = fields.List(fields.Int())


# XRF Maps
class NewXRFMapSchema(Schema):
    maproiid = fields.Int(required=True, description="The roiid")
    subsampleid = fields.Int(required=True, description="Object this map relates to")
    datacollectionid = fields.Int(required=True)


class XRFMapHistogramSchema(Schema):
    bins = fields.List(fields.Float())
    hist = fields.List(fields.Float())
    width = fields.List(fields.Float())


class XRFMapSchema(NewXRFMapSchema):
    maproiid = fields.Int(description="The roiid")
    subsampleid = fields.Int(description="Object this map relates to")
    datacollectionid = fields.Int()
    datacollectiongroupid = fields.Int()
    datacollectionnumber = fields.Int()
    mapid = fields.Int(description="The id of the map")
    sampleid = fields.Int()
    w = fields.Int(description="Width of the map")
    h = fields.Int(description="Height of the map")
    snaked = fields.Bool(
        description="Whether the map is collected interleaved left to right"
    )
    orientation = fields.Str(
        description="Wehther the map is collected horizontally or vertically"
    )
    element = fields.Str()
    edge = fields.Str()
    scalar = fields.Str()
    composites = fields.Int()

    points = fields.Int(description="Number of completed points")

    opacity = fields.Float()
    min = fields.Int()
    max = fields.Int()
    colourmap = fields.String()
    log = fields.Bool()

    url = fields.Str()


class XRFMapHistogramSchema(Schema):
    mapid = fields.Int()
    histogram = fields.Nested(
        XRFMapHistogramSchema, description="Histogram of the map points"
    )


class XRFMapValueSchema(Schema):
    mapid = fields.Int()
    x = fields.Int()
    y = fields.Int()
    value = fields.Float()


# XRF Composite Maps
class NewXRFCompositeMapSchema(Schema):
    subsampleid = fields.Int(required=True, description="Object this map relates to")
    r = fields.Int(required=True)
    g = fields.Int(required=True)
    b = fields.Int(required=True)
    ropacity = fields.Float()
    gopacity = fields.Float()
    bopacity = fields.Float()


class XRFCompositeMapSchema(NewXRFCompositeMapSchema):
    compositeid = fields.Int(description="The id of the composite map")
    subsampleid = fields.Int()
    sampleid = fields.Int()
    opacity = fields.Float()
    r = fields.Int()
    g = fields.Int()
    b = fields.Int()
    rroi = fields.Str()
    groi = fields.Str()
    broi = fields.Str()

    url = fields.Str()


# XRF Map ROIs
class NewXRFMapROISchema(Schema):
    sampleid = fields.Int(required=True, title="Sample ROI is associated to ")
    element = ValidatedRegexp(
        "word-dash", required=True, title="Element", validate=validate.Length(max=2)
    )
    edge = ValidatedRegexp(
        "word-dash", required=True, title="Edge", validate=validate.Length(max=3)
    )
    start = fields.Float(
        required=True, title="Start", description="Start Energy of ROI", unit="eV"
    )
    end = fields.Float(
        required=True, title="End", description="End Energy of ROI", unit="eV"
    )

    class Meta:
        uiorder = ["element", "edge", "start", "end", "sampleid"]
        uischema = {"sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"}}


class XRFMapROISchema(NewXRFMapROISchema):
    maproiid = fields.Int()
    maps = fields.Int()

    class Meta:
        uiorder = ["maproiid", "element", "edge", "start", "end", "maps", "sampleid"]
        uischema = {
            "maproiid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "maps": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }


# Auto Processing
class AutoProcProgramSchema(Schema):
    autoprocprogramid = fields.Int(required=True)
    datacollectionid = fields.Int()
    status = fields.Int()
    message = fields.Str()
    starttime = fields.DateTime()
    endtime = fields.DateTime()
    duration = fields.Int()
    programs = fields.Str()
    automatic = fields.Int()
    warnings = fields.Int()
    errors = fields.Int()


class AutoProcProgramAttachmentSchema(Schema):
    autoprocprogramattachmentid = fields.Int(required=True)
    autoprocprogramid = fields.Int(required=True)
    filetype = fields.Str()
    filename = fields.Str()
    rank = fields.Int()


class AutoProcProgramMessageSchema(Schema):
    autoprocprogrammessageid = fields.Int(required=True)
    autoprocprogramid = fields.Int(required=True)
    timestamp = fields.DateTime()
    severity = fields.Str()
    message = fields.Str()
    description = fields.Str()
