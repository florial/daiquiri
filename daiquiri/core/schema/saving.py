#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields
from daiquiri.core.schema.validators import ValidatedRegexp


class SavingSchema(Schema):
    arguments = fields.Dict(
        keys=fields.Str, values=fields.Str, required=True, title="Arguments"
    )

    class Meta:
        uiorder = ["arguments"]
