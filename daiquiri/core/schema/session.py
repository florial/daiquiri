#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.schema.validators import ValidatedRegexp

import logging

logger = logging.getLogger(__name__)

# Deliberately exclude token
class SessionListSchema(Schema):
    operator = fields.Bool(
        description="Denotes if this session is the current operator"
    )
    last_access = fields.Float(description="Last epoch time that this session was seen")
    sessionid = fields.UUID(description="The uuid of the session")
    data = fields.Dict()
    operator_pending = fields.Bool()


class SignSchema(Schema):
    url = ValidatedRegexp("path", description="Url to sign", required=True)
    bewit = fields.Str(description="The signed bewit")
