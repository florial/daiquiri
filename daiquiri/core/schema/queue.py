#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields
from daiquiri.core.schema.validators import Any

import logging

logger = logging.getLogger(__name__)


class QueueItemSchema(Schema):
    args = fields.Dict(description="Arguments passed to the queue item")
    cls = fields.Str(description="The class of which this is an instance of")
    desc = fields.Str(description="Description of the item")
    name = fields.Str(description="Name of the item")
    running = fields.Bool(description="Whether the queue item is running")
    uid = fields.Str(description="The unique identifier")
    created = fields.Float(description="Time added to queue")
    started = fields.Float(description="Time started")
    finished = fields.Float(description="Time finished")
    estimate = fields.Float(description="Estimated time required")
    status = fields.Str()


class QueueStatusSchema(Schema):
    running = fields.Bool(description="Whether the queue is running")
    stack = fields.Nested(
        QueueItemSchema, many=True, description="A list of queue items in the stack"
    )
    current = fields.Nested(
        QueueItemSchema, description="The currently running queue item"
    )
    all = fields.Nested(
        QueueItemSchema, many=True, description="A list of all queue items"
    )
    pause_on_fail = fields.Bool(
        description="Whether to pause the queue if an actor fails"
    )


class ChangeQueueStatusSchema(Schema):
    state = fields.Bool(required=True, description="The new queue state")


class ChangeQueueSettingSchema(Schema):
    setting = fields.Str(required=True, description="The new queue setting")
    value = Any(required=True, description="The queue setting value")


class MoveQueueItemSchema(Schema):
    uid = fields.Str(description="The queue item uuid")
    position = fields.Int(description="The new position")
