#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields, validates_schema, ValidationError

# Config Schema
class ScanSchema(Schema):
    actor = fields.Str(required=True)
    require_staff = fields.Bool()


class ScanProcessingSchema(Schema):
    actor = fields.Str(required=True)
    post = fields.Bool(description="Processing happens at end of scan")
    interval = fields.Int(
        description="Processing happens at periodic interval during scan", unit="s"
    )
    description = fields.Str(description="Description of the processing")

    @validates_schema
    def validate_type(self, data, **kwargs):
        if not data.get("post") and not data.get("interval"):
            raise ValidationError(
                "post or interval must be specified for a ProcessingSchema"
            )


class ScanConfigSchema(Schema):
    scans = fields.Nested(ScanSchema, many=True)
    processing = fields.Dict(
        keys=fields.Str(), values=fields.Nested(ScanProcessingSchema, many=True)
    )


# Endpoint Schema
class AvailableScansSchema(Schema):
    actor = fields.Str()
    processing = fields.Nested(ScanProcessingSchema, many=True)
