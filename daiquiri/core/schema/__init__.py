#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

import marshmallow
from marshmallow import fields, ValidationError, INCLUDE
from marshmallow_jsonschema import JSONSchema

from flask import g

from daiquiri.core import (
    CoreBase,
    CoreResource,
    marshal,
    require_valid_session,
    require_control,
)

logger = logging.getLogger(__name__)


class ErrorSchema(marshmallow.Schema):
    error = fields.Str()


class MessageSchema(marshmallow.Schema):
    message = fields.Str()


class SchemasSchema(marshmallow.Schema):
    name = fields.Str()


class SchemasResource(CoreResource):
    def get(self):
        """All schemas in a JSON API spec format"""
        return self._parent.schemas(), 200


class SchemaListResource(CoreResource):
    @marshal(out=[[200, SchemasSchema(many=True), "A list of schamas"]])
    def get(self):
        """List of schemas"""
        return [{"name": n} for n in self._parent.list()], 200


class SchemaResource(CoreResource):
    @marshal(out=[[404, ErrorSchema(), "No such schema"]])
    def get(self, name):
        """Get a single schema in JSON API Spec format"""
        schema = self._parent.get(name)
        if schema:
            return schema, 200
        else:
            return {"error": "No such schema"}, 404


class SchemaValidateResource(CoreResource):
    @require_control
    @marshal(inp={"data": fields.Dict()}, out=[[404, ErrorSchema(), "No such schema"]])
    def post(self, name, **kwargs):
        """Validate and compute calculated parameters for a schema"""
        validated = self._parent.validate(name, kwargs["data"])
        if validated:
            return validated, 200
        else:
            return {"error": "No such schema"}, 404


class Schema(CoreBase):
    """The schema handler

    Allows schemas to be registered, and retrieved via a flask resource
    """

    _require_session = True
    _schemas = {}

    def setup(self):
        self._schema = self
        self.register_route(SchemasResource, "")
        self.register_route(SchemaListResource, "/list")
        self.register_route(SchemaResource, "/<string:name>")
        self.register_route(SchemaValidateResource, "/validate/<string:name>")

    def set_session(self, session):
        self._session = session

    def register(self, schema, url=None, method=None):
        """Register a schema under its class name"""
        cls = schema.__class__.__name__
        if cls not in self._schemas:
            self._schemas[cls] = schema

        if url and method:
            self._schemas[cls].url = {"url": url, "method": method}

    def schemas(self):
        """ Get all schemas """
        return {key: self.get(key) for key in self._filter_schemas()}

    def list(self):
        """Get a list of registered schemas"""
        return self._filter_schemas()

    def _filter_schemas(self):
        keys = []
        for key, schema in self._schemas.items():
            if (hasattr(schema, "_require_staff") and g.user.staff()) or not hasattr(
                schema, "_require_staff"
            ):
                keys.append(key)
        return keys

    def iterate_schema(self, flds, root):
        for f, p in flds.items():
            if isinstance(p, fields.List):
                inner = p.inner

                if isinstance(inner, fields.Nested):
                    nested = inner.nested

                    if nested.Meta:
                        for k in ["uiorder", "uischema", "presets", "cache"]:
                            if hasattr(nested.Meta, k) and nested.__name__ in root:
                                root[nested.__name__][k] = getattr(nested.Meta, k)

                    # print('iterate_schema  list nested', f, root)
                    if nested.__name__ in root:
                        self.iterate_schema(
                            nested._declared_fields, root[nested.__name__]
                        )

            if isinstance(p, fields.Nested):
                nested = p.nested

                if nested.Meta:
                    for k in ["uiorder", "uischema", "presets", "cache"]:
                        if hasattr(nested.Meta, k) and nested.__name__ in root:
                            root[nested.__name__][k] = getattr(nested.Meta, k)

                # print('iterate_schema nested', f, dir(nested))
                if nested.__name__ in root:
                    self.iterate_schema(nested._declared_fields, root[nested.__name__])

    def get(self, name):
        """Get a specific schema

        Args:
            name (str): The schema to retreive
        """
        if name in self.list():
            schema = self._schemas[name]

            json_schema = JSONSchema()
            json = json_schema.dump(schema)

            if hasattr(schema, "url"):
                json["url"] = schema.url["url"]
                json["method"] = schema.url["method"]

            #  Try to attach ui:schema,order to nested schemas
            self.iterate_schema(schema.fields, json["definitions"])

            if schema.Meta:
                for k in ["uiorder", "uischema", "presets", "cache"]:
                    if hasattr(schema.Meta, k):
                        json[k] = getattr(schema.Meta, k)

            asyncValidate = False
            for m in ["calculated", "schema_validate", "time_estimate"]:
                if hasattr(schema, m):
                    asyncValidate = True

            json["asyncValidate"] = asyncValidate

            return json

    def validate(self, name, data):
        """Async Schema Validation

        This allows for schemas to make asynchronous validation, i.e. to check
        combinations of beamline parameters.

        It will also compute any calculated parameters if the schema defines them
        and also a time estimate if defined

        Args:
            name (str): Schema name
            data (dict): Kwargs to validate

        Returns:
            errors (dict): Dictionary of errors
            warnings (dict): Dictionary of warnings
            calculated (dict): Dictionary of calculated params
            time_estimate (float): Time estimate for these params
        """
        if name in self._schemas:
            sch = self._schemas[name]
            validated = {
                "errors": {},
                "warnings": {},
                "calculated": {},
                "time_estimate": None,
            }

            try:
                sch.load(data, unknown=INCLUDE)
            except ValidationError as err:
                print("ValidationError", err)
                if "_schema" in err.messages:
                    validated["errors"] = {"schema": err.messages["_schema"]}

            for k in ["calculated", "time_estimate", "warnings"]:
                if hasattr(sch, k):
                    try:
                        validated[k] = getattr(sch, k)(data)
                    except Exception as e:
                        logger.exception(f"{k} failed with: {e}")

            return validated
