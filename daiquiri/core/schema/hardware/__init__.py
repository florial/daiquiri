#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields
import marshmallow

from daiquiri.core.schema.validators import ValidatedRegexp, Any

import logging

logger = logging.getLogger(__name__)


class HardwareObjectBaseSchema(Schema):
    id = fields.Str()
    protocol = fields.Str()
    type = fields.Str()
    online = fields.Bool()
    name = fields.Str()
    require_staff = fields.Bool()
    callables = fields.Dict()
    properties = fields.Dict()


class SetObjectProperty(Schema):
    property = ValidatedRegexp("word", required=True)
    value = Any(required=True)


class CallObjectFunction(Schema):
    function = ValidatedRegexp("word", required=True)
    value = Any()


class HardwareGroupSchema(Schema):
    groupid = fields.Str()
    name = fields.Str()
    description = fields.Str()
    objects = fields.List(fields.Str())
    state = fields.Bool()


class HardwareTypeSchema(Schema):
    type = fields.Str()
    schema = fields.Str()


class HOConfigAttribute(Schema):
    """The Hardware Object Config Attribute Schema"""

    id = fields.Str(required=True, description="Attribute id")
    name = fields.Str(description="Attribute name (can be customised)")
    ui_schema = fields.Dict(description="Define how the UI renders this attribute")
    type = fields.Str(
        enum=["float", "int", "bool", "str"], description="Attribute type"
    )
    step = fields.Float(description="Step size for attribute")
    min = fields.Float(description="Minimum value for attribute")
    max = fields.Float(description="Maximum value for attribute")


class HOConfigSchema(Schema):
    """HardwareObject base configuration schema"""

    name = fields.Str(required=True, description="Object name")
    id = fields.Str(required=True, description="Object id")
    protocol = fields.Str(required=True, description="Protocol handler to use")
    require_staff = fields.Bool(
        description="Whether this object requires staff to modify"
    )
    attributes = fields.Nested(
        HOConfigAttribute,
        many=True,
        description="Attribute configuration for run time schemas",
    )


class HardwareSchema(Schema):
    def read_only(self, prop):
        return self.fields[prop].metadata.get("readOnly", False)

    def __contains__(self, val):
        return val in self.fields

    def __iter__(self):
        self.__iter = iter(self.fields.keys())
        return self.__iter

    def __next__(self):
        return next(self.__iter)

    def validate(self, prop, value):
        data = {}
        data[prop] = value
        valid = self.load(data)

        return valid[prop]
