#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
from marshmallow import Schema, fields


class SessionStorageSchema(Schema):
    sessionid = fields.UUID()
    token = fields.Str()
    last_access = fields.Float()
    operator = fields.Bool()
    data = fields.Dict()


class SessionStorage(ABC):
    """The abstract session storage interface"""

    _generate_token = None

    @staticmethod
    def set_generate_token(fn):
        """Set a method from which to generate the session token

        The function should return a token that can be stored into the session
        """
        assert callable(fn)
        SessionStorage._generate_token = fn

    @abstractmethod
    def create(self, data):
        """Create a new session
        
        Args:
            data (dict): Any data to store along side the basic session info

        Returns:
            session: A `SessionStorageSchema` validated dict
        """
        pass

    @abstractmethod
    def remove(self, sessionid, token=None):
        """Remove a session

        Args:
            sessionid (uuid): The sessionid to remove

        Kwargs:
            token (str): The token to remove if sessionid is none

        Returns:
            sessionid (uuid): The sessionid removed

        """
        pass

    @abstractmethod
    def list(self):
        """List existing sessions

        Returns:
            sessions (list(dict)): A list of `SessionStorageSchema` validated dicts
        """
        pass

    @abstractmethod
    def get(self, sessionid):
        """Get a session

        Args:
            sessionid (uuid): The sessionid

        Returns:
            session: A `SessionStorageSchema` validated dict
        """
        pass

    @abstractmethod
    def exists(self, sessionid):
        """Check if a session exists

        Args:
            sessionid (uuid): The sessionid

        Returns:
            True if the session exists, False if not
        """
        pass

    @abstractmethod
    def update(self, sessionid, **kwargs):
        """Update a a value in the data dict of a session

        Updates a data property on the session and updates last_access time
        Args:
            sessionid (uuid): The sessionid

        Kwargs:
            The properties in the data dict to update

        """
        pass

    @abstractmethod
    def take_control(self, sessionid):
        """Set operator=True for the specified session
        
        Sets the operator to True for this sessionid and to false for all the other existing
        sessions

        Args:
            sessionid (uuid): The sessionid
        """
        pass

    @abstractmethod
    def yield_control(self, sessionid):
        """Set operator=False for the specified session
        
        If the current session has control yield it. Return true if control was yielded

        Args:
            sessionid (uuid): The sessionid
        """
        pass

    @abstractmethod
    def has_control(self, sessionid):
        """Check if this session has control

        Args:
            sessionid (uuid): The sessionid

        Returns:
            True if sessionid operator=True, False if not
        """
        pass

    @abstractmethod
    def no_control(self):
        """ Check if someone in the application has control

        Returns:
            True if no one has control
        """
        pass
