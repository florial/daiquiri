#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
from abc import ABC, abstractmethod

from flask import Blueprint, request, jsonify, g
from flask_restful import Api, abort
from flask_apispec import MethodResource, doc as apispec_doc, marshal_with, use_kwargs

from marshmallow import fields

import logging

logger = logging.getLogger(__name__)


def require_valid_session(fn):
    """Require Valid Session
    
    Decorate a route class http method with this to enforce that there is a valid
    session token with the request
    """
    fn._require_valid_session = True
    return fn


def require_staff(fn):
    """Require User to be Staff
    
    Decorate a route class http method with this to enforce that there is a staff
    member making the request
    """
    fn._require_staff = True
    return fn


def no_blsession_required(fn):
    """Indicate that a blsession is not required

    """
    fn._no_blsession_required = True
    return fn


def require_control(fn):
    """Require Control Decorator
    
    Decorate a route class http method with this to enforce that the current session
    has control for the request to be served
    """
    fn._require_control = True
    return fn


# @doc(description="Get list of current sessions", tags=["session"])
def doc(description, tags=[]):
    def wrap(f):
        return apispec_doc(description=description)(f)

    return wrap


# @marshal_with(SessionListSchema(many=True), code=200)
# @marshal_with(ErrorSchema(), code=401)
def marshal(inp=None, out=[], paged=False, filtered=False, ordered=False):
    """Marshal Decorator

    Marshals this request with input and output schemas, and appends any
    input schema

    Args:
        inp (schema): Input schema to validate request with
        out (list[list[code, schema, description]]): A list of response codes, schemas, and descriptions
        paged (bool): Enable paging inputs: per_page, page
        filtered (bool): Enable filtering inputs: s
        ordered (bool): Enable ordering inputs: order, order_by
    """

    def wrap(f):
        inp_copy = inp
        if (inp_copy is None) and (paged or filtered or ordered):
            inp_copy = {}

        if inp_copy is not None:
            if filtered:
                inp_copy["s"] = fields.Str()

            if paged:
                inp_copy["page"] = fields.Int()
                inp_copy["per_page"] = fields.Int()

            if ordered:
                inp_copy["order"] = fields.Str(enum=["asc", "desc"], default="asc")
                inp_copy["order_by"] = fields.Str()

            f = use_kwargs(inp_copy)(f)
            if inspect.isclass(inp_copy):
                f.__schemas__ = [inp_copy()]

        for i in out:
            desc = None
            if len(i) > 2:
                desc = i[2]
            f = marshal_with(i[1], code=i[0], description=desc)(f)

        return f

    return wrap


class CoreResource(MethodResource):
    """CoreResource is the base resource class that all flask Resources inherit from
    
    kwargs are mapped to _(kwargs) for convenince
    """

    def __init__(self, *args, **kwargs):
        # logger.debug('Loaded {c}'.format(c=self.__class__.__name__))
        self.__dict__.update(("_" + k, v) for k, v in kwargs.items())


class CoreBase(ABC):
    """CoreBase is the class that all application module inherit from

    The CoreBase maps kwargs to _(kwargs) for convenience, sets up a blueprint if
    _blueprint = True (default), provides a convenience method to register flask route
    and registers swagger documentation. The blueprint is registered with a prefix of
    the current class name

    """

    _blueprint = True
    _base_url = None
    _namespace = None
    _require_session = False
    _require_blsession = False

    def __init__(self, *args, **kwargs):
        self.initkwargs = kwargs
        self.__dict__.update(("_" + k, v) for k, v in kwargs.items())

        self._bp = self.__class__.__name__
        if self._blueprint:
            self._api_bp = Blueprint(self._bp, __name__)
            self._api = Api(self._api_bp)
            self._docs_to_reg = []
            self._app.before_request_funcs[self._bp] = [self.before_request]

        self.setup()

        if self._blueprint:
            if self._base_url is None:
                logger.warning(
                    f"base_url is empty, defaulting to class name: {self._bp.lower()}"
                )
                self._base_url = self._bp.lower()

            kwargs["app"].register_blueprint(
                self._api_bp, url_prefix=f"/api/{self._base_url}"
            )
            for d in self._docs_to_reg:
                kwargs["docs"].register(d, blueprint=self._bp)

            self.after_setup()

    def after_setup(self):
        pass

    def before_request(self):
        """Flask before_request handler
        
        Checks the current session is valid (delegates to Session.require_valid_session),
        then check if the route class function has a _require control property as set by
        the above decorator.

        Returns:
            None if everything is valid, otherwise return a json error to the request
        """
        logger.debug(f"Core:before_request {request.method}")
        if request.method == "OPTIONS":
            return

        # This is delving pretty deep into the internals of flask
        # May be a better way to achieve this
        if request.endpoint in self._app.view_functions:
            view_func = self._app.view_functions[request.endpoint]
            cls = view_func.view_class
            method = getattr(cls, request.method.lower())

            if self._require_session or hasattr(method, "_require_valid_session"):
                require_blsession = self._require_blsession
                if hasattr(method, "_no_blsession_required"):
                    require_blsession = False

                invalid = self._session.require_valid_session(
                    require_blsession=require_blsession
                )
                if invalid:
                    return jsonify(invalid[0]), invalid[1]

                req_staff = hasattr(method, "_require_staff")
                if req_staff:
                    staff = g.user.staff()
                    if not g.user.staff():
                        # The best response is to return 405 rather than give
                        # away that this resource might exist
                        abort(405)

                req_control = hasattr(method, "_require_control")
                if req_control:
                    nocontrol = self._session.require_control()
                    # print("before_request: no control", nocontrol)
                    if nocontrol:
                        return jsonify(nocontrol), 400

    def register_route(self, route_class, url, route_keywords={}):
        """Register a flask route with a route class
        
        By deault the parent (this class) is passed into the route class for convenience
        The doc string from each http method on the class is regsitered into the swagger
        docs. Any input schema are also registered into the schema resource for use by the
        client

        Args:
            route_class (obj:CoreResource): A CoreResource class to register
            url (str): The slug to register the route class under
            route_keywors (dict): Any other keywords to pass to the route class
        """
        kwargs = {"parent": self}
        self._api.add_resource(
            route_class,
            url,
            resource_class_kwargs={**self.initkwargs, **kwargs, **route_keywords},
        )

        for k in ["post", "get", "put", "patch", "delete"]:
            fn = getattr(route_class, k, None)
            if fn:
                security = []
                if self._require_session or hasattr(fn, "_require_valid_session"):
                    security.append({"bearer": []})

                fn = apispec_doc(
                    description=fn.__doc__, tags=[self._bp], security=security
                )(fn)
                if hasattr(fn, "__schemas__") and self._schema:
                    for sch in fn.__schemas__:
                        path = self._base_url if self._base_url else self._bp.lower()
                        self._schema.register(sch, f"/{path}{url}", k)

        self._docs_to_reg.append(route_class)

    def emit(self, *args, **kwargs):
        """Convenience method to emit a socketio event"""
        if "namespace" not in kwargs:
            kwargs["namespace"] = f"/{self._namespace}"

        self._socketio.emit(*args, **kwargs)

    def on(self, *args, **kwargs):
        """Convenience method to register a callback for a socketio event"""
        if "namespace" not in kwargs:
            kwargs["namespace"] = f"/{self._namespace}"

        if (
            args[0] in ["connect", "disconnect"]
            and self.__class__.__name__ != "Session"
        ):
            raise KeyError(
                "Connect and disconnect events cannot be overridden by components"
            )

        return self._socketio.on(*args, **kwargs)

    @abstractmethod
    def setup(self):
        """Setup Initialiser
        
        Abstract method for any setup for the child class
        """
        pass

    def reload(self):
        """Component Reloader
        
        A function that can reload the component once the config file
        has been reloaded. Each component must decide how much of its 
        internal state to reload
        """
        pass
