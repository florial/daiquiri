# Basic Configuration
All configuration of Daiquiri is done using yaml files

!!! tip
    An example of each configuration file can be found in `daiquiri/resources/config`

!!! info
    For general usage configuration files should be stored in the local beamline project i.e. `daiquiri_idxx/daiquiri_idxx/resources/config`

## config.yml
This file defines the core configuration of the application. The example can be copied from `daiquiri/resources/config` to `daiquiri_idxx/daiquiri_idxx/resources/config/config.yml` and updated accordingly.

### Secrets
Unique secrets should be generated for `iosecret`, `secret`, and `url_secret`, this can be done with:
```bash
cat /dev/urandom | env LC_CTYPE=C tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1
```

### SSL
SSL must be enabled and for this to work a local ssl key / cert pair must be available which matche the machine name, then can be defined with:
```yaml
ssl: true
ssl_cert: my_cert.bundle
ssl_key: my_key.key
```

### Authentication
Authentication is modular, currently supporting `ldap`
```yaml
auth_type: ldap
auth_server: ldap.esrf.fr
```

### Metadata
The `metadata` handler should be set to `ispyalchemy` to make use of the mysql database. For worse case scenarios it is possible to drop back to a `mock` `metadata` handler. The `meta_beamline` should match the beamline session information, usually the beamline in upper case. `meta_staff` denotes the permission for which daiquiri deems the current user to be a staff member
```yaml
meta_type: ispyalchemy
meta_user: daiquiri
meta_pass: pass
meta_url: host:3306/daiquiri
meta_beamline: ID21
meta_staff: id21_staff
```

### Saving
The data policy can be automatically applied by using daiquiri's saving
```yaml
saving_type: bliss_esrf
saving_session: bliss_session
saving_arguments:
    proposal: "{sessionid.proposal}"
    sample: "{sampleid.name}"
    dataset: "{datacollectionid}"
```

### Controls Session
A controls session can be injected automatically into actors to make available session globals:
```yaml
controls_session_type: bliss
controls_session_name: bliss_session
```

### Implementors
The implementors directive should point to the local project implementors package name. This will be automatically set by the `daiquiri-server-idxx` shell script
```yaml
implementors: daiquiri_id21.implementors
```

## components.yml
Components can be loaded by defining them in `components.yml`. The `daiquiri/resources/components.yml` comes with a set of sensible defaults. Each component itself takes a yaml file for configuration. This is also defined in this file. A list of available components can be found in [Components](components/introduction.md) 
```yaml
components:
  - type: logging

  - type: scans
    config: scans.yml

  - type: hardware
    config: hardware.yml

  - type: synoptic
    config: synoptic.yml

  - type: simplescan
    config: simplescan.yml
```
