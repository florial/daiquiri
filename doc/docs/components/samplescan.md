Sample Scan is a simple component that allows [Actors](../actors.md) to be executed against a sample. The configuration yaml just requires a list of scans, and whether they are to be restricted to staff members. 

An example config is provided as follow:
```yaml
scans:
  - actor: scan1
  - actor: scan2
    require_staff: true
```
