Daiquiri segments functionality into `components`. These components can be selectively loaded via configuration as described in the [Configuration](../configuration.md#componentsyml) section. Components can execute [Actors](../actors.md) segregating UI code from beamline specific code

## Actors

!!! info
    For general usage actor files should be stored in the local beamline project i.e. `daiquiri_idxx/daiquiri_idxx/implementors`

The actors for components should be placed into a subfolder with the lowercase name of the component in the local implementors folder for each component. For example the Samplescan component the actors would live in
```
implementors/samplescan
```

!!! example
    Say the local project is `daiquiri_id01`, the actors for Samplescan would live in:
    ```
    daiquiri_id01/daiquiri_id01/implementors/samplescan/scan1.py
    ```
