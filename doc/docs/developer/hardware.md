Creating a new hardware object is relatively straight forward. If creating a new type of object, first an abstract object needs to be created, then the controls system specific implementation

For example consider create a motor (assuming one doesnt exist), first create the abstract object:

```python
# daiquiri/core/hardware/abstract/motor.py
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty


MotorStates = ["READY", "MOVING", "FAULT", "UNKNOWN", "LOWLIMIT", "HIGHLIMIT"]


class MotorPropertiesSchema(HardwareSchema):
    position = fields.Float()
    ...
    unit = fields.Str(readOnly=True)


class MotorCallablesSchema(HardwareSchema):
    move = fields.Float()
    stop = RequireEmpty()
    wait = RequireEmpty()


class Motor(HardwareObject):
    _type = "motor"
    _state_ok = [MotorStates[0], MotorStates[1]]

    _properties = MotorPropertiesSchema()
    _callables = MotorCallablesSchema()
```

An abstract object defines `Properties` and `Callables` along with a validation schema. `_state_ok` is used to define which of the states are considered *good*, and when objects are in a group to define whether the group status is *ok*. This is displayed as a green status symbol in the synoptic view for instance.

Then create the controls system specific implementation. In the case of `Bliss` this is a simple mapping between the Bliss objects properties / functions and the abstract object.

```python
# daiquiri/core/hardware/bliss/motor.py
from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.motor import Motor as AbstractMotor, MotorStates
from daiquiri.core.hardware.bliss.object import BlissObject


class BlissMotorTranslator(HardwareTranslator):
    ...

    def from_state(self, value):
        states = []
        for s in MotorStates:
            if s in value:
                states.append(s)

        if len(states):
            return states

        return ["UNKNOWN"]

    def from_position(self, value):
        return round(value, 4)


class Motor(BlissObject, AbstractMotor):
    translator = BlissMotorTranslator

    property_map = {
        "position": "position",
        ...
        "unit": "unit",
    }

    callable_map = {"stop": "stop", "wait": "wait_move"}
```

The controls specific implementation can also provide a `HardwareTranslator`, this allows the object to translate between for example abstract states and the control system states. For a motor this might be as simple as converting an Bliss objects `object.Ready` to a 'READY' string

Finally for Bliss objects a mapping must be made between the defined class and the class of the Bliss object. This can be done by appending to `_class_map` in `daiquiri/core/hardware/bliss/__init__.py`.

```python
_class_map = {
    # Bliss class: daiquiri class
    "Axis": "motor",
    ...
}
```

For tango objects the class is auto loaded from the `type` property defined in `hardware.yml`

```yaml
  - name: lima
    id: lima_simulator
    protocol: tango
    type: lima
    tango_url: tango://localhost:20000/id00/limaccds/simulator1
```

!!! info "Module Resolution"
    The loader expects the daiquiri classname in lower case. It will resolve to, 
    for example: 

     * motor -> bliss.motor.Motor
     * measurementgroup -> bliss.measurementgroup.Measurementgroup
     * lima -> tango.lima.Lima
