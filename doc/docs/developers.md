# Developing

## Code Style
Code linting uses flake8 and style is enforced using [black](https://github.com/psf/black). CI on the server will automatically enforce style, but not linting.

## Setup

### Local Database
A local database can be installed for development and testing. The simplest way to do this is with docker:
```
docker run --rm -e MYSQL_ROOT_PASSWORD=password -e MYSQL_PASSWORD=daiquiri -e MYSQL_USER=daiquiri -e MYSQL_DATABASE=daiquiri -p 4406:3306 mariadb:latest
```

Then insert the test data:
```
./scripts/create_database.sh <database> <user> <pass>
```

## Testing
Testing uses pytest. As daiquiri has a database dependency, CI based testing will generate a new database on each run. For running tests locally a local empty database is required. Tests will drop the existing database and create a new one each time tests are run.

A local database for testing can be started easily with docker:
```
docker run --rm -e MYSQL_ROOT_PASSWORD=password -e MYSQL_PASSWORD=test -e MYSQL_USER=test -e MYSQL_DATABASE=test -p 4406:3306 mariadb:latest
```
This will expose a mariadb/mysql database locally on port 4406 (to avoid conflicts with a local running database)

Then in `tests/resources/config/config.yml` the database url `meta_url` needs updating:
```
meta_url: localhost:4406/test
```

Now tests can be run normally
```
pytest
```

## CI

Gitlab CI will run an extensive pipeline upon branch / master commit including:

* checks:
    - flake8 for linting
    - black for style
    - bandit for security

* tests:
    - [coverage](https://ui.gitlab-pages.esrf.fr/daiquiri/coverage/) for master

* build
    - swagger/open api [specification](https://ui.gitlab-pages.esrf.fr/daiquiri/api/spec/spec.json)

* documentation
    - user documentation (these pages)
    - [redoc](https://github.com/Redocly/redoc) api [documentation](https://ui.gitlab-pages.esrf.fr/daiquiri/api/spec/)

