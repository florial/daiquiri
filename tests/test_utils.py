from pytest import approx
from daiquiri.core.utils import to_wavelength, to_energy

wavelength1 = 0.971054
wavelength2 = 1.549802

energy1 = 12768
energy2 = 8000


def test_to_wavelength():
    wave = to_wavelength(energy1)
    assert wavelength1 == approx(wave)

    wave = to_wavelength(energy2)
    assert wavelength2 == approx(wave)


def test_to_energy():
    energy = to_energy(wavelength1)
    assert energy1 == approx(energy)

    energy = to_energy(wavelength2)
    assert energy2 == approx(energy)
