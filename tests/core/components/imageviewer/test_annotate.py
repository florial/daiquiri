import os
import pytest
from pytest import approx

import numpy as np
from PIL import Image, ImageChops
from daiquiri.core.components.imageviewer.annotate import AnnotateImage

DIR_PATH = os.path.dirname(os.path.realpath(__file__))

DATA = {
    "focalpoint": [-63, 42],
    "downstream": "false",
    "image": {
        "center": [-2636248.050000000, -4644439.94000000],
        "pixelsize": [1497.6500000, -1551.43000000],
    },
    "beam": {
        "position": [-2730600.0, -4709600.0],
        "center": [-2730482.6321093035, -4709600.0],
    },
    "subsample": {
        "subsampleid": 1,
        "type": "roi",
        "x": -2730637,
        "x2": -2610637,
        "y": 4709605,
        "y2": 4829605,
    },
}

DATA2 = {
    "focalpoint": [63, 42],
    "downstream": "true",
    "image": {
        "center": [2045848.0499999993, -4166239.9399999995],
        "pixelsize": [-1497.6499999999976, -1551.4300000000046],
    },
    "beam": {
        "position": [2140199.9999999995, -4231399.999999999],
        "center": [2140082.632109303, -4231399.999999999],
    },
    "subsample": {
        "subsampleid": 2,
        "type": "roi",
        "x": 2140152,
        "x2": 2740152,
        "y": 4231416,
        "y2": 4831416,
    },
}


@pytest.mark.parametrize("data", (DATA, DATA2))
def test_annotate(data):
    orig_path = f"{DIR_PATH}/assets/downstream_{data['downstream']}.png"
    compare_path = f"{DIR_PATH}/assets/downstream_{data['downstream']}_annotated.png"
    copy_path = f"/tmp/downstream_{data['downstream']}_test.png"

    with Image.open(orig_path) as orig:
        copy = orig.copy()
        copy.save(copy_path)

    ann = AnnotateImage(copy_path)
    details = ann.annotate(
        data["image"]["center"],
        data["beam"]["position"],
        data["image"]["pixelsize"],
        1e-9,
        data["subsample"],
    )

    with Image.open(compare_path) as compare:
        with Image.open(copy_path) as copy:
            img_diff = ImageChops.difference(compare, copy)
            diff = np.array(img_diff)

            assert diff.sum() == approx(0)

    os.unlink(copy_path)
