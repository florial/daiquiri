import pytest
from bliss.config.static import get_config


@pytest.fixture
def bliss_omega(app):
    return get_config().get("omega")


@pytest.fixture
def bliss_diode(app):
    return get_config().get("diode")


@pytest.fixture
def bliss_mca(app):
    return get_config().get("simu1")


@pytest.fixture
def scans(app):
    yield app.components.get_component("scans")
