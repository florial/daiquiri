def test_login(with_database, client):
    res = client.post(
        "/api/authenticator/login", data={"username": "abcd", "password": "abcd"}
    )
    assert res.status_code == 201


def test_login_fail(with_database, client):
    res = client.post(
        "/api/authenticator/login", data={"username": "abce", "password": "abcd"}
    )
    assert res.status_code == 401


def test_app(with_database, client):
    res = client.get("/api/components/config")
    assert res.status_code == 200
