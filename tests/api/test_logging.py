def test_log(auth_client, with_session):
    res = auth_client.get("/api/logging")

    assert res.status_code == 400


def test_log_admin(auth_client_admin, with_session_admin):
    res = auth_client_admin.get("/api/logging")

    assert res.status_code == 200


def test_log_ui_error(auth_client_admin, with_session_admin):
    ui_error = {
        "status": "resolved",
        "message": "a is not defined",
        "frame": "componentWillUnmount (/path/to/daiquiri-ui/src/components/2dview/Overlay.jsx:134:)",
        "store": {"session": {"key": 1}},
    }

    # Need to use json={} for nested dict
    res = auth_client_admin.post("/api/logging/ui", json=ui_error)
    assert res.status_code == 200

    res = auth_client_admin.get("/api/logging")
    assert res.status_code == 200

    ui = res.json["logs"]["ui"]

    assert ui[0]["message"] == ui_error["message"]


def test_log_ui_error_unresolved(auth_client_admin, with_session_admin):
    ui_error = {
        "status": "error",
        "message": "a is not defined",
        "error": "Erorr resolving msf.js:135",
        "store": {"session": {"key": "val"}},
    }

    res = auth_client_admin.post("/api/logging/ui", json=ui_error)
    assert res.status_code == 200

    res = auth_client_admin.get("/api/logging")
    assert res.status_code == 200

    ui = res.json["logs"]["ui"]

    assert ui[0]["message"] == ui_error["message"]
