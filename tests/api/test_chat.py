def test_get_messages(auth_client, with_session):
    res = auth_client.get("/api/chat")

    assert res.status_code == 200


def test_add_message(auth_client, with_session):
    message = auth_client.post("/api/chat", payload={"message": "test message"})

    assert message.status_code == 200

    messages = auth_client.get("/api/chat")

    assert messages.status_code == 200
    assert messages.json["total"] == 1
    assert "test message" in messages.json["rows"][0]["message"]


def test_mark_messages_read(auth_client, with_session):
    message = auth_client.post("/api/chat", payload={"message": "test message"})
    assert message.status_code == 200

    mark_all = auth_client.patch("/api/chat")
    assert mark_all.status_code == 200

    assert mark_all.json["maxid"] == message.json["messageid"]


def test_mark_message_read(auth_client, with_session):
    message = auth_client.post("/api/chat", payload={"message": "test message"})
    assert message.status_code == 200

    # Message should already be read, return 400
    mark = auth_client.patch(f"/api/chat/{message.json['messageid']}")
    assert mark.status_code == 400
