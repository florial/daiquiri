import time
from pytest import approx


def test_sources(app, auth_client, with_session, translation):
    for m in translation:
        m.move(0.5)
        m.wait()

    res = auth_client.get("/api/imageviewer/sources")
    assert res.status_code == 200

    beam = res.json[0]["markings"]["beam"]["position"]
    assert beam[0] == approx(500000, rel=1e-2)
    assert beam[1] == approx(-500000, rel=1e-2)


def test_sources_image(app, auth_client, with_session, lima):
    res = auth_client.post("/api/imageviewer/sources/image", payload={"sampleid": 1})
    assert res.status_code == 200


def test_move(auth_client, with_session, with_control, translation):
    m1, m2 = translation

    res = auth_client.post(
        "/api/imageviewer/move", payload={"x": 1000000, "y": 1000000}
    )
    assert res.status_code == 200

    while "MOVING" not in m1.get("state"):
        time.sleep(1)

    m1.wait()
    m2.wait()

    assert m1.get("position") == approx(1, rel=1e-3)
    assert m2.get("position") == approx(1, rel=1e-3)


def test_move_to(app, auth_client, with_session, with_control, translation):
    m1, m2 = translation

    res = auth_client.post("/api/imageviewer/move/1")
    assert res.status_code == 200

    while "MOVING" not in m1.get("state"):
        time.sleep(1)

    m1.wait()
    m2.wait()

    assert m1.get("position") == approx(7.41, rel=1e-3)
    assert m2.get("position") == approx(0.907, rel=1e-3)
