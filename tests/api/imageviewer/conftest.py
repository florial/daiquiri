import pytest
import time


@pytest.fixture
def lima(app, lima_simulator):
    device_fqdn, proxy = lima_simulator
    proxy.video_live = True

    lima = app.hardware.get_object("lima_simulator")

    # Wait for daiquiri to get object
    while not lima.online():
        time.sleep(1)

    # Wait until a frame is available
    while lima.frame() is None:
        time.sleep(1)

    yield lima


@pytest.fixture
def translation(app):
    m1 = app.hardware.get_object("m1")
    m2 = app.hardware.get_object("m2")

    yield m1, m2
