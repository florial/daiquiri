import os


def test_get_directories(auth_client_admin, with_session_admin):
    res = auth_client_admin.get("/api/editor/directory")
    assert res.status_code == 200


def test_get_file(auth_client_admin, with_session_admin, resource_folder):
    file = os.path.join(resource_folder, "config", "components.yml")
    res = auth_client_admin.get(f"/api/editor/file{os.path.abspath(file)}")

    assert res.status_code == 200

    with open(file) as file:
        assert res.json["contents"] == file.read()


def test_get_file_404(auth_client_admin, with_session_admin, resource_folder):
    file = os.path.join(resource_folder, "config", "test.yml")
    res = auth_client_admin.get(f"/api/editor/file{os.path.abspath(file)}")

    assert res.status_code == 400


def test_get_file_not_in_root(auth_client_admin, with_session_admin, resource_folder):
    res = auth_client_admin.get("/api/editor/file/var/log/beacon.log")

    assert res.status_code == 400


def test_modify_file(auth_client_admin, with_session_admin, resource_folder):
    test_file = os.path.join(resource_folder, "config", "test.yml")

    with open(test_file, "w") as f:
        f.write("value: true")

    new_contents = "value: false"
    res = auth_client_admin.patch(
        f"/api/editor/file{os.path.abspath(test_file)}",
        payload={"contents": new_contents},
    )

    assert res.status_code == 200

    with open(test_file) as file:
        assert new_contents == file.read()

    os.unlink(test_file)
