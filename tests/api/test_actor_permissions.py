import pytest

PAYLOADS = [
    {
        "sampleid": 1,
        "motor_start": 0,
        "motor_end": 10,
        "npoints": 5,
        "time": 0.1,
        "detectors": ["diode"],
    },
    {"sampleid": 1, "motor_start": 0, "motor_end": 10, "detectors": ["diode"]},
]


CLIENTS = [
    ("auth_client", "with_session", "with_control", PAYLOADS, [200, 405]),
    (
        "auth_client_admin",
        "with_session_admin",
        "with_control_admin",
        PAYLOADS,
        [200, 200],
    ),
]

# Test that none admin client cannot start actors with require_staff
@pytest.mark.parametrize(
    "get_client, get_with_session, get_with_control, payloads, expected",
    CLIENTS,
    indirect=["get_client", "get_with_session", "get_with_control"],
)
def test_actor_permissions(
    get_client, get_with_session, get_with_control, payloads, expected
):
    res = get_client.post("/api/samplescan/scan1", payload=payloads[0])
    assert res.status_code == expected[0]

    res = get_client.post("/api/samplescan/scan2", payload=payloads[1])
    assert res.status_code == expected[1]

    # Clean up
    res = get_client.delete("/api/queue")
