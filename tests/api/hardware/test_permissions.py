import pytest

CLIENTS = [
    ("auth_client", "with_session", [404, 200]),
    ("auth_client_admin", "with_session_admin", [200, 200]),
]

# Test that none admin client cannot list hardware objects with require_staff
@pytest.mark.parametrize(
    "get_client, get_with_session, expected",
    CLIENTS,
    indirect=["get_client", "get_with_session"],
)
def test_hardware_get(get_client, get_with_session, expected):
    res = get_client.get("/api/hardware/s1ho")
    assert res.status_code == expected[0]

    res = get_client.get("/api/hardware/s1hg")
    assert res.status_code == expected[1]


CLIENTS = [
    ("auth_client", "with_session", "with_control", [404, 200]),
    ("auth_client_admin", "with_session_admin", "with_control_admin", [200, 200]),
]

METHODS = [
    ("put", {"property": "position", "value": "2"}),
    ("post", {"function": "stop"}),
]

# Test that none admin client cannot change hardware objects with require_staff
@pytest.mark.parametrize("method, payload", METHODS)
@pytest.mark.parametrize(
    "get_client, get_with_session, get_with_control, expected",
    CLIENTS,
    indirect=["get_client", "get_with_session", "get_with_control"],
)
def test_hardware(
    get_client, get_with_session, get_with_control, expected, method, payload
):
    res = getattr(get_client, method)("/api/hardware/s1ho", payload=payload)
    assert res.status_code == expected[0]

    res = getattr(get_client, method)("/api/hardware/s1hg", payload=payload)
    assert res.status_code == expected[1]
