import time
import pytest


@pytest.mark.parametrize("auth_sioclient", ["/hardware"], indirect=True)
def test_hardware_object_event(
    app, auth_client, auth_sioclient, with_session, with_control, omega
):
    value = 5
    res = auth_client.post(
        "/api/hardware/omega", payload={"function": "move", "value": value}
    )
    assert res.status_code == 200

    omega.wait()

    # Events could be delayed by up to 0.5s as they are throttled
    # TODO: Poor synchronisation :(
    time.sleep(1)

    res = auth_client.get("/api/hardware/omega")
    assert res.status_code == 200

    messages = auth_sioclient.get_received("/hardware")

    position = False
    ready = False
    for m in messages:
        if m["name"] == "change":
            if m["args"][0]["data"].get("state") == res.json["properties"]["state"]:
                ready = True

            if m["args"][0]["data"].get("position") == value:
                position = True

    assert all([position, ready])
