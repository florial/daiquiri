def test_get_user(auth_client):
    res = auth_client.get("/api/metadata/users/current")
    assert res.status_code == 200


def test_get_sessions(auth_client):
    res = auth_client.get("/api/metadata/sessions")
    assert res.status_code == 200


def test_get_samples(auth_client, with_session):
    res = auth_client.get("/api/metadata/samples")
    assert res.status_code == 200


def test_add_sample(auth_client, with_session):
    data = {"name": "test", "offsetx": 0, "offsety": 0}

    res = auth_client.post("/api/metadata/samples", payload=data)
    assert res.status_code == 201

    for k in data:
        assert res.json[k] == data[k]


def test_update_sample(auth_client, with_session):
    res_init = auth_client.get("/api/metadata/samples")
    last = res_init.json[-1]

    name = "test1"
    res = auth_client.patch(
        f"/api/metadata/samples/{last['sampleid']}", payload={"name": name}
    )

    assert res.status_code == 200
    assert res.json["name"] == name


def test_get_subsamples(auth_client, with_session):
    res = auth_client.get("/api/metadata/samples/sub")
    assert res.status_code == 200


def test_add_subsample(auth_client, with_session):
    res_init = auth_client.get("/api/metadata/samples")
    last = res_init.json[-1]["sampleid"]

    data = {"sampleid": last, "x": 1000, "y": 2000, "type": "poi"}

    res = auth_client.post("/api/metadata/samples/sub", payload=data)
    assert res.status_code == 201

    for k in data:
        assert res.json[k] == data[k]


def test_update_subsample(auth_client, with_session):
    res_init = auth_client.get("/api/metadata/samples/sub")
    last = res_init.json[-1]["subsampleid"]

    x = 1500
    res = auth_client.patch(f"/api/metadata/samples/sub/{last}", payload={"x": x})

    assert res.status_code == 200
    assert res.json["x"] == x


def test_remove_subsample(auth_client, with_session):
    res_init = auth_client.get("/api/metadata/samples/sub")
    last = res_init.json[-1]["subsampleid"]

    res = auth_client.delete(f"/api/metadata/samples/sub/{last}")
    assert res.status_code == 200


def test_get_sampleimages(auth_client, with_session):
    res = auth_client.get("/api/metadata/samples/images")
    assert res.status_code == 200


def test_add_sampleimage(auth_client, with_session):
    pass


def test_get_datacollections(auth_client, with_session):
    res = auth_client.get("/api/metadata/datacollections")
    assert res.status_code == 200


def test_get_xrf_maps(auth_client, with_session):
    res = auth_client.get("/api/metadata/xrf/maps")
    assert res.status_code == 200


def test_update_xrf_map(auth_client, with_session):
    res_init = auth_client.get("/api/metadata/xrf/maps")
    last = res_init.json[-1]["mapid"]

    opacity = 0.5
    res = auth_client.patch(
        f"/api/metadata/xrf/maps/{last}", payload={"opacity": opacity}
    )
    assert res.json["opacity"] == opacity


def test_remove_xrf_map(auth_client, with_session):
    res = auth_client.get("/api/metadata/xrf/maps")
    last = res.json[-1]["mapid"]

    res = auth_client.delete(f"/api/metadata/xrf/maps/{last}")
    assert res.status_code == 200


def test_get_xrf_composite(auth_client, with_session):
    res = auth_client.get("/api/metadata/xrf/maps/composite")
    assert res.status_code == 200


def test_add_xrf_composite(auth_client, with_session, with_control):
    res_init = auth_client.get("/api/metadata/xrf/maps")
    last = res_init.json[-1]

    data = {
        "subsampleid": last["subsampleid"],
        "r": last["mapid"],
        "g": last["mapid"],
        "b": last["mapid"],
    }

    res = auth_client.post("/api/metadata/xrf/maps/composite", payload=data)
    assert res.status_code == 201

    for k in data:
        assert res.json[k] == data[k]


def test_update_xrf_composite(auth_client, with_session):
    res_init = auth_client.get("/api/metadata/xrf/maps/composite")
    last = res_init.json[-1]["compositeid"]

    opacity = 0.5
    res = auth_client.patch(
        f"/api/metadata/xrf/maps/composite/{last}", payload={"opacity": opacity}
    )
    assert res.json["opacity"] == opacity


def test_remove_xrf_composite(auth_client, with_session):
    res = auth_client.get("/api/metadata/xrf/maps/composite")
    last = res.json[-1]["compositeid"]

    res = auth_client.delete(f"/api/metadata/xrf/maps/composite/{last}")
    assert res.status_code == 200


def test_get_xrf_map_rois(auth_client, with_session):
    res = auth_client.get("/api/metadata/xrf/maps/rois")
    assert res.status_code == 200


def test_add_xrf_map_rois(auth_client, with_session, with_control):
    res = auth_client.get("/api/metadata/samples")
    assert res.status_code == 200

    data = {
        "start": 100,
        "end": 200,
        "element": "A",
        "edge": "b",
        "sampleid": res.json[-1]["sampleid"],
    }

    res = auth_client.post("/api/metadata/xrf/maps/rois", payload=data)
    assert res.status_code == 201

    for k in data:
        assert res.json[k] == data[k]


def test_update_xrf_map_roi(auth_client, with_session, with_control):
    res_init = auth_client.get("/api/metadata/xrf/maps/rois")
    last = res_init.json[0]

    start = 50
    res = auth_client.patch(
        f"/api/metadata/xrf/maps/rois/{last['maproiid']}",
        payload={
            "start": start,
            "end": last["end"],
            "element": last["element"],
            "edge": last["edge"],
            "sampleid": last["sampleid"],
        },
    )

    assert res.status_code == 200
    assert res.json["start"] == start


def test_remove_xrf_map_roi(auth_client, with_session, with_control):
    res = auth_client.get("/api/metadata/xrf/maps/rois")
    last = res.json[-1]["maproiid"]

    res = auth_client.delete(f"/api/metadata/xrf/maps/rois/{last}")
    assert res.status_code == 200
