import datetime

# Test functions that are used within the application without a flask app_context


def test_add_datacollection(metadata):
    data = {
        "sessionid": 1,
        "sampleid": 1,
        "subsampleid": 1,
        "starttime": datetime.datetime.now() - datetime.timedelta(seconds=600),
        "endtime": datetime.datetime.now() - datetime.timedelta(seconds=400),
        "experimenttype": "experiment",
    }

    dc = metadata.add_datacollection(no_context=True, **data)

    for k in ["sampleid", "subsampleid"]:
        assert data[k] == dc[k]


def test_update_datacollection(metadata):
    dcs = metadata.get_datacollections(no_context=True)
    last = dcs["rows"][-1]["datacollectionid"]

    runstatus = "Failure"
    dc = metadata.update_datacollection(
        datacollectionid=last, no_context=True, runstatus=runstatus
    )

    assert dc["runstatus"] == runstatus


def test_add_sampleimage(metadata):
    pass


def test_queue_subsample(metadata):
    pass


def test_unqueue_subsample(metadata):
    pass


def test_add_xrf_map_large(metadata):
    import random

    data = {
        "datacollectionid": 1,
        "maproiid": 1,
        "data": [random.randrange(1e4, 1e6) for i in range(512 * 512)],
    }

    xrfmap = metadata.add_xrf_map(no_context=True, return_data=True, **data)

    for k in ["datacollectionid", "maproiid", "data"]:
        assert data[k] == xrfmap[k]


def test_add_xrf_map(metadata):
    data = {"datacollectionid": 1, "maproiid": 1, "data": list(range(20 * 20))}

    xrfmap = metadata.add_xrf_map(no_context=True, return_data=True, **data)

    for k in ["datacollectionid", "maproiid", "data"]:
        assert data[k] == xrfmap[k]


def test_update_xrf_map(metadata):
    maps = metadata.get_xrf_maps(no_context=True)
    last = maps[-1]["mapid"]

    opacity = 0.5
    xrfmap = metadata.update_xrf_map(mapid=last, no_context=True, opacity=opacity)

    assert xrfmap["opacity"] == opacity
