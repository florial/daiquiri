-- Test data for Daiquiri

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

INSERT INTO `Permission` (`permissionId`, `type`, `description`) VALUES (81,'bl_admin','bl admin');
INSERT INTO `UserGroup` (`userGroupId`, `name`) VALUES (1, 'bl_admin');
INSERT INTO `UserGroup_has_Permission` (`userGroupId`, `permissionId`) VALUES (1, 81);

INSERT INTO `Person` (`personId`, `familyName`, `givenName`, `login`) VALUES (1, 'User', 'Test', 'abcd'),(2, 'User', 'Admin', 'efgh');
INSERT INTO `UserGroup_has_Person` (`userGroupId`, `personId`) VALUES (1, 2);

INSERT INTO `Proposal` (`proposalId`, `personId`, `title`, `proposalCode`, `proposalNumber`) VALUES (1, 1, 'Test Proposal', 'blc', '00001');

INSERT INTO `BLSession` (`sessionId`, `proposalId`, `scheduled`, `startDate`, `endDate`, `beamlineName`, `visit_number`) VALUES (1, 1, 1, '2020-01-01 00:00:00', '2030-01-01 23:59:59', 'bl', 1);
INSERT INTO `Session_has_Person` (`sessionId`, `personId`) VALUES (1, 1);


INSERT INTO `Shipping` (`shippingId`, `proposalId`, `shippingName`, `deliveryAgent_agentName`, `deliveryAgent_shippingDate`, `deliveryAgent_deliveryDate`, `deliveryAgent_agentCode`, `deliveryAgent_flightCode`, `shippingStatus`, `bltimeStamp`, `laboratoryId`, `isStorageShipping`, `creationDate`, `comments`, `sendingLabContactId`, `returnLabContactId`, `returnCourier`, `dateOfShippingToUser`, `shippingType`, `SAFETYLEVEL`, `deliveryAgent_flightCodeTimestamp`, `deliveryAgent_label`, `readyByTime`, `closeTime`, `physicalLocation`, `deliveryAgent_pickupConfirmationTimestamp`, `deliveryAgent_pickupConfirmation`, `deliveryAgent_readyByTime`, `deliveryAgent_callinTime`, `deliveryAgent_productcode`, `deliveryAgent_flightCodePersonId`) VALUES
(1, 1, 'blc00001-1_Shipment1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-06-16 15:42:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `Dewar` (`dewarId`, `shippingId`, `code`, `comments`, `storageLocation`, `dewarStatus`, `bltimeStamp`, `isStorageDewar`, `barCode`, `firstExperimentId`, `customsValue`, `transportValue`, `trackingNumberToSynchrotron`, `trackingNumberFromSynchrotron`, `type`, `FACILITYCODE`, `weight`, `deliveryAgent_barcode`) VALUES
(1, 1, 'blc00001-1_Dewar1', NULL, NULL, 'processing', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'Dewar', NULL, NULL, NULL);

INSERT INTO `Container` (`containerId`, `dewarId`, `code`, `containerType`, `capacity`, `sampleChangerLocation`, `containerStatus`, `bltimeStamp`, `beamlineLocation`, `screenId`, `scheduleId`, `barcode`, `imagerId`, `sessionId`, `ownerId`, `requestedImagerId`, `requestedReturn`, `comments`, `experimentType`, `storageTemperature`, `containerRegistryId`, `scLocationUpdated`, `priorityPipelineId`) VALUES
(1, 1, 'blc00001-1_Container1', 'Box', 25, '1', 'processing', '2020-06-16 15:42:44', 'bl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `ContainerHistory` (`containerHistoryId`, `containerId`, `location`, `blTimeStamp`, `status`, `beamlineName`) VALUES
(1, 1, '1', '2020-06-16 13:42:44', 'processing', 'bl');

INSERT INTO `ContainerInspection` (`containerInspectionId`, `containerId`, `inspectionTypeId`, `imagerId`, `temperature`, `blTimeStamp`, `scheduleComponentid`, `state`, `priority`, `manual`, `scheduledTimeStamp`, `completedTimeStamp`) VALUES
(1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-06-16 15:42:52', '2020-06-16 15:42:52');

INSERT INTO `ContainerQueue` (`containerQueueId`, `containerId`, `personId`, `createdTimeStamp`, `completedTimeStamp`) VALUES
(1, 1, 1, '2020-06-16 13:52:03', NULL);


INSERT INTO `Protein` (`proteinId`, `proposalId`, `name`, `acronym`, `description`, `hazardGroup`, `containmentLevel`, `safetyLevel`, `molecularMass`, `proteinType`, `personId`, `bltimeStamp`, `isCreatedBySampleSheet`, `sequence`, `MOD_ID`, `componentTypeId`, `concentrationTypeId`, `global`, `externalId`, `density`, `abundance`) VALUES
(1, 1, 'Component 1', 'comp1', NULL, 1, 1, NULL, NULL, NULL, NULL, '2020-06-16 13:42:40', 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL);

INSERT INTO `Crystal` (`crystalId`, `diffractionPlanId`, `proteinId`, `crystalUUID`, `name`, `spaceGroup`, `morphology`, `color`, `size_X`, `size_Y`, `size_Z`, `cell_a`, `cell_b`, `cell_c`, `cell_alpha`, `cell_beta`, `cell_gamma`, `comments`, `pdbFileName`, `pdbFilePath`, `recordTimeStamp`, `abundance`, `theoreticalDensity`) VALUES
(1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-16 13:42:44', NULL, NULL);

INSERT INTO `Position` (`positionId`, `relativePositionId`, `posX`, `posY`, `posZ`, `scale`, `recordTimeStamp`) VALUES
(1, NULL, 0, 0, NULL, NULL, NULL),
(2, NULL, 7410327, 907061, NULL, NULL, NULL),
(3, NULL, 7610327, 1107061, NULL, NULL, NULL),
(4, NULL, 8331997, 467086, NULL, NULL, NULL),
(5, NULL, 8613623, 1668137, NULL, NULL, NULL),
(6, NULL, 9077477, 1311963, NULL, NULL, NULL);

INSERT INTO `BLSample` (`blSampleId`, `diffractionPlanId`, `crystalId`, `containerId`, `name`, `code`, `location`, `holderLength`, `loopLength`, `loopType`, `wireWidth`, `comments`, `completionStage`, `structureStage`, `publicationStage`, `publicationComments`, `blSampleStatus`, `isInSampleChanger`, `lastKnownCenteringPosition`, `POSITIONID`, `recordTimeStamp`, `SMILES`, `blSubSampleId`, `lastImageURL`, `screenComponentGroupId`, `volume`, `dimension1`, `dimension2`, `dimension3`, `shape`, `packingFraction`, `preparationTemeprature`, `preparationHumidity`, `blottingTime`, `blottingForce`, `blottingDrainTime`, `support`, `subLocation`) VALUES
(1, NULL, 1, 1, 'sample1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-06-16 13:42:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `BLSubSample` (`blSubSampleId`, `blSampleId`, `diffractionPlanId`, `blSampleImageId`, `positionId`, `position2Id`, `motorPositionId`, `type`, `blSubSampleUUID`, `imgFileName`, `imgFilePath`, `comments`, `recordTimeStamp`) VALUES
(1, 1, NULL, NULL, 2, 3, NULL, 'roi', NULL, NULL, NULL, NULL, '2020-06-16 13:43:22'),
(2, 1, NULL, NULL, 4, NULL, NULL, 'poi', NULL, NULL, NULL, NULL, '2020-06-16 13:57:47'),
(3, 1, NULL, NULL, 5, 6, NULL, 'loi', NULL, NULL, NULL, NULL, '2020-06-16 13:57:49');


INSERT INTO `ContainerQueueSample` (`containerQueueSampleId`, `containerQueueId`, `blSubSampleId`, `blSampleId`, `diffractionPlanId`) VALUES
(1, 1, 1, NULL, 5);

INSERT INTO `DiffractionPlan` (`diffractionPlanId`, `name`, `experimentKind`, `observedResolution`, `minimalResolution`, `exposureTime`, `oscillationRange`, `maximalResolution`, `screeningResolution`, `radiationSensitivity`, `anomalousScatterer`, `preferredBeamSizeX`, `preferredBeamSizeY`, `preferredBeamDiameter`, `comments`, `DIFFRACTIONPLANUUID`, `aimedCompleteness`, `aimedIOverSigmaAtHighestRes`, `aimedMultiplicity`, `aimedResolution`, `anomalousData`, `complexity`, `estimateRadiationDamage`, `forcedSpaceGroup`, `requiredCompleteness`, `requiredMultiplicity`, `requiredResolution`, `strategyOption`, `kappaStrategyOption`, `numberOfPositions`, `minDimAccrossSpindleAxis`, `maxDimAccrossSpindleAxis`, `radiationSensitivityBeta`, `radiationSensitivityGamma`, `minOscWidth`, `recordTimeStamp`, `monochromator`, `energy`, `transmission`, `boxSizeX`, `boxSizeY`, `kappaStart`, `axisStart`, `axisRange`, `numberOfImages`, `presetForProposalId`, `beamLineName`, `detectorId`, `distance`, `orientation`, `monoBandwidth`, `centringMethod`) VALUES
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-16 13:52:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `Positioner` (`positionerId`, `positioner`, `value`) VALUES
(1, 'z', 3),
(2, 'z', 3),
(3, 'z', 3);
INSERT INTO `BLSubSample_has_Positioner` (`blSubSampleHasPositioner`, `blSubSampleId`, `positionerId`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);


INSERT INTO `DataCollectionGroup` (`dataCollectionGroupId`, `sessionId`, `comments`, `blSampleId`, `experimentType`, `startTime`, `endTime`, `crystalClass`, `detectorMode`, `actualSampleBarcode`, `actualSampleSlotInContainer`, `actualContainerBarcode`, `actualContainerSlotInSC`, `workflowId`, `xtalSnapshotFullPath`, `scanParameters`) VALUES
(1, 1, NULL, NULL, 'XRF map', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `DataCollection` (`dataCollectionId`, `BLSAMPLEID`, `SESSIONID`, `experimenttype`, `dataCollectionNumber`, `startTime`, `endTime`, `runStatus`, `axisStart`, `axisEnd`, `axisRange`, `overlap`, `numberOfImages`, `startImageNumber`, `numberOfPasses`, `exposureTime`, `imageDirectory`, `imagePrefix`, `imageSuffix`, `imageContainerSubPath`, `fileTemplate`, `wavelength`, `resolution`, `detectorDistance`, `xBeam`, `yBeam`, `comments`, `printableForReport`, `CRYSTALCLASS`, `slitGapVertical`, `slitGapHorizontal`, `transmission`, `synchrotronMode`, `xtalSnapshotFullPath1`, `xtalSnapshotFullPath2`, `xtalSnapshotFullPath3`, `xtalSnapshotFullPath4`, `rotationAxis`, `phiStart`, `kappaStart`, `omegaStart`, `chiStart`, `resolutionAtCorner`, `detector2Theta`, `DETECTORMODE`, `undulatorGap1`, `undulatorGap2`, `undulatorGap3`, `beamSizeAtSampleX`, `beamSizeAtSampleY`, `centeringMethod`, `averageTemperature`, `ACTUALSAMPLEBARCODE`, `ACTUALSAMPLESLOTINCONTAINER`, `ACTUALCONTAINERBARCODE`, `ACTUALCONTAINERSLOTINSC`, `actualCenteringPosition`, `beamShape`, `dataCollectionGroupId`, `POSITIONID`, `detectorId`, `FOCALSPOTSIZEATSAMPLEX`, `POLARISATION`, `FOCALSPOTSIZEATSAMPLEY`, `APERTUREID`, `screeningOrigId`, `startPositionId`, `endPositionId`, `flux`, `strategySubWedgeOrigId`, `blSubSampleId`, `flux_end`, `bestWilsonPlotPath`, `processedDataFile`, `datFullPath`, `magnification`, `totalAbsorbedDose`, `binning`, `particleDiameter`, `boxSize_CTF`, `minResolution`, `minDefocus`, `maxDefocus`, `defocusStepSize`, `amountAstigmatism`, `extractSize`, `bgRadius`, `voltage`, `objAperture`, `c1aperture`, `c2aperture`, `c3aperture`, `c1lens`, `c2lens`, `c3lens`, `diffractionPlanId`, `totalExposedDose`, `nominalMagnification`, `nominalDefocus`, `imageSizeX`, `imageSizeY`, `pixelSizeOnImage`, `phasePlate`) VALUES
(1, 1, 0, NULL, 2073510334, '2020-06-16 15:52:28', '2020-06-16 15:53:17', 'Successful', NULL, NULL, NULL, NULL, 16, NULL, NULL, 0.1, '/data/bl/tmp/blc00001/sample1/sample1_roi1_1', NULL, NULL, '1.1/measurement', 'sample1_roi1_1.h5', 1.23984, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, '/data/bl/tmp/blc00001/sample1/sample1_roi1_1/snapshot1_1592315556.5263455.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `GridInfo` (`gridInfoId`, `xOffset`, `yOffset`, `dx_mm`, `dy_mm`, `steps_x`, `steps_y`, `meshAngle`, `recordTimeStamp`, `workflowMeshId`, `orientation`, `dataCollectionGroupId`, `dataCollectionId`, `pixelsPerMicronX`, `pixelsPerMicronY`, `snapshot_offsetXPixel`, `snapshot_offsetYPixel`, `snaked`) VALUES
(1, NULL, NULL, 0.05, 0.05, 4, 4, NULL, '2020-06-16 13:52:36', NULL, 'horizontal', 1, 1, 5.6338, -5.1338, 482.058, 562.012, 0);


INSERT INTO `XRFFluorescenceMapping` (`xrfFluorescenceMappingId`, `xrfFluorescenceMappingROIId`, `gridInfoId`, `dataFormat`, `data`, `opacity`, `colourMap`, `min`, `max`) VALUES
(1, 1, 1, 'json+gzip', 0x1f8b0800cfcee85e02ff8b36d0512001c5020062281f7c30000000, 1, 'viridis', NULL, NULL),
(2, 2, 1, 'json+gzip', 0x1f8b0800decee85e02ff2d8e810d0331080357e9005505180ccc5275ff35eaffbc9448c4383e7fed93a021742d8d1ed3efd725f6ac6f4c34a38dbcc5b4c97164392febad69a60fb5618083638ca84e338e6db89f44286e19440a39c757881d2b3029cad1c01a940816c80349056b86fe95010f58af4eca39c21d5f2307e619aa403f81151b30b61a74563d4eaa78e58e50ceed078deda90ebff647baeab594e85d7fd0b91d3de13a1aebf707b780e6a746010000, 1, NULL, NULL, NULL);

INSERT INTO `XRFFluorescenceMappingROI` (`xrfFluorescenceMappingROIId`, `startEnergy`, `endEnergy`, `element`, `edge`, `scalar`, `r`, `g`, `b`, `blSampleId`) VALUES
(1, 6370, 6440, 'Fe', 'Ka', NULL, NULL, NULL, NULL, 1),
(2, 0, 0, NULL, NULL, 'simu1:deadtime_det1', NULL, NULL, NULL, NULL);


INSERT INTO `ProcessingJob` (`processingJobId`, `dataCollectionId`, `displayName`, `comments`, `recordTimestamp`, `recipe`, `automatic`) VALUES
(1, 1, NULL, NULL, '2020-12-28 18:12:16', NULL, 0),
(2, 1, NULL, NULL, '2020-12-28 18:21:51', NULL, 1),
(3, 1, NULL, NULL, '2020-12-30 15:40:27', NULL, 1);

INSERT INTO `AutoProcProgram` (`autoProcProgramId`, `processingCommandLine`, `processingPrograms`, `processingStatus`, `processingMessage`, `processingStartTime`, `processingEndTime`, `processingEnvironment`, `recordTimeStamp`, `processingJobId`, `dataCollectionId`) VALUES
(1, 'test program (auto)', 'test program', NULL, NULL, NULL, NULL, NULL, '2020-12-28 18:12:16', 1, NULL),
(2, 'test program (auto)', 'test program', 0, 'processing failure', '2020-12-28 18:21:52', '2020-12-28 18:21:52', NULL, '2020-12-28 18:21:51', 2, NULL),
(3, 'test program (auto)', 'test program', 1, 'processing successful', '2020-12-30 15:40:28', '2020-12-30 15:40:33', NULL, '2020-12-30 15:40:27', 3, NULL);

INSERT INTO `AutoProcProgramAttachment` (`autoProcProgramAttachmentId`, `autoProcProgramId`, `fileType`, `fileName`, `filePath`, `recordTimeStamp`, `importanceRank`) VALUES
(1, 3, 'Result', 'moo.txt', '/data', NULL, NULL);

INSERT INTO `AutoProcProgramMessage` (`autoProcProgramMessageId`, `autoProcProgramId`, `recordTimeStamp`, `severity`, `message`, `description`) VALUES
(1, 3, '2021-01-14 16:12:23', 'WARNING', 'moo', 'a warning'),
(2, 3, '2021-01-14 16:12:40', 'INFO', 'info', 'some info');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
