import subprocess
import os
import sys
import socket
import shutil
import atexit
from collections import namedtuple
from contextlib import contextmanager

import yaml
import gevent
import pytest
import redis

from bliss.config import static
from bliss.config.conductor import client, connection
from bliss.tango.clients.utils import wait_tango_device, wait_tango_db
from bliss.common.tango import Database

from daiquiri.app import init_server


def pytest_configure(config):
    import sys

    sys._running_pytest = True


def pytest_addoption(parser):
    parser.addoption(
        "--apispec",
        action="store_true",
        help="specific test to generate and check apispec",
    )


def pytest_runtest_setup(item):
    if "apispec" in item.keywords and not item.config.getoption("--apispec"):
        pytest.skip("need --apispec option to run this test")


def get_open_ports(n):
    sockets = [socket.socket() for _ in range(n)]
    try:
        for s in sockets:
            s.bind(("", 0))
        return [s.getsockname()[1] for s in sockets]
    finally:
        for s in sockets:
            s.close()


def wait_for(stream, target):
    def do_wait_for(stream, target, data=b""):
        target = target.encode()
        while target not in data:
            char = stream.read(1)
            if not char:
                raise RuntimeError(
                    "Target {!r} not found in the following stream:\n{}".format(
                        target, data.decode()
                    )
                )
            data += char

    return do_wait_for(stream, target)


BLISS = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
BEACON = [sys.executable, "-m", "bliss.config.conductor.server"]
BEACON_DB_PATH = os.path.join(BLISS, "tests", "bliss_configuration")


@pytest.fixture(scope="session")
def beacon_directory(tmpdir_factory):
    tmpdir = str(tmpdir_factory.mktemp("beacon"))
    beacon_dir = os.path.join(tmpdir, "bliss_configuration")
    shutil.copytree(BEACON_DB_PATH, beacon_dir)
    yield beacon_dir


@pytest.fixture(scope="session")
def ports(beacon_directory):
    redis_uds = os.path.join(beacon_directory, "redis.sock")
    # redis_data_uds = os.path.join(beacon_directory, "redis_data.sock")

    port_names = [
        "redis_port",
        "redis_data_port",
        "tango_port",
        "beacon_port",
        "cfgapp_port",
        "logserver_port",
        "homepage_port",
    ]

    ports = namedtuple("Ports", " ".join(port_names))(*get_open_ports(7))
    args = [
        "--port=%d" % ports.beacon_port,
        "--redis_port=%d" % ports.redis_port,
        "--redis_socket=" + redis_uds,
        # "--redis-data-port=%d" % ports.redis_data_port,
        # "--redis-data-socket=" + redis_data_uds,
        "--db_path=%s" % beacon_directory,
        "--tango_port=%d" % ports.tango_port,
        "--webapp_port=%d" % ports.cfgapp_port,
        "--homepage-port=%d" % ports.homepage_port,
        "--log_server_port=%d" % ports.logserver_port,
        # "--log_output_folder=%s" % log_directory,
        "--log-level=WARN",
        "--tango_debug_level=0",
    ]
    proc = subprocess.Popen(BEACON + args)
    wait_ports(ports)

    # disable .rdb files saving (redis persistence)
    r = redis.Redis(host="localhost", port=ports.redis_port)
    r.config_set("SAVE", "")
    del r

    os.environ["TANGO_HOST"] = "localhost:%d" % ports.tango_port
    os.environ["BEACON_HOST"] = "localhost:%d" % ports.beacon_port

    yield ports

    atexit._run_exitfuncs()
    wait_terminate(proc)


def wait_ports(ports, timeout=10):
    with gevent.Timeout(timeout):
        wait_tcp_online("localhost", ports.beacon_port)
        wait_tango_db(port=ports.tango_port, db=2)


@pytest.fixture(scope="session")
def beacon(ports):
    redis_db = redis.Redis(port=ports.redis_port)
    redis_db.flushall()
    # redis_data_db = redis.Redis(port=ports.redis_data_port)
    # redis_data_db.flushall()
    static.Config.instance = None
    client._default_connection = connection.Connection("localhost", ports.beacon_port)
    config = static.get_config()
    yield config
    config.close()
    client._default_connection.close()
    # Ensure no connections are created due to garbage collection:
    client._default_connection = None


def wait_tcp_online(host, port, timeout=10):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        with gevent.Timeout(timeout):
            while True:
                try:
                    sock.connect((host, port))
                    break
                except ConnectionError:
                    pass
                gevent.sleep(0.1)
    finally:
        sock.close()


def wait_terminate(process):
    process.terminate()
    try:
        with gevent.Timeout(10):
            process.wait()
    except gevent.Timeout:
        process.kill()
        with gevent.Timeout(10):
            process.wait()


@contextmanager
def start_tango_server(*cmdline_args, **kwargs):
    device_fqdn = kwargs["device_fqdn"]
    exception = None
    for i in range(3):
        p = subprocess.Popen(cmdline_args)
        try:
            dev_proxy = wait_tango_device(**kwargs)
        except Exception as e:
            exception = e
            wait_terminate(p)
        else:
            break
    else:
        raise RuntimeError(f"could not start {device_fqdn}") from exception

    try:
        yield dev_proxy
    finally:
        wait_terminate(p)


@contextmanager
def lima_simulator_context(personal_name, device_name):
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/LimaCCDs/{personal_name}"

    with start_tango_server(
        "LimaCCDs",
        personal_name,
        # "-v4",               # to enable debug
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=None,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def lima_simulator(ports):
    with lima_simulator_context("simulator", "id00/limaccds/simulator1") as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture(scope="session")
def app(with_database, beacon):
    resource_folders = [os.path.join(os.path.dirname(__file__), "resources")]
    app, socketio = init_server(resource_folders=resource_folders, testing=True)
    app.socketio = socketio
    return app


@pytest.fixture(scope="session")
def write_spec(with_database, beacon):
    resource_folders = [os.path.join(os.path.dirname(__file__), "resources")]
    try:
        app, socketio = init_server(
            resource_folders=resource_folders, testing=True, save_spec=True
        )
    except SystemExit:
        pass


@pytest.fixture(scope="session")
def with_database():
    with open(f"{os.path.dirname(__file__)}/resources/config/config.yml") as yml:
        config = yaml.safe_load(yml)

        host, database = config["meta_url"].split("/")
        hostname, port = host.split(":")

        command = [
            "mysql",
            "-u",
            config["meta_user"],
            f"-p{config['meta_pass']}",
            "-h",
            hostname,
            "-P",
            port,
            database,
        ]

        if os.environ.get("CI"):
            print("Running in CI, not dropping database")
        else:
            print("Dropping and creating database")
            p = subprocess.run(
                command,
                input=f"DROP DATABASE {database}\n;CREATE DATABASE {database};\nexit",
                encoding="ascii",
                stdout=subprocess.PIPE,
            )

        root = os.path.dirname(__file__)
        for sql in [
            f"{root}/metadata/db/tables.sql",
            f"{root}/metadata/db/lookups.sql",
            f"{root}/../daiquiri/core/metadata/ispyalchemy/updates.sql",
            f"{root}/metadata/db/data.sql",
        ]:
            with open(sql, "r") as sql_content:
                print("inserting", sql)
                p = subprocess.Popen(
                    command,
                    stdin=sql_content,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                print(p.communicate("exit\n"))


@pytest.fixture
def resource_folder():
    yield os.path.join(os.path.dirname(__file__), "resources")
