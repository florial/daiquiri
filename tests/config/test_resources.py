import os
import pytest

from daiquiri.resources.utils import load_config
from daiquiri.core.exceptions import SyntaxErrorYAML


def test_resource_loader(with_mock_resource):
    file = os.path.join(with_mock_resource, "config", "test.yml")
    with open(file, "w") as f:
        f.write("value: true")

    conf = load_config("test.yml")
    assert conf["value"] is True

    os.unlink(file)


def test_no_such_resource():
    with pytest.raises(RuntimeError) as e:
        load_config("none.yml")

    assert "No such resource" in str(e.value)


def test_synax_error_resource(with_mock_resource):
    file = os.path.join(with_mock_resource, "config", "test.yml")

    with open(file, "w") as f:
        f.write(
            """value: true
second true"""
        )

    with pytest.raises(SyntaxErrorYAML):
        load_config("test.yml")

    os.unlink(file)
