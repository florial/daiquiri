import os
import shutil
import pytest
from daiquiri.resources.utils import add_resource_root


@pytest.fixture
def with_mock_resource():
    mock_resources = "/tmp/resources"
    os.mkdir(mock_resources)
    os.mkdir(os.path.join(mock_resources, "config"))

    add_resource_root(mock_resources)

    yield mock_resources

    shutil.rmtree(mock_resources)
