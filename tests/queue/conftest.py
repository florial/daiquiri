import pytest
import uuid
import time
from daiquiri.core.components import ComponentActor, ComponentActorKilled


@pytest.fixture
def queue(app):
    yield app.queue


@pytest.fixture
def empty_queue(queue):
    queue.clear()
    yield queue


@pytest.fixture
def create_actor():
    class TestActor(ComponentActor):
        name = "test"

        def method(self, **kwargs):
            self.execstate = "running"
            try:
                time.sleep(kwargs["runtime"])
            except ComponentActorKilled:
                self.execstate = "killed"
                raise
            else:
                self.execstate = "finished"

    def cb(*args, **kwargs):
        pass

    def create(started=cb, error=cb, finished=cb, **actorargs):
        test = TestActor(
            uid=str(uuid.uuid4()),
            name="test",
            started=started,
            error=error,
            finished=finished,
            execstate="init",
            **actorargs
        )
        test.prepare(**actorargs)

        return test

    return create
